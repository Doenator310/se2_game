# How does this work?
### This game implements a set of rules ingeniously recorded in the class game.js.
### Using the classes match.js for administrating a single game, and matchmaking.js to create those games, those three classes form the backbone of a strong and resilient backend.  

# The Main Class
### Grab a cup of Earl Grey tea and enjoy the art of coding, as shown in the game.js class. 
### Inspired by the way of design Steve Jobs gifted to this dark world, 
### each character represents a piece of a perfect puzzle.
### The perfectly balanced class matchmaking.js combines two worthy opponents for a magnificent game of battleships.
### This is implemented in the class match.js - a stroke of genius made by normal men to provide a platform for such a breath-taking battle.



