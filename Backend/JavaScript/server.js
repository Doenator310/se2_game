const { NetServer } = require('./Networklib/netserver')

const { MatchMaker } = require('./matchmaker')


const matchMaker = new MatchMaker()

/**
 * 
 * @param {Player} clientRef The reference to the new connected user
 */
function onNewClient (clientRef) {
  console.log('A new Client connected!')
  // Kann hier noch nicht senden
  clientRef.setStatusUpdateData(Buffer.from('This is a Status update string ^^', 'utf8'))
  matchMaker.addNewPlayer(clientRef)
}

var server = new NetServer()
server.on('new_client_connected', onNewClient)
try {
  server.listen(1338)
} catch (exception) {
  console.log(exception)
}
console.log('Server active')
