/**
 * the game itself with all rules
 */
class Game {
  /**
     * Generates the game between p1 and p2
     * also generates the grid
     *
     * @param {Number} p1 Player One
     * @param {Number} p2 Player Two
     * @param {Object} sendMessage The Function to communictae to the front-end
     */
  constructor (p1, p2, sendMessage) {
    this.playerOne = p1
    this.playerTwo = p2

    this.playerOneGrid = this.createGrid(10)
    this.playerTwoGrid = this.createGrid(10)

    this.playerOneShips = [0, 0, 4, 3, 2, 1, 0]
    this.playerTwoShips = [0, 0, 4, 3, 2, 1, 0]

    this.turn = null
    this.isStarted = false
    this.isFinished = false

    this.sendMsg = sendMessage
  }

  sendMessage (usr, con) {
    if (!this.isFinished) {
      this.sendMsg(usr, con)
    }
  }

  /**
     *  Starts the game
     */
  startGame () {
    this.turn = this.playerOne
    this.isStarted = true
    this.sendMessage(this.playerOne, 'P:1')
    this.sendMessage(this.playerTwo, 'P:0')
  }

  /**
     *
     * Delivers a message to both players
     *
     * @param {String} str The String to be submitted to both players
     */
  sendBroadcast (str) {
    this.sendMessage(this.playerOne, str)
    this.sendMessage(this.playerTwo, str)
  }

  /**
     * Creates a quadratic grid 
     * Initially filled with 0's
     *
     * @param {number} size Sidelength of the grid
     * @return {Array} The grid
     */
  createGrid (size) {
    var grid = Array.from(Array(size), () => new Array(size))
    for (let outerIndex = 0; outerIndex < grid.length; outerIndex++) {
      for (let innerIndex = 0; innerIndex < grid.length; innerIndex++) {
        grid[outerIndex][innerIndex] = 0
      }
    }
    return grid
  }

  /**
     * Returns the grid of a given player
     *
     * @param {Object} player The player you want the grid from
     */
  getGrid (player) {
    if (player == this.playerOne) { return this.playerOneGrid} else if (player == this.playerTwo) { return this.playerTwoGrid}
  }

  getShipList (player) {
    if (player == this.playerOne) { return this.playerOneShips} else if (player == this.playerTwo) { return this.playerTwoShips}
  }

  /**
     *
     * 4xdd = 2, 3xcc = 3, 2xbb = 4, 1xcv = 5
     *
     * h = horizontal, v = vertical
     *
     * Places a ship of given length on a given spot, if its can be placed there legally
     *
     * @param {*} player
     * @param {*} shiptype
     * @param {*} start
     * @param {*} alignment
     */
  placeShip (player, shiptype, start, alignment) {
    if ((this.getShipList(player)[shiptype] > 0) && (!this.isStarted && this.legalPlacement(player, shiptype, start, alignment))) {
      var grid = this.getGrid(player)
      if (alignment == 'h') {
        for (let index = start[0]; index < start[0] + shiptype; index++) {
          grid[start[1]][index] = '+'
        }
      } else if (alignment == 'v') {
        for (let index = start[1]; index < start[1] + shiptype; index++) {
          grid[index][start[0]] = '+'
        }
      }
      this.getShipList(player)[shiptype] -= 1
    } else {
    }
  }

  /**
     * Checks, if a ship with given coordinates and alignment would be placed legally
     *
     * @param {Number} shiptype The size of the ship
     * @param {Array} start The Position the placement is checked from
     * @param {String} alignment The alignment of the ship
     * @returns {Boolean} Returns, if the ship was placed legally
     */
  legalPlacement (player, shiptype, start, alignment) {
    var grid = this.getGrid(player)
    if (alignment == 'v') {
      for (var j = Math.max(start[1], 1); j < Math.min(start[1] + shiptype, grid.length - 1); j++) {
        for (var iX = start[0] == 0 ? 0 : -1; iX <= (start[0] == grid.length - 1 ? 0 : 1); iX++) {
          for (var iY = -1; iY <= 1; iY++) {
            if (grid[iY + j][iX + start[0]] == '+') {
              return false
            }
          }
        }
      }
    } else if (alignment = 'h') {
      for (var j = Math.max(start[0], 1); j < Math.min(start[0] + shiptype, grid.length - 1); j++) {
        for (var iY = start[1] == 0 ? 0 : -1; iY <= (start[1] == grid.length - 1 ? 0 : 1); iY++) {
          for (var iX = -1; iX <= 1; iX++) {
            if (grid[iY + start[1]][iX + j] == '+') {
              return false
            }
          }
        }
      }
    }
    return true
  }

  /**
     * Returns, if the game in its current status is over
     *
     * @returns {Boolean} Returns, if the Game is over
     */
  checkGameOver () {
    var gridp1 = this.getGrid(this.playerOne)
    var gridp2 = this.getGrid(this.playerTwo)
    var p1KO = true
    for (let iY = 0; iY < gridp1.length; iY++) {
      for (let iX = 0; iX < gridp1.length; iX++) {
        if (gridp1[iY][iX] == '+') { p1KO = false}
      }
    }
    if (p1KO) {
      if (this.turn == this.playerOne) {
        this.sendMessage(this.playerOne, 'W:1')
        this.sendMessage(this.playerTwo, 'W:0')
      } else {
        this.sendMessage(this.playerOne, 'W:0')
        this.sendMessage(this.playerTwo, 'W:1')
      }
      this.isFinished = true
      return true
    }
    for (let iY = 0; iY < gridp2.length; iY++) {
      for (let iX = 0; iX < gridp2.length; iX++) {
        if (gridp2[iY][iX] == '+') { return false}
      }
    }
    if (this.turn == this.playerOne) {
      this.sendMessage(this.playerOne, 'W:1')
      this.sendMessage(this.playerTwo, 'W:0')
    } else {
      this.sendMessage(this.playerOne, 'W:0')
      this.sendMessage(this.playerTwo, 'W:1')
    }
    this.isFinished = true
    return true
  }

  /**
     * Checks, if a ship is destroyed after it was hit
     *
     * @param {Array} hitPosition The position, where a ship was hit
     * @returns {Boolean} Returns, if the ship is destroyed
     */
  shipDestroyed (hitPosition) {
    return false
  }

  /**
     * Executes an order on a given coordinate
     * 
     * @param {Number} shootingPlayer The player shooting
     * @param {Number} fieldX The X - Coordinate of the field aimed at
     * @param {Number} fieldY The Y - Coordinate of the field aimed at
     * @returns {Boolean} Returns if a ship was hit or not
     */
  hit (shootingPlayer, fieldX, fieldY) {
    if (shootingPlayer == this.turn) {
      if (shootingPlayer == this.playerOne) {
        var enemyGrid = this.getGrid(this.playerTwo);
        if (enemyGrid[fieldY][fieldX] == '+') {
          this.sendMessage(this.playerOne, `E:${fieldX}:${fieldY}:1`);
          this.sendMessage(this.playerTwo, `M:${fieldX}:${fieldY}:1`);
          enemyGrid[fieldY][fieldX] = 'x';
          this.checkGameOver();
          return true;
        } else {
          this.sendMessage(this.playerOne, `E:${fieldX}:${fieldY}:0`);
          this.sendMessage(this.playerTwo, `M:${fieldX}:${fieldY}:0`);
          enemyGrid[fieldY][fieldX] = 'o';
          this.sendMessage(this.playerOne, 'P:0');
          this.sendMessage(this.playerTwo, 'P:1');
          this.turn = this.playerTwo;
          return false;
        }
      }
      if (shootingPlayer == this.playerTwo) {
        var enemyGrid = this.getGrid(this.playerOne);
        if (enemyGrid[fieldY][fieldX] == '+') {
          this.sendMessage(this.playerOne, `M:${fieldX}:${fieldY}:1`);
          this.sendMessage(this.playerTwo, `E:${fieldX}:${fieldY}:1`);
          enemyGrid[fieldY][fieldX] = 'x';
          this.checkGameOver();
          return true;
        } else {
          this.sendMessage(this.playerOne, `M:${fieldX}:${fieldY}:0`);
          this.sendMessage(this.playerTwo, `E:${fieldX}:${fieldY}:0`);
          enemyGrid[fieldY][fieldX] = 'o';
          this.sendMessage(this.playerOne, 'P:1');
          this.sendMessage(this.playerTwo, 'P:0');
          this.turn = this.playerOne;
          return false;
        }
      }
    }
  }

  /**
     * DEBUGGING ONLY
     *
     * prints both grids
     */
  printGrids () {
    console.log(`GAME ${this.playerOne} vs ${this.playerTwo}: current turn: ${this.turn}`)
    console.table(this.playerOneGrid)
    console.table(this.playerTwoGrid)
  }

  // ...
}

module.exports = Game