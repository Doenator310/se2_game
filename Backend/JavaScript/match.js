const EventEmitter = require('events')
const Game = require('./game');

/**
 * The match class is hosting the game between two players
 */
class Match extends EventEmitter {
  static matchId = 0
  constructor (player1=null, player2=null) {
    super()
    if(player1 == null || player2 == null){
        console.log("Error! A Match needs 2 valid players!")
    }
    Match.matchId++
    this.connectEventsOfPlayer(player1)
    this.connectEventsOfPlayer(player2)
    this.player1 = player1
    this.player2 = player2    
    this.id = Match.matchId
    this.game = new Game(player1, player2, this.onGameMessage.bind(this));
    this.isActive = true
  }

  /**
   * Binds communication-functions to the player
   * 
   * @param {Player} playerRef The new Player
   */
  connectEventsOfPlayer(playerRef){
    playerRef.on('client_request', this.onPlayerClientRequest.bind(this))
    playerRef.on('client_response', this.onPlayerClientResponse.bind(this))
    playerRef.on('client_disconnected', this.onPlayerClientDisconnected.bind(this))    
  }

  /**
   * Executes requests given by players, such as placing ships and ordering hits
   * 
   * @param {Player} sender The Player that sent a request
   * @param {Request} data The Request
   * @returns null
   */
  onPlayerClientRequest(sender, data){
    if(this.game.isFinished === false && this.isActive === true) {
      if(data == null){
          console.log("Request: invalid byte buffer");
          return;
      }
      let cmd = data.toString('utf8');
      let cmdList = cmd.split(":")
      if(cmdList[0] == "I") {
        this.onPlaceShips(sender, cmd);
      } else if(cmdList[0] == "H") {        
        let xPos = parseInt(cmdList[1])
        let yPos = parseInt(cmdList[2])
        this.game.hit(sender, xPos, yPos)
      } 
      else if(cmdList[0] == "R"){
        if(this.game.isFinished === false) {
          if(sender == this.player1 && this.player2 != null)
            this.player2.sendServerResponseToClient(Buffer.from("F", 'utf8'))
          if(sender == this.player2 && this.player1 != null)
            this.player1.sendServerResponseToClient(Buffer.from("F", 'utf8'))
          this.OnMatchEnded()
        }
      }
    }
  }

  /**
   * Places multiple ships, as ordered by the player 
   * 
   * @param {Player} sender The player that ordered the ships to be placed
   * @param {Request} shipCMD The quantity, length and coordinates of the ships
   * @returns null
   */
  onPlaceShips(sender, shipCMD){
    console.log(shipCMD);
    let cmd = shipCMD.split(":")
    console.log(cmd)
    let count = parseInt(cmd[1])
    console.log("count of ship list:", count)
    for(let i = 0; i < count; i++){
        let lengthOfShip = parseInt(cmd[2+i*4])
        let x = parseInt(cmd[2+i*4+1])
        let y = parseInt(cmd[2+i*4+2])
        let alignment = cmd[2+i*4+3].toLowerCase()
        console.log(`trying to place ship at (${x+1}|${y+1}), length of ${lengthOfShip}, alignment ${alignment}`)
        let result = this.game.legalPlacement(sender, lengthOfShip, [x,y], alignment)
        if(result == false){
            // nicht okay
            console.log("Failed to place ship at " +  [x,y].toString())
            sender.sendServerResponseToClient(Buffer.from("I:N", 'utf8'))
            return
        }
    }
    for(let i = 0; i < count; i++){
      let lengthOfShip = parseInt(cmd[2+i*4])
      let x = parseInt(cmd[2+i*4+1])
      let y = parseInt(cmd[2+i*4+2])
      let alignment = cmd[2+i*4+3].toLowerCase()
      this.game.placeShip(sender, lengthOfShip, [x,y], alignment)
    }
    sender.sendServerResponseToClient(Buffer.from("I:Y", 'utf8'))
    //
    this.checkForGameStart();
  }

  /**
   * Checks, if all ships have been placed.
   * If so, the game will start
   */
  checkForGameStart() {
    console.table(this.game.playerOneShips);
    console.table(this.game.playerTwoShips);
    console.table(this.game.getGrid(this.game.playerTwo));
    console.table(this.game.getGrid(this.game.playerOne));
    if(this.isZeros(this.game.playerTwoShips) && this.isZeros(this.game.playerOneShips)) {
      this.game.startGame();
    }
  }

  /**
   * Checks, if all elements of an array are 0's
   * 
   * @param {Array} arr The array to be investigated
   * @returns The conditiion of the statement
   */
  isZeros(arr) {
    for (let index = 0; index < arr.length; index++) {
      if(arr[index] != 0) {
        return false;
      }
    }
    return true;
  }

  /**
   * Executed the Response given by the player      
   *  
   * @param {Player} sender The Player
   * @param {Response} data The Response
   * @returns null
   */
  onPlayerClientResponse(sender, data){
    if(this.game.isFinished  === false && this.isActive === true) {
      if(data == null){
          console.log("Request: invalid byte buffer")
          return;
      }
      let commandText = data.toString('utf8')
    }
  }

  /**
   * Sends messages from the backend to the players on the frontend
   * 
   * @param {Player} rec The Player who shall receive the message
   * @param {String} message The Message that shall be delivered
   */
  onGameMessage(rec, message) {
    // console.log("mId: ", this.getId(), "!pId: ", rec.getClientId(), "!", message)
    rec.sendServerResponseToClient(Buffer.from(message, "utf8"));
  }

  /**
   * Handles the case of a disconnecting player
   * 
   * @param {Player} sender The disconnecting player
   */
  onPlayerClientDisconnected(sender){
    if(this.game.isFinished === false && this.isActive === true) {
      this.sendBroadcastResponse("F")
      this.OnMatchEnded();
    }
  }

  /**
   * Initiates the game
   */
  startGame(){
    console.log("Starting Match:", this.getId());
    this.sendBroadcastResponse("X");
  }

  /**
   * Delivers a specified message to all players
   * 
   * @param {String} text The text that shall be delivered to all gamemembers
   */
  sendBroadcastResponse (text) {
    let toSend = Buffer.from(text, 'utf8')
    console.log(toSend)
    try{
        if(this.player1 != null)
          this.player1.sendServerResponseToClient(toSend)
        if(this.player2 != null)
          this.player2.sendServerResponseToClient(toSend)
    } catch (exception) {
        console.log(exception)
    }
  }

  /**
   * Return the unique game-ID
   * 
   * @returns The id
   */
  getId(){
      return this.id
  }

  /**
   * Handles the cleanup after the game
   */
  OnMatchEnded () {
    console.log("Match ", this.getId(), " Ended")    
    this.isActive = false
    this.emit('onMatchEnded', this)
  }
  
}

module.exports = { Match: Match }
