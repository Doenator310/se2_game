const EventEmitter = require('events')
const { NetMessageReceiveBuffer } = require('./netreceivebuffer')
const { Client } = require('./client')
const { NetMessageType, ProtocolTool } = require('./protocol')
const { NetMessageSendBuffer } = require('./netsendbuffer')

// TODO: wenn client reconnected, dann weise send buffer richtigen socket/netconnection zu!
/**
 * The ClientManagerClass Manages all Client class instances and creates or deletes them depending on their connection status.
 * It also serves as a bridge between the client and the corisponding tcpsocket instance by delegating their packages.
 * <br><br>
 * Signals:
 * <br><br>
 * Signal 'new_client_connected' (Client clientInstance)
 */
class ClientManager extends EventEmitter {
  constructor () {
    super()
    this.recVBuffers = []
    this.sendBuffers = {}
    this.clients = []
    this.debugClientId = 1
  }

  // erwartet inetconnection
  onNewNetConnection (newConnection) {
    var netMessageRecVBuffer = new NetMessageReceiveBuffer(newConnection)
    newConnection.on('end', this.onNetConnectionClosed.bind(this))
    netMessageRecVBuffer.on('new_netmessage', this.onNewNetMessage.bind(this))
    this.recVBuffers.push(netMessageRecVBuffer)
  }

  onNetConnectionClosed (netConnectionRef) {

  }

  onNewNetMessage (netMessage, netMessageRecVBufferRef) {
    // console.log('onNewNetMessage!', netMessage)
    if (netMessage.type === NetMessageType.RegisterClient) {
      this.registerNewClient(netMessageRecVBufferRef.getNetConnection())
    } else {
      const targetClient = this.getClientById(netMessage.clientId)
      if (targetClient !== null) {
        targetClient.onNewNetMessageReceived(netMessage)
      }
    }
  }

  getClientById (id) {
    for (let i = 0; i < this.clients.length; i++) {
      if (this.clients[i].getClientId() === id) {
        return this.clients[i]
      }
    }
    return null
  }

  getNextFreeClientId () {
    this.debugClientId += 1
    // TODO
    return this.debugClientId
  }

  getSendBufferOfClientId (clientId) {
    if (clientId in this.sendBuffers) {
      return this.sendBuffers[clientId]
    }
    return null
  }

  sendServerStatusUpdate () {
    for (var i = 0; i < this.clients.length; i++) {
      const targetClient = this.clients[i]
      const netMessage = ProtocolTool.CreateMessage(NetMessageType.ServerStatusUpdate, targetClient.getStatusUpdateData(), targetClient.getClientId())
      // console.log('Server Status Update!: ', netMessage, i)
      this.onSendNetMessageToClient(netMessage, targetClient)
    }
  }

  onSendNetMessageToClient (netMessageToSend, clientRef) {
    var sendBufferRef = this.getSendBufferOfClientId(clientRef.getClientId())
    if (sendBufferRef == null) {
      throw Error('There was no SendBuffer found for client id ' + clientRef.getClientId())
    }
    // console.log('trying to transmit net message ', netMessageToSend)
    sendBufferRef.sendNetMessage(netMessageToSend)
  }

  createSendBufferForClientId (newClientId, netConnection) {
    var newMessageSendBuffer = new NetMessageSendBuffer(netConnection)
    // save in dict by id
    this.sendBuffers[newClientId] = newMessageSendBuffer
    return newMessageSendBuffer
  }

  removeSendBufferForClientId (clientId) {
    const targetBuffer = this.getSendBufferOfClientId(clientId)
    for (let i = 0; i < this.sendBuffers.length; i++) {
      if (this.sendBuffers[i] === targetBuffer) {
        // remove from list
        this.sendBuffers.splice(i, 1)
        i--
      }
    }
  }

  onClientDisconnect (clientRef) {
    // Remove client
    for (let i = 0; i < this.clients.length; i++) {
      if (this.clients[i] === clientRef) {
        // remove from list
        this.clients.splice(i, 1)
        i--
      }
    }
  }

  registerNewClient (newNetConnection) {
    const newClientId = this.getNextFreeClientId()
    if (newClientId === -1) {
      throw Error('No Client ids left: Maximum connection count reached!')
    }
    this.createSendBufferForClientId(newClientId, newNetConnection)
    var newClientToAdd = new Client(newNetConnection)
    newClientToAdd.setClientId(newClientId)
    newClientToAdd.on('send_netmessage', this.onSendNetMessageToClient.bind(this))
    newClientToAdd.on('client_disconnected', this.onClientDisconnect.bind(this))
    this.clients.push(newClientToAdd)
    this.emit('new_client_connected', newClientToAdd)
    newClientToAdd.init()
  }

  update () {
    for (let i = 0; i < this.clients.length; i++) {
      this.clients[i].update()
    }
    this.sendServerStatusUpdate()
  }
}

module.exports = { ClientManager: ClientManager }
