const { ProtocolTool } = require('./protocol')
const EventEmitter = require('events')
/**
 * The NetMessageSendBuffer contains a List that acts like a ringbuffer, for netMessages to send.
 * If there is a valid TCPConnection object, then all NetMessages in the List get sended.
 * If a sended message got corrupted, this class can resend this NetMessage if the
 * messageId wasnt overridden with a new Netmessage in the ringbuffer.
 */
class NetMessageSendBuffer extends EventEmitter {
  constructor (
    clientNetConnection
  ) {
    super()
    this.clientNetConnection = clientNetConnection
    this.netMessagesToSend = {}
    this.firstFailedMessageId = -1
    this.currentMessageId = 0
    this.lastAddedMessageId = 0
  }

  setNewNetConnection (netConnection) {
    this.clientNetConnection = netConnection
    if (this.clientNetConnection.isOpen() === true) {
      this.sendAllNetMessagesInBuffer()
    }
  }

  cleanUpNextMessage () {
    const nextMessageId = (this.currentMessageId + 1) % 256
    this.netMessagesToSend[nextMessageId] = null
  }

  sendAllNetMessagesInBuffer () {
    var result = false
    do {
      const nextNetMessageId = this.currentMessageId
      result = this.sendNetMessageFromBuffer(nextNetMessageId)
      if (result === true) {
        // console.log('transmitted net message with msg id: ', this.currentMessageId)
        this.incrementMessageId()
        this.cleanUpNextMessage()
      }
    }
    while (result === true)
  }

  incrementMessageId () {
    this.currentMessageId = (this.currentMessageId + 1) % 256
  }

  sendNetMessageFromBuffer (msgIdToSend) {
    const netMessageToSend = this.netMessagesToSend[msgIdToSend]
    if (netMessageToSend === undefined || netMessageToSend === null) { return false }
    const netMessageAsByteArray = ProtocolTool.NetMessageToByteArray(netMessageToSend)
    const success = this.clientNetConnection.write(netMessageAsByteArray)
    if (success === false) {
      console.log('Cannot write NetMessage with socket, waiting for new valid socket!')
      if (this.firstFailedMessageId === -1) {
        this.firstFailedMessageId = msgIdToSend
      }
    } else {
      if (this.firstFailedMessageId !== -1 && this.firstFailedMessageId === this.currentMessageId) {
        this.firstFailedMessageId = -1
      }
    }
    return success
  }

  incrementLastAddedMessageId () {
    this.lastAddedMessageId = (this.lastAddedMessageId + 1) % 256
  }

  sendNetMessage (netMessageToSend) {
    netMessageToSend.messageId = this.lastAddedMessageId
    this.incrementLastAddedMessageId()
    const msgId = netMessageToSend.messageId
    this.netMessagesToSend[msgId] = netMessageToSend
    this.sendAllNetMessagesInBuffer()
  }
}

module.exports = { NetMessageSendBuffer: NetMessageSendBuffer }
