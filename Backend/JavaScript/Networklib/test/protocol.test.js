const { describe, it } = require('mocha')
const assert = require('chai').assert
const { ProtocolTool, NetMessage, NetMessageType } = require('../protocol')

describe('NetMessage check default values', function () {
  it('Member values should be zeroed', function () {
    var nm = new NetMessage()
    assert.equal(nm.clientId, 0, 'clientid should be zero')
    assert.equal(nm.crc, 0, 'crc should be zero')
    assert.equal(nm.messageId, 0, 'messageId should be zero')
    assert.equal(nm.payloadSize, 0, 'payloadSize should be zero')
    assert.equal(nm.type, 0, 'type should be zero')
  })
})

describe('Payload CRC Correctnes', function () {
  it('CRC Testing', function () {
    const buff = Buffer.alloc(100)
    for (let i = 0; i < buff.length; i++) {
      buff[i] = i
    }
    var crc = ProtocolTool.GetCRC(buff, buff.length, 0)
    assert.equal(crc, 0x0F, 'CRC algorithm is not correct')

    for (let i = 0; i < buff.length; i++) {
      buff[i] = 77
    }

    crc = ProtocolTool.GetCRC(buff, buff.length, 0)
    assert.equal(crc, 0xA0, 'CRC algorithm is not correct')

    crc = ProtocolTool.GetCRC(null, 0, 0)
    assert.equal(crc, 0x00, 'CRC algorithm must handle a null buffer correctly')
  })
})

describe('NetMessage Handling', function () {
  it('Netmessages content validation', function () {
    var netMessage = ProtocolTool.CreateMessage(NetMessageType.RegisterClient, Buffer.alloc(0), 0)
    assert.equal(netMessage.type, NetMessageType.RegisterClient, 'NetMessage Type differs')
    assert.equal(netMessage.payloadSize, 0, 'Payloadsize must be 0')
    assert.equal(netMessage.clientId, 0, 'clientid must be 0')
    assert.isNotNull(netMessage.payload, 'Payload must be not null')
    assert.equal(Buffer.isBuffer(netMessage.payload), true, 'Payload must be a Buffer')

    netMessage = ProtocolTool.CreateMessage(655, Buffer.alloc(99), 666)
    assert.equal(netMessage.type, ProtocolTool.ReduceToByte(655), 'Type id not correctly reduced to a byte')
    assert.equal(netMessage.payloadSize, 99, 'Payloadsize must be 99')
    assert.equal(netMessage.clientId, ProtocolTool.ReduceToByte(666), 'clientid not correctly reduced to a byte')
    assert.isNotNull(netMessage.payload, 'Payload must be not null')
    assert.equal(Buffer.isBuffer(netMessage.payload), true, 'Payload must be a Buffer')

    netMessage = ProtocolTool.CreateMessage(622, null, 622)
    assert.equal(netMessage.type, ProtocolTool.ReduceToByte(622), 'Type id not correctly reduced to a byte')
    assert.equal(netMessage.payloadSize, 0, 'Payloadsize must be 0')
    assert.equal(netMessage.clientId, ProtocolTool.ReduceToByte(622), 'clientid not correctly reduced to a byte')
    assert.isNull(netMessage.payload, 'Payload must be null')
  })

  it('Netmessages De/Serialization', function () {
    var payload = Buffer.alloc(100)
    for (let i = 0; i < payload.length; i++) {
      payload[i] = i % 256
    }
    const crc = ProtocolTool.GetCRC(payload, payload.length)
    var netMessage = ProtocolTool.CreateMessage(NetMessageType.RegisterClient, payload, 0)

    assert.equal(crc, netMessage.crc, 'Payload CRC must be equal to the NetMessage payload CRC')

    var netMessageAsBytes = ProtocolTool.NetMessageToByteArray(netMessage)
    var nMHeader = ProtocolTool.GetNetMessageHeaderFromByteArray(netMessageAsBytes)
    assert.isTrue(nMHeader.isHeaderEqual(netMessage), 'NetMessage Header must be the same after the Serialization process')

    const deSerializedNetMessage = ProtocolTool.GetNetMessageFromByteArray(netMessageAsBytes)
    assert.isTrue(deSerializedNetMessage.isHeaderEqual(netMessage), 'NetMessage Header must be the same after the deserialization process')
    assert.equal(ProtocolTool.GetCRC(deSerializedNetMessage.payload, deSerializedNetMessage.payloadSize), deSerializedNetMessage.crc, 'CRC Sum must be correct for payload')

    var incompleteNMBuffer = Buffer.alloc(5)
    assert.isNull(ProtocolTool.GetNetMessageFromByteArray(incompleteNMBuffer), 'Excepted null if not event the header is in the buffer')
    assert.equal(ProtocolTool.GetSizeOfNetMessageInBuffer(incompleteNMBuffer), 6, 'Exptected Size of Header')
    assert.equal(ProtocolTool.GetSizeOfNetMessageInBuffer(netMessageAsBytes), 106, 'Excpected a NetMessage size of 106')
  })
})
