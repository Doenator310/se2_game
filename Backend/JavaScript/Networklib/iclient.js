const EventEmitter = require('events')
/**
 * The IClient is an abstract Class for a Client
 */
class IClient extends EventEmitter {
  constructor () {
    super()
    if (this.constructor === IClient) {
      throw new Error('You cannot create an instance of Abstract Class IClient')
    }
  }

  notImplemented () {
    console.log((new Error()).stack[1])
    throw new Error('Abstract method ' + (new Error()).stack.split('\n')[1].split(' ').filter(function (e) { return e.length > 0 })[1] + ' has no implementation')
  }

  sendResponseToClient (byteBuffer) {
    this.notImplemented()
  }

  sendRequestToClient (byteBuffer) {
    this.notImplemented()
  }

  sendServerStatusUpdate (byteBuffer) {
    this.notImplemented()
  }

  onNewNetMessageReceived (netMessage) {
    this.notImplemented()
  }

  onConnectionResetReceived () {
    this.notImplemented()
  }

  update () {
    this.notImplemented()
  }
}

module.exports = { IClient: IClient }
