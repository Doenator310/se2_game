const { Match } = require('./match')

/**
 * The matchmaker class administrates all players and initiates matches between players
 */
class MatchMaker {
  constructor () {
    this.playersAvailable = []
    this.playersSearching = []
    this.activeGameList = []
  }

  /**
   * Adds a new player to the available players
   * 
   * @param {Player} playerClientRef The new player
   */
  addNewPlayer (playerClientRef) {
    this.playersAvailable.push(playerClientRef)
    playerClientRef.on('client_request', this.onPlayerClientRequest.bind(this))
    playerClientRef.on('client_disconnected', this.onPlayerClientDisconnected.bind(this))
  }

  /**
   * Removes an available player that disconnected from any queues and lists
   * 
   * @param {Player} playerRef The player that disconnected
   */
  onPlayerClientDisconnected (playerRef) {
    for (let i = 0; i < this.playersAvailable.length; i++) {
      if (this.playersAvailable[i] === playerRef) {
        this.playersAvailable.splice(i--, 1)
      }
    }
    this.removePlayerFromMatchmakingQueue(playerRef)
  }

  /**
   * Removes a player from the matchmaking queue
   * 
   * @param {Player} playerRef The player that has to be removed
   */
  removePlayerFromMatchmakingQueue (playerRef) {
    console.log('Remove player from matchmaking queue: ', playerRef.getClientId())
    for (let i = 0; i < this.playersSearching.length; i++) {
      if (this.playersSearching[i] === playerRef) {
        this.playersSearching.splice(i--, 1)
      }
    }
  }

  /**
   * Enqueues a player into the matchmaking queue
   * 
   * @param {Player} targetClient The player that has to be moved
   */
  enqueueClientIntoMatchmaking (targetClient) {
    console.log('Adding Player to the matchmaking queue', targetClient.getClientId())
    this.playersSearching.push(targetClient)
    this.checkForNewMatchopportunity()
  }

  /**
   * Executes matchmaking request from players
   * 
   * @param {Player} sender The Player sending the request
   * @param {Message} byteBuffer The request
   * @returns null
   */
  onPlayerClientRequest (sender, byteBuffer) {
    if (byteBuffer == null) {
      console.log('MatchMaker-Request: invalid byte buffer')
      return
    }
    var text = byteBuffer.toString('utf8')
    if (text[0] === 'A') {
      this.enqueueClientIntoMatchmaking(sender)
    } else {
    }
  }

  /**
   * Checks, if there are enough players (2) to initiate a new match, and if this is the case, also creates the game
   */
  checkForNewMatchopportunity () {
    if (this.playersSearching.length >= 2) {
      const player1 = this.playersSearching[0]
      const player2 = this.playersSearching[1]
      this.removePlayerFromMatchmakingQueue(player1)
      this.removePlayerFromMatchmakingQueue(player2)
      this.createGame(player1, player2)
    }
  }

  /**
   * Initiates the game, and stores it in the active games
   * 
   * @param {Player} player1 The first player
   * @param {Player} player2 The second player
   */
  createGame (player1, player2) {
    console.log('Creating a new match with players: ', player1.getClientId(), player2.getClientId())
    const newMatch = new Match(player1, player2)
    this.activeGameList.push(newMatch)
    newMatch.on('on_match_ended', this.onMatchEnded.bind(this))
    newMatch.startGame()
  }

  /**
   * Cleans up after the game, by removing the players from it, and remocing it from active games
   * 
   * @param {Match} matchRef The match that just ended
   */
  onMatchEnded (matchRef) {
    // remove from match list
    for (let i = 0; i < this.activeGameList.length; i++) {
      if (this.activeGameList[i] === matchRef) {
        this.activeGameList[i].player1 = null
        this.activeGameList[i].player2 = null
        this.activeGameList.splice(i--, 1)
      }
    }
  }
}

module.exports = { MatchMaker: MatchMaker }
