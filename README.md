# Schiffeversenken

## Developer Documentations:

The documentation of the components, can be found in the associated documentation folder. To display these correctly, please open the index.html file in the documentation folder, with your web browser.

# Build process / Requirements

## Game Client

To build the client you need to install Unity3d 2020-20f1

1. Open the project "Frontend/schiffe_versenken" in Unity3d

1. In Unity, build the game by clicking the following in the Menu bar "File" -> "build and run" 

<img src="images/build_and_run.png" width="500"/>

3. The built game will be in the user specified folder

## Game Server

Install NodeJS version 14.16.0 or later on your System

# How to play

To play the game, you first need to start the Game Server

## Game Server
1. Navigate into the following project directory with cmd: "Backend/JavaScript"

1. execute "node server.js" to start the Game-Server

<img src="images/node.png" width="400"/>

## Game Client

1. Make sure that the Game server is started

1. Execute the .exe File, that was built using the Unity Engine

1. Once started, press Search Match to enter the matchmaking queue

<img src="images/find_game.png" width="300"/>

4. If 2 players are searching for a game, these two will get matched against each other

1. Place your ships

<img src="images/schiffe_platzieren.png" width="500"/>

6. If both players set their ships, the game will start and a text indicator will show, which players turn it is

<img src="images/ingame.png" width="600"/>
