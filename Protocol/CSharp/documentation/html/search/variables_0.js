var searchData=
[
  ['clientid_172',['clientId',['../class_protocol_client_1_1_t_c_p_client.html#aac7b04fe280451c741867ead8b3eb8da',1,'ProtocolClient.TCPClient.clientId()'],['../class_protocol_client_1_1_net_message.html#ae3c9cb9ebd110c746cf5922483bc5b9c',1,'ProtocolClient.NetMessage.clientId()']]],
  ['connection_173',['connection',['../class_protocol_client_1_1_t_c_p_client.html#a6181e5a4cbe7b76f35066aed53fb7df3',1,'ProtocolClient.TCPClient.connection()'],['../class_protocol_client_1_1_net_send_buffer.html#a72cc88b9832ea3eb7296ff5eed393fba',1,'ProtocolClient.NetSendBuffer.connection()']]],
  ['crc_174',['crc',['../class_protocol_client_1_1_net_message.html#ab56e583cd962ebc6b5144954e4cce7fb',1,'ProtocolClient::NetMessage']]],
  ['currentmessageid_175',['currentMessageId',['../class_protocol_client_1_1_t_c_p_client.html#a01df10f2e4fc98d5c87cbcf3a74da2c9',1,'ProtocolClient::TCPClient']]]
];
