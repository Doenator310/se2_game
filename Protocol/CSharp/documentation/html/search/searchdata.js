var indexSectionsWithContent =
{
  0: "abcdgikmnoprstu",
  1: "inpst",
  2: "p",
  3: "acinprt",
  4: "abcgiorstu",
  5: "cdmprst",
  6: "cn",
  7: "ckmnrs",
  8: "cs",
  9: "i"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "events",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Events",
  9: "Pages"
};

