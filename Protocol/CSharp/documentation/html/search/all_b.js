var searchData=
[
  ['readall_59',['ReadAll',['../interface_protocol_client_1_1_i_net_connection.html#afa10ef2a3f492618944756a24a7e1cb3',1,'ProtocolClient.INetConnection.ReadAll()'],['../class_protocol_client_1_1_t_c_p_connection.html#a22098aa748dd030a92dbe7853a6c4c08',1,'ProtocolClient.TCPConnection.ReadAll()']]],
  ['readdata_60',['ReadData',['../class_protocol_client_1_1_t_c_p_client.html#ab53d147e8041dc7f785c833f50ad3804',1,'ProtocolClient::TCPClient']]],
  ['readme_2emd_61',['README.md',['../_r_e_a_d_m_e_8md.html',1,'']]],
  ['received_62',['received',['../struct_protocol_client_1_1_statistics.html#ab42abbb87ae8d7750d626d37c2842bda',1,'ProtocolClient::Statistics']]],
  ['recvbuffer_63',['recVBuffer',['../class_protocol_client_1_1_t_c_p_client.html#a07182d29c70e612b3aa6c6b0a55a0016',1,'ProtocolClient::TCPClient']]],
  ['registerclient_64',['RegisterClient',['../namespace_protocol_client.html#af72ffff3c6df527d42846ac8adb12667ab0ea4b458ab45d2da241f7c59008d1a1',1,'ProtocolClient']]],
  ['reset_65',['Reset',['../class_protocol_client_1_1_net_receive_buffer.html#aeaca29ea83167b27f5d60c017b981468',1,'ProtocolClient.NetReceiveBuffer.Reset()'],['../class_protocol_client_1_1_net_send_buffer.html#af1c4ff446956d7fcee73facab2dcd3b1',1,'ProtocolClient.NetSendBuffer.Reset()']]],
  ['resetclient_66',['ResetClient',['../namespace_protocol_client.html#af72ffff3c6df527d42846ac8adb12667afe259b68af69b11bbef2eaf8fa66f72d',1,'ProtocolClient']]],
  ['resetconnection_67',['ResetConnection',['../class_protocol_client_1_1_t_c_p_client.html#a82cc636f3b03ccab4c37e5ee3b4f4c30',1,'ProtocolClient.TCPClient.ResetConnection()'],['../class_protocol_client_1_1_i_client.html#a5c342bebdfac32f4a0c41f12b4a48b08',1,'ProtocolClient.IClient.ResetConnection()']]],
  ['resetlastexception_68',['ResetLastException',['../class_protocol_client_1_1_t_c_p_connection.html#a4a5cf01bd3d83f1c8c79f548e7d77fbb',1,'ProtocolClient::TCPConnection']]],
  ['runtimestatistics_69',['runtimeStatistics',['../class_protocol_client_1_1_t_c_p_client.html#ab4247e5fe558fb10bf918ddceb5323aa',1,'ProtocolClient::TCPClient']]]
];
