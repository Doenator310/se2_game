var searchData=
[
  ['checkfortimeout_128',['CheckForTimeOut',['../class_protocol_client_1_1_t_c_p_client.html#aa9a65d3ee9704e67b03d208e2facd8c8',1,'ProtocolClient.TCPClient.CheckForTimeOut()'],['../class_protocol_client_1_1_i_client.html#acf0b2d2967a20a1e706a7e4b04529d69',1,'ProtocolClient.IClient.CheckForTimeOut()']]],
  ['closeconnection_129',['CloseConnection',['../interface_protocol_client_1_1_i_net_channel.html#a83ff7dda8ec290258deca815fc2ae13f',1,'ProtocolClient.INetChannel.CloseConnection()'],['../class_protocol_client_1_1_t_c_p_connection.html#a3dbbb209836f50a62337a1bff8e19530',1,'ProtocolClient.TCPConnection.CloseConnection()']]],
  ['connecttoremotehost_130',['ConnectToRemoteHost',['../interface_protocol_client_1_1_i_net_channel.html#a1797b263d7362604918ca5138d84eaf0',1,'ProtocolClient.INetChannel.ConnectToRemoteHost()'],['../class_protocol_client_1_1_t_c_p_connection.html#a3d4f471b3f328b2f08db10443a1c9a02',1,'ProtocolClient.TCPConnection.ConnectToRemoteHost()']]],
  ['connecttoserver_131',['ConnectToServer',['../class_protocol_client_1_1_t_c_p_client.html#af6d9083a3dd1f9fd99e0b77c3ee3e54c',1,'ProtocolClient.TCPClient.ConnectToServer()'],['../class_protocol_client_1_1_i_client.html#a88f50f04fe9223c0c6ad336237f03103',1,'ProtocolClient.IClient.ConnectToServer()']]]
];
