var searchData=
[
  ['iclient_23',['IClient',['../class_protocol_client_1_1_i_client.html',1,'ProtocolClient']]],
  ['iclient_2ecs_24',['IClient.cs',['../_i_client_8cs.html',1,'']]],
  ['inetchannel_25',['INetChannel',['../interface_protocol_client_1_1_i_net_channel.html',1,'ProtocolClient']]],
  ['inetconnection_26',['INetConnection',['../interface_protocol_client_1_1_i_net_connection.html',1,'ProtocolClient']]],
  ['inetconnection_2ecs_27',['INetConnection.cs',['../_i_net_connection_8cs.html',1,'']]],
  ['introduction_28',['Introduction',['../index.html',1,'']]],
  ['invokeexceptionevent_29',['InvokeExceptionEvent',['../class_protocol_client_1_1_i_client.html#aeb437dcb95670ec2d183fd1a753697a1',1,'ProtocolClient::IClient']]],
  ['invokeserverrequestevent_30',['InvokeServerRequestEvent',['../class_protocol_client_1_1_i_client.html#a4fd6da4cff4d5d00c543047dafab81c8',1,'ProtocolClient::IClient']]],
  ['invokeserverresponseevent_31',['InvokeServerResponseEvent',['../class_protocol_client_1_1_i_client.html#a6a6adce90cac044b25196bda29b47590',1,'ProtocolClient::IClient']]],
  ['invokeserverstatusupdateevent_32',['InvokeServerStatusUpdateEvent',['../class_protocol_client_1_1_i_client.html#a20f164c9091ffb764fec67aeaac630e2',1,'ProtocolClient::IClient']]],
  ['isconnected_33',['IsConnected',['../interface_protocol_client_1_1_i_net_channel.html#ab6be4d601b604d1cc49be352b0c14898',1,'ProtocolClient.INetChannel.IsConnected()'],['../class_protocol_client_1_1_t_c_p_connection.html#a75236575baa02f31d096dd2a325fb25b',1,'ProtocolClient.TCPConnection.IsConnected()']]],
  ['ishandshakecomplete_34',['IsHandshakeComplete',['../class_protocol_client_1_1_t_c_p_client.html#a7f5b0472232c11de92099c90181ae903',1,'ProtocolClient.TCPClient.IsHandshakeComplete()'],['../class_protocol_client_1_1_i_client.html#a5d1c0f1d18607b0f52208f61b4e80d11',1,'ProtocolClient.IClient.IsHandshakeComplete()']]]
];
