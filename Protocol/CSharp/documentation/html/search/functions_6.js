var searchData=
[
  ['readall_150',['ReadAll',['../interface_protocol_client_1_1_i_net_connection.html#afa10ef2a3f492618944756a24a7e1cb3',1,'ProtocolClient.INetConnection.ReadAll()'],['../class_protocol_client_1_1_t_c_p_connection.html#a22098aa748dd030a92dbe7853a6c4c08',1,'ProtocolClient.TCPConnection.ReadAll()']]],
  ['readdata_151',['ReadData',['../class_protocol_client_1_1_t_c_p_client.html#ab53d147e8041dc7f785c833f50ad3804',1,'ProtocolClient::TCPClient']]],
  ['reset_152',['Reset',['../class_protocol_client_1_1_net_receive_buffer.html#aeaca29ea83167b27f5d60c017b981468',1,'ProtocolClient.NetReceiveBuffer.Reset()'],['../class_protocol_client_1_1_net_send_buffer.html#af1c4ff446956d7fcee73facab2dcd3b1',1,'ProtocolClient.NetSendBuffer.Reset()']]],
  ['resetconnection_153',['ResetConnection',['../class_protocol_client_1_1_t_c_p_client.html#a82cc636f3b03ccab4c37e5ee3b4f4c30',1,'ProtocolClient.TCPClient.ResetConnection()'],['../class_protocol_client_1_1_i_client.html#a5c342bebdfac32f4a0c41f12b4a48b08',1,'ProtocolClient.IClient.ResetConnection()']]],
  ['resetlastexception_154',['ResetLastException',['../class_protocol_client_1_1_t_c_p_connection.html#a4a5cf01bd3d83f1c8c79f548e7d77fbb',1,'ProtocolClient::TCPConnection']]]
];
