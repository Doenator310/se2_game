var annotated_dup =
[
    [ "ProtocolClient", "namespace_protocol_client.html", [
      [ "TCPClient", "class_protocol_client_1_1_t_c_p_client.html", "class_protocol_client_1_1_t_c_p_client" ],
      [ "Statistics", "struct_protocol_client_1_1_statistics.html", "struct_protocol_client_1_1_statistics" ],
      [ "IClient", "class_protocol_client_1_1_i_client.html", "class_protocol_client_1_1_i_client" ],
      [ "INetConnection", "interface_protocol_client_1_1_i_net_connection.html", "interface_protocol_client_1_1_i_net_connection" ],
      [ "INetChannel", "interface_protocol_client_1_1_i_net_channel.html", "interface_protocol_client_1_1_i_net_channel" ],
      [ "NetReceiveBuffer", "class_protocol_client_1_1_net_receive_buffer.html", "class_protocol_client_1_1_net_receive_buffer" ],
      [ "NetSendBuffer", "class_protocol_client_1_1_net_send_buffer.html", "class_protocol_client_1_1_net_send_buffer" ],
      [ "Program", "class_protocol_client_1_1_program.html", null ],
      [ "NetMessage", "class_protocol_client_1_1_net_message.html", "class_protocol_client_1_1_net_message" ],
      [ "TCPConnection", "class_protocol_client_1_1_t_c_p_connection.html", "class_protocol_client_1_1_t_c_p_connection" ]
    ] ]
];