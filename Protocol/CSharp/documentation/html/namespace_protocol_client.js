var namespace_protocol_client =
[
    [ "TCPClient", "class_protocol_client_1_1_t_c_p_client.html", "class_protocol_client_1_1_t_c_p_client" ],
    [ "Statistics", "struct_protocol_client_1_1_statistics.html", "struct_protocol_client_1_1_statistics" ],
    [ "IClient", "class_protocol_client_1_1_i_client.html", "class_protocol_client_1_1_i_client" ],
    [ "INetConnection", "interface_protocol_client_1_1_i_net_connection.html", "interface_protocol_client_1_1_i_net_connection" ],
    [ "INetChannel", "interface_protocol_client_1_1_i_net_channel.html", "interface_protocol_client_1_1_i_net_channel" ],
    [ "NetReceiveBuffer", "class_protocol_client_1_1_net_receive_buffer.html", "class_protocol_client_1_1_net_receive_buffer" ],
    [ "NetSendBuffer", "class_protocol_client_1_1_net_send_buffer.html", "class_protocol_client_1_1_net_send_buffer" ],
    [ "Program", "class_protocol_client_1_1_program.html", null ],
    [ "NetMessage", "class_protocol_client_1_1_net_message.html", "class_protocol_client_1_1_net_message" ],
    [ "TCPConnection", "class_protocol_client_1_1_t_c_p_connection.html", "class_protocol_client_1_1_t_c_p_connection" ],
    [ "ConnectionState", "namespace_protocol_client.html#a7e990d70fcb563cfd41450c5d6bca5d6", [
      [ "NOT_CONNECTED", "namespace_protocol_client.html#a7e990d70fcb563cfd41450c5d6bca5d6ad91be9b5ceb9fe0af02b9b02413eccf8", null ],
      [ "CONNECTING", "namespace_protocol_client.html#a7e990d70fcb563cfd41450c5d6bca5d6a9a14f95e151eec641316e7c784ce832d", null ],
      [ "CONNECTED", "namespace_protocol_client.html#a7e990d70fcb563cfd41450c5d6bca5d6aa5afd6edd5336d91316964e493936858", null ]
    ] ],
    [ "NetMessageType", "namespace_protocol_client.html#af72ffff3c6df527d42846ac8adb12667", [
      [ "RegisterClient", "namespace_protocol_client.html#af72ffff3c6df527d42846ac8adb12667ab0ea4b458ab45d2da241f7c59008d1a1", null ],
      [ "ResetClient", "namespace_protocol_client.html#af72ffff3c6df527d42846ac8adb12667afe259b68af69b11bbef2eaf8fa66f72d", null ],
      [ "KeepAlive", "namespace_protocol_client.html#af72ffff3c6df527d42846ac8adb12667a17229586d3c63a13bfdc6c71c569c867", null ],
      [ "ServerStatusUpdate", "namespace_protocol_client.html#af72ffff3c6df527d42846ac8adb12667a7803b6fb262d3c2a4a808f7158ee25b4", null ],
      [ "ClientRequest", "namespace_protocol_client.html#af72ffff3c6df527d42846ac8adb12667a9c9ef408c42391f13a93c10dbcd22187", null ],
      [ "ClientResponse", "namespace_protocol_client.html#af72ffff3c6df527d42846ac8adb12667a27e72c25fe3fdb316ce1bcee99f88903", null ],
      [ "ServerRequest", "namespace_protocol_client.html#af72ffff3c6df527d42846ac8adb12667a672fbaa3721170a948eb10d881057a1b", null ],
      [ "ServerResponse", "namespace_protocol_client.html#af72ffff3c6df527d42846ac8adb12667ac96dbb5dfd0a2e53b722c160d05f9417", null ],
      [ "MessageError", "namespace_protocol_client.html#af72ffff3c6df527d42846ac8adb12667ad5b6f5b8fac988f18abbd54cc11ab66f", null ],
      [ "CloseConnection", "namespace_protocol_client.html#af72ffff3c6df527d42846ac8adb12667a6afd507c18b528b47bfe70deb8912305", null ]
    ] ]
];