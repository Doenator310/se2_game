var hierarchy =
[
    [ "ProtocolClient.IClient", "class_protocol_client_1_1_i_client.html", [
      [ "ProtocolClient.TCPClient", "class_protocol_client_1_1_t_c_p_client.html", null ]
    ] ],
    [ "ProtocolClient.INetChannel", "interface_protocol_client_1_1_i_net_channel.html", [
      [ "ProtocolClient.TCPConnection", "class_protocol_client_1_1_t_c_p_connection.html", null ]
    ] ],
    [ "ProtocolClient.INetConnection", "interface_protocol_client_1_1_i_net_connection.html", [
      [ "ProtocolClient.TCPConnection", "class_protocol_client_1_1_t_c_p_connection.html", null ]
    ] ],
    [ "ProtocolClient.NetMessage", "class_protocol_client_1_1_net_message.html", null ],
    [ "ProtocolClient.NetReceiveBuffer", "class_protocol_client_1_1_net_receive_buffer.html", null ],
    [ "ProtocolClient.NetSendBuffer", "class_protocol_client_1_1_net_send_buffer.html", null ],
    [ "ProtocolClient.Program", "class_protocol_client_1_1_program.html", null ],
    [ "ProtocolClient.Statistics", "struct_protocol_client_1_1_statistics.html", null ]
];