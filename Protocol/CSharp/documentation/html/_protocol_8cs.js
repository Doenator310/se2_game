var _protocol_8cs =
[
    [ "NetMessage", "class_protocol_client_1_1_net_message.html", "class_protocol_client_1_1_net_message" ],
    [ "NetMessageType", "_protocol_8cs.html#af72ffff3c6df527d42846ac8adb12667", [
      [ "RegisterClient", "_protocol_8cs.html#af72ffff3c6df527d42846ac8adb12667ab0ea4b458ab45d2da241f7c59008d1a1", null ],
      [ "ResetClient", "_protocol_8cs.html#af72ffff3c6df527d42846ac8adb12667afe259b68af69b11bbef2eaf8fa66f72d", null ],
      [ "KeepAlive", "_protocol_8cs.html#af72ffff3c6df527d42846ac8adb12667a17229586d3c63a13bfdc6c71c569c867", null ],
      [ "ServerStatusUpdate", "_protocol_8cs.html#af72ffff3c6df527d42846ac8adb12667a7803b6fb262d3c2a4a808f7158ee25b4", null ],
      [ "ClientRequest", "_protocol_8cs.html#af72ffff3c6df527d42846ac8adb12667a9c9ef408c42391f13a93c10dbcd22187", null ],
      [ "ClientResponse", "_protocol_8cs.html#af72ffff3c6df527d42846ac8adb12667a27e72c25fe3fdb316ce1bcee99f88903", null ],
      [ "ServerRequest", "_protocol_8cs.html#af72ffff3c6df527d42846ac8adb12667a672fbaa3721170a948eb10d881057a1b", null ],
      [ "ServerResponse", "_protocol_8cs.html#af72ffff3c6df527d42846ac8adb12667ac96dbb5dfd0a2e53b722c160d05f9417", null ],
      [ "MessageError", "_protocol_8cs.html#af72ffff3c6df527d42846ac8adb12667ad5b6f5b8fac988f18abbd54cc11ab66f", null ],
      [ "CloseConnection", "_protocol_8cs.html#af72ffff3c6df527d42846ac8adb12667a6afd507c18b528b47bfe70deb8912305", null ]
    ] ]
];