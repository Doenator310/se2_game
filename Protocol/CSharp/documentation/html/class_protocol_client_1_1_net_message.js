var class_protocol_client_1_1_net_message =
[
    [ "clientId", "class_protocol_client_1_1_net_message.html#ae3c9cb9ebd110c746cf5922483bc5b9c", null ],
    [ "crc", "class_protocol_client_1_1_net_message.html#ab56e583cd962ebc6b5144954e4cce7fb", null ],
    [ "messageId", "class_protocol_client_1_1_net_message.html#af835f3cc43df830eebc85b2089db8c1d", null ],
    [ "payload", "class_protocol_client_1_1_net_message.html#aeea643f39dd80841b0f1e2d0c0bc01ce", null ],
    [ "payloadSize", "class_protocol_client_1_1_net_message.html#a9a3dc378c07c5f14b9b10571b598eaaa", null ],
    [ "type", "class_protocol_client_1_1_net_message.html#ae71ca18de6e52c35904d17168ec68bb5", null ]
];