var _i_client_8cs =
[
    [ "Statistics", "struct_protocol_client_1_1_statistics.html", "struct_protocol_client_1_1_statistics" ],
    [ "IClient", "class_protocol_client_1_1_i_client.html", "class_protocol_client_1_1_i_client" ],
    [ "ConnectionState", "_i_client_8cs.html#a7e990d70fcb563cfd41450c5d6bca5d6", [
      [ "NOT_CONNECTED", "_i_client_8cs.html#a7e990d70fcb563cfd41450c5d6bca5d6ad91be9b5ceb9fe0af02b9b02413eccf8", null ],
      [ "CONNECTING", "_i_client_8cs.html#a7e990d70fcb563cfd41450c5d6bca5d6a9a14f95e151eec641316e7c784ce832d", null ],
      [ "CONNECTED", "_i_client_8cs.html#a7e990d70fcb563cfd41450c5d6bca5d6aa5afd6edd5336d91316964e493936858", null ]
    ] ]
];