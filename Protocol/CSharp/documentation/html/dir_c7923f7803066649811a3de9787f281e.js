var dir_c7923f7803066649811a3de9787f281e =
[
    [ "obj", "dir_4af6043d1675c573a0f0dfa1da43f89f.html", "dir_4af6043d1675c573a0f0dfa1da43f89f" ],
    [ "Properties", "dir_0d4e2f4d863658af8de2ae610732ffa0.html", "dir_0d4e2f4d863658af8de2ae610732ffa0" ],
    [ "Client.cs", "_unit_test_project_2_client_8cs.html", [
      [ "TCPClient", "class_protocol_client_1_1_t_c_p_client.html", "class_protocol_client_1_1_t_c_p_client" ]
    ] ],
    [ "IClient.cs", "_unit_test_project_2_i_client_8cs.html", "_unit_test_project_2_i_client_8cs" ],
    [ "INetConnection.cs", "_unit_test_project_2_i_net_connection_8cs.html", [
      [ "INetConnection", "interface_protocol_client_1_1_i_net_connection.html", "interface_protocol_client_1_1_i_net_connection" ],
      [ "INetChannel", "interface_protocol_client_1_1_i_net_channel.html", "interface_protocol_client_1_1_i_net_channel" ]
    ] ],
    [ "NetReceiveBuffer.cs", "_unit_test_project_2_net_receive_buffer_8cs.html", [
      [ "NetReceiveBuffer", "class_protocol_client_1_1_net_receive_buffer.html", "class_protocol_client_1_1_net_receive_buffer" ]
    ] ],
    [ "NetSendBuffer.cs", "_unit_test_project_2_net_send_buffer_8cs.html", [
      [ "NetSendBuffer", "class_protocol_client_1_1_net_send_buffer.html", "class_protocol_client_1_1_net_send_buffer" ]
    ] ],
    [ "Program.cs", "_unit_test_project_2_program_8cs.html", [
      [ "Program", "class_protocol_client_1_1_program.html", null ]
    ] ],
    [ "Protocol.cs", "_unit_test_project_2_protocol_8cs.html", "_unit_test_project_2_protocol_8cs" ],
    [ "TCPConnection.cs", "_unit_test_project_2_t_c_p_connection_8cs.html", [
      [ "TCPConnection", "class_protocol_client_1_1_t_c_p_connection.html", "class_protocol_client_1_1_t_c_p_connection" ]
    ] ],
    [ "UnitTest1.cs", "_unit_test1_8cs.html", [
      [ "ProtocolToolTest", "class_unit_test_project_1_1_protocol_tool_test.html", "class_unit_test_project_1_1_protocol_tool_test" ]
    ] ]
];