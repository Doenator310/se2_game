﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProtocolClient
{
    interface INetConnection
    {
        void SendAll(byte[] bytes);
        byte[] ReadAll();
    }
    interface INetChannel
    {
        bool ConnectToRemoteHost(string ip, UInt16 port);
        bool CloseConnection();
        bool IsConnected();
    }
}
