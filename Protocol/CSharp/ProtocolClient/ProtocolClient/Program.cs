﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
namespace ProtocolClient
{
    /**
     * The Program class serves as an example on how to implement the Protocol client 
     * 
     */
    class Program
    {
        static IClient client;
        static void OnServerResponse(object a, byte[] data)
        {
            string received = new string(Encoding.UTF8.GetChars(data));
            Console.WriteLine("OnClientResponse: " + a.ToString() + "; Text/Data:" + received);
            client.SendClientRequestToServer(data);
        }

        static void OnServerRequest(object a, byte[] data)
        {
            string received = new string(Encoding.UTF8.GetChars(data));
            Console.WriteLine("OnClientRequest: " + a.ToString() + "; Text/Data:" + received);
            client.SendClientResponseToServer(data);
        }

        static void OnServerStatusUpdate(object a, byte[] data)
        {
            string received = new string(Encoding.UTF8.GetChars(data));
            Console.WriteLine("OnServerStatusUpdate: " + a.ToString() + "; Text/Data:" + received);
        }

        static void OnConnectionException(object a, Exception e)
        {   
            Console.WriteLine("OnClientException: " + e.ToString());
        }

        static void Main(string[] args)
        {
            client = new TCPClient();
            client.ServerRequestEvent  += OnServerRequest;
            client.ServerResponseEvent += OnServerResponse;
            client.ConnectionException += OnConnectionException;
            client.ServerStatusUpdateEvent += OnServerStatusUpdate;
            client.ConnectToServer("127.0.0.1", 1338);

            if (client.GetConnectionState() == ConnectionState.NOT_CONNECTED) {
                Console.WriteLine("Not Connected!");
            }
            //wait for handshake completion
            while (client.GetConnectionState() == ConnectionState.CONNECTING)
            {
                client.Update();
                Thread.Sleep(1);
            }
            // Now that we are connected build the initial test-messages
            byte[] requestByteArray = null;
            {
                var msgAsCharArray = "Client->Request Server->Response".ToCharArray();
                requestByteArray = Encoding.UTF8.GetBytes(msgAsCharArray);
            }

            byte[] responseByteArray = null;
            {
                var msgAsCharArray = "Server->Request Client->Response!".ToCharArray();
                responseByteArray = Encoding.UTF8.GetBytes(msgAsCharArray);
            }
            // Send them and start the sending loop
            client.SendClientRequestToServer(requestByteArray);
            client.SendClientResponseToServer(responseByteArray);

            while (true) {
                try {
                    client.Update();
                    if (client.GetConnectionState() != ConnectionState.CONNECTED)
                    {
                        Console.WriteLine("Connection lost");
                        Console.ReadKey();
                        return;
                    }
                }
                catch (NotImplementedException exception) {
                    Console.WriteLine("An Exception Occoured: "  + exception.GetType().ToString());
                }
                Thread.Sleep(1000);
            }
        }
    }
}
