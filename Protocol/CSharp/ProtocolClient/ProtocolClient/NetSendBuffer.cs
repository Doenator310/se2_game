﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProtocolClient
{
    /**
     * The NetSendBuffer Class contains a ring buffer that 
     * sends the NetMessage with the TCPSocket Instance to the Server
     * 
     * Because of the ringbuffer, it is possible to resend corrupted NetMessages by specifieing their id
     */ 
    class NetSendBuffer
    {
        Dictionary<byte, NetMessage> netMessagesPendingList = new Dictionary<byte, NetMessage>();
        public TCPConnection connection = null;
        private byte currentAddMessageId = 0;
        private byte lastAddedMessageId = 0;

        public void SetConnection(TCPConnection newConnection) {
            connection = newConnection;
        }

        public void AddDataToBuffer(NetMessage toSend)
        {
            if (netMessagesPendingList.ContainsKey(currentAddMessageId))
                netMessagesPendingList.Remove(currentAddMessageId);
            toSend.messageId = currentAddMessageId;
            netMessagesPendingList.Add(currentAddMessageId, toSend);
            lastAddedMessageId = currentAddMessageId;
            currentAddMessageId++;
        }

        public void Reset()
        {
            currentAddMessageId = 0;
            lastAddedMessageId = 0;
            netMessagesPendingList.Clear();
        }

        public bool SendMessageById(byte id) {
            if (!netMessagesPendingList.ContainsKey(id)) {
                return false;
            }
            NetMessage nmToSend = netMessagesPendingList[id];
            connection.SendAll(ProtocolTool.NetMessageToByteArray(nmToSend));
            return true;
        }

        public bool SendNetMessage(NetMessage toSend) {
            AddDataToBuffer(toSend);
            return SendMessageById(lastAddedMessageId);
        }
    }
}
