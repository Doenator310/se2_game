﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProtocolClient
{
    public enum ConnectionState
    {
        NOT_CONNECTED,
        CONNECTING,
        CONNECTED
    }

    public struct Statistics {
        public Statistics(int sendCount = 0, int receivedCount = 0) {
            sent = sendCount;
            received = receivedCount;
        }
        public int sent;
        public int received;
    }

    abstract class IClient
    {
        public event EventHandler<byte[]> ServerRequestEvent;
        public event EventHandler<byte[]> ServerResponseEvent;
        public event EventHandler<byte[]> ServerStatusUpdateEvent;
        public event EventHandler<Exception> ConnectionException;
        public abstract void ConnectToServer(string host, UInt16 port);
        public abstract void ResetConnection();
        public abstract void SendClientRequestToServer(byte[] request);
        public abstract void SendClientResponseToServer(byte[] request);
        
        public abstract void Update();  

        protected abstract void SendRegisterClient();

        protected abstract void SendKeepAlive();    
        protected abstract void SendServerMessageWasCorrupted(byte messageId); 
        protected abstract void SendCloseConnection();
        
        protected abstract void OnClientRegistered(NetMessage nM);
        protected abstract void OnConnectionClosed();
        protected abstract void OnServerRequest(NetMessage nM);
        protected abstract void OnServerResponse(NetMessage nM);
        protected abstract void OnClientMessageWasCorrupted(NetMessage netMsg);
        protected abstract void OnServerStatusUpdate(NetMessage nM);
        protected abstract void UpdateTimeOut();
        protected abstract void CheckForTimeOut();
        protected abstract void OnNewNetMessageReceived(NetMessage nM);
        protected abstract void OnConnectionResetReceived();
        protected abstract void BuildAndSendNetMessage(NetMessageType type, byte[] dataToSend);
        protected abstract bool IsHandshakeComplete();

        public abstract Statistics GetStatistics();

        public abstract ConnectionState GetConnectionState();

        protected void InvokeServerRequestEvent(byte[] dataToSend) {
            ServerRequestEvent?.Invoke(this, dataToSend);
        }

        protected void InvokeServerResponseEvent(byte[] dataToSend)
        {
            ServerResponseEvent?.Invoke(this, dataToSend);
        }

        protected void InvokeServerStatusUpdateEvent(byte[] dataToSend)
        {
            ServerStatusUpdateEvent?.Invoke(this, dataToSend);
        }

        protected void InvokeExceptionEvent(Exception exceptionToSend)
        {
            ConnectionException?.Invoke(this, exceptionToSend);
        }
    }   
}
