﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
namespace ProtocolClient
{
    /**
     * The TCPClient class combines all defined classes to provide a server communication.
     * 
     * This class is used to connect to a target host, write and receive data. 
     * 
     */ 
    [System.Serializable]
    class TCPClient : IClient
    {
        protected byte clientId = 0;
        protected TCPConnection connection = new TCPConnection();
        protected NetReceiveBuffer recVBuffer = new NetReceiveBuffer();
        protected NetSendBuffer sendBuffer = new NetSendBuffer();
        private string lastHost;
        private UInt16 lastPort;
        private DateTime lastResponse;
        private UInt32 timeOutMilliseconds = 5000;
        //Runtime generated:    
        protected byte currentMessageId = 0;

        private void IncrementNetMessageId() {
            currentMessageId++;
        }

        /**
         * The GetConnectionState Method returns the current ConnectionState to the
         * remote host
         */ 
        public override ConnectionState GetConnectionState() {
            bool isSocketConnected = connection.IsConnected();
            if (isSocketConnected) {
                if (IsHandshakeComplete() == false)
                    return ConnectionState.CONNECTING;
                else
                    return ConnectionState.CONNECTED;
            }
            return ConnectionState.NOT_CONNECTED;
        }

        protected override bool IsHandshakeComplete() {
            return clientId != 0;
        }

        public override void ConnectToServer(string host, ushort port)
        {
            lastHost = host;
            lastPort = port;
            sendBuffer.SetConnection(connection);
            if (connection.ConnectToRemoteHost(host, port))
            {
                lastResponse = DateTime.Now;
                SendRegisterClient();
            }
            else {
                OnConnectionClosed();
            }
        }
        
        protected override void OnConnectionClosed()
        {
            clientId = 0;
            //TODO
            currentMessageId = 0;
            recVBuffer.Reset();
            //throw new NotImplementedException();
        }

        public override void ResetConnection()
        {
            connection.CloseConnection();
            connection.ConnectToRemoteHost(lastHost, lastPort);
        }

        public override void SendClientRequestToServer(byte[] request)
        {
            BuildAndSendNetMessage(NetMessageType.ClientRequest, request);
        }

        protected override void OnServerResponse(NetMessage netMsg)
        {
            InvokeServerResponseEvent(netMsg.payload);
        }

        protected override void OnServerRequest(NetMessage netMsg)
        {
            InvokeServerRequestEvent(netMsg.payload);
        }

        public override void SendClientResponseToServer(byte[] request)
        {
            BuildAndSendNetMessage(NetMessageType.ClientResponse, request);
        }

        protected override void OnClientMessageWasCorrupted(NetMessage netMsg)
        {
            byte corruptedId = 0xFF;
            if (netMsg.payload?.Count() > 0) {
                corruptedId = netMsg.payload[0];
                sendBuffer.SendMessageById(corruptedId);
            }
        }

        protected override void OnServerStatusUpdate(NetMessage netMsg)
        {
            InvokeServerStatusUpdateEvent(netMsg.payload);
        }

        protected override void SendCloseConnection()
        {
            BuildAndSendNetMessage(NetMessageType.CloseConnection, null);
        }

        protected override void SendKeepAlive()
        {
            BuildAndSendNetMessage(NetMessageType.KeepAlive, null);
        }

        protected override void SendServerMessageWasCorrupted(byte messageId)
        {
            BuildAndSendNetMessage(NetMessageType.MessageError, BitConverter.GetBytes(messageId));
        }   

        protected virtual byte[] ReadData()
        {
            return connection.ReadAll();
        }

        protected override void SendRegisterClient()
        {
            Debug.Assert(clientId == 0, "Client Id should be 0 if registering!");
            Debug.Assert(currentMessageId == 0, "currentMessage Id should be 0 if registering!");
            BuildAndSendNetMessage(NetMessageType.RegisterClient, null);
        }

        protected override void OnClientRegistered(NetMessage netMsg)
        {
            clientId = netMsg.clientId;
            //Server sendet die erste message Id!
            //currentMessageId = netMsg.messageId;
            UpdateTimeOut();
        }

        protected override void UpdateTimeOut()
        {
            lastResponse = DateTime.Now;
        }

        protected override void BuildAndSendNetMessage(NetMessageType type, byte[] dataToSend)
        {
            var netMessage = ProtocolTool.CreateMessage(type, dataToSend, clientId);
            // WriteData(ProtocolTool.NetMessageToByteArray(netMessage));
            if (type != NetMessageType.RegisterClient) {
                netMessage.clientId = this.clientId;
            }
            sendBuffer.SendNetMessage(netMessage);
        }

        private void HandleNewNetMessages() {
            NetMessage netMessage = null;
            do
            {
                netMessage = recVBuffer.TakeNextNetMessageById(currentMessageId);
                if (netMessage != null)
                {
                    OnNewNetMessageReceived(netMessage);
                }
            } while (netMessage != null);
        }

        /**
         * The Update Method must be regularly called if the client 
         * is connected or in the server-connection process.
         */ 
        public override void Update()
        {
            recVBuffer.AddDataToBuffer(ReadData());
            SendKeepAlive();
            recVBuffer.SearchInBuffer();
            HandleNewNetMessages();   
            Exception exceptionFound = connection.GetLastException();
            if (exceptionFound != null) {
                connection.ResetLastException();
                InvokeExceptionEvent(exceptionFound);
            }
            CheckForTimeOut();
        }

        protected override void CheckForTimeOut()
        {
            var diffTime = (DateTime.Now.Subtract(lastResponse));
            if (diffTime.TotalMilliseconds > timeOutMilliseconds) {
                throw new Exception("Timeout!");
            }
        }

        protected override void OnNewNetMessageReceived(NetMessage nM)
        {            
            byte crcValue = ProtocolTool.GetCRC(nM.payload, (int)nM.payloadSize, 0);
            bool isSame = (crcValue == nM.crc);
            if (isSame == false)
            {
                OnClientMessageWasCorrupted(nM);
                UpdateTimeOut();
                throw new Exception("CRC mismatch handling Not Yet Implemented!");
            }
            else {
                IncrementNetMessageId();
                NetMessageType nMType = (NetMessageType)nM.type;
                switch (nMType) {
                    case NetMessageType.ServerStatusUpdate:
                        this.OnServerStatusUpdate(nM);
                        break;
                    case NetMessageType.ServerRequest:
                        this.OnServerRequest(nM);
                        break;
                    case NetMessageType.ServerResponse:
                        this.OnServerResponse(nM);
                        break;
                    case NetMessageType.MessageError:
                        this.OnClientMessageWasCorrupted(nM);
                        break;
                    case NetMessageType.RegisterClient:
                        this.OnClientRegistered(nM);
                        break;
                    case NetMessageType.CloseConnection:
                        this.OnConnectionClosed();
                        break;
                    case NetMessageType.ResetClient:
                        this.OnConnectionResetReceived();
                        break;

                    default:
                        break;
                }   
            }
            UpdateTimeOut();
        }

        protected override void OnConnectionResetReceived()
        {
            ResetConnection();
            throw new NotImplementedException();
        }
    }
}
