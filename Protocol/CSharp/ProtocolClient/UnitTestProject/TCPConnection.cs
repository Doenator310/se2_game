﻿using System;
using System.Net.Sockets;
namespace ProtocolClient
{
    /**
     * The TCPConnection Class implements the INetConnection and the INetChannel interface
     * to provide an TCP connection to the target host
     * 
     * 
     */
    [System.Serializable]
    class TCPConnection : INetConnection, INetChannel
    {
        protected Socket tcpSocket= new Socket(SocketType.Stream, ProtocolType.Tcp);
        private Exception lastException = null;

        public Exception GetLastException() {            
            return lastException;
        }

        public void ResetLastException() {
            lastException = null;
        }

        public TCPConnection(){
            tcpSocket.NoDelay = true;
            tcpSocket.SendTimeout = 5000;
        }

        public bool CloseConnection()
        {
            if (tcpSocket.Connected)
            {
                try
                {
                    tcpSocket.Close();
                }
                catch (Exception catchedException)
                {
                    this.lastException = catchedException;
                }
            }
            return tcpSocket.Connected == false;
        }

        public bool ConnectToRemoteHost(string ip, ushort port)
        {
            try {
                tcpSocket.Connect(ip, port);
            }
            catch (Exception catchedException) {
                this.lastException = catchedException;
            }
            return tcpSocket.Connected;
        }

        public byte[] ReadAll()
        {
            byte[] readBuffer = new byte[1024];
            int bytesRead = -1;
            try {
                bytesRead = tcpSocket.Receive(readBuffer);
            }
            catch (Exception catchedException) {
                this.lastException = catchedException;
                return null;
            }

            byte[] recVData = new byte[bytesRead];
            Array.Copy(readBuffer, recVData, bytesRead);
            return recVData;
        }

        public void SendAll(byte[] bytes)
        {
            try
            {
                tcpSocket.Send(bytes);
            }
            catch (Exception catchedException) {
                this.lastException = catchedException;
            }
        }

        public bool IsConnected()
        {
            return this.tcpSocket.Connected;
        }
    }
}
