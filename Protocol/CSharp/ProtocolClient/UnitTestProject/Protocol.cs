﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * The protocol.cs file contains helper functions to serialize/deserialize NetMessages
 * 
 */ 
namespace ProtocolClient
{
    
    public enum NetMessageType
    {
        RegisterClient=1,
        ResetClient=2,
        KeepAlive=3, //Client Sending Only!
        ServerStatusUpdate=4,
        ClientRequest=5,
        ClientResponse=6,
        ServerRequest=7,
        ServerResponse=8,
        MessageError=9,
        CloseConnection=10
    };

    /**
     * The NetMessage class is the packet class of the protocol
     * 
     */ 
    public class NetMessage    
    {
        public byte messageId;
        public byte clientId;
        public byte type;
        public byte crc;    
        //limit size to 15 bit
        public UInt16 payloadSize;
        public byte[] payload;//MAX: 0x7FFF 

        public bool IsHeaderEqual(NetMessage other) {
            return (this.messageId == other.messageId && this.clientId == other.clientId &&
            this.type == other.type && this.crc == other.crc && this.payloadSize == other.payloadSize);
        }
    }

    /**
     * The ProtocolTool class contains global helper functions for serializing and deserializing 
     * the protocol packets
     * 
     */ 
    public static class ProtocolTool {
        const int HeaderSizeOfNetMessage = 6;

        public static NetMessage CreateMessage(NetMessageType messageType, byte[] payload, byte clientId = 0) {
            
            NetMessage newMessage = new NetMessage
            {
                type = (byte)messageType,
                payload = payload,
                clientId = clientId           
            };
            if (payload != null)
            {
                newMessage.payloadSize = (ushort)payload.Length;
                newMessage.crc = GetCRC(payload, newMessage.payloadSize, 0);
            }
            return newMessage;
        }

        public static byte GetCRC(byte[] data, int length, int startIndex = 0) {
            byte i;
            byte crc = 0;        // Initial value
            int dataIndex = startIndex;
            while (length > startIndex)
            {
                length--;
                crc ^= data[dataIndex];        // crc ^= *data; 
                dataIndex++;//data++;
                for (i = 0; i < 8; i++)
                {
                    if ((crc & 0x80) != 0)
                        crc = (byte)((crc << 1) ^ 0x07);
                    else
                        crc <<= 1;
                }
            }   
            return crc;
        }

        public static byte[] NetMessageToByteArray(NetMessage toConvert) {
            byte[] rawNetMessage = new byte[toConvert.payloadSize + HeaderSizeOfNetMessage];
            rawNetMessage[0] = toConvert.messageId; 
            rawNetMessage[1] = toConvert.clientId;
            rawNetMessage[2] = toConvert.type;
            rawNetMessage[4] = (byte)toConvert.payloadSize;
            rawNetMessage[5] = (byte)(toConvert.payloadSize >> 8);
            rawNetMessage[3] = GetCRC(toConvert.payload, toConvert.payloadSize);
            if(toConvert.payload != null) { 
                Array.ConstrainedCopy(toConvert.payload, 0, rawNetMessage, HeaderSizeOfNetMessage, toConvert.payloadSize);
            }
            return rawNetMessage;
        }

        public static int GetSizeOfNetMessageInBuffer(IEnumerable<byte> rawNetMessage) {
            if (rawNetMessage.Count() < HeaderSizeOfNetMessage) return HeaderSizeOfNetMessage;
            ushort sizeOfPayload = (ushort)(rawNetMessage.ElementAt(4) | rawNetMessage.ElementAt(5) << 8);
            return HeaderSizeOfNetMessage + sizeOfPayload;
        }

        public static NetMessage GetNetMessageHeaderFromByteArray(IEnumerable<byte> rawNetMessage) {
            if (HeaderSizeOfNetMessage > rawNetMessage.Count()) return null;
            NetMessage result = new NetMessage();
            result.messageId = rawNetMessage.ElementAt(0);
            result.clientId = rawNetMessage.ElementAt(1);
            result.type = rawNetMessage.ElementAt(2);
            result.crc = rawNetMessage.ElementAt(3);
            result.payloadSize = (ushort)(rawNetMessage.ElementAt(4) | rawNetMessage.ElementAt(5) << 8);
            return result;
        }

        public static NetMessage GetMessageFromByteArray(IEnumerable<byte> rawNetMessage)
        {
            if (HeaderSizeOfNetMessage > rawNetMessage.Count()) return null;
            NetMessage result = new NetMessage();
            result.messageId = rawNetMessage.ElementAt(0);
            result.clientId = rawNetMessage.ElementAt(1);
            result.type = rawNetMessage.ElementAt(2);
            result.crc = rawNetMessage.ElementAt(3);
            result.payloadSize = (ushort)(rawNetMessage.ElementAt(4) | rawNetMessage.ElementAt(5) << 8);
            result.payload = new byte[result.payloadSize];
            if(result.payloadSize > 0)
                Array.ConstrainedCopy(rawNetMessage.ToArray(), HeaderSizeOfNetMessage, result.payload, 0, result.payloadSize);
            return result;
        }
    }
}
