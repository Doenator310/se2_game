﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ProtocolClient;
namespace UnitTestProject
{
    [TestClass]
    public class ProtocolToolTest
    {
        [TestMethod]
        public void TestNetMessageCreation()
        {
            NetMessage examplePackage = new NetMessage();
            Assert.AreEqual<byte>(examplePackage.clientId, 0, "Client id is not the same");
            Assert.AreEqual<ushort>(examplePackage.payloadSize, 0, "The Playersize must be 0 if there is no Payload");
            Assert.AreEqual<byte>(examplePackage.type, 0, "The NetMessageType must be zero as default value");
            Assert.AreEqual<byte>(examplePackage.crc, 0, "crc must be zero if there is no payload");
            Assert.AreEqual<byte[]>(examplePackage.payload, null, "The default payload must be null if not specified");
        }

        [TestMethod]
        public void TestCRCSum()
        {
            byte[] buffer = new byte[100];
            for (byte i = 0; i < (byte)buffer.Length; i++)
            {
                buffer[i] = i;
            }

            byte crc = ProtocolTool.GetCRC(buffer, buffer.Length, 0);
            Assert.AreEqual<byte>(crc, 0x0F, "CRC algorithm is not correct");

            for (byte i = 0; i < (byte)buffer.Length; i++)
            {
                buffer[i] = 77;
            }

            crc = ProtocolTool.GetCRC(buffer, buffer.Length, 0);
            Assert.AreEqual<byte>(crc, 0xA0, "CRC algorithm is not correct");

            crc = ProtocolTool.GetCRC(null, 0, 0);
            Assert.AreEqual<byte>(crc, 0x00, "CRC algorithm must handle a null buffer correctly");
        }

        [TestMethod]
        public void TestPackageCreation()
        {
            var netMessage = ProtocolTool.CreateMessage(NetMessageType.RegisterClient, new byte[0], 0);
            Assert.AreEqual<byte>(netMessage.type, (byte)NetMessageType.RegisterClient, "NetMessage Type differs");
            Assert.AreEqual<ushort>(netMessage.payloadSize, 0, "Payloadsize must be 0");
            Assert.AreEqual<byte>(netMessage.clientId, 0, "clientid must be 0");
            Assert.IsNotNull(netMessage.payload, "Payload must be not null");


            netMessage = ProtocolTool.CreateMessage(NetMessageType.MessageError, new byte[99], 44);
            Assert.AreEqual<ushort>(netMessage.payloadSize, 99, "Payloadsize must be 99");
            Assert.IsNotNull(netMessage.payload, "Payload must be not null");
            Assert.AreEqual<byte>(netMessage.clientId, 44, "clientid must be 0");
            Assert.AreEqual<byte>(netMessage.type, (byte)NetMessageType.MessageError, "Type id not correct");

            netMessage = ProtocolTool.CreateMessage(NetMessageType.ServerRequest, null, 3);
            Assert.AreEqual<byte>(netMessage.type, (byte)NetMessageType.ServerRequest, "Type id not correct");
            Assert.AreEqual<ushort>(netMessage.payloadSize, 0, "Payloadsize must be 0");
            Assert.AreEqual<byte>(netMessage.clientId, (3), "clientid not correctly");
            Assert.IsNull(netMessage.payload, "Payload must be null");
        }

        [TestMethod]
        public void NetMessageDeAndSerialization()
        {
            var payload = new byte[100];
            for (int i = 0; i < payload.Length; i++)
            {
                payload[i] = (byte)(i % 256);
            }
            byte crc = ProtocolTool.GetCRC(payload, payload.Length);
            NetMessage netMessage = ProtocolTool.CreateMessage(NetMessageType.RegisterClient, payload, 0);
            Assert.AreEqual<byte>(crc, netMessage.crc, "Payload CRC must be equal to the NetMessage payload CRC");

            byte[] netMessageAsBytes = ProtocolTool.NetMessageToByteArray(netMessage);
            var nMHeader = ProtocolTool.GetNetMessageHeaderFromByteArray(netMessageAsBytes);
            Assert.IsTrue(nMHeader.IsHeaderEqual(netMessage), "NetMessage Header must be the same after the Serialization process");

            NetMessage deSerializedNetMessage = ProtocolTool.GetMessageFromByteArray(netMessageAsBytes);
            Assert.IsTrue(deSerializedNetMessage.IsHeaderEqual(netMessage), "NetMessage Header must be the same after the deserialization process");
            Assert.AreEqual<int>(ProtocolTool.GetCRC(deSerializedNetMessage.payload, deSerializedNetMessage.payloadSize), deSerializedNetMessage.crc, "CRC Sum must be correct for payload");

            byte[] incompleteNMBuffer = new byte[5];
            Assert.IsNull(ProtocolTool.GetMessageFromByteArray(incompleteNMBuffer), "Excepted null if not event the header is in the buffer");
            Assert.AreEqual<int>(ProtocolTool.GetSizeOfNetMessageInBuffer(incompleteNMBuffer), 6, "Exptected Size of Header");
            Assert.AreEqual<int>(ProtocolTool.GetSizeOfNetMessageInBuffer(netMessageAsBytes), 106, "Excpected a NetMessage size of 106");
        }
    }
}
