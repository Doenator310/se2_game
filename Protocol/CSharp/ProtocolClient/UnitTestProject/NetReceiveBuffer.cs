﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProtocolClient
{
    /**
     * The NetReceiveBuffer Class has a buffer that contains raw data 
     * and searches for a new NetMessage in it. These NetMessages then get saved 
     * in a ringbuffer, where they can be accessed by their message id.
     * 
     * 
     */
    class NetReceiveBuffer
    {
        public List<byte> dynamicBuffer = new List<byte>();
        Dictionary<byte, NetMessage> netMessagesPendingList = new Dictionary<byte, NetMessage>();

        private void AddNewKeyValue(byte messageId, NetMessage newNetMessage) {
            if (netMessagesPendingList.ContainsKey(messageId) == true) {
                netMessagesPendingList.Remove(messageId);
            }
            netMessagesPendingList.Add(messageId, newNetMessage);
        }

        public void Reset()
        {
            dynamicBuffer.Clear();
            netMessagesPendingList.Clear();
        }

        public void AddDataToBuffer(byte[] bufferData) {
            if (bufferData != null)
            {
                dynamicBuffer.AddRange(bufferData);
            }
        }

        public void SearchInBuffer() {
            NetMessage newNetMessage = null;
            do {
                newNetMessage = SearchForNetMessageInBuffer();
                if (newNetMessage != null)
                {
                    AddNewKeyValue(newNetMessage.messageId, newNetMessage);
                }
            } while (newNetMessage != null);
        }

        public NetMessage TakeNextNetMessageById(byte id)
        {
            NetMessage msg = GetNetMessageById(id);
            if (msg != null) {
                AddNewKeyValue(id, null);
            }
            return msg;
        }

        public NetMessage GetNetMessageById(byte id) {
            if (netMessagesPendingList.TryGetValue(id, out NetMessage value)) {
                return value;
            }            
            return null;
        }

        protected NetMessage SearchForNetMessageInBuffer()
        {
            int neededSize = ProtocolTool.GetSizeOfNetMessageInBuffer(dynamicBuffer);
            NetMessage netMessage = null;
            if (neededSize <= dynamicBuffer.Count)
            {
                netMessage = ProtocolTool.GetMessageFromByteArray(dynamicBuffer);
                dynamicBuffer.RemoveRange(0, neededSize);
            }
            return netMessage;
        }
    }
}
