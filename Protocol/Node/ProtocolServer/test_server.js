const { NetServer } = require('./netserver')

function onClientRequest (senderClient, dataReceived) {
  console.log('client ' + senderClient.getClientId().toString() + ': onClientRequest', dataReceived)
  senderClient.sendServerResponseToClient(dataReceived)
}

function onClientResponse (senderClient, dataReceived) {
  console.log('client ' + senderClient.getClientId().toString() + ': onClientResponse', dataReceived)
  senderClient.sendServerRequestToClient(dataReceived)
}

function onClientDisconnected (senderClient) {
  console.log('client ' + senderClient.getClientId().toString() + ' disconnected from the server')
}

function onNewClient (clientRef) {
  console.log('A new Client connected!')
  clientRef.setStatusUpdateData(Buffer.from('This is a Status update string ^^', 'utf8'))
  clientRef.on('client_request', onClientRequest)
  clientRef.on('client_response', onClientResponse)
  clientRef.on('client_disconnected', onClientDisconnected)
  // Kann hier nicht senden
}

var server = new NetServer()
server.on('new_client_connected', onNewClient)
try {
  server.listen(1338)
} catch (exception) {
  console.log(exception)
}
