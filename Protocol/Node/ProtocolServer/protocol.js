/**
 * This class acts as a Enum Type
 */
class NetMessageType
{
    constructor () {}
    static RegisterClient= 1;
    static ResetClient= 2;
    static KeepAlive= 3; // Client Sending Only!
    static ServerStatusUpdate= 4;
    static ClientRequest= 5;
    static ClientResponse= 6;
    static ServerRequest= 7;
    static ServerResponse= 8;
    static MessageError= 9;
    static CloseConnection= 10;
}

class NetMessage {
  constructor () {
    this.messageId = 0
    this.clientId = 0
    this.type = 0
    this.crc = 0
    this.payloadSize = 0
    this.payload = []
  }

  isHeaderEqual(other){
    return (this.messageId == other.messageId && this.clientId == other.clientId && 
        this.type == other.type && this.crc == other.crc && this.payloadSize == other.payloadSize)
  }
}
/**
 * The ProtocolTool class contains Static Functions to handle raw Data and NetMessages 
 */
class ProtocolTool  {
  constructor () {}

  static concatTypedArrays (a, b) { // a, b TypedArray of same type
    var c = Buffer.alloc(a.length + b.length)
    c.set(a, 0)
    c.set(b, a.length)
    return c
  }

  static ReduceToByte(inputNumber){
    return inputNumber & 0xFF;    
  }

  static ReduceToShort(inputNumber){
    return inputNumber & 0xFFFF;    
  }

  static HeaderSizeOfNetMessage = 6;
  /**
   * 
   * @param {NetMessageType} messageType 
   * @param {Buffer} payload 
   * @param {int8} clientId 
   * @returns 
   */
  static CreateMessage(messageType, payload, clientId = 0) {
        var newMessage = new NetMessage();
        {
            newMessage.type = this.ReduceToByte(messageType);
            newMessage.payload = payload;
            if(payload != null){
                newMessage.payloadSize = this.ReduceToShort(payload.length)
                newMessage.crc = this.GetCRC(payload, newMessage.payloadSize)
            }
            newMessage.clientId = this.ReduceToByte(clientId);
        }
        return newMessage;
    }
    /**
     * Calculates a CRC Sum for a given Data Buffer
     * @param {Buffer} data 
     * @param {int} length 
     * @param {int} startIndex 
     * @returns 
     */
    static GetCRC(data, length, startIndex = 0) {
        var i;
        var crc = 0;        // Initial value
        var dataIndex = startIndex;
        while (length > startIndex)
        {
            length--;
            crc ^= this.ReduceToByte(data[dataIndex]);        // crc ^= *data; 
            dataIndex++;//data++;
            for (i = 0; i < 8; i++)
            {
                if ((crc & 0x80) != 0)
                    crc = this.ReduceToByte((crc << 1) ^ 0x07);
                else    
                    crc <<= 1;
            }
        }   
        return crc;
    }
/**
 * 
 * @param {NetMessage} toConvert 
 * @returns {Buffer}
 */
    static NetMessageToByteArray(toConvert) {
        var rawNetMessage = Buffer.alloc(toConvert.payloadSize + this.HeaderSizeOfNetMessage);
        rawNetMessage[0] = this.ReduceToByte(toConvert.messageId); 
        rawNetMessage[1] = this.ReduceToByte(toConvert.clientId);
        rawNetMessage[2] = this.ReduceToByte(toConvert.type);
        rawNetMessage[3] = this.ReduceToByte(this.GetCRC(toConvert.payload, toConvert.payloadSize));
        rawNetMessage[4] = this.ReduceToByte(toConvert.payloadSize);
        rawNetMessage[5] = this.ReduceToByte(toConvert.payloadSize >> 8);
        if(toConvert.payload != null) { 
            toConvert.payload.copy(rawNetMessage, this.HeaderSizeOfNetMessage, 0, toConvert.payloadSize)
        }
        return rawNetMessage;
    }   

    static GetSizeOfNetMessageInBuffer(rawNetMessage) {
        if (rawNetMessage.length < this.HeaderSizeOfNetMessage) return this.HeaderSizeOfNetMessage;
        let sizeOfPayload = this.ReduceToShort(rawNetMessage.readUInt8(4) | rawNetMessage.readUInt8(5) << 8);
        return this.HeaderSizeOfNetMessage + sizeOfPayload;
    }
/**
 * Only the Header of the NetMessage gets extracted and returned if the Buffer is big enough
 * @param {Buffer} rawNetMessage 
 * @returns {NetMessage} or null
 */
    static GetNetMessageHeaderFromByteArray(rawNetMessage) {
        if (this.HeaderSizeOfNetMessage > rawNetMessage.length) return null;
        var result = new NetMessage();
        result.messageId = this.ReduceToByte(rawNetMessage.readUInt8(0));
        result.clientId = this.ReduceToByte(rawNetMessage.readUInt8(1));
        result.type = this.ReduceToByte(rawNetMessage.readUInt8(2));
        result.crc = this.ReduceToByte(rawNetMessage.readUInt8(3));
        result.payloadSize = this.ReduceToShort(this.ReduceToByte(rawNetMessage.readUInt8(4)) | this.ReduceToByte(rawNetMessage.readUInt8(5)) << 8);
        return result;
    }
/**
 * Extracts NetMessage from Buffer
 * @param {Buffer} rawNetMessage 
 * @returns {NetMessage} or {null}
 */
    static GetNetMessageFromByteArray(rawNetMessage)
    {
        if (this.HeaderSizeOfNetMessage > rawNetMessage.length) return null;
        var result = new NetMessage();
        result.messageId = this.ReduceToByte(rawNetMessage.readUInt8(0));
        result.clientId  = this.ReduceToByte(rawNetMessage.readUInt8(1));
        result.type = this.ReduceToByte(rawNetMessage.readUInt8(2)); 
        result.crc  = this.ReduceToByte(rawNetMessage.readUInt8(3));
        result.payloadSize = this.ReduceToShort(this.ReduceToByte(rawNetMessage.readUInt8(4)) | this.ReduceToByte(rawNetMessage.readUInt8(5)) << 8);
        result.payload = Buffer.alloc(result.payloadSize);
        rawNetMessage.copy(result.payload, 0, this.HeaderSizeOfNetMessage, this.HeaderSizeOfNetMessage + result.payloadSize)
        return result;
    }
}


module.exports = {"ProtocolTool":ProtocolTool, "NetMessage":NetMessage, "NetMessageType":NetMessageType};
