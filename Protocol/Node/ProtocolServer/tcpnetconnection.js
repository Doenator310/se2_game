const { INetConnection } = require('./inetconnection')
/**
 * This Class Represents a TCP Connection to a Client and abstracts the NodeJS Socket.
 * <br><br>
 * Signals:
 * <br><br>
 * Signal 'data' (Buffer data)
 */
class TCPNetConnection extends INetConnection {
  constructor (socketRef = null) {
    super()
    this.socket = socketRef

    if (this.socket === undefined) {
      this.socket = null
    }

    if (this.socket != null) {
      this.socket.on('data', function (recVData) {
        this.emit('data', recVData)
      }.bind(this))
      this.socket.on('end', function () {
        this.socket = null
        this.emit('end', this)
      }.bind(this))
      this.socket.on('error', function (err) { console.log(err) })
    }
  }

  write (byteBuffer) {
    if (this.isOpen() === true) {
      this.socket.write(byteBuffer)
      return true
    } else {
      return false
    }
  }

  isOpen () {
    if (this.socket === null) return false
    return (this.socket.readyState === 'open')
  }

  close () {
    if (this.isOpen()) {
      this.socket.destroy()
    }
  }
}

module.exports = { TCPNetConnection: TCPNetConnection }
