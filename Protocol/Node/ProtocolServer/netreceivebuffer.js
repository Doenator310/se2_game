const EventEmitter = require('events')
const { ProtocolTool } = require('./protocol')

/**
 * The NetMessageReceiveBuffer class receives Raw Data, add it to the internal Buffer
 * and then searches for new NetMessages in the Buffer. If a NetMessage is found,
 * then the data signal is emitted
 * <br><br>
 * Signals:
 * <br><br>
 * Signal 'data' (NetMessage netMessage)
 */
class NetMessageReceiveBuffer extends EventEmitter {
  constructor (
    clientNetConnection
  ) {
    super()
    this.clientNetConnection = clientNetConnection
    this.clientNetConnection.on('data', this.onNewRawData.bind(this))
    this.dataBuffer = Buffer.alloc(0)
  }

  getNetConnection () {
    return this.clientNetConnection
  }

  /**
   *
   * @param {} data
   */
  onNewRawData (data) {
    this.dataBuffer = ProtocolTool.concatTypedArrays(this.dataBuffer, data)
    // console.log('DATA>>', this.dataBuffer.buffer)
    this.searchAllInBuffer()
  }

  /**
 * Searches in the receive Buffer for a NetMessages and emits a "new_netmessage" signal if one was found
 */
  searchAllInBuffer () {
    var newNetMessage = null
    do {
      newNetMessage = this.searchForNetMessageInBuffer()
      if (newNetMessage != null) {
        this.emit('new_netmessage', newNetMessage, this)
      }
    } while (newNetMessage != null)
  }

  /**
 * Searches for a NetMessage at the Beginning of the Buffer
 * if a Valid NetMessage was found, it will be returned and removed from the receiving Buffer
 * @returns NetMessage or null
 */
  searchForNetMessageInBuffer () {
    const neededSize = ProtocolTool.GetSizeOfNetMessageInBuffer(this.dataBuffer)
    var netMessage = null
    if (neededSize <= this.dataBuffer.length) {
      netMessage = ProtocolTool.GetNetMessageFromByteArray(this.dataBuffer)
      // Remove extracted NetMessage from Buffer!
      const sizeOfBufferWithoutNetMessage = this.dataBuffer.length - neededSize
      var newBuffer = Buffer.alloc(sizeOfBufferWithoutNetMessage)
      this.dataBuffer.copy(newBuffer, 0, neededSize)
      this.dataBuffer = newBuffer
    }
    return netMessage
  }
}

module.exports = { NetMessageReceiveBuffer: NetMessageReceiveBuffer }
