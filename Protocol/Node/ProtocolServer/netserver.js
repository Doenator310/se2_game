const { TCPServer } = require('./tcpserver')
const { ClientManager } = require('./clientmanager')
const EventEmitter = require('events')

/**
 * The NetServer class manages the Protocol Server and Clientconnections
 *
 * If a new Client connects, this class will emit a signal with the
 * client object
 * <br><br>
 * Signals:
 * <br><br>
 * Signal 'new_client_connected' (Client clientObject)
 */
class NetServer extends EventEmitter {
  constructor () {
    super()
    this.clientManager = new ClientManager()
    this.server = new TCPServer()
    this.server.on('new_connection', function (netConnObj) { this.clientManager.onNewNetConnection(netConnObj) }.bind(this))
    this.clientManager.on('new_client_connected', function (newClientRef) { this.emit('new_client_connected', newClientRef) }.bind(this))
  }

  async updateClientManager () {
    this.clientManager.update()
    this.startNewUpdateTimer()
  }

  async startNewUpdateTimer () {
    setTimeout(this.updateClientManager.bind(this), 200)
  }

  /**
   * The listen Method starts the tcpserver on specified port and starts the required status update routine
   *
   * @param {Number} Port value that the server starts listening on
   */
  listen (port) {
    this.startNewUpdateTimer()
    this.server.listen(port)
  }
}

module.exports = { NetServer: NetServer }
