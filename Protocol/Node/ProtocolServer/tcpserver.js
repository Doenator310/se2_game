var net = require('net')
const EventEmitter = require('events')
const { TCPNetConnection } = require('./tcpnetconnection')

// signals: new_connection(clientSocket)

/**
 * The TCPServer Class uses the NodeJS net module to create a TCPServer
 * If a new Client connects, this class wraps the client socket into the TCPNetConnectionClass and emits
 * a new_connection signal to notify.
 * <br><br>
 * Signals:
 * <br><br>
 * Signal 'new_connection' (TCPNetConnection tcpConnection)
 */
class TCPServer extends EventEmitter {
  constructor () {
    super()
    this.socketTimeout = 0
    this.server = net.createServer(this.onNewServerClientConnection.bind(this))
    // ForEach socket create a buffer that searches for net messages :)
    this.netBuffer = []
  }

  onNewServerClientConnection (client) {
    client.setTimeout(this.socketTimeout)
    this.onClientSocketConnected(client)
    console.log('Client connect. Client local address : ' + client.localAddress + ':' + client.localPort + '. client remote address : ' + client.remoteAddress + ':' + client.remotePort)
  }

  onClientSocketConnected (clientSocket) {
    clientSocket.on('end', this.onClientSocketDisconnected.bind(this))
    clientSocket.on('timeout', function () { console.log('Client request time out. ') })
    var tcpConnection = new TCPNetConnection(clientSocket)
    this.emit('new_connection', tcpConnection)
    console.log('Client connected!')
  }

  onClientSocketDisconnected (clientSocket) {
    for (let i = 0; i < this.netBuffer.length; i++) {
      if (this.netBuffer[i].clientSocket === clientSocket) {
        this.netBuffer.splice(i, 1)
        i--
      }
    }
    console.log('Client disconnected!')
  }

  onStartedListening () {
    this.server.on('close', function () {
      console.log('TCP server socket is closed.')
    })
    this.server.on('error', function (error) {
      console.error(JSON.stringify(error))
    })
  }

  listen (port) {
    var serverInfo = this.server.address()

    var serverInfoJson = JSON.stringify(serverInfo)

    console.log('TCP server listen on address : ' + serverInfoJson)
    this.server.listen(port, this.onStartedListening.bind(this))
  }
}

module.exports = { TCPServer: TCPServer }
