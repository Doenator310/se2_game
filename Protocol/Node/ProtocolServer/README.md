# Where to start?
### The easiest way to get started is to view the example file that implements all the necessary functions to start the protocol server. The file name is "test_server.js" 

# The Main Classes
### The Client class represents the remote client and offers a interface for communication
### The NetServer class provides a server that handles new connections and emits a signal if a new client connected


