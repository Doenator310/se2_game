const { IClient } = require('./iclient')
const { NetMessageType, ProtocolTool } = require('./protocol')

/**
 * This class represents a remote client and is the main interface to communicate with that client
 * using the protocol
 * <br><br>
 * Signals:
 * <br><br>
 * Signal 'client_request' (Client clientObject, Buffer payload)
 * <br>
 * Signal 'client_response' (Client clientObject, Buffer payload)
 * <br>
 * Signal 'client_disconnected' (Client clientObject)
 */
class Client extends IClient {
  constructor (netConnection) {
    super()
    this.clientId = 0
    this.setNetConnection(netConnection)
    this.lastResponseTime = null
    this.timeOut = 5000
    this.statusUpdateData = null
    this.statistics = { sent: 0, received: 0 }
  }

  incrementSentStatistic () {
    this.statistics.sent++
  }

  incrementReceivedStatistic () {
    this.statistics.received++
  }

  /**
   * The newUpdateData that is set by this method will be transmitted every serverStatusUpdate
   * <br>
   * This enables a customized statusUpdate for each client
   *
   * @param {Buffer} newUpdateData
   */
  setStatusUpdateData (newUpdateData) {
    this.statusUpdateData = newUpdateData
  }

  getStatusUpdateData (statusUpdateData) {
    return this.statusUpdateData
  }

  getClientId () {
    return this.clientId
  }

  /**
   * Sets the Client Id
   * The Client Id must be bigger than 0
   * @param {int} newId number
   */
  setClientId (newId) {
    this.clientId = newId
    if (this.clientId < 1) {
      throw Error('Invalid Client Id assigned')
    }
  }

  /**
 * Sends a Packet of type ServerResponse to the client
 * @param {Buffer} data A buffer with the data that will be sent to the client
 */
  sendServerResponseToClient (data) {
    const messageToSend = ProtocolTool.CreateMessage(NetMessageType.ServerResponse, data, this.getClientId())
    this.sendNetMessage(messageToSend)
  }

  /**
 * Sends a Packet of type ServerRequest to the client
 * @param {Buffer} data A buffer with the data that will be sent to the client
 */
  sendServerRequestToClient (data) {
    const messageToSend = ProtocolTool.CreateMessage(NetMessageType.ServerRequest, data, this.getClientId())
    this.sendNetMessage(messageToSend)
  }

  /**
 * Starts the Initialiuation process of the client class *
 */
  init () {
    // send initial message
    const messageToSend = ProtocolTool.CreateMessage(NetMessageType.RegisterClient, null, this.getClientId())
    this.sendNetMessage(messageToSend)
    this.updateLastResponseTime()
  }

  setNetConnection (newNetConnection) {
    this.lastSocket = newNetConnection
  }

  /**
 * checks if the last response of the client is too old. The client connection will be closed if that is the case.
 */
  checkForTimeout () {
    if (this.lastResponseTime != null) {
      const msecDiff = new Date() - this.lastResponseTime
      if (msecDiff > this.timeOut) {
        this.timeOut = null
        console.log('Timeout! client is not responding for: ' + msecDiff)
        this.closeConnection()
      }
    }
  }

  /**
   * Closes the client connection cleanly by sending a CloseConnection Message to the client before closing the socket
   */
  closeConnection () {
    console.log('closing client connection')
    if (this.lastSocket.isOpen() === true) {
      console.log('sending close connection message to client')
      const messageToSend = ProtocolTool.CreateMessage(NetMessageType.CloseConnection, null, this.getClientId())
      this.sendNetMessage(messageToSend)
      this.lastSocket.close()
    }
    this.emit('client_disconnected', this)
  }

  /**
 * Sends a NetMessage to the target Client
 * @param {NetMessage} netMessage  netMessage that is about to get sended to the client
 */
  sendNetMessage (netMessage) {
    this.incrementSentStatistic()
    netMessage.clientId = this.clientId
    this.emit('send_netmessage', netMessage, this)
  }

  /**
   * updates the last Response Time Value for calculating the timeout value
   */
  updateLastResponseTime () {
    this.lastResponseTime = new Date()
  }

  /**
   * This Function Handles received NetMessages and reacts appropietly based on the requested type of the client
   * @param {NetMessage} netMessage received NetMessage
   */
  onNewNetMessageReceived (netMessage) {
    this.incrementReceivedStatistic()
    // ist das so in ordnung ????
    this.updateLastResponseTime()
    switch (netMessage.type) {
      case NetMessageType.ClientRequest:
        this.emit('client_request', this, netMessage.payload)
        break
      case NetMessageType.ClientResponse:
        this.emit('client_response', this, netMessage.payload)
        break
      case NetMessageType.KeepAlive:
        this.updateLastResponseTime()
        break
      case NetMessageType.CloseConnection:
        this.closeConnection()
        break
    }
  }

  /**
   *
   * @returns {Statistics-Dictionary}
   */
  getStatistics () {
    return this.statistics
  }

  /**
   * checks for client timeout
   */
  update () {
    this.checkForTimeout()
  }
}

module.exports = { Client: Client }
