const EventEmitter = require('events')
class INetConnection extends EventEmitter {
  constructor () {
    super()
    if (this.constructor === INetConnection) {
      throw new Error('You cannot create an instance of the abstract class INetConnection')
    }
  }

  notImplemented () {
    console.log((new Error()).stack[1])
    throw new Error('Abstract method ' + (new Error()).stack.split('\n')[1].split(' ').filter(function (e) { return e.length > 0 })[1] + ' has no implementation')
  }

  write (byteBuffer) { this.notImplemented() }
  isOpen () { this.notImplemented() }
  close () { this.notImplemented() }
}

module.exports = { INetConnection: INetConnection }
