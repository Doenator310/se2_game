﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CellEventArgs::.ctor()
extern void CellEventArgs__ctor_m77B26E0518E7D0D30CD856D44ED172B4B096F60B (void);
// 0x00000002 System.Void CellEventArgs::.ctor(System.Boolean)
extern void CellEventArgs__ctor_m42CBF75EDC8AEB81C32D6A6299981BB5BE3481BC (void);
// 0x00000003 System.Boolean CellEventArgs::get_Hovered()
extern void CellEventArgs_get_Hovered_m2C28642E496CC89649BE2B23DAFAF606407BD189 (void);
// 0x00000004 System.Void CellEventArgs::set_Hovered(System.Boolean)
extern void CellEventArgs_set_Hovered_mED2C25ABB5B9CFDB913D36CE1E569EBB01E19EC0 (void);
// 0x00000005 System.Void Cell::add_CellHoverEvent(Cell/CellHoverEventHandler)
extern void Cell_add_CellHoverEvent_m62C3C1BCCCDDF49C3D831FDA239E013B6988BE99 (void);
// 0x00000006 System.Void Cell::remove_CellHoverEvent(Cell/CellHoverEventHandler)
extern void Cell_remove_CellHoverEvent_mCE5ADEEAF176E719EC9545C1D291E37E4FC81C3B (void);
// 0x00000007 System.Void Cell::add_CellClickedEvent(Cell/CellClickedEventHandler)
extern void Cell_add_CellClickedEvent_m2F0AB758089BDF792D350B2DA4B5B6557E78D59F (void);
// 0x00000008 System.Void Cell::remove_CellClickedEvent(Cell/CellClickedEventHandler)
extern void Cell_remove_CellClickedEvent_mACB5AB8439AB65417AF009E14D8172E610D9C402 (void);
// 0x00000009 System.Void Cell::RaiseCellHoverEvent(System.Boolean)
extern void Cell_RaiseCellHoverEvent_m4AD37176E69A4F5BE3203E49AC1D80C5EA76FB60 (void);
// 0x0000000A System.Void Cell::RaiseCellClickedEvent()
extern void Cell_RaiseCellClickedEvent_mB23D573B3CF437B447E3329CD6087D445221CB7E (void);
// 0x0000000B System.Void Cell::OnMouseDown()
extern void Cell_OnMouseDown_m3E6D6A41C9F25AE5727461FB513AD77490E35B31 (void);
// 0x0000000C System.Void Cell::OnMouseEnter()
extern void Cell_OnMouseEnter_m8FF697CD2B368D468228474E1BC3413F0ED87E0E (void);
// 0x0000000D System.Void Cell::OnMouseExit()
extern void Cell_OnMouseExit_mF19FDB97E04ABE368255FBA7190F1EE5CBAAF396 (void);
// 0x0000000E System.Void Cell::SetIndexes(System.Int32,System.Int32)
extern void Cell_SetIndexes_mB97C2F2C7D4CEE6478CD0F5B4A52A8231ED69459 (void);
// 0x0000000F System.Void Cell::.ctor()
extern void Cell__ctor_m8A64B8FBB74A0ECF5EE0871E80CD28E644CBAAF7 (void);
// 0x00000010 System.Void Cell::.cctor()
extern void Cell__cctor_m1D14F72D9A407DAAA7C0DD55C80DC5FE02ABC734 (void);
// 0x00000011 System.Void EnemyField::Start()
extern void EnemyField_Start_mCDC39F0FF180625E36308DE521FCF42567D0A7AA (void);
// 0x00000012 System.Void EnemyField::InitializeCells()
extern void EnemyField_InitializeCells_mDCDC5379AA9F15B92BC0DC7FCE1598BC127743D5 (void);
// 0x00000013 System.Void EnemyField::.ctor()
extern void EnemyField__ctor_mF63B12AA5DBF59910F390E93614C11C72B9C6A1D (void);
// 0x00000014 System.Void Field::Start()
extern void Field_Start_m8DCCF1A66DB33A7821A590994D8F79043580A310 (void);
// 0x00000015 System.Void Field::InitializeCells()
extern void Field_InitializeCells_m373BF5F93A8819CCD48A7A35E5C81312DA8CC6A7 (void);
// 0x00000016 System.Void Field::OnCellHit(System.Int32,System.Int32,System.Boolean)
extern void Field_OnCellHit_mB36A003572E4C64763E34AC8E0CCA3678602967E (void);
// 0x00000017 System.Void Field::.ctor()
extern void Field__ctor_m850074110EC0DC28CBDCF47344CC06B3C7CA3F03 (void);
// 0x00000018 System.Void NetworkManager::Awake()
extern void NetworkManager_Awake_m264F69270EE58EA94BD639B7CFC5DBB6CEAD7E2F (void);
// 0x00000019 System.Void NetworkManager::Start()
extern void NetworkManager_Start_mF62F7D5D531194AFEF4C3FFAC8BFE07E10F72365 (void);
// 0x0000001A System.Void NetworkManager::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void NetworkManager_OnSceneLoaded_mC352C4F66FD7C5E2CA0498FE9AF8C58ED2324C4B (void);
// 0x0000001B System.Void NetworkManager::CheckInstance()
extern void NetworkManager_CheckInstance_m81FC480048046FB7E6E353CC73CCB22FF917626D (void);
// 0x0000001C System.Void NetworkManager::StartPlacementPhase()
extern void NetworkManager_StartPlacementPhase_m50F17522A3F45C7D26E4DA3FB359479860843E84 (void);
// 0x0000001D System.Void NetworkManager::ThrowLogicError()
extern void NetworkManager_ThrowLogicError_mEBCF9F1A963227EC18FB69A35302146F5857A197 (void);
// 0x0000001E System.Void NetworkManager::OnCellHit(System.Boolean,System.Int32,System.Int32,System.Boolean)
extern void NetworkManager_OnCellHit_mCECB2629807C8147F4E2972B46E0B8A096F63CE5 (void);
// 0x0000001F System.Void NetworkManager::OnServerResponse(System.Object,System.Byte[])
extern void NetworkManager_OnServerResponse_mFDD6F10C6BD01FD06FD36F43D47CB21BDD5D732F (void);
// 0x00000020 System.Void NetworkManager::SendShootCell(System.Int32,System.Int32)
extern void NetworkManager_SendShootCell_m1C5E85545E665BCF5AFE42903BD71095BBAFF50F (void);
// 0x00000021 System.Void NetworkManager::SendShips(System.Collections.Generic.List`1<Ship>)
extern void NetworkManager_SendShips_mB5DF67D6B2CD0624ED0D71ADF06780EBB288584B (void);
// 0x00000022 System.Void NetworkManager::SendMatchmakingRequest()
extern void NetworkManager_SendMatchmakingRequest_m6A3DA0D4DBFF14A179AD7A81F2D6FA6763FA2022 (void);
// 0x00000023 System.Void NetworkManager::OnServerRequest(System.Object,System.Byte[])
extern void NetworkManager_OnServerRequest_m61A6A7F2EA27F355998FCCB66EA5B56AD54F3209 (void);
// 0x00000024 System.Void NetworkManager::OnServerStatusUpdate(System.Object,System.Byte[])
extern void NetworkManager_OnServerStatusUpdate_m7B66E753A550AEA74B48ECA487105D8A8F26A3E4 (void);
// 0x00000025 System.Void NetworkManager::OnConnectionException(System.Object,System.Exception)
extern void NetworkManager_OnConnectionException_mFE229675BCBDD084EE00546034929AA6AEC8702F (void);
// 0x00000026 System.Collections.IEnumerator NetworkManager::HandleConnection()
extern void NetworkManager_HandleConnection_mA5E86302F2531DC5A2058DEEF9FE738D5243B87B (void);
// 0x00000027 System.Void NetworkManager::.ctor()
extern void NetworkManager__ctor_m1C3DBB165C04540C090C4D74FEDBD83DEFC5C62F (void);
// 0x00000028 System.Void Player::Start()
extern void Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021 (void);
// 0x00000029 System.Void Player::OnDestroy()
extern void Player_OnDestroy_m2A962F7BBA90A997ED723C07BF86A0681AA24F6E (void);
// 0x0000002A System.Void Player::Init()
extern void Player_Init_m401115D83CF3607956B7E500383545F8B6588042 (void);
// 0x0000002B System.Void Player::UpdatePlacementShips()
extern void Player_UpdatePlacementShips_m432F1874992B4A70527D0E6EDE75753B57083ECF (void);
// 0x0000002C System.Void Player::OnShipSelected(System.Int32)
extern void Player_OnShipSelected_m07473B2D38372EE1CFBDA985E484A6B8B097AED0 (void);
// 0x0000002D System.Void Player::OnCellHovered(System.Object,CellEventArgs)
extern void Player_OnCellHovered_mAE7B9A82B03E6BB91C8D787CA947A5357BE3ECF5 (void);
// 0x0000002E System.Void Player::OnCellClicked(System.Object,CellEventArgs)
extern void Player_OnCellClicked_mA6B6B1109EE15590E94170CE40E8611B3517FDAF (void);
// 0x0000002F System.Void Player::PlaceShip(System.Object,CellEventArgs)
extern void Player_PlaceShip_mF3755A13493938EF34FE4AEB7E5B69801E8D0145 (void);
// 0x00000030 System.Void Player::ShowErrorPopUp(System.String)
extern void Player_ShowErrorPopUp_mD579E7D01D32349AB1B4EA0F0272FC77B36F2440 (void);
// 0x00000031 System.Void Player::ShootCell(System.Object,CellEventArgs)
extern void Player_ShootCell_m26EF3D2B0F3339F73B3F4D89C655CAF57C8C60A6 (void);
// 0x00000032 System.Void Player::ToggleOrientation()
extern void Player_ToggleOrientation_m53CF237E2F37D86948EAEA3991B86C7BA7E8AB96 (void);
// 0x00000033 System.Void Player::DeleteLastShip()
extern void Player_DeleteLastShip_m3881E67CC897B2B09A7DB7348D5E569732B74FD0 (void);
// 0x00000034 System.Void Player::ConfirmPlacements()
extern void Player_ConfirmPlacements_m2B819263E472A19CA9850D66BF45D9407B8A7369 (void);
// 0x00000035 System.Void Player::OnShipsNotAccepted()
extern void Player_OnShipsNotAccepted_m4FC3F38E952098A8BCC9B71E6244D78773A156B2 (void);
// 0x00000036 System.Void Player::SetPlayersTurn(System.Boolean)
extern void Player_SetPlayersTurn_m4BC7801FD622C695EA4953D22BE8A438142F9E66 (void);
// 0x00000037 System.Void Player::ShowEndScreen(System.Boolean)
extern void Player_ShowEndScreen_m72605505E37D77917E7CA26DF82CC792A7AD7247 (void);
// 0x00000038 System.Void Player::StartPlayingphase()
extern void Player_StartPlayingphase_m86A50FA9BBB7D0508A33D2929D3659442B4E58A9 (void);
// 0x00000039 System.Void Player::.ctor()
extern void Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7 (void);
// 0x0000003A System.Void Player::.cctor()
extern void Player__cctor_m38DB9DEEDAE88802EC9823ECFF4A17859AABF96F (void);
// 0x0000003B System.Void Player::<Init>b__27_0()
extern void Player_U3CInitU3Eb__27_0_m8D6827D28CAB82B07877F797AA211BB18E56B3BD (void);
// 0x0000003C System.Void Player::<Init>b__27_1()
extern void Player_U3CInitU3Eb__27_1_mCDD656E8EF18AA918150F421711C319ABB31A4F2 (void);
// 0x0000003D System.Void Player::<Init>b__27_2()
extern void Player_U3CInitU3Eb__27_2_m3CB994442E169D8C9F468B27B339CB5FA2AAFFE8 (void);
// 0x0000003E System.Void Player::<Init>b__27_3()
extern void Player_U3CInitU3Eb__27_3_mC230339C8E8E63933ECC2C90B23875FCC7F736D3 (void);
// 0x0000003F System.Int32 Ship::get_size()
extern void Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5 (void);
// 0x00000040 System.Void Ship::set_size(System.Int32)
extern void Ship_set_size_mB69F1B1C31D843999D4B226F7A54D53CCEB8EB11 (void);
// 0x00000041 UnityEngine.GameObject[] Ship::get_cells()
extern void Ship_get_cells_m3B9D3AF56A593555CCF9630DFE9D766BB763A91A (void);
// 0x00000042 System.Void Ship::set_cells(UnityEngine.GameObject[])
extern void Ship_set_cells_m62750C426A0B1E8A06CAA44EE64B6C00F6070949 (void);
// 0x00000043 System.Void Ship::.ctor(System.Int32)
extern void Ship__ctor_m5734D7D291677BF934981BBF9F7B42B0EC457056 (void);
// 0x00000044 System.Void Ship::AddCell(UnityEngine.GameObject,System.Int32)
extern void Ship_AddCell_mFE93A99E3A478DBDED4B3AA6FBE1D0BEA8408D4D (void);
// 0x00000045 System.Void Ship::Delete()
extern void Ship_Delete_m7E022DC8EECBAB392587D14CC54C2245D12211AF (void);
// 0x00000046 System.Void Ship::.cctor()
extern void Ship__cctor_m75C4224EC2259358122532BE23FF2DAB441EBF39 (void);
// 0x00000047 System.Void ProtocolClient.TCPClient::IncrementNetMessageId()
extern void TCPClient_IncrementNetMessageId_mBB6ED4FEBC70571FFDEE75AC13C1E16A9A18C4AE (void);
// 0x00000048 ProtocolClient.ConnectionState ProtocolClient.TCPClient::GetConnectionState()
extern void TCPClient_GetConnectionState_m5DD3E995295BD398FF0217A553B044D551F35601 (void);
// 0x00000049 System.Boolean ProtocolClient.TCPClient::IsHandshakeComplete()
extern void TCPClient_IsHandshakeComplete_m8426F9E0E6ABBA575F777C0049E24B28ACCA4EAB (void);
// 0x0000004A System.Void ProtocolClient.TCPClient::ConnectToServer(System.String,System.UInt16)
extern void TCPClient_ConnectToServer_m482B17ACD66A39EB20CB1DBC7025ECC21E11B346 (void);
// 0x0000004B System.Void ProtocolClient.TCPClient::OnConnectionClosed()
extern void TCPClient_OnConnectionClosed_mBA3B3BBC6E498FB41843B96DA29DB58B52D57EF2 (void);
// 0x0000004C System.Void ProtocolClient.TCPClient::ResetConnection()
extern void TCPClient_ResetConnection_mC3FF01B88B24DD066D9661D27515FA9174D84131 (void);
// 0x0000004D System.Void ProtocolClient.TCPClient::SendClientRequestToServer(System.Byte[])
extern void TCPClient_SendClientRequestToServer_m869CF66785F9717FA1F1DE863289CD04D5EA9F8D (void);
// 0x0000004E System.Void ProtocolClient.TCPClient::OnServerResponse(ProtocolClient.NetMessage)
extern void TCPClient_OnServerResponse_m1E3B2B85F307579FBEDD1AF33490C07FE21A18DE (void);
// 0x0000004F System.Void ProtocolClient.TCPClient::OnServerRequest(ProtocolClient.NetMessage)
extern void TCPClient_OnServerRequest_mCF3D19889EDBF627C97B282E7DC9512A3F4C1456 (void);
// 0x00000050 System.Void ProtocolClient.TCPClient::SendClientResponseToServer(System.Byte[])
extern void TCPClient_SendClientResponseToServer_m2FCF8084F6F36DD1785628B3722F1CB64D059025 (void);
// 0x00000051 System.Void ProtocolClient.TCPClient::OnClientMessageWasCorrupted(ProtocolClient.NetMessage)
extern void TCPClient_OnClientMessageWasCorrupted_mEB98637188773289641783D5D8432F7A9AA174B0 (void);
// 0x00000052 System.Void ProtocolClient.TCPClient::OnServerStatusUpdate(ProtocolClient.NetMessage)
extern void TCPClient_OnServerStatusUpdate_mF8FFABEA850ACAA1369A0E9EE6A85006472A53E2 (void);
// 0x00000053 System.Void ProtocolClient.TCPClient::SendCloseConnection()
extern void TCPClient_SendCloseConnection_mF69A8364BD2B4A99A53B9028633D8A81A722EB8F (void);
// 0x00000054 System.Void ProtocolClient.TCPClient::SendKeepAlive()
extern void TCPClient_SendKeepAlive_m8B9AB748AF11B32E631457696A7075A64757F340 (void);
// 0x00000055 System.Void ProtocolClient.TCPClient::SendServerMessageWasCorrupted(System.Byte)
extern void TCPClient_SendServerMessageWasCorrupted_m9A56635B3F8DA16AB5BCCC7C365C59D4D3823CC6 (void);
// 0x00000056 System.Byte[] ProtocolClient.TCPClient::ReadData()
extern void TCPClient_ReadData_mAE5DC4C255E03C400C43731E36EC7A3BA050A6FA (void);
// 0x00000057 System.Void ProtocolClient.TCPClient::SendRegisterClient()
extern void TCPClient_SendRegisterClient_mB97E379DD079177034D4B9CC186B8906A43217B1 (void);
// 0x00000058 System.Void ProtocolClient.TCPClient::OnClientRegistered(ProtocolClient.NetMessage)
extern void TCPClient_OnClientRegistered_mEA4CD57E24E074F2F95AB6ABCE935C95B804C83E (void);
// 0x00000059 System.Void ProtocolClient.TCPClient::UpdateTimeOut()
extern void TCPClient_UpdateTimeOut_m98DC9616111657B768763ABEDD8CF2D397DB5C27 (void);
// 0x0000005A System.Void ProtocolClient.TCPClient::BuildAndSendNetMessage(ProtocolClient.NetMessageType,System.Byte[])
extern void TCPClient_BuildAndSendNetMessage_mF1A8A4F4C48821475627AF77FE50D4C01CD776C4 (void);
// 0x0000005B System.Void ProtocolClient.TCPClient::HandleNewNetMessages()
extern void TCPClient_HandleNewNetMessages_mC1D0A4F5F587F147D7CE405BEBCD6B40B61E2FFD (void);
// 0x0000005C System.Void ProtocolClient.TCPClient::Update()
extern void TCPClient_Update_mDFFF455DA91EF9842289A1CE47C5A66E60A3D408 (void);
// 0x0000005D System.Void ProtocolClient.TCPClient::CheckForTimeOut()
extern void TCPClient_CheckForTimeOut_m5055E62D1E62E7E9106D5137B54EADB7F759E451 (void);
// 0x0000005E System.Void ProtocolClient.TCPClient::OnNewNetMessageReceived(ProtocolClient.NetMessage)
extern void TCPClient_OnNewNetMessageReceived_m9357397956DB11F8F05FF4DD8C3CB8E1534BDE99 (void);
// 0x0000005F System.Void ProtocolClient.TCPClient::OnConnectionResetReceived()
extern void TCPClient_OnConnectionResetReceived_mA3AAF6AFD258C5B91AA92C435BEC4C304FFD63EB (void);
// 0x00000060 System.Void ProtocolClient.TCPClient::.ctor()
extern void TCPClient__ctor_m032ED8789EFF986A3548F635F8383BBE08DB858E (void);
// 0x00000061 System.Void ProtocolClient.IClient::add_ServerRequestEvent(System.EventHandler`1<System.Byte[]>)
extern void IClient_add_ServerRequestEvent_m97136FA563855AF87640E7865F21753CD3E10115 (void);
// 0x00000062 System.Void ProtocolClient.IClient::remove_ServerRequestEvent(System.EventHandler`1<System.Byte[]>)
extern void IClient_remove_ServerRequestEvent_mB4A54D2F46DB957DD2A147A80A70AB92A3347515 (void);
// 0x00000063 System.Void ProtocolClient.IClient::add_ServerResponseEvent(System.EventHandler`1<System.Byte[]>)
extern void IClient_add_ServerResponseEvent_mFF1D827B95AA6BF11F7AB5C97F77340DA5B1692C (void);
// 0x00000064 System.Void ProtocolClient.IClient::remove_ServerResponseEvent(System.EventHandler`1<System.Byte[]>)
extern void IClient_remove_ServerResponseEvent_mA823C49285E37C297DB68A9FF3C44C3A28B23D33 (void);
// 0x00000065 System.Void ProtocolClient.IClient::add_ServerStatusUpdateEvent(System.EventHandler`1<System.Byte[]>)
extern void IClient_add_ServerStatusUpdateEvent_m874656AF3D714B25C12F7CF629134ACF4830CEE0 (void);
// 0x00000066 System.Void ProtocolClient.IClient::remove_ServerStatusUpdateEvent(System.EventHandler`1<System.Byte[]>)
extern void IClient_remove_ServerStatusUpdateEvent_m82AA4C8B7534F8EF8AEB54F0AE9EADDE5B50E4F3 (void);
// 0x00000067 System.Void ProtocolClient.IClient::add_ConnectionException(System.EventHandler`1<System.Exception>)
extern void IClient_add_ConnectionException_mE43AA0290A2010DBF9451129E56253D50563F0B3 (void);
// 0x00000068 System.Void ProtocolClient.IClient::remove_ConnectionException(System.EventHandler`1<System.Exception>)
extern void IClient_remove_ConnectionException_m59F4E68B067C426B1564F816E26B595CE53462CF (void);
// 0x00000069 System.Void ProtocolClient.IClient::ConnectToServer(System.String,System.UInt16)
// 0x0000006A System.Void ProtocolClient.IClient::ResetConnection()
// 0x0000006B System.Void ProtocolClient.IClient::SendClientRequestToServer(System.Byte[])
// 0x0000006C System.Void ProtocolClient.IClient::SendClientResponseToServer(System.Byte[])
// 0x0000006D System.Void ProtocolClient.IClient::Update()
// 0x0000006E System.Void ProtocolClient.IClient::SendRegisterClient()
// 0x0000006F System.Void ProtocolClient.IClient::SendKeepAlive()
// 0x00000070 System.Void ProtocolClient.IClient::SendServerMessageWasCorrupted(System.Byte)
// 0x00000071 System.Void ProtocolClient.IClient::SendCloseConnection()
// 0x00000072 System.Void ProtocolClient.IClient::OnClientRegistered(ProtocolClient.NetMessage)
// 0x00000073 System.Void ProtocolClient.IClient::OnConnectionClosed()
// 0x00000074 System.Void ProtocolClient.IClient::OnServerRequest(ProtocolClient.NetMessage)
// 0x00000075 System.Void ProtocolClient.IClient::OnServerResponse(ProtocolClient.NetMessage)
// 0x00000076 System.Void ProtocolClient.IClient::OnClientMessageWasCorrupted(ProtocolClient.NetMessage)
// 0x00000077 System.Void ProtocolClient.IClient::OnServerStatusUpdate(ProtocolClient.NetMessage)
// 0x00000078 System.Void ProtocolClient.IClient::UpdateTimeOut()
// 0x00000079 System.Void ProtocolClient.IClient::CheckForTimeOut()
// 0x0000007A System.Void ProtocolClient.IClient::OnNewNetMessageReceived(ProtocolClient.NetMessage)
// 0x0000007B System.Void ProtocolClient.IClient::OnConnectionResetReceived()
// 0x0000007C System.Void ProtocolClient.IClient::BuildAndSendNetMessage(ProtocolClient.NetMessageType,System.Byte[])
// 0x0000007D System.Boolean ProtocolClient.IClient::IsHandshakeComplete()
// 0x0000007E ProtocolClient.ConnectionState ProtocolClient.IClient::GetConnectionState()
// 0x0000007F System.Void ProtocolClient.IClient::InvokeServerRequestEvent(System.Byte[])
extern void IClient_InvokeServerRequestEvent_m0D9439F86E2BBB0D74AAE21C75DE2F4A9520D328 (void);
// 0x00000080 System.Void ProtocolClient.IClient::InvokeServerResponseEvent(System.Byte[])
extern void IClient_InvokeServerResponseEvent_m9B0164790F2DA73105E5667A06B83270466C9E5D (void);
// 0x00000081 System.Void ProtocolClient.IClient::InvokeServerStatusUpdateEvent(System.Byte[])
extern void IClient_InvokeServerStatusUpdateEvent_mF960B2A19E3CD03F38AE28F4E2A3B35D31F58A6C (void);
// 0x00000082 System.Void ProtocolClient.IClient::InvokeExceptionEvent(System.Exception)
extern void IClient_InvokeExceptionEvent_mE37866660EDE33F55DA29AA097356B87AB12B898 (void);
// 0x00000083 System.Void ProtocolClient.IClient::.ctor()
extern void IClient__ctor_m0D07757F9C99DE9634940B5CC6F78547A53848E0 (void);
// 0x00000084 System.Void ProtocolClient.INetConnection::SendAll(System.Byte[])
// 0x00000085 System.Byte[] ProtocolClient.INetConnection::ReadAll()
// 0x00000086 System.Boolean ProtocolClient.INetChannel::ConnectToRemoteHost(System.String,System.UInt16)
// 0x00000087 System.Boolean ProtocolClient.INetChannel::CloseConnection()
// 0x00000088 System.Boolean ProtocolClient.INetChannel::IsConnected()
// 0x00000089 System.Void ProtocolClient.NetReceiveBuffer::AddNewKeyValue(System.Byte,ProtocolClient.NetMessage)
extern void NetReceiveBuffer_AddNewKeyValue_m0E0E250207541900778C2311A7D92044583DCE57 (void);
// 0x0000008A System.Void ProtocolClient.NetReceiveBuffer::Reset()
extern void NetReceiveBuffer_Reset_m54E919D930053D4AF69BBA24FA0EBE4FE5DB1C93 (void);
// 0x0000008B System.Void ProtocolClient.NetReceiveBuffer::AddDataToBuffer(System.Byte[])
extern void NetReceiveBuffer_AddDataToBuffer_mEF538D68769E179CD93D8D5E80144EA57405E06C (void);
// 0x0000008C System.Void ProtocolClient.NetReceiveBuffer::SearchInBuffer()
extern void NetReceiveBuffer_SearchInBuffer_m825A53363B87F61FB7EE0CA4D76E550C6F2B6690 (void);
// 0x0000008D ProtocolClient.NetMessage ProtocolClient.NetReceiveBuffer::TakeNextNetMessageById(System.Byte)
extern void NetReceiveBuffer_TakeNextNetMessageById_m14D6F45C0DC5E5EB6E95C8E7FBF746BE6F4AE24C (void);
// 0x0000008E ProtocolClient.NetMessage ProtocolClient.NetReceiveBuffer::GetNetMessageById(System.Byte)
extern void NetReceiveBuffer_GetNetMessageById_m23DFEC39FE560EC0F2ED5CE316C1F0073FB7E04F (void);
// 0x0000008F ProtocolClient.NetMessage ProtocolClient.NetReceiveBuffer::SearchForNetMessageInBuffer()
extern void NetReceiveBuffer_SearchForNetMessageInBuffer_mB24D4BC21C300B8BBC9ABA78C3A8F460CB0BB0F8 (void);
// 0x00000090 System.Void ProtocolClient.NetReceiveBuffer::.ctor()
extern void NetReceiveBuffer__ctor_mA9D01D5830750AA6550E569EE006BF2D61DE6B17 (void);
// 0x00000091 System.Void ProtocolClient.NetSendBuffer::SetConnection(ProtocolClient.TCPConnection)
extern void NetSendBuffer_SetConnection_mCE9EB7BFBA86CCB47B22217122D58D82C9E595F8 (void);
// 0x00000092 System.Void ProtocolClient.NetSendBuffer::AddDataToBuffer(ProtocolClient.NetMessage)
extern void NetSendBuffer_AddDataToBuffer_m5D25260CCB2126C33732948AE63B963BF55DB169 (void);
// 0x00000093 System.Void ProtocolClient.NetSendBuffer::Reset()
extern void NetSendBuffer_Reset_mB35E52BD9238AC87BFE12EEB39845411ADCA5F7D (void);
// 0x00000094 System.Boolean ProtocolClient.NetSendBuffer::SendMessageById(System.Byte)
extern void NetSendBuffer_SendMessageById_m54A10CDCB7A0F39E5C7DC328531BA25CD94C256E (void);
// 0x00000095 System.Boolean ProtocolClient.NetSendBuffer::SendNetMessage(ProtocolClient.NetMessage)
extern void NetSendBuffer_SendNetMessage_m1F1D322E9AA41FCA8BC955B3F53F9F0FC2FB0817 (void);
// 0x00000096 System.Void ProtocolClient.NetSendBuffer::.ctor()
extern void NetSendBuffer__ctor_m450503519157A952342CDA00B9FD06B8C1730814 (void);
// 0x00000097 System.Void ProtocolClient.Program::OnServerResponse(System.Object,System.Byte[])
extern void Program_OnServerResponse_mC8C36D4F38846D1C686CFE44D14D42C4B4B04054 (void);
// 0x00000098 System.Void ProtocolClient.Program::OnServerRequest(System.Object,System.Byte[])
extern void Program_OnServerRequest_m2D0A0E1642BA74E65B0BE9E897A5B50A64544415 (void);
// 0x00000099 System.Void ProtocolClient.Program::OnServerStatusUpdate(System.Object,System.Byte[])
extern void Program_OnServerStatusUpdate_mCB1063529DE2E3330D8915545C4859A935172FCC (void);
// 0x0000009A System.Void ProtocolClient.Program::OnConnectionException(System.Object,System.Exception)
extern void Program_OnConnectionException_m63BF7B0C6CDAFC553DA9EE126D7C14E802191003 (void);
// 0x0000009B System.Void ProtocolClient.Program::Main(System.String[])
extern void Program_Main_m1B3FC9CF9690BECCD200565DEBA25978D8E14899 (void);
// 0x0000009C System.Void ProtocolClient.Program::.ctor()
extern void Program__ctor_m1FF83F114A9435990F39E63E8A396F697DD5A6F8 (void);
// 0x0000009D System.Void ProtocolClient.NetMessage::.ctor()
extern void NetMessage__ctor_mA4C1F498E1F2D598A9ACD6F14BBF921B3F639C7B (void);
// 0x0000009E ProtocolClient.NetMessage ProtocolClient.ProtocolTool::CreateMessage(ProtocolClient.NetMessageType,System.Byte[],System.Byte)
extern void ProtocolTool_CreateMessage_m441288169BBE1CC1C48836E7755683B5A637C2E5 (void);
// 0x0000009F System.Byte ProtocolClient.ProtocolTool::GetCRC(System.Byte[],System.Int32,System.Int32)
extern void ProtocolTool_GetCRC_mAAC3331A0C1C131150EE5196ECDFB30353E20590 (void);
// 0x000000A0 System.Byte[] ProtocolClient.ProtocolTool::NetMessageToByteArray(ProtocolClient.NetMessage)
extern void ProtocolTool_NetMessageToByteArray_mCA7A90C8FF3813DC49D9DE9B039FD261C7AFBDFD (void);
// 0x000000A1 System.Int32 ProtocolClient.ProtocolTool::GetSizeOfNetMessageInBuffer(System.Collections.Generic.IEnumerable`1<System.Byte>)
extern void ProtocolTool_GetSizeOfNetMessageInBuffer_m051D0740337106C72030ED21A975CA266F3C4045 (void);
// 0x000000A2 ProtocolClient.NetMessage ProtocolClient.ProtocolTool::GetNetMessageHeaderFromByteArray(System.Collections.Generic.IEnumerable`1<System.Byte>)
extern void ProtocolTool_GetNetMessageHeaderFromByteArray_mD826FE9DD204E15BCDBEC861D88A1746269CCDF8 (void);
// 0x000000A3 ProtocolClient.NetMessage ProtocolClient.ProtocolTool::GetMessageFromByteArray(System.Collections.Generic.IEnumerable`1<System.Byte>)
extern void ProtocolTool_GetMessageFromByteArray_mBD85E536ED14B56570D1CEE94F5CCFEC234121A7 (void);
// 0x000000A4 System.Exception ProtocolClient.TCPConnection::GetLastException()
extern void TCPConnection_GetLastException_m4312EE21BDC7253C84B58F1889624532F658C4A6 (void);
// 0x000000A5 System.Void ProtocolClient.TCPConnection::ResetLastException()
extern void TCPConnection_ResetLastException_m67BB221266DB25CC43F373CB1ED5A4347D08A87C (void);
// 0x000000A6 System.Void ProtocolClient.TCPConnection::.ctor()
extern void TCPConnection__ctor_mF343843AB3AD623C131AAF1A9721FE426E8F25DC (void);
// 0x000000A7 System.Boolean ProtocolClient.TCPConnection::CloseConnection()
extern void TCPConnection_CloseConnection_mDFDCFAE5342A9BADB24EFB94979FDB7DAEF6FEB9 (void);
// 0x000000A8 System.Boolean ProtocolClient.TCPConnection::ConnectToRemoteHost(System.String,System.UInt16)
extern void TCPConnection_ConnectToRemoteHost_m48C2352E350516CF0CA667BFD6CC6523392429F0 (void);
// 0x000000A9 System.Byte[] ProtocolClient.TCPConnection::ReadAll()
extern void TCPConnection_ReadAll_m67CC289F1F234B79CF29BBB18578C84B3A9C328A (void);
// 0x000000AA System.Void ProtocolClient.TCPConnection::SendAll(System.Byte[])
extern void TCPConnection_SendAll_m5E7C42B504544CA6CFF78C397604D9825F5B5194 (void);
// 0x000000AB System.Boolean ProtocolClient.TCPConnection::IsConnected()
extern void TCPConnection_IsConnected_m4D516CEE807E45D142058548598798839D2CD523 (void);
// 0x000000AC System.Void Cell/CellHoverEventHandler::.ctor(System.Object,System.IntPtr)
extern void CellHoverEventHandler__ctor_mA75DD0C304EFFDF2B7434946DE2A306DFEA4474F (void);
// 0x000000AD System.Void Cell/CellHoverEventHandler::Invoke(System.Object,CellEventArgs)
extern void CellHoverEventHandler_Invoke_m3617B3846462554FF0110231A58370BE5E446409 (void);
// 0x000000AE System.IAsyncResult Cell/CellHoverEventHandler::BeginInvoke(System.Object,CellEventArgs,System.AsyncCallback,System.Object)
extern void CellHoverEventHandler_BeginInvoke_m7CF7D6D3415DD7441AAF0FE42F80EF95592D6204 (void);
// 0x000000AF System.Void Cell/CellHoverEventHandler::EndInvoke(System.IAsyncResult)
extern void CellHoverEventHandler_EndInvoke_m747B67D81DC5CEA0585EA8276D7D82724DCC4381 (void);
// 0x000000B0 System.Void Cell/CellClickedEventHandler::.ctor(System.Object,System.IntPtr)
extern void CellClickedEventHandler__ctor_m0E624D3592920AD801A5117354902BBC0671D60A (void);
// 0x000000B1 System.Void Cell/CellClickedEventHandler::Invoke(System.Object,CellEventArgs)
extern void CellClickedEventHandler_Invoke_mA777B45CCDD94D5BE52E8BC63679ACF954DC4515 (void);
// 0x000000B2 System.IAsyncResult Cell/CellClickedEventHandler::BeginInvoke(System.Object,CellEventArgs,System.AsyncCallback,System.Object)
extern void CellClickedEventHandler_BeginInvoke_m612BDEDC33D7EDE53D6250C1C4638EF19C3A2BC9 (void);
// 0x000000B3 System.Void Cell/CellClickedEventHandler::EndInvoke(System.IAsyncResult)
extern void CellClickedEventHandler_EndInvoke_m03B4956DBE6C776C7121299AF0CF01414D44BC45 (void);
// 0x000000B4 System.Void NetworkManager/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m8977A56E78603A833C559237A86A4EE44E5BBC58 (void);
// 0x000000B5 System.Void NetworkManager/<>c__DisplayClass14_0::<SendShips>b__0(Ship)
extern void U3CU3Ec__DisplayClass14_0_U3CSendShipsU3Eb__0_mA5DBBC94404A3AC08A32CA6247039C842132B91A (void);
// 0x000000B6 System.Void NetworkManager/<HandleConnection>d__19::.ctor(System.Int32)
extern void U3CHandleConnectionU3Ed__19__ctor_m0B975E74604C0702C0AD73310F88842A78A9B3C1 (void);
// 0x000000B7 System.Void NetworkManager/<HandleConnection>d__19::System.IDisposable.Dispose()
extern void U3CHandleConnectionU3Ed__19_System_IDisposable_Dispose_mC4F11A17118E3616415623F816761F66938BC246 (void);
// 0x000000B8 System.Boolean NetworkManager/<HandleConnection>d__19::MoveNext()
extern void U3CHandleConnectionU3Ed__19_MoveNext_mF1E5CB4AD3088A07508DD744A9C130B8274B3AE4 (void);
// 0x000000B9 System.Object NetworkManager/<HandleConnection>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHandleConnectionU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60F3F861D4AF9D9D153D3ED56092E76F13CD33B3 (void);
// 0x000000BA System.Void NetworkManager/<HandleConnection>d__19::System.Collections.IEnumerator.Reset()
extern void U3CHandleConnectionU3Ed__19_System_Collections_IEnumerator_Reset_mC513FF3D1591032404B4F3DA1967D46B0C6C2430 (void);
// 0x000000BB System.Object NetworkManager/<HandleConnection>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CHandleConnectionU3Ed__19_System_Collections_IEnumerator_get_Current_m6351AEBEE9885118D26A214B3E29CC71D86AFF35 (void);
static Il2CppMethodPointer s_methodPointers[187] = 
{
	CellEventArgs__ctor_m77B26E0518E7D0D30CD856D44ED172B4B096F60B,
	CellEventArgs__ctor_m42CBF75EDC8AEB81C32D6A6299981BB5BE3481BC,
	CellEventArgs_get_Hovered_m2C28642E496CC89649BE2B23DAFAF606407BD189,
	CellEventArgs_set_Hovered_mED2C25ABB5B9CFDB913D36CE1E569EBB01E19EC0,
	Cell_add_CellHoverEvent_m62C3C1BCCCDDF49C3D831FDA239E013B6988BE99,
	Cell_remove_CellHoverEvent_mCE5ADEEAF176E719EC9545C1D291E37E4FC81C3B,
	Cell_add_CellClickedEvent_m2F0AB758089BDF792D350B2DA4B5B6557E78D59F,
	Cell_remove_CellClickedEvent_mACB5AB8439AB65417AF009E14D8172E610D9C402,
	Cell_RaiseCellHoverEvent_m4AD37176E69A4F5BE3203E49AC1D80C5EA76FB60,
	Cell_RaiseCellClickedEvent_mB23D573B3CF437B447E3329CD6087D445221CB7E,
	Cell_OnMouseDown_m3E6D6A41C9F25AE5727461FB513AD77490E35B31,
	Cell_OnMouseEnter_m8FF697CD2B368D468228474E1BC3413F0ED87E0E,
	Cell_OnMouseExit_mF19FDB97E04ABE368255FBA7190F1EE5CBAAF396,
	Cell_SetIndexes_mB97C2F2C7D4CEE6478CD0F5B4A52A8231ED69459,
	Cell__ctor_m8A64B8FBB74A0ECF5EE0871E80CD28E644CBAAF7,
	Cell__cctor_m1D14F72D9A407DAAA7C0DD55C80DC5FE02ABC734,
	EnemyField_Start_mCDC39F0FF180625E36308DE521FCF42567D0A7AA,
	EnemyField_InitializeCells_mDCDC5379AA9F15B92BC0DC7FCE1598BC127743D5,
	EnemyField__ctor_mF63B12AA5DBF59910F390E93614C11C72B9C6A1D,
	Field_Start_m8DCCF1A66DB33A7821A590994D8F79043580A310,
	Field_InitializeCells_m373BF5F93A8819CCD48A7A35E5C81312DA8CC6A7,
	Field_OnCellHit_mB36A003572E4C64763E34AC8E0CCA3678602967E,
	Field__ctor_m850074110EC0DC28CBDCF47344CC06B3C7CA3F03,
	NetworkManager_Awake_m264F69270EE58EA94BD639B7CFC5DBB6CEAD7E2F,
	NetworkManager_Start_mF62F7D5D531194AFEF4C3FFAC8BFE07E10F72365,
	NetworkManager_OnSceneLoaded_mC352C4F66FD7C5E2CA0498FE9AF8C58ED2324C4B,
	NetworkManager_CheckInstance_m81FC480048046FB7E6E353CC73CCB22FF917626D,
	NetworkManager_StartPlacementPhase_m50F17522A3F45C7D26E4DA3FB359479860843E84,
	NetworkManager_ThrowLogicError_mEBCF9F1A963227EC18FB69A35302146F5857A197,
	NetworkManager_OnCellHit_mCECB2629807C8147F4E2972B46E0B8A096F63CE5,
	NetworkManager_OnServerResponse_mFDD6F10C6BD01FD06FD36F43D47CB21BDD5D732F,
	NetworkManager_SendShootCell_m1C5E85545E665BCF5AFE42903BD71095BBAFF50F,
	NetworkManager_SendShips_mB5DF67D6B2CD0624ED0D71ADF06780EBB288584B,
	NetworkManager_SendMatchmakingRequest_m6A3DA0D4DBFF14A179AD7A81F2D6FA6763FA2022,
	NetworkManager_OnServerRequest_m61A6A7F2EA27F355998FCCB66EA5B56AD54F3209,
	NetworkManager_OnServerStatusUpdate_m7B66E753A550AEA74B48ECA487105D8A8F26A3E4,
	NetworkManager_OnConnectionException_mFE229675BCBDD084EE00546034929AA6AEC8702F,
	NetworkManager_HandleConnection_mA5E86302F2531DC5A2058DEEF9FE738D5243B87B,
	NetworkManager__ctor_m1C3DBB165C04540C090C4D74FEDBD83DEFC5C62F,
	Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021,
	Player_OnDestroy_m2A962F7BBA90A997ED723C07BF86A0681AA24F6E,
	Player_Init_m401115D83CF3607956B7E500383545F8B6588042,
	Player_UpdatePlacementShips_m432F1874992B4A70527D0E6EDE75753B57083ECF,
	Player_OnShipSelected_m07473B2D38372EE1CFBDA985E484A6B8B097AED0,
	Player_OnCellHovered_mAE7B9A82B03E6BB91C8D787CA947A5357BE3ECF5,
	Player_OnCellClicked_mA6B6B1109EE15590E94170CE40E8611B3517FDAF,
	Player_PlaceShip_mF3755A13493938EF34FE4AEB7E5B69801E8D0145,
	Player_ShowErrorPopUp_mD579E7D01D32349AB1B4EA0F0272FC77B36F2440,
	Player_ShootCell_m26EF3D2B0F3339F73B3F4D89C655CAF57C8C60A6,
	Player_ToggleOrientation_m53CF237E2F37D86948EAEA3991B86C7BA7E8AB96,
	Player_DeleteLastShip_m3881E67CC897B2B09A7DB7348D5E569732B74FD0,
	Player_ConfirmPlacements_m2B819263E472A19CA9850D66BF45D9407B8A7369,
	Player_OnShipsNotAccepted_m4FC3F38E952098A8BCC9B71E6244D78773A156B2,
	Player_SetPlayersTurn_m4BC7801FD622C695EA4953D22BE8A438142F9E66,
	Player_ShowEndScreen_m72605505E37D77917E7CA26DF82CC792A7AD7247,
	Player_StartPlayingphase_m86A50FA9BBB7D0508A33D2929D3659442B4E58A9,
	Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7,
	Player__cctor_m38DB9DEEDAE88802EC9823ECFF4A17859AABF96F,
	Player_U3CInitU3Eb__27_0_m8D6827D28CAB82B07877F797AA211BB18E56B3BD,
	Player_U3CInitU3Eb__27_1_mCDD656E8EF18AA918150F421711C319ABB31A4F2,
	Player_U3CInitU3Eb__27_2_m3CB994442E169D8C9F468B27B339CB5FA2AAFFE8,
	Player_U3CInitU3Eb__27_3_mC230339C8E8E63933ECC2C90B23875FCC7F736D3,
	Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5,
	Ship_set_size_mB69F1B1C31D843999D4B226F7A54D53CCEB8EB11,
	Ship_get_cells_m3B9D3AF56A593555CCF9630DFE9D766BB763A91A,
	Ship_set_cells_m62750C426A0B1E8A06CAA44EE64B6C00F6070949,
	Ship__ctor_m5734D7D291677BF934981BBF9F7B42B0EC457056,
	Ship_AddCell_mFE93A99E3A478DBDED4B3AA6FBE1D0BEA8408D4D,
	Ship_Delete_m7E022DC8EECBAB392587D14CC54C2245D12211AF,
	Ship__cctor_m75C4224EC2259358122532BE23FF2DAB441EBF39,
	TCPClient_IncrementNetMessageId_mBB6ED4FEBC70571FFDEE75AC13C1E16A9A18C4AE,
	TCPClient_GetConnectionState_m5DD3E995295BD398FF0217A553B044D551F35601,
	TCPClient_IsHandshakeComplete_m8426F9E0E6ABBA575F777C0049E24B28ACCA4EAB,
	TCPClient_ConnectToServer_m482B17ACD66A39EB20CB1DBC7025ECC21E11B346,
	TCPClient_OnConnectionClosed_mBA3B3BBC6E498FB41843B96DA29DB58B52D57EF2,
	TCPClient_ResetConnection_mC3FF01B88B24DD066D9661D27515FA9174D84131,
	TCPClient_SendClientRequestToServer_m869CF66785F9717FA1F1DE863289CD04D5EA9F8D,
	TCPClient_OnServerResponse_m1E3B2B85F307579FBEDD1AF33490C07FE21A18DE,
	TCPClient_OnServerRequest_mCF3D19889EDBF627C97B282E7DC9512A3F4C1456,
	TCPClient_SendClientResponseToServer_m2FCF8084F6F36DD1785628B3722F1CB64D059025,
	TCPClient_OnClientMessageWasCorrupted_mEB98637188773289641783D5D8432F7A9AA174B0,
	TCPClient_OnServerStatusUpdate_mF8FFABEA850ACAA1369A0E9EE6A85006472A53E2,
	TCPClient_SendCloseConnection_mF69A8364BD2B4A99A53B9028633D8A81A722EB8F,
	TCPClient_SendKeepAlive_m8B9AB748AF11B32E631457696A7075A64757F340,
	TCPClient_SendServerMessageWasCorrupted_m9A56635B3F8DA16AB5BCCC7C365C59D4D3823CC6,
	TCPClient_ReadData_mAE5DC4C255E03C400C43731E36EC7A3BA050A6FA,
	TCPClient_SendRegisterClient_mB97E379DD079177034D4B9CC186B8906A43217B1,
	TCPClient_OnClientRegistered_mEA4CD57E24E074F2F95AB6ABCE935C95B804C83E,
	TCPClient_UpdateTimeOut_m98DC9616111657B768763ABEDD8CF2D397DB5C27,
	TCPClient_BuildAndSendNetMessage_mF1A8A4F4C48821475627AF77FE50D4C01CD776C4,
	TCPClient_HandleNewNetMessages_mC1D0A4F5F587F147D7CE405BEBCD6B40B61E2FFD,
	TCPClient_Update_mDFFF455DA91EF9842289A1CE47C5A66E60A3D408,
	TCPClient_CheckForTimeOut_m5055E62D1E62E7E9106D5137B54EADB7F759E451,
	TCPClient_OnNewNetMessageReceived_m9357397956DB11F8F05FF4DD8C3CB8E1534BDE99,
	TCPClient_OnConnectionResetReceived_mA3AAF6AFD258C5B91AA92C435BEC4C304FFD63EB,
	TCPClient__ctor_m032ED8789EFF986A3548F635F8383BBE08DB858E,
	IClient_add_ServerRequestEvent_m97136FA563855AF87640E7865F21753CD3E10115,
	IClient_remove_ServerRequestEvent_mB4A54D2F46DB957DD2A147A80A70AB92A3347515,
	IClient_add_ServerResponseEvent_mFF1D827B95AA6BF11F7AB5C97F77340DA5B1692C,
	IClient_remove_ServerResponseEvent_mA823C49285E37C297DB68A9FF3C44C3A28B23D33,
	IClient_add_ServerStatusUpdateEvent_m874656AF3D714B25C12F7CF629134ACF4830CEE0,
	IClient_remove_ServerStatusUpdateEvent_m82AA4C8B7534F8EF8AEB54F0AE9EADDE5B50E4F3,
	IClient_add_ConnectionException_mE43AA0290A2010DBF9451129E56253D50563F0B3,
	IClient_remove_ConnectionException_m59F4E68B067C426B1564F816E26B595CE53462CF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IClient_InvokeServerRequestEvent_m0D9439F86E2BBB0D74AAE21C75DE2F4A9520D328,
	IClient_InvokeServerResponseEvent_m9B0164790F2DA73105E5667A06B83270466C9E5D,
	IClient_InvokeServerStatusUpdateEvent_mF960B2A19E3CD03F38AE28F4E2A3B35D31F58A6C,
	IClient_InvokeExceptionEvent_mE37866660EDE33F55DA29AA097356B87AB12B898,
	IClient__ctor_m0D07757F9C99DE9634940B5CC6F78547A53848E0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NetReceiveBuffer_AddNewKeyValue_m0E0E250207541900778C2311A7D92044583DCE57,
	NetReceiveBuffer_Reset_m54E919D930053D4AF69BBA24FA0EBE4FE5DB1C93,
	NetReceiveBuffer_AddDataToBuffer_mEF538D68769E179CD93D8D5E80144EA57405E06C,
	NetReceiveBuffer_SearchInBuffer_m825A53363B87F61FB7EE0CA4D76E550C6F2B6690,
	NetReceiveBuffer_TakeNextNetMessageById_m14D6F45C0DC5E5EB6E95C8E7FBF746BE6F4AE24C,
	NetReceiveBuffer_GetNetMessageById_m23DFEC39FE560EC0F2ED5CE316C1F0073FB7E04F,
	NetReceiveBuffer_SearchForNetMessageInBuffer_mB24D4BC21C300B8BBC9ABA78C3A8F460CB0BB0F8,
	NetReceiveBuffer__ctor_mA9D01D5830750AA6550E569EE006BF2D61DE6B17,
	NetSendBuffer_SetConnection_mCE9EB7BFBA86CCB47B22217122D58D82C9E595F8,
	NetSendBuffer_AddDataToBuffer_m5D25260CCB2126C33732948AE63B963BF55DB169,
	NetSendBuffer_Reset_mB35E52BD9238AC87BFE12EEB39845411ADCA5F7D,
	NetSendBuffer_SendMessageById_m54A10CDCB7A0F39E5C7DC328531BA25CD94C256E,
	NetSendBuffer_SendNetMessage_m1F1D322E9AA41FCA8BC955B3F53F9F0FC2FB0817,
	NetSendBuffer__ctor_m450503519157A952342CDA00B9FD06B8C1730814,
	Program_OnServerResponse_mC8C36D4F38846D1C686CFE44D14D42C4B4B04054,
	Program_OnServerRequest_m2D0A0E1642BA74E65B0BE9E897A5B50A64544415,
	Program_OnServerStatusUpdate_mCB1063529DE2E3330D8915545C4859A935172FCC,
	Program_OnConnectionException_m63BF7B0C6CDAFC553DA9EE126D7C14E802191003,
	Program_Main_m1B3FC9CF9690BECCD200565DEBA25978D8E14899,
	Program__ctor_m1FF83F114A9435990F39E63E8A396F697DD5A6F8,
	NetMessage__ctor_mA4C1F498E1F2D598A9ACD6F14BBF921B3F639C7B,
	ProtocolTool_CreateMessage_m441288169BBE1CC1C48836E7755683B5A637C2E5,
	ProtocolTool_GetCRC_mAAC3331A0C1C131150EE5196ECDFB30353E20590,
	ProtocolTool_NetMessageToByteArray_mCA7A90C8FF3813DC49D9DE9B039FD261C7AFBDFD,
	ProtocolTool_GetSizeOfNetMessageInBuffer_m051D0740337106C72030ED21A975CA266F3C4045,
	ProtocolTool_GetNetMessageHeaderFromByteArray_mD826FE9DD204E15BCDBEC861D88A1746269CCDF8,
	ProtocolTool_GetMessageFromByteArray_mBD85E536ED14B56570D1CEE94F5CCFEC234121A7,
	TCPConnection_GetLastException_m4312EE21BDC7253C84B58F1889624532F658C4A6,
	TCPConnection_ResetLastException_m67BB221266DB25CC43F373CB1ED5A4347D08A87C,
	TCPConnection__ctor_mF343843AB3AD623C131AAF1A9721FE426E8F25DC,
	TCPConnection_CloseConnection_mDFDCFAE5342A9BADB24EFB94979FDB7DAEF6FEB9,
	TCPConnection_ConnectToRemoteHost_m48C2352E350516CF0CA667BFD6CC6523392429F0,
	TCPConnection_ReadAll_m67CC289F1F234B79CF29BBB18578C84B3A9C328A,
	TCPConnection_SendAll_m5E7C42B504544CA6CFF78C397604D9825F5B5194,
	TCPConnection_IsConnected_m4D516CEE807E45D142058548598798839D2CD523,
	CellHoverEventHandler__ctor_mA75DD0C304EFFDF2B7434946DE2A306DFEA4474F,
	CellHoverEventHandler_Invoke_m3617B3846462554FF0110231A58370BE5E446409,
	CellHoverEventHandler_BeginInvoke_m7CF7D6D3415DD7441AAF0FE42F80EF95592D6204,
	CellHoverEventHandler_EndInvoke_m747B67D81DC5CEA0585EA8276D7D82724DCC4381,
	CellClickedEventHandler__ctor_m0E624D3592920AD801A5117354902BBC0671D60A,
	CellClickedEventHandler_Invoke_mA777B45CCDD94D5BE52E8BC63679ACF954DC4515,
	CellClickedEventHandler_BeginInvoke_m612BDEDC33D7EDE53D6250C1C4638EF19C3A2BC9,
	CellClickedEventHandler_EndInvoke_m03B4956DBE6C776C7121299AF0CF01414D44BC45,
	U3CU3Ec__DisplayClass14_0__ctor_m8977A56E78603A833C559237A86A4EE44E5BBC58,
	U3CU3Ec__DisplayClass14_0_U3CSendShipsU3Eb__0_mA5DBBC94404A3AC08A32CA6247039C842132B91A,
	U3CHandleConnectionU3Ed__19__ctor_m0B975E74604C0702C0AD73310F88842A78A9B3C1,
	U3CHandleConnectionU3Ed__19_System_IDisposable_Dispose_mC4F11A17118E3616415623F816761F66938BC246,
	U3CHandleConnectionU3Ed__19_MoveNext_mF1E5CB4AD3088A07508DD744A9C130B8274B3AE4,
	U3CHandleConnectionU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60F3F861D4AF9D9D153D3ED56092E76F13CD33B3,
	U3CHandleConnectionU3Ed__19_System_Collections_IEnumerator_Reset_mC513FF3D1591032404B4F3DA1967D46B0C6C2430,
	U3CHandleConnectionU3Ed__19_System_Collections_IEnumerator_get_Current_m6351AEBEE9885118D26A214B3E29CC71D86AFF35,
};
static const int32_t s_InvokerIndices[187] = 
{
	1472,
	1277,
	1459,
	1277,
	2373,
	2373,
	2373,
	2373,
	1277,
	1472,
	1472,
	1472,
	1472,
	740,
	1472,
	2413,
	1472,
	1472,
	1472,
	1472,
	1472,
	492,
	1472,
	1472,
	1472,
	811,
	1472,
	1472,
	1472,
	346,
	799,
	740,
	1260,
	1472,
	799,
	799,
	799,
	1440,
	1472,
	1472,
	1472,
	1472,
	1472,
	1249,
	799,
	799,
	799,
	1260,
	799,
	1472,
	1472,
	1472,
	1472,
	1277,
	1277,
	1472,
	1472,
	2413,
	1472,
	1472,
	1472,
	1472,
	1428,
	1249,
	1440,
	1260,
	1249,
	796,
	1472,
	2413,
	1472,
	1428,
	1459,
	795,
	1472,
	1472,
	1260,
	1260,
	1260,
	1260,
	1260,
	1260,
	1472,
	1472,
	1277,
	1440,
	1472,
	1260,
	1472,
	748,
	1472,
	1472,
	1472,
	1260,
	1472,
	1472,
	1260,
	1260,
	1260,
	1260,
	1260,
	1260,
	1260,
	1260,
	795,
	1472,
	1260,
	1260,
	1472,
	1472,
	1472,
	1277,
	1472,
	1260,
	1472,
	1260,
	1260,
	1260,
	1260,
	1472,
	1472,
	1260,
	1472,
	748,
	1459,
	1428,
	1260,
	1260,
	1260,
	1260,
	1472,
	1260,
	1440,
	645,
	1459,
	1459,
	809,
	1472,
	1260,
	1472,
	1014,
	1014,
	1440,
	1472,
	1260,
	1260,
	1472,
	1136,
	1117,
	1472,
	2191,
	2191,
	2191,
	2191,
	2373,
	1472,
	1472,
	1889,
	1930,
	2305,
	2261,
	2305,
	2305,
	1440,
	1472,
	1472,
	1459,
	645,
	1440,
	1260,
	1459,
	798,
	799,
	248,
	1260,
	798,
	799,
	248,
	1260,
	1472,
	1260,
	1249,
	1472,
	1459,
	1440,
	1472,
	1440,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	187,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
