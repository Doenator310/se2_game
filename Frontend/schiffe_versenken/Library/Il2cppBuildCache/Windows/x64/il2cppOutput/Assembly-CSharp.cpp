﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericVirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericInterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// System.Action`1<System.Object>
struct Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC;
// System.Action`1<Ship>
struct Action_1_tCCB45E12289F4906FABB0046E40D54208C8DAB5C;
// System.Collections.Generic.Dictionary`2<System.Byte,ProtocolClient.NetMessage>
struct Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262;
// System.Collections.Generic.Dictionary`2<System.Byte,System.Object>
struct Dictionary_2_t7DB3847BFAFC9452C96798C33FA6D63A1DFEBF9A;
// System.EventHandler`1<System.Byte[]>
struct EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8;
// System.EventHandler`1<System.Exception>
struct EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D;
// System.EventHandler`1<System.Object>
struct EventHandler_1_tFA1C30E54FA1061D79E711F65F9A174BFBD8CDCB;
// System.Collections.Generic.IEnumerable`1<System.Byte>
struct IEnumerable_1_t87C38B0EE9F1DE9AFC8F366EEAE5D497C061B4E1;
// System.Collections.Generic.IEqualityComparer`1<System.Byte>
struct IEqualityComparer_1_t95750B1CF3F7626EC7AB30893A86E08F445DD75D;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Byte,ProtocolClient.NetMessage>
struct KeyCollection_tA9884EA24191C42BBB260C4AB929522760348374;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t34AA4AF4E7352129CA58045901530E41445AC16D;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t815A476B0A21E183042059E705F9E505478CD8AE;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<Ship>
struct List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,System.Int32Enum>
struct UnityAction_2_t808E43EBC9AA89CEA5830BD187EC213182A02B50;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,ProtocolClient.NetMessage>
struct ValueCollection_tB416558D480190C87B084819B8FAC806570080A7;
// System.Collections.Generic.Dictionary`2/Entry<System.Byte,ProtocolClient.NetMessage>[]
struct EntryU5BU5D_t4399F4D773327C2DDF5153EDA2A02F8C11126FF1;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535;
// Ship[]
struct ShipU5BU5D_tCEAB3803C0678DE5410AA4A992B3CEBCC7436DE1;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// UnityEngine.GameObject[0...,0...]
struct GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// Cell
struct Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149;
// CellEventArgs
struct CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// System.Text.DecoderFallback
struct DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// System.Text.EncoderFallback
struct EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4;
// System.Text.Encoding
struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827;
// System.Net.EndPoint
struct EndPoint_t18D4AE8D03090A2B262136E59F95CE61418C34DA;
// EnemyField
struct EnemyField_t935BD6B66CC4595689CD03E7600CAFA8B3893784;
// System.Exception
struct Exception_t;
// Field
struct Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153;
// UnityEngine.UI.FontData
struct FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// System.Collections.Hashtable
struct Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// ProtocolClient.IClient
struct IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// System.IOAsyncCallback
struct IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// ProtocolClient.NetMessage
struct NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670;
// ProtocolClient.NetReceiveBuffer
struct NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF;
// ProtocolClient.NetSendBuffer
struct NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3;
// NetworkManager
struct NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606;
// System.NotImplementedException
struct NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// Player
struct Player_t5689617909B48F7640EA0892D85C92C13CC22C6F;
// ProtocolClient.Program
struct Program_t84DC950B444F8124814710510D84842D4A59581C;
// UnityEngine.UI.RawImage
struct RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.Net.Sockets.SafeSocketHandle
struct SafeSocketHandle_t5050671179FB886DA1763A0E4EFB3FCD072363C9;
// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385;
// Ship
struct Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E;
// System.Net.Sockets.Socket
struct Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// System.String
struct String_t;
// ProtocolClient.TCPClient
struct TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E;
// ProtocolClient.TCPConnection
struct TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// System.Type
struct Type_t;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t675FCE2AEFAC5C924A4020474C997FF2CDD3F4C5;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F;
// Cell/CellClickedEventHandler
struct CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437;
// Cell/CellHoverEventHandler
struct CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// NetworkManager/<>c__DisplayClass14_0
struct U3CU3Ec__DisplayClass14_0_tCCAB38C50CA018337D1FABDD31482528BBE3F1DF;
// NetworkManager/<HandleConnection>d__19
struct U3CHandleConnectionU3Ed__19_t2B2D10AA4C55A2999E347B3F7F12289D2BB5D833;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE;

IL2CPP_EXTERN_C RuntimeClass* Action_1_tCCB45E12289F4906FABB0046E40D54208C8DAB5C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Console_t79987B1B5914E76054A8CBE506B9E11936A8BC07_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IndexOutOfRangeException_tDC9EF7A0346CE39E54DA1083F07BE6DFC3CE2EDD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CHandleConnectionU3Ed__19_t2B2D10AA4C55A2999E347B3F7F12289D2BB5D833_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass14_0_tCCAB38C50CA018337D1FABDD31482528BBE3F1DF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForFixedUpdate_t675FCE2AEFAC5C924A4020474C997FF2CDD3F4C5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0275001C02F73E21B4F7E80C7B45DE35755D42CF;
IL2CPP_EXTERN_C String_t* _stringLiteral062DB096C728515E033CF8C48A1C1F0B9A79384B;
IL2CPP_EXTERN_C String_t* _stringLiteral079998E3393B6BDC1FAFFA63A54F724488AE5306;
IL2CPP_EXTERN_C String_t* _stringLiteral086ACF708E659BABAC341FF91E92F3393010FE37;
IL2CPP_EXTERN_C String_t* _stringLiteral0C2606B489C63F0DF336C88CA02DAA3D51AC64CC;
IL2CPP_EXTERN_C String_t* _stringLiteral10D6F9AEBD4F517BBCDCBFC3E369C08377331E12;
IL2CPP_EXTERN_C String_t* _stringLiteral13F2FF26B335B8BC9BBB7A01E7CBA33A3A93FDE9;
IL2CPP_EXTERN_C String_t* _stringLiteral16A950F8ED51C142DED24CF66F507945DFE09106;
IL2CPP_EXTERN_C String_t* _stringLiteral18E02A56A35A0E2B7B0C47F4586B2C4F19B38FFD;
IL2CPP_EXTERN_C String_t* _stringLiteral35FA7EC05BA14E9CC38AC240741774D8D99798AD;
IL2CPP_EXTERN_C String_t* _stringLiteral3ACC4CC1ADEC59220C31AAE3AEFE4D604CB500A9;
IL2CPP_EXTERN_C String_t* _stringLiteral3F178296412650029A46BF49F4A367936D3796A2;
IL2CPP_EXTERN_C String_t* _stringLiteral410E5346BCA8EE150FFD507311DD85789F2E171E;
IL2CPP_EXTERN_C String_t* _stringLiteral4340D3686DEB126422F53A38EF24AD59BFAEEDEF;
IL2CPP_EXTERN_C String_t* _stringLiteral4442A2F11FF7A9FB25557A681CB38556A3CA1E15;
IL2CPP_EXTERN_C String_t* _stringLiteral457A7B275BF30EDA4D263CB412F36AF1888D0988;
IL2CPP_EXTERN_C String_t* _stringLiteral4C1398A837E1F0C532DEE7917D63C248C072132F;
IL2CPP_EXTERN_C String_t* _stringLiteral505B0F1B5A3487FE7058988A9F84A1A850B987A4;
IL2CPP_EXTERN_C String_t* _stringLiteral50639CAD49418C7B223CC529395C0E2A3892501C;
IL2CPP_EXTERN_C String_t* _stringLiteral5377EFB7D3540F7856D33807A13661041629AB2F;
IL2CPP_EXTERN_C String_t* _stringLiteral579A50C67ADB00AC3925859C23590322B2D0BE9D;
IL2CPP_EXTERN_C String_t* _stringLiteral58549ED8B8D9F9B518B419295904911D7280D5B6;
IL2CPP_EXTERN_C String_t* _stringLiteral59FA9E3480810057109834B562A27C1C58E3CE26;
IL2CPP_EXTERN_C String_t* _stringLiteral6CB05FD18E12F98F81A204339D25DD82BC993FDD;
IL2CPP_EXTERN_C String_t* _stringLiteral6E3E236877E32C8AA063023C2D8DFDB6F4D06132;
IL2CPP_EXTERN_C String_t* _stringLiteral6FE2772A25510BC2C002B4FCD5E9C590A0DF1DDE;
IL2CPP_EXTERN_C String_t* _stringLiteral70BF4C9D283826DF9CDEF1AD58DD09DC37CC109D;
IL2CPP_EXTERN_C String_t* _stringLiteral74841B32DE49F25B899B48D56A5D35FDED7AD32F;
IL2CPP_EXTERN_C String_t* _stringLiteral76C3D4024DE9EE847070E35CC5A197DC21F66FEE;
IL2CPP_EXTERN_C String_t* _stringLiteral7C5EF74B0595BB6BD48A25BD0580CC74406F83C8;
IL2CPP_EXTERN_C String_t* _stringLiteral7F24B739AA45780EA08FF218BC64454668DC866A;
IL2CPP_EXTERN_C String_t* _stringLiteral82C9E73181CA72F47C910A7EF02BE15659453D35;
IL2CPP_EXTERN_C String_t* _stringLiteral84908AE02AE2F5956C34FD61A53E6762038E2440;
IL2CPP_EXTERN_C String_t* _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D;
IL2CPP_EXTERN_C String_t* _stringLiteral8BD159C3D7F4D6D6F5C894061300DC61F9604327;
IL2CPP_EXTERN_C String_t* _stringLiteral92AA5B2BF2682814D726B425F6401630816FCADB;
IL2CPP_EXTERN_C String_t* _stringLiteral9F7324E0CFE9755D866091DF20AC370998BCB0CE;
IL2CPP_EXTERN_C String_t* _stringLiteralA387B4B1E5AE081CE48BC1B1CAF6D2442E00297B;
IL2CPP_EXTERN_C String_t* _stringLiteralA9BB20C09F4F520E63DB862B4C15AEBB8F186DD8;
IL2CPP_EXTERN_C String_t* _stringLiteralAEF11DB910A8B41A49B693B7DA8885A63EC450E4;
IL2CPP_EXTERN_C String_t* _stringLiteralB2F5E43AE5BCE860EA9E569C97183101099BEAD3;
IL2CPP_EXTERN_C String_t* _stringLiteralB76C73E87558DB3053EA9968982CB95A2747BB5C;
IL2CPP_EXTERN_C String_t* _stringLiteralC5CB235FDF341E57B3A3E3D289810AD3382B4E8B;
IL2CPP_EXTERN_C String_t* _stringLiteralC79460AD6EC7DAD42BBDE9AD92EA622183A394A4;
IL2CPP_EXTERN_C String_t* _stringLiteralCD98A9DD263E7154BD50DF9B4E84377553129BB9;
IL2CPP_EXTERN_C String_t* _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE;
IL2CPP_EXTERN_C String_t* _stringLiteralDD7461C99FE0AF610527A1F4273DBC4696AB5F17;
IL2CPP_EXTERN_C String_t* _stringLiteralE42E8BB820D4F7550A0F04619F4E15FDC56943B9;
IL2CPP_EXTERN_C String_t* _stringLiteralE5DD6B874CDA2CC3D2A0AABF1B63977712E9BD29;
IL2CPP_EXTERN_C String_t* _stringLiteralE64AFDAF0040B375D3BD8A9F4E8E8065E273167F;
IL2CPP_EXTERN_C String_t* _stringLiteralE91FE173F59B063D620A934CE1A010F2B114C1F3;
IL2CPP_EXTERN_C String_t* _stringLiteralED18E4874205E7C94B9863D6E8A3DCC112F2FA7D;
IL2CPP_EXTERN_C String_t* _stringLiteralEF420ABFDDBDA7B9EE665D85EF62E4A437554003;
IL2CPP_EXTERN_C String_t* _stringLiteralF0D7280F6BAA958E5EB0B700A4D9AE5A0E790473;
IL2CPP_EXTERN_C String_t* _stringLiteralF6E1A632EC5908721EE7EB7EFE8CF41B4C1C2E69;
IL2CPP_EXTERN_C String_t* _stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024;
IL2CPP_EXTERN_C String_t* _stringLiteralFCA440D9A4F7E2D10772A726D6FB427BAE128D34;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_m3240BECDA2A79E0895E7E9C13C662E65B8BE7709_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_mFFB7D3AB068FCD0390EC514E63DF3B73853D1DBC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Clear_m6A531EDA6DD13A0BEDC4C0ECB7EA76BE75606AF8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_m478627FE9A3A2D2A4FBC3454D861BDDAB5EC22AA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Remove_m0CC5596DB8DC24BD58FE8EF0891DC2B267526386_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_m165079D4EC60902B2BD406C3F965F0A3E93709CF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m7CDFF19F468AD909A00FDE97E4F7EE902D01ACD5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_m8EF7F2B5D88191D43F306EE9B70BF02239AEEE52_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Count_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m1AFC4338BF90E364F564717BDBFDD9D1217C2980_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m00F16F6DDED1338D9656B4EDE7F8AE2B2E2251E4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventHandler_1_Invoke_m134E3553E044A5F7F21E5C4178920C19082A2400_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventHandler_1_Invoke_m6A54CC2E93E4E7E38A8E2F4F63FE0DB149028B54_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventHandler_1__ctor_mB90F555001341586440DD1B03C6894D2E0730A26_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventHandler_1__ctor_mD4C0E5291CBD3101F3D364590F483445B7A27994_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisBoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_mAA495A9AAD3FB4D260B3E2B6DDBAFEC18DC49C3C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisField_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_m47142A7D5BBB4BD6A40C784E8C4F2DF0D366C708_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16ABC1D7B4E804984517841F8D26E9C769FAE710_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisNetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606_m08D4E52A3ED9AA766CA6959A49D06F448CF3820A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisPlayer_t5689617909B48F7640EA0892D85C92C13CC22C6F_m82212DD756C5F3A2B4844722FD1EE504E9AAD906_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_AddRange_m207006733154C20C7F72BFDD693CF79C1B8F4B5F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m552D9F94492155DFC4DE265624DA1F5E5EBB0319_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_mC92A676A3C9BFEFF1C88BADCAF85070A2DCABED4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_ForEach_m0ACE5773DCA4A37490AB5FC2FD86B532EE86251B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveAt_mC9BE02B104DF58BE5358E859E4DFE2C936C4EFAD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveRange_m9D8324CBE5E7209DD2BE2F04E5E5513C9D665110_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m42BD5721699395D9FC6B6D889CE5A29C4C2F1778_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mCCD54CB0212EBDA5809120252A5F13BB00288588_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m1C5E87EEF4B8F8CB985E6C4FD69D863ABAD79F14_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m52B8BE5835DE4DEBA361371438815F24116AD4B2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m1D3A0CB5B5817CB82948B52D881DD9C0A8F397C5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkManager_OnConnectionException_mFE229675BCBDD084EE00546034929AA6AEC8702F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkManager_OnSceneLoaded_mC352C4F66FD7C5E2CA0498FE9AF8C58ED2324C4B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkManager_OnServerRequest_m61A6A7F2EA27F355998FCCB66EA5B56AD54F3209_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkManager_OnServerResponse_mFDD6F10C6BD01FD06FD36F43D47CB21BDD5D732F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkManager_OnServerStatusUpdate_m7B66E753A550AEA74B48ECA487105D8A8F26A3E4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkManager_ThrowLogicError_mEBCF9F1A963227EC18FB69A35302146F5857A197_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Player_OnCellClicked_mA6B6B1109EE15590E94170CE40E8611B3517FDAF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Player_OnCellHovered_mAE7B9A82B03E6BB91C8D787CA947A5357BE3ECF5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Player_U3CInitU3Eb__27_0_m8D6827D28CAB82B07877F797AA211BB18E56B3BD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Player_U3CInitU3Eb__27_1_mCDD656E8EF18AA918150F421711C319ABB31A4F2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Player_U3CInitU3Eb__27_2_m3CB994442E169D8C9F468B27B339CB5FA2AAFFE8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Player_U3CInitU3Eb__27_3_mC230339C8E8E63933ECC2C90B23875FCC7F736D3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Program_OnConnectionException_m63BF7B0C6CDAFC553DA9EE126D7C14E802191003_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Program_OnServerRequest_m2D0A0E1642BA74E65B0BE9E897A5B50A64544415_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Program_OnServerResponse_mC8C36D4F38846D1C686CFE44D14D42C4B4B04054_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Program_OnServerStatusUpdate_mCB1063529DE2E3330D8915545C4859A935172FCC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TCPClient_CheckForTimeOut_m5055E62D1E62E7E9106D5137B54EADB7F759E451_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TCPClient_OnConnectionResetReceived_mA3AAF6AFD258C5B91AA92C435BEC4C304FFD63EB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TCPClient_OnNewNetMessageReceived_m9357397956DB11F8F05FF4DD8C3CB8E1534BDE99_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CHandleConnectionU3Ed__19_System_Collections_IEnumerator_Reset_mC513FF3D1591032404B4F3DA1967D46B0C6C2430_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass14_0_U3CSendShipsU3Eb__0_mA5DBBC94404A3AC08A32CA6247039C842132B91A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityAction_2__ctor_mE0417B33CF845A6B9324E67D296ADEA562B91DE0_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
struct GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// System.Object


// System.Collections.Generic.Dictionary`2<System.Byte,ProtocolClient.NetMessage>
struct  Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t4399F4D773327C2DDF5153EDA2A02F8C11126FF1* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_tA9884EA24191C42BBB260C4AB929522760348374 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_tB416558D480190C87B084819B8FAC806570080A7 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262, ___entries_1)); }
	inline EntryU5BU5D_t4399F4D773327C2DDF5153EDA2A02F8C11126FF1* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t4399F4D773327C2DDF5153EDA2A02F8C11126FF1** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t4399F4D773327C2DDF5153EDA2A02F8C11126FF1* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262, ___keys_7)); }
	inline KeyCollection_tA9884EA24191C42BBB260C4AB929522760348374 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_tA9884EA24191C42BBB260C4AB929522760348374 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_tA9884EA24191C42BBB260C4AB929522760348374 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262, ___values_8)); }
	inline ValueCollection_tB416558D480190C87B084819B8FAC806570080A7 * get_values_8() const { return ___values_8; }
	inline ValueCollection_tB416558D480190C87B084819B8FAC806570080A7 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_tB416558D480190C87B084819B8FAC806570080A7 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Byte>
struct  List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF, ____items_1)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get__items_1() const { return ____items_1; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF_StaticFields, ____emptyArray_5)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct  List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____items_1)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_StaticFields, ____emptyArray_5)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get__emptyArray_5() const { return ____emptyArray_5; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Ship>
struct  List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ShipU5BU5D_tCEAB3803C0678DE5410AA4A992B3CEBCC7436DE1* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835, ____items_1)); }
	inline ShipU5BU5D_tCEAB3803C0678DE5410AA4A992B3CEBCC7436DE1* get__items_1() const { return ____items_1; }
	inline ShipU5BU5D_tCEAB3803C0678DE5410AA4A992B3CEBCC7436DE1** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ShipU5BU5D_tCEAB3803C0678DE5410AA4A992B3CEBCC7436DE1* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ShipU5BU5D_tCEAB3803C0678DE5410AA4A992B3CEBCC7436DE1* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835_StaticFields, ____emptyArray_5)); }
	inline ShipU5BU5D_tCEAB3803C0678DE5410AA4A992B3CEBCC7436DE1* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ShipU5BU5D_tCEAB3803C0678DE5410AA4A992B3CEBCC7436DE1** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ShipU5BU5D_tCEAB3803C0678DE5410AA4A992B3CEBCC7436DE1* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// CellEventArgs
struct  CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6  : public RuntimeObject
{
public:
	// System.Boolean CellEventArgs::<Hovered>k__BackingField
	bool ___U3CHoveredU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CHoveredU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6, ___U3CHoveredU3Ek__BackingField_0)); }
	inline bool get_U3CHoveredU3Ek__BackingField_0() const { return ___U3CHoveredU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CHoveredU3Ek__BackingField_0() { return &___U3CHoveredU3Ek__BackingField_0; }
	inline void set_U3CHoveredU3Ek__BackingField_0(bool value)
	{
		___U3CHoveredU3Ek__BackingField_0 = value;
	}
};


// System.Text.Encoding
struct  Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E * ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 * ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D * ___decoderFallback_14;

public:
	inline static int32_t get_offset_of_m_codePage_9() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___m_codePage_9)); }
	inline int32_t get_m_codePage_9() const { return ___m_codePage_9; }
	inline int32_t* get_address_of_m_codePage_9() { return &___m_codePage_9; }
	inline void set_m_codePage_9(int32_t value)
	{
		___m_codePage_9 = value;
	}

	inline static int32_t get_offset_of_dataItem_10() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___dataItem_10)); }
	inline CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E * get_dataItem_10() const { return ___dataItem_10; }
	inline CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E ** get_address_of_dataItem_10() { return &___dataItem_10; }
	inline void set_dataItem_10(CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E * value)
	{
		___dataItem_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataItem_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_deserializedFromEverett_11() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___m_deserializedFromEverett_11)); }
	inline bool get_m_deserializedFromEverett_11() const { return ___m_deserializedFromEverett_11; }
	inline bool* get_address_of_m_deserializedFromEverett_11() { return &___m_deserializedFromEverett_11; }
	inline void set_m_deserializedFromEverett_11(bool value)
	{
		___m_deserializedFromEverett_11 = value;
	}

	inline static int32_t get_offset_of_m_isReadOnly_12() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___m_isReadOnly_12)); }
	inline bool get_m_isReadOnly_12() const { return ___m_isReadOnly_12; }
	inline bool* get_address_of_m_isReadOnly_12() { return &___m_isReadOnly_12; }
	inline void set_m_isReadOnly_12(bool value)
	{
		___m_isReadOnly_12 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_13() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___encoderFallback_13)); }
	inline EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 * get_encoderFallback_13() const { return ___encoderFallback_13; }
	inline EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 ** get_address_of_encoderFallback_13() { return &___encoderFallback_13; }
	inline void set_encoderFallback_13(EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 * value)
	{
		___encoderFallback_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encoderFallback_13), (void*)value);
	}

	inline static int32_t get_offset_of_decoderFallback_14() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___decoderFallback_14)); }
	inline DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D * get_decoderFallback_14() const { return ___decoderFallback_14; }
	inline DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D ** get_address_of_decoderFallback_14() { return &___decoderFallback_14; }
	inline void set_decoderFallback_14(DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D * value)
	{
		___decoderFallback_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___decoderFallback_14), (void*)value);
	}
};

struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_15;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultEncoding_0), (void*)value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unicodeEncoding_1), (void*)value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bigEndianUnicode_2), (void*)value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf7Encoding_3), (void*)value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf8Encoding_4), (void*)value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf32Encoding_5), (void*)value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___asciiEncoding_6), (void*)value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___latin1Encoding_7), (void*)value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___encodings_8)); }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encodings_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_15() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___s_InternalSyncObject_15)); }
	inline RuntimeObject * get_s_InternalSyncObject_15() const { return ___s_InternalSyncObject_15; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_15() { return &___s_InternalSyncObject_15; }
	inline void set_s_InternalSyncObject_15(RuntimeObject * value)
	{
		___s_InternalSyncObject_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InternalSyncObject_15), (void*)value);
	}
};


// ProtocolClient.IClient
struct  IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB  : public RuntimeObject
{
public:
	// System.EventHandler`1<System.Byte[]> ProtocolClient.IClient::ServerRequestEvent
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * ___ServerRequestEvent_0;
	// System.EventHandler`1<System.Byte[]> ProtocolClient.IClient::ServerResponseEvent
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * ___ServerResponseEvent_1;
	// System.EventHandler`1<System.Byte[]> ProtocolClient.IClient::ServerStatusUpdateEvent
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * ___ServerStatusUpdateEvent_2;
	// System.EventHandler`1<System.Exception> ProtocolClient.IClient::ConnectionException
	EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * ___ConnectionException_3;

public:
	inline static int32_t get_offset_of_ServerRequestEvent_0() { return static_cast<int32_t>(offsetof(IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB, ___ServerRequestEvent_0)); }
	inline EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * get_ServerRequestEvent_0() const { return ___ServerRequestEvent_0; }
	inline EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 ** get_address_of_ServerRequestEvent_0() { return &___ServerRequestEvent_0; }
	inline void set_ServerRequestEvent_0(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * value)
	{
		___ServerRequestEvent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ServerRequestEvent_0), (void*)value);
	}

	inline static int32_t get_offset_of_ServerResponseEvent_1() { return static_cast<int32_t>(offsetof(IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB, ___ServerResponseEvent_1)); }
	inline EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * get_ServerResponseEvent_1() const { return ___ServerResponseEvent_1; }
	inline EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 ** get_address_of_ServerResponseEvent_1() { return &___ServerResponseEvent_1; }
	inline void set_ServerResponseEvent_1(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * value)
	{
		___ServerResponseEvent_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ServerResponseEvent_1), (void*)value);
	}

	inline static int32_t get_offset_of_ServerStatusUpdateEvent_2() { return static_cast<int32_t>(offsetof(IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB, ___ServerStatusUpdateEvent_2)); }
	inline EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * get_ServerStatusUpdateEvent_2() const { return ___ServerStatusUpdateEvent_2; }
	inline EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 ** get_address_of_ServerStatusUpdateEvent_2() { return &___ServerStatusUpdateEvent_2; }
	inline void set_ServerStatusUpdateEvent_2(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * value)
	{
		___ServerStatusUpdateEvent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ServerStatusUpdateEvent_2), (void*)value);
	}

	inline static int32_t get_offset_of_ConnectionException_3() { return static_cast<int32_t>(offsetof(IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB, ___ConnectionException_3)); }
	inline EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * get_ConnectionException_3() const { return ___ConnectionException_3; }
	inline EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D ** get_address_of_ConnectionException_3() { return &___ConnectionException_3; }
	inline void set_ConnectionException_3(EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * value)
	{
		___ConnectionException_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ConnectionException_3), (void*)value);
	}
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// ProtocolClient.NetMessage
struct  NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670  : public RuntimeObject
{
public:
	// System.Byte ProtocolClient.NetMessage::messageId
	uint8_t ___messageId_0;
	// System.Byte ProtocolClient.NetMessage::clientId
	uint8_t ___clientId_1;
	// System.Byte ProtocolClient.NetMessage::type
	uint8_t ___type_2;
	// System.Byte ProtocolClient.NetMessage::crc
	uint8_t ___crc_3;
	// System.UInt16 ProtocolClient.NetMessage::payloadSize
	uint16_t ___payloadSize_4;
	// System.Byte[] ProtocolClient.NetMessage::payload
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___payload_5;

public:
	inline static int32_t get_offset_of_messageId_0() { return static_cast<int32_t>(offsetof(NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670, ___messageId_0)); }
	inline uint8_t get_messageId_0() const { return ___messageId_0; }
	inline uint8_t* get_address_of_messageId_0() { return &___messageId_0; }
	inline void set_messageId_0(uint8_t value)
	{
		___messageId_0 = value;
	}

	inline static int32_t get_offset_of_clientId_1() { return static_cast<int32_t>(offsetof(NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670, ___clientId_1)); }
	inline uint8_t get_clientId_1() const { return ___clientId_1; }
	inline uint8_t* get_address_of_clientId_1() { return &___clientId_1; }
	inline void set_clientId_1(uint8_t value)
	{
		___clientId_1 = value;
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670, ___type_2)); }
	inline uint8_t get_type_2() const { return ___type_2; }
	inline uint8_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(uint8_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_crc_3() { return static_cast<int32_t>(offsetof(NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670, ___crc_3)); }
	inline uint8_t get_crc_3() const { return ___crc_3; }
	inline uint8_t* get_address_of_crc_3() { return &___crc_3; }
	inline void set_crc_3(uint8_t value)
	{
		___crc_3 = value;
	}

	inline static int32_t get_offset_of_payloadSize_4() { return static_cast<int32_t>(offsetof(NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670, ___payloadSize_4)); }
	inline uint16_t get_payloadSize_4() const { return ___payloadSize_4; }
	inline uint16_t* get_address_of_payloadSize_4() { return &___payloadSize_4; }
	inline void set_payloadSize_4(uint16_t value)
	{
		___payloadSize_4 = value;
	}

	inline static int32_t get_offset_of_payload_5() { return static_cast<int32_t>(offsetof(NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670, ___payload_5)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_payload_5() const { return ___payload_5; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_payload_5() { return &___payload_5; }
	inline void set_payload_5(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___payload_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___payload_5), (void*)value);
	}
};


// ProtocolClient.NetReceiveBuffer
struct  NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Byte> ProtocolClient.NetReceiveBuffer::dynamicBuffer
	List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * ___dynamicBuffer_0;
	// System.Collections.Generic.Dictionary`2<System.Byte,ProtocolClient.NetMessage> ProtocolClient.NetReceiveBuffer::netMessagesPendingList
	Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * ___netMessagesPendingList_1;

public:
	inline static int32_t get_offset_of_dynamicBuffer_0() { return static_cast<int32_t>(offsetof(NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF, ___dynamicBuffer_0)); }
	inline List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * get_dynamicBuffer_0() const { return ___dynamicBuffer_0; }
	inline List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF ** get_address_of_dynamicBuffer_0() { return &___dynamicBuffer_0; }
	inline void set_dynamicBuffer_0(List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * value)
	{
		___dynamicBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dynamicBuffer_0), (void*)value);
	}

	inline static int32_t get_offset_of_netMessagesPendingList_1() { return static_cast<int32_t>(offsetof(NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF, ___netMessagesPendingList_1)); }
	inline Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * get_netMessagesPendingList_1() const { return ___netMessagesPendingList_1; }
	inline Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 ** get_address_of_netMessagesPendingList_1() { return &___netMessagesPendingList_1; }
	inline void set_netMessagesPendingList_1(Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * value)
	{
		___netMessagesPendingList_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___netMessagesPendingList_1), (void*)value);
	}
};


// ProtocolClient.NetSendBuffer
struct  NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Byte,ProtocolClient.NetMessage> ProtocolClient.NetSendBuffer::netMessagesPendingList
	Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * ___netMessagesPendingList_0;
	// ProtocolClient.TCPConnection ProtocolClient.NetSendBuffer::connection
	TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * ___connection_1;
	// System.Byte ProtocolClient.NetSendBuffer::currentAddMessageId
	uint8_t ___currentAddMessageId_2;
	// System.Byte ProtocolClient.NetSendBuffer::lastAddedMessageId
	uint8_t ___lastAddedMessageId_3;

public:
	inline static int32_t get_offset_of_netMessagesPendingList_0() { return static_cast<int32_t>(offsetof(NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3, ___netMessagesPendingList_0)); }
	inline Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * get_netMessagesPendingList_0() const { return ___netMessagesPendingList_0; }
	inline Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 ** get_address_of_netMessagesPendingList_0() { return &___netMessagesPendingList_0; }
	inline void set_netMessagesPendingList_0(Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * value)
	{
		___netMessagesPendingList_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___netMessagesPendingList_0), (void*)value);
	}

	inline static int32_t get_offset_of_connection_1() { return static_cast<int32_t>(offsetof(NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3, ___connection_1)); }
	inline TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * get_connection_1() const { return ___connection_1; }
	inline TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C ** get_address_of_connection_1() { return &___connection_1; }
	inline void set_connection_1(TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * value)
	{
		___connection_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___connection_1), (void*)value);
	}

	inline static int32_t get_offset_of_currentAddMessageId_2() { return static_cast<int32_t>(offsetof(NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3, ___currentAddMessageId_2)); }
	inline uint8_t get_currentAddMessageId_2() const { return ___currentAddMessageId_2; }
	inline uint8_t* get_address_of_currentAddMessageId_2() { return &___currentAddMessageId_2; }
	inline void set_currentAddMessageId_2(uint8_t value)
	{
		___currentAddMessageId_2 = value;
	}

	inline static int32_t get_offset_of_lastAddedMessageId_3() { return static_cast<int32_t>(offsetof(NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3, ___lastAddedMessageId_3)); }
	inline uint8_t get_lastAddedMessageId_3() const { return ___lastAddedMessageId_3; }
	inline uint8_t* get_address_of_lastAddedMessageId_3() { return &___lastAddedMessageId_3; }
	inline void set_lastAddedMessageId_3(uint8_t value)
	{
		___lastAddedMessageId_3 = value;
	}
};


// ProtocolClient.Program
struct  Program_t84DC950B444F8124814710510D84842D4A59581C  : public RuntimeObject
{
public:

public:
};

struct Program_t84DC950B444F8124814710510D84842D4A59581C_StaticFields
{
public:
	// ProtocolClient.IClient ProtocolClient.Program::client
	IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * ___client_0;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(Program_t84DC950B444F8124814710510D84842D4A59581C_StaticFields, ___client_0)); }
	inline IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * get_client_0() const { return ___client_0; }
	inline IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB ** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___client_0), (void*)value);
	}
};


// ProtocolClient.ProtocolTool
struct  ProtocolTool_tED22F8FFA8220E2102F7A8354575BD2DC20E24C6  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// ProtocolClient.TCPConnection
struct  TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C  : public RuntimeObject
{
public:
	// System.Net.Sockets.Socket ProtocolClient.TCPConnection::tcpSocket
	Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * ___tcpSocket_0;
	// System.Exception ProtocolClient.TCPConnection::lastException
	Exception_t * ___lastException_1;

public:
	inline static int32_t get_offset_of_tcpSocket_0() { return static_cast<int32_t>(offsetof(TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C, ___tcpSocket_0)); }
	inline Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * get_tcpSocket_0() const { return ___tcpSocket_0; }
	inline Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 ** get_address_of_tcpSocket_0() { return &___tcpSocket_0; }
	inline void set_tcpSocket_0(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * value)
	{
		___tcpSocket_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tcpSocket_0), (void*)value);
	}

	inline static int32_t get_offset_of_lastException_1() { return static_cast<int32_t>(offsetof(TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C, ___lastException_1)); }
	inline Exception_t * get_lastException_1() const { return ___lastException_1; }
	inline Exception_t ** get_address_of_lastException_1() { return &___lastException_1; }
	inline void set_lastException_1(Exception_t * value)
	{
		___lastException_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastException_1), (void*)value);
	}
};


// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct  YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// NetworkManager/<>c__DisplayClass14_0
struct  U3CU3Ec__DisplayClass14_0_tCCAB38C50CA018337D1FABDD31482528BBE3F1DF  : public RuntimeObject
{
public:
	// System.String NetworkManager/<>c__DisplayClass14_0::message
	String_t* ___message_0;

public:
	inline static int32_t get_offset_of_message_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_tCCAB38C50CA018337D1FABDD31482528BBE3F1DF, ___message_0)); }
	inline String_t* get_message_0() const { return ___message_0; }
	inline String_t** get_address_of_message_0() { return &___message_0; }
	inline void set_message_0(String_t* value)
	{
		___message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___message_0), (void*)value);
	}
};


// NetworkManager/<HandleConnection>d__19
struct  U3CHandleConnectionU3Ed__19_t2B2D10AA4C55A2999E347B3F7F12289D2BB5D833  : public RuntimeObject
{
public:
	// System.Int32 NetworkManager/<HandleConnection>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object NetworkManager/<HandleConnection>d__19::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// NetworkManager NetworkManager/<HandleConnection>d__19::<>4__this
	NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CHandleConnectionU3Ed__19_t2B2D10AA4C55A2999E347B3F7F12289D2BB5D833, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CHandleConnectionU3Ed__19_t2B2D10AA4C55A2999E347B3F7F12289D2BB5D833, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CHandleConnectionU3Ed__19_t2B2D10AA4C55A2999E347B3F7F12289D2BB5D833, ___U3CU3E4__this_2)); }
	inline NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>
struct  Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14, ___list_0)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_list_0() const { return ___list_0; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14, ___current_3)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_current_3() const { return ___current_3; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct  Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Char
struct  Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// UnityEngine.Color
struct  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.DateTime
struct  DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth365_29), (void*)value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth366_30), (void*)value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MinValue_31)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MaxValue_32)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MaxValue_32 = value;
	}
};


// System.Double
struct  Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int16
struct  Int16_tD0F031114106263BB459DA1F099FF9F42691295A 
{
public:
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int16_tD0F031114106263BB459DA1F099FF9F42691295A, ___m_value_0)); }
	inline int16_t get_m_value_0() const { return ___m_value_0; }
	inline int16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int16_t value)
	{
		___m_value_0 = value;
	}
};


// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Rect
struct  Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// UnityEngine.SceneManagement.Scene
struct  Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.UI.SpriteState
struct  SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_HighlightedSprite_0)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_PressedSprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_SelectedSprite_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_DisabledSprite_3)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_pinvoke
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_com
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};

// System.UInt16
struct  UInt16_t894EA9D4FB7C799B244E7BBF2DF0EEEDBC77A8BD 
{
public:
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt16_t894EA9D4FB7C799B244E7BBF2DF0EEEDBC77A8BD, ___m_value_0)); }
	inline uint16_t get_m_value_0() const { return ___m_value_0; }
	inline uint16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint16_t value)
	{
		___m_value_0 = value;
	}
};


// System.UInt32
struct  UInt32_tE60352A06233E4E69DD198BCC67142159F686B15 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Events.UnityEvent
struct  UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Vector2
struct  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector2Int
struct  Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 
{
public:
	// System.Int32 UnityEngine.Vector2Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector2Int::m_Y
	int32_t ___m_Y_1;

public:
	inline static int32_t get_offset_of_m_X_0() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9, ___m_X_0)); }
	inline int32_t get_m_X_0() const { return ___m_X_0; }
	inline int32_t* get_address_of_m_X_0() { return &___m_X_0; }
	inline void set_m_X_0(int32_t value)
	{
		___m_X_0 = value;
	}

	inline static int32_t get_offset_of_m_Y_1() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9, ___m_Y_1)); }
	inline int32_t get_m_Y_1() const { return ___m_Y_1; }
	inline int32_t* get_address_of_m_Y_1() { return &___m_Y_1; }
	inline void set_m_Y_1(int32_t value)
	{
		___m_Y_1 = value;
	}
};

struct Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields
{
public:
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Zero
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___s_Zero_2;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_One
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___s_One_3;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Up
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___s_Up_4;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Down
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___s_Down_5;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Left
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___s_Left_6;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Right
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___s_Right_7;

public:
	inline static int32_t get_offset_of_s_Zero_2() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields, ___s_Zero_2)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_s_Zero_2() const { return ___s_Zero_2; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_s_Zero_2() { return &___s_Zero_2; }
	inline void set_s_Zero_2(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___s_Zero_2 = value;
	}

	inline static int32_t get_offset_of_s_One_3() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields, ___s_One_3)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_s_One_3() const { return ___s_One_3; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_s_One_3() { return &___s_One_3; }
	inline void set_s_One_3(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___s_One_3 = value;
	}

	inline static int32_t get_offset_of_s_Up_4() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields, ___s_Up_4)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_s_Up_4() const { return ___s_Up_4; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_s_Up_4() { return &___s_Up_4; }
	inline void set_s_Up_4(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___s_Up_4 = value;
	}

	inline static int32_t get_offset_of_s_Down_5() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields, ___s_Down_5)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_s_Down_5() const { return ___s_Down_5; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_s_Down_5() { return &___s_Down_5; }
	inline void set_s_Down_5(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___s_Down_5 = value;
	}

	inline static int32_t get_offset_of_s_Left_6() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields, ___s_Left_6)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_s_Left_6() const { return ___s_Left_6; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_s_Left_6() { return &___s_Left_6; }
	inline void set_s_Left_6(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___s_Left_6 = value;
	}

	inline static int32_t get_offset_of_s_Right_7() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields, ___s_Right_7)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_s_Right_7() const { return ___s_Right_7; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_s_Right_7() { return &___s_Right_7; }
	inline void set_s_Right_7(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___s_Right_7 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct  Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.WaitForFixedUpdate
struct  WaitForFixedUpdate_t675FCE2AEFAC5C924A4020474C997FF2CDD3F4C5  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:

public:
};


// System.Net.Sockets.AddressFamily
struct  AddressFamily_tFCF4C888B95C069AB2D4720EC8C2E19453C28B33 
{
public:
	// System.Int32 System.Net.Sockets.AddressFamily::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AddressFamily_tFCF4C888B95C069AB2D4720EC8C2E19453C28B33, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.ColorBlock
struct  ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_NormalColor_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_HighlightedColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_PressedColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_SelectedColor_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_DisabledColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};

struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields
{
public:
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___defaultColorBlock_7;

public:
	inline static int32_t get_offset_of_defaultColorBlock_7() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields, ___defaultColorBlock_7)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_defaultColorBlock_7() const { return ___defaultColorBlock_7; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_defaultColorBlock_7() { return &___defaultColorBlock_7; }
	inline void set_defaultColorBlock_7(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___defaultColorBlock_7 = value;
	}
};


// ProtocolClient.ConnectionState
struct  ConnectionState_tA73DD1BD0F9F13CAFC3B2D0A93F48302076DC49A 
{
public:
	// System.Int32 ProtocolClient.ConnectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectionState_tA73DD1BD0F9F13CAFC3B2D0A93F48302076DC49A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.ConsoleKey
struct  ConsoleKey_t4708A928547D666CF632F6F46340F476155566D4 
{
public:
	// System.Int32 System.ConsoleKey::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConsoleKey_t4708A928547D666CF632F6F46340F476155566D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.ConsoleModifiers
struct  ConsoleModifiers_t8465A8BC80F4BDB8816D80AA7CBE483DC7D01EBE 
{
public:
	// System.Int32 System.ConsoleModifiers::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConsoleModifiers_t8465A8BC80F4BDB8816D80AA7CBE483DC7D01EBE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Coroutine
struct  Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.SceneManagement.LoadSceneMode
struct  LoadSceneMode_tF5060E18B71D524860ECBF7B9B56193B1907E5CC 
{
public:
	// System.Int32 UnityEngine.SceneManagement.LoadSceneMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoadSceneMode_tF5060E18B71D524860ECBF7B9B56193B1907E5CC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// ProtocolClient.NetMessageType
struct  NetMessageType_t7E1D999B882D529FD54B19DC295F55C42F507079 
{
public:
	// System.Int32 ProtocolClient.NetMessageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NetMessageType_t7E1D999B882D529FD54B19DC295F55C42F507079, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Globalization.NumberStyles
struct  NumberStyles_t379EFBF2535E1C950DEC8042704BB663BF636594 
{
public:
	// System.Int32 System.Globalization.NumberStyles::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NumberStyles_t379EFBF2535E1C950DEC8042704BB663BF636594, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.Net.Sockets.ProtocolType
struct  ProtocolType_t07C7AB65B583B132A2D99BC06BB2A909BDDCE156 
{
public:
	// System.Int32 System.Net.Sockets.ProtocolType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProtocolType_t07C7AB65B583B132A2D99BC06BB2A909BDDCE156, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// Ship
struct  Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E  : public RuntimeObject
{
public:
	// System.Boolean Ship::isHorizontal
	bool ___isHorizontal_1;
	// UnityEngine.Vector2Int Ship::startPos
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___startPos_2;
	// System.Int32 Ship::<size>k__BackingField
	int32_t ___U3CsizeU3Ek__BackingField_3;
	// UnityEngine.GameObject[] Ship::<cells>k__BackingField
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___U3CcellsU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_isHorizontal_1() { return static_cast<int32_t>(offsetof(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E, ___isHorizontal_1)); }
	inline bool get_isHorizontal_1() const { return ___isHorizontal_1; }
	inline bool* get_address_of_isHorizontal_1() { return &___isHorizontal_1; }
	inline void set_isHorizontal_1(bool value)
	{
		___isHorizontal_1 = value;
	}

	inline static int32_t get_offset_of_startPos_2() { return static_cast<int32_t>(offsetof(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E, ___startPos_2)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_startPos_2() const { return ___startPos_2; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_startPos_2() { return &___startPos_2; }
	inline void set_startPos_2(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___startPos_2 = value;
	}

	inline static int32_t get_offset_of_U3CsizeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E, ___U3CsizeU3Ek__BackingField_3)); }
	inline int32_t get_U3CsizeU3Ek__BackingField_3() const { return ___U3CsizeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CsizeU3Ek__BackingField_3() { return &___U3CsizeU3Ek__BackingField_3; }
	inline void set_U3CsizeU3Ek__BackingField_3(int32_t value)
	{
		___U3CsizeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CcellsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E, ___U3CcellsU3Ek__BackingField_4)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_U3CcellsU3Ek__BackingField_4() const { return ___U3CcellsU3Ek__BackingField_4; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_U3CcellsU3Ek__BackingField_4() { return &___U3CcellsU3Ek__BackingField_4; }
	inline void set_U3CcellsU3Ek__BackingField_4(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___U3CcellsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcellsU3Ek__BackingField_4), (void*)value);
	}
};

struct Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E_StaticFields
{
public:
	// UnityEngine.Color Ship::color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color_0;

public:
	inline static int32_t get_offset_of_color_0() { return static_cast<int32_t>(offsetof(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E_StaticFields, ___color_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_color_0() const { return ___color_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_color_0() { return &___color_0; }
	inline void set_color_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___color_0 = value;
	}
};


// System.Net.Sockets.SocketType
struct  SocketType_t234FBD298C115F92305ABC40D2E592FC535DFF94 
{
public:
	// System.Int32 System.Net.Sockets.SocketType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SocketType_t234FBD298C115F92305ABC40D2E592FC535DFF94, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// ProtocolClient.TCPClient
struct  TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E  : public IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB
{
public:
	// System.Byte ProtocolClient.TCPClient::clientId
	uint8_t ___clientId_4;
	// ProtocolClient.TCPConnection ProtocolClient.TCPClient::connection
	TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * ___connection_5;
	// ProtocolClient.NetReceiveBuffer ProtocolClient.TCPClient::recVBuffer
	NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * ___recVBuffer_6;
	// ProtocolClient.NetSendBuffer ProtocolClient.TCPClient::sendBuffer
	NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * ___sendBuffer_7;
	// System.String ProtocolClient.TCPClient::lastHost
	String_t* ___lastHost_8;
	// System.UInt16 ProtocolClient.TCPClient::lastPort
	uint16_t ___lastPort_9;
	// System.DateTime ProtocolClient.TCPClient::lastResponse
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___lastResponse_10;
	// System.UInt32 ProtocolClient.TCPClient::timeOutMilliseconds
	uint32_t ___timeOutMilliseconds_11;
	// System.Byte ProtocolClient.TCPClient::currentMessageId
	uint8_t ___currentMessageId_12;

public:
	inline static int32_t get_offset_of_clientId_4() { return static_cast<int32_t>(offsetof(TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E, ___clientId_4)); }
	inline uint8_t get_clientId_4() const { return ___clientId_4; }
	inline uint8_t* get_address_of_clientId_4() { return &___clientId_4; }
	inline void set_clientId_4(uint8_t value)
	{
		___clientId_4 = value;
	}

	inline static int32_t get_offset_of_connection_5() { return static_cast<int32_t>(offsetof(TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E, ___connection_5)); }
	inline TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * get_connection_5() const { return ___connection_5; }
	inline TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C ** get_address_of_connection_5() { return &___connection_5; }
	inline void set_connection_5(TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * value)
	{
		___connection_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___connection_5), (void*)value);
	}

	inline static int32_t get_offset_of_recVBuffer_6() { return static_cast<int32_t>(offsetof(TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E, ___recVBuffer_6)); }
	inline NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * get_recVBuffer_6() const { return ___recVBuffer_6; }
	inline NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF ** get_address_of_recVBuffer_6() { return &___recVBuffer_6; }
	inline void set_recVBuffer_6(NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * value)
	{
		___recVBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___recVBuffer_6), (void*)value);
	}

	inline static int32_t get_offset_of_sendBuffer_7() { return static_cast<int32_t>(offsetof(TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E, ___sendBuffer_7)); }
	inline NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * get_sendBuffer_7() const { return ___sendBuffer_7; }
	inline NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 ** get_address_of_sendBuffer_7() { return &___sendBuffer_7; }
	inline void set_sendBuffer_7(NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * value)
	{
		___sendBuffer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sendBuffer_7), (void*)value);
	}

	inline static int32_t get_offset_of_lastHost_8() { return static_cast<int32_t>(offsetof(TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E, ___lastHost_8)); }
	inline String_t* get_lastHost_8() const { return ___lastHost_8; }
	inline String_t** get_address_of_lastHost_8() { return &___lastHost_8; }
	inline void set_lastHost_8(String_t* value)
	{
		___lastHost_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastHost_8), (void*)value);
	}

	inline static int32_t get_offset_of_lastPort_9() { return static_cast<int32_t>(offsetof(TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E, ___lastPort_9)); }
	inline uint16_t get_lastPort_9() const { return ___lastPort_9; }
	inline uint16_t* get_address_of_lastPort_9() { return &___lastPort_9; }
	inline void set_lastPort_9(uint16_t value)
	{
		___lastPort_9 = value;
	}

	inline static int32_t get_offset_of_lastResponse_10() { return static_cast<int32_t>(offsetof(TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E, ___lastResponse_10)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_lastResponse_10() const { return ___lastResponse_10; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_lastResponse_10() { return &___lastResponse_10; }
	inline void set_lastResponse_10(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___lastResponse_10 = value;
	}

	inline static int32_t get_offset_of_timeOutMilliseconds_11() { return static_cast<int32_t>(offsetof(TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E, ___timeOutMilliseconds_11)); }
	inline uint32_t get_timeOutMilliseconds_11() const { return ___timeOutMilliseconds_11; }
	inline uint32_t* get_address_of_timeOutMilliseconds_11() { return &___timeOutMilliseconds_11; }
	inline void set_timeOutMilliseconds_11(uint32_t value)
	{
		___timeOutMilliseconds_11 = value;
	}

	inline static int32_t get_offset_of_currentMessageId_12() { return static_cast<int32_t>(offsetof(TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E, ___currentMessageId_12)); }
	inline uint8_t get_currentMessageId_12() const { return ___currentMessageId_12; }
	inline uint8_t* get_address_of_currentMessageId_12() { return &___currentMessageId_12; }
	inline void set_currentMessageId_12(uint8_t value)
	{
		___currentMessageId_12 = value;
	}
};


// System.TimeSpan
struct  TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___Zero_0;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___MaxValue_1;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___MinValue_2;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_5;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___Zero_0)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_Zero_0() const { return ___Zero_0; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___Zero_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___MaxValue_1)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_MaxValue_1() const { return ___MaxValue_1; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinValue_2() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___MinValue_2)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_MinValue_2() const { return ___MinValue_2; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_MinValue_2() { return &___MinValue_2; }
	inline void set_MinValue_2(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___MinValue_2 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_4() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ____legacyConfigChecked_4)); }
	inline bool get__legacyConfigChecked_4() const { return ____legacyConfigChecked_4; }
	inline bool* get_address_of__legacyConfigChecked_4() { return &____legacyConfigChecked_4; }
	inline void set__legacyConfigChecked_4(bool value)
	{
		____legacyConfigChecked_4 = value;
	}

	inline static int32_t get_offset_of__legacyMode_5() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ____legacyMode_5)); }
	inline bool get__legacyMode_5() const { return ____legacyMode_5; }
	inline bool* get_address_of__legacyMode_5() { return &____legacyMode_5; }
	inline void set__legacyMode_5(bool value)
	{
		____legacyMode_5 = value;
	}
};


// UnityEngine.UI.Button/ButtonClickedEvent
struct  ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F  : public UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4
{
public:

public:
};


// UnityEngine.UI.Image/FillMethod
struct  FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/Type
struct  Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Navigation/Mode
struct  Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable/Transition
struct  Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.ConsoleKeyInfo
struct  ConsoleKeyInfo_tDA8AC07839288484FCB167A81B4FBA92ECCEAF88 
{
public:
	// System.Char System.ConsoleKeyInfo::_keyChar
	Il2CppChar ____keyChar_0;
	// System.ConsoleKey System.ConsoleKeyInfo::_key
	int32_t ____key_1;
	// System.ConsoleModifiers System.ConsoleKeyInfo::_mods
	int32_t ____mods_2;

public:
	inline static int32_t get_offset_of__keyChar_0() { return static_cast<int32_t>(offsetof(ConsoleKeyInfo_tDA8AC07839288484FCB167A81B4FBA92ECCEAF88, ____keyChar_0)); }
	inline Il2CppChar get__keyChar_0() const { return ____keyChar_0; }
	inline Il2CppChar* get_address_of__keyChar_0() { return &____keyChar_0; }
	inline void set__keyChar_0(Il2CppChar value)
	{
		____keyChar_0 = value;
	}

	inline static int32_t get_offset_of__key_1() { return static_cast<int32_t>(offsetof(ConsoleKeyInfo_tDA8AC07839288484FCB167A81B4FBA92ECCEAF88, ____key_1)); }
	inline int32_t get__key_1() const { return ____key_1; }
	inline int32_t* get_address_of__key_1() { return &____key_1; }
	inline void set__key_1(int32_t value)
	{
		____key_1 = value;
	}

	inline static int32_t get_offset_of__mods_2() { return static_cast<int32_t>(offsetof(ConsoleKeyInfo_tDA8AC07839288484FCB167A81B4FBA92ECCEAF88, ____mods_2)); }
	inline int32_t get__mods_2() const { return ____mods_2; }
	inline int32_t* get_address_of__mods_2() { return &____mods_2; }
	inline void set__mods_2(int32_t value)
	{
		____mods_2 = value;
	}
};

// Native definition for P/Invoke marshalling of System.ConsoleKeyInfo
struct ConsoleKeyInfo_tDA8AC07839288484FCB167A81B4FBA92ECCEAF88_marshaled_pinvoke
{
	uint8_t ____keyChar_0;
	int32_t ____key_1;
	int32_t ____mods_2;
};
// Native definition for COM marshalling of System.ConsoleKeyInfo
struct ConsoleKeyInfo_tDA8AC07839288484FCB167A81B4FBA92ECCEAF88_marshaled_com
{
	uint8_t ____keyChar_0;
	int32_t ____key_1;
	int32_t ____mods_2;
};

// UnityEngine.GameObject
struct  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.UI.Navigation
struct  Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_WrapAround_1() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_WrapAround_1)); }
	inline bool get_m_WrapAround_1() const { return ___m_WrapAround_1; }
	inline bool* get_address_of_m_WrapAround_1() { return &___m_WrapAround_1; }
	inline void set_m_WrapAround_1(bool value)
	{
		___m_WrapAround_1 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_2() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnUp_2)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnUp_2() const { return ___m_SelectOnUp_2; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnUp_2() { return &___m_SelectOnUp_2; }
	inline void set_m_SelectOnUp_2(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnUp_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_3() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnDown_3)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnDown_3() const { return ___m_SelectOnDown_3; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnDown_3() { return &___m_SelectOnDown_3; }
	inline void set_m_SelectOnDown_3(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnDown_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_4() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnLeft_4)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnLeft_4() const { return ___m_SelectOnLeft_4; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnLeft_4() { return &___m_SelectOnLeft_4; }
	inline void set_m_SelectOnLeft_4(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnLeft_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_5() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnRight_5)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnRight_5() const { return ___m_SelectOnRight_5; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnRight_5() { return &___m_SelectOnRight_5; }
	inline void set_m_SelectOnRight_5(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnRight_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_5), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};

// System.Net.Sockets.Socket
struct  Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09  : public RuntimeObject
{
public:
	// System.Boolean System.Net.Sockets.Socket::is_closed
	bool ___is_closed_6;
	// System.Boolean System.Net.Sockets.Socket::is_listening
	bool ___is_listening_7;
	// System.Boolean System.Net.Sockets.Socket::useOverlappedIO
	bool ___useOverlappedIO_8;
	// System.Int32 System.Net.Sockets.Socket::linger_timeout
	int32_t ___linger_timeout_9;
	// System.Net.Sockets.AddressFamily System.Net.Sockets.Socket::addressFamily
	int32_t ___addressFamily_10;
	// System.Net.Sockets.SocketType System.Net.Sockets.Socket::socketType
	int32_t ___socketType_11;
	// System.Net.Sockets.ProtocolType System.Net.Sockets.Socket::protocolType
	int32_t ___protocolType_12;
	// System.Net.Sockets.SafeSocketHandle System.Net.Sockets.Socket::m_Handle
	SafeSocketHandle_t5050671179FB886DA1763A0E4EFB3FCD072363C9 * ___m_Handle_13;
	// System.Net.EndPoint System.Net.Sockets.Socket::seed_endpoint
	EndPoint_t18D4AE8D03090A2B262136E59F95CE61418C34DA * ___seed_endpoint_14;
	// System.Threading.SemaphoreSlim System.Net.Sockets.Socket::ReadSem
	SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * ___ReadSem_15;
	// System.Threading.SemaphoreSlim System.Net.Sockets.Socket::WriteSem
	SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * ___WriteSem_16;
	// System.Boolean System.Net.Sockets.Socket::is_blocking
	bool ___is_blocking_17;
	// System.Boolean System.Net.Sockets.Socket::is_bound
	bool ___is_bound_18;
	// System.Boolean System.Net.Sockets.Socket::is_connected
	bool ___is_connected_19;
	// System.Int32 System.Net.Sockets.Socket::m_IntCleanedUp
	int32_t ___m_IntCleanedUp_20;
	// System.Boolean System.Net.Sockets.Socket::connect_in_progress
	bool ___connect_in_progress_21;
	// System.Int32 System.Net.Sockets.Socket::ID
	int32_t ___ID_22;

public:
	inline static int32_t get_offset_of_is_closed_6() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___is_closed_6)); }
	inline bool get_is_closed_6() const { return ___is_closed_6; }
	inline bool* get_address_of_is_closed_6() { return &___is_closed_6; }
	inline void set_is_closed_6(bool value)
	{
		___is_closed_6 = value;
	}

	inline static int32_t get_offset_of_is_listening_7() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___is_listening_7)); }
	inline bool get_is_listening_7() const { return ___is_listening_7; }
	inline bool* get_address_of_is_listening_7() { return &___is_listening_7; }
	inline void set_is_listening_7(bool value)
	{
		___is_listening_7 = value;
	}

	inline static int32_t get_offset_of_useOverlappedIO_8() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___useOverlappedIO_8)); }
	inline bool get_useOverlappedIO_8() const { return ___useOverlappedIO_8; }
	inline bool* get_address_of_useOverlappedIO_8() { return &___useOverlappedIO_8; }
	inline void set_useOverlappedIO_8(bool value)
	{
		___useOverlappedIO_8 = value;
	}

	inline static int32_t get_offset_of_linger_timeout_9() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___linger_timeout_9)); }
	inline int32_t get_linger_timeout_9() const { return ___linger_timeout_9; }
	inline int32_t* get_address_of_linger_timeout_9() { return &___linger_timeout_9; }
	inline void set_linger_timeout_9(int32_t value)
	{
		___linger_timeout_9 = value;
	}

	inline static int32_t get_offset_of_addressFamily_10() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___addressFamily_10)); }
	inline int32_t get_addressFamily_10() const { return ___addressFamily_10; }
	inline int32_t* get_address_of_addressFamily_10() { return &___addressFamily_10; }
	inline void set_addressFamily_10(int32_t value)
	{
		___addressFamily_10 = value;
	}

	inline static int32_t get_offset_of_socketType_11() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___socketType_11)); }
	inline int32_t get_socketType_11() const { return ___socketType_11; }
	inline int32_t* get_address_of_socketType_11() { return &___socketType_11; }
	inline void set_socketType_11(int32_t value)
	{
		___socketType_11 = value;
	}

	inline static int32_t get_offset_of_protocolType_12() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___protocolType_12)); }
	inline int32_t get_protocolType_12() const { return ___protocolType_12; }
	inline int32_t* get_address_of_protocolType_12() { return &___protocolType_12; }
	inline void set_protocolType_12(int32_t value)
	{
		___protocolType_12 = value;
	}

	inline static int32_t get_offset_of_m_Handle_13() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___m_Handle_13)); }
	inline SafeSocketHandle_t5050671179FB886DA1763A0E4EFB3FCD072363C9 * get_m_Handle_13() const { return ___m_Handle_13; }
	inline SafeSocketHandle_t5050671179FB886DA1763A0E4EFB3FCD072363C9 ** get_address_of_m_Handle_13() { return &___m_Handle_13; }
	inline void set_m_Handle_13(SafeSocketHandle_t5050671179FB886DA1763A0E4EFB3FCD072363C9 * value)
	{
		___m_Handle_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Handle_13), (void*)value);
	}

	inline static int32_t get_offset_of_seed_endpoint_14() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___seed_endpoint_14)); }
	inline EndPoint_t18D4AE8D03090A2B262136E59F95CE61418C34DA * get_seed_endpoint_14() const { return ___seed_endpoint_14; }
	inline EndPoint_t18D4AE8D03090A2B262136E59F95CE61418C34DA ** get_address_of_seed_endpoint_14() { return &___seed_endpoint_14; }
	inline void set_seed_endpoint_14(EndPoint_t18D4AE8D03090A2B262136E59F95CE61418C34DA * value)
	{
		___seed_endpoint_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___seed_endpoint_14), (void*)value);
	}

	inline static int32_t get_offset_of_ReadSem_15() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___ReadSem_15)); }
	inline SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * get_ReadSem_15() const { return ___ReadSem_15; }
	inline SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 ** get_address_of_ReadSem_15() { return &___ReadSem_15; }
	inline void set_ReadSem_15(SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * value)
	{
		___ReadSem_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ReadSem_15), (void*)value);
	}

	inline static int32_t get_offset_of_WriteSem_16() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___WriteSem_16)); }
	inline SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * get_WriteSem_16() const { return ___WriteSem_16; }
	inline SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 ** get_address_of_WriteSem_16() { return &___WriteSem_16; }
	inline void set_WriteSem_16(SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * value)
	{
		___WriteSem_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WriteSem_16), (void*)value);
	}

	inline static int32_t get_offset_of_is_blocking_17() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___is_blocking_17)); }
	inline bool get_is_blocking_17() const { return ___is_blocking_17; }
	inline bool* get_address_of_is_blocking_17() { return &___is_blocking_17; }
	inline void set_is_blocking_17(bool value)
	{
		___is_blocking_17 = value;
	}

	inline static int32_t get_offset_of_is_bound_18() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___is_bound_18)); }
	inline bool get_is_bound_18() const { return ___is_bound_18; }
	inline bool* get_address_of_is_bound_18() { return &___is_bound_18; }
	inline void set_is_bound_18(bool value)
	{
		___is_bound_18 = value;
	}

	inline static int32_t get_offset_of_is_connected_19() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___is_connected_19)); }
	inline bool get_is_connected_19() const { return ___is_connected_19; }
	inline bool* get_address_of_is_connected_19() { return &___is_connected_19; }
	inline void set_is_connected_19(bool value)
	{
		___is_connected_19 = value;
	}

	inline static int32_t get_offset_of_m_IntCleanedUp_20() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___m_IntCleanedUp_20)); }
	inline int32_t get_m_IntCleanedUp_20() const { return ___m_IntCleanedUp_20; }
	inline int32_t* get_address_of_m_IntCleanedUp_20() { return &___m_IntCleanedUp_20; }
	inline void set_m_IntCleanedUp_20(int32_t value)
	{
		___m_IntCleanedUp_20 = value;
	}

	inline static int32_t get_offset_of_connect_in_progress_21() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___connect_in_progress_21)); }
	inline bool get_connect_in_progress_21() const { return ___connect_in_progress_21; }
	inline bool* get_address_of_connect_in_progress_21() { return &___connect_in_progress_21; }
	inline void set_connect_in_progress_21(bool value)
	{
		___connect_in_progress_21 = value;
	}

	inline static int32_t get_offset_of_ID_22() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___ID_22)); }
	inline int32_t get_ID_22() const { return ___ID_22; }
	inline int32_t* get_address_of_ID_22() { return &___ID_22; }
	inline void set_ID_22(int32_t value)
	{
		___ID_22 = value;
	}
};

struct Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields
{
public:
	// System.Object System.Net.Sockets.Socket::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_0;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.Sockets.Socket::s_SupportsIPv4
	bool ___s_SupportsIPv4_1;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.Sockets.Socket::s_SupportsIPv6
	bool ___s_SupportsIPv6_2;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.Sockets.Socket::s_OSSupportsIPv6
	bool ___s_OSSupportsIPv6_3;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.Sockets.Socket::s_Initialized
	bool ___s_Initialized_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.Sockets.Socket::s_LoggingEnabled
	bool ___s_LoggingEnabled_5;
	// System.AsyncCallback System.Net.Sockets.Socket::AcceptAsyncCallback
	AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___AcceptAsyncCallback_23;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginAcceptCallback
	IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * ___BeginAcceptCallback_24;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginAcceptReceiveCallback
	IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * ___BeginAcceptReceiveCallback_25;
	// System.AsyncCallback System.Net.Sockets.Socket::ConnectAsyncCallback
	AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___ConnectAsyncCallback_26;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginConnectCallback
	IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * ___BeginConnectCallback_27;
	// System.AsyncCallback System.Net.Sockets.Socket::DisconnectAsyncCallback
	AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___DisconnectAsyncCallback_28;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginDisconnectCallback
	IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * ___BeginDisconnectCallback_29;
	// System.AsyncCallback System.Net.Sockets.Socket::ReceiveAsyncCallback
	AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___ReceiveAsyncCallback_30;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginReceiveCallback
	IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * ___BeginReceiveCallback_31;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginReceiveGenericCallback
	IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * ___BeginReceiveGenericCallback_32;
	// System.AsyncCallback System.Net.Sockets.Socket::ReceiveFromAsyncCallback
	AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___ReceiveFromAsyncCallback_33;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginReceiveFromCallback
	IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * ___BeginReceiveFromCallback_34;
	// System.AsyncCallback System.Net.Sockets.Socket::SendAsyncCallback
	AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___SendAsyncCallback_35;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginSendGenericCallback
	IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * ___BeginSendGenericCallback_36;
	// System.AsyncCallback System.Net.Sockets.Socket::SendToAsyncCallback
	AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___SendToAsyncCallback_37;

public:
	inline static int32_t get_offset_of_s_InternalSyncObject_0() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___s_InternalSyncObject_0)); }
	inline RuntimeObject * get_s_InternalSyncObject_0() const { return ___s_InternalSyncObject_0; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_0() { return &___s_InternalSyncObject_0; }
	inline void set_s_InternalSyncObject_0(RuntimeObject * value)
	{
		___s_InternalSyncObject_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InternalSyncObject_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_SupportsIPv4_1() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___s_SupportsIPv4_1)); }
	inline bool get_s_SupportsIPv4_1() const { return ___s_SupportsIPv4_1; }
	inline bool* get_address_of_s_SupportsIPv4_1() { return &___s_SupportsIPv4_1; }
	inline void set_s_SupportsIPv4_1(bool value)
	{
		___s_SupportsIPv4_1 = value;
	}

	inline static int32_t get_offset_of_s_SupportsIPv6_2() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___s_SupportsIPv6_2)); }
	inline bool get_s_SupportsIPv6_2() const { return ___s_SupportsIPv6_2; }
	inline bool* get_address_of_s_SupportsIPv6_2() { return &___s_SupportsIPv6_2; }
	inline void set_s_SupportsIPv6_2(bool value)
	{
		___s_SupportsIPv6_2 = value;
	}

	inline static int32_t get_offset_of_s_OSSupportsIPv6_3() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___s_OSSupportsIPv6_3)); }
	inline bool get_s_OSSupportsIPv6_3() const { return ___s_OSSupportsIPv6_3; }
	inline bool* get_address_of_s_OSSupportsIPv6_3() { return &___s_OSSupportsIPv6_3; }
	inline void set_s_OSSupportsIPv6_3(bool value)
	{
		___s_OSSupportsIPv6_3 = value;
	}

	inline static int32_t get_offset_of_s_Initialized_4() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___s_Initialized_4)); }
	inline bool get_s_Initialized_4() const { return ___s_Initialized_4; }
	inline bool* get_address_of_s_Initialized_4() { return &___s_Initialized_4; }
	inline void set_s_Initialized_4(bool value)
	{
		___s_Initialized_4 = value;
	}

	inline static int32_t get_offset_of_s_LoggingEnabled_5() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___s_LoggingEnabled_5)); }
	inline bool get_s_LoggingEnabled_5() const { return ___s_LoggingEnabled_5; }
	inline bool* get_address_of_s_LoggingEnabled_5() { return &___s_LoggingEnabled_5; }
	inline void set_s_LoggingEnabled_5(bool value)
	{
		___s_LoggingEnabled_5 = value;
	}

	inline static int32_t get_offset_of_AcceptAsyncCallback_23() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___AcceptAsyncCallback_23)); }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * get_AcceptAsyncCallback_23() const { return ___AcceptAsyncCallback_23; }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA ** get_address_of_AcceptAsyncCallback_23() { return &___AcceptAsyncCallback_23; }
	inline void set_AcceptAsyncCallback_23(AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * value)
	{
		___AcceptAsyncCallback_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AcceptAsyncCallback_23), (void*)value);
	}

	inline static int32_t get_offset_of_BeginAcceptCallback_24() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___BeginAcceptCallback_24)); }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * get_BeginAcceptCallback_24() const { return ___BeginAcceptCallback_24; }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E ** get_address_of_BeginAcceptCallback_24() { return &___BeginAcceptCallback_24; }
	inline void set_BeginAcceptCallback_24(IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * value)
	{
		___BeginAcceptCallback_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BeginAcceptCallback_24), (void*)value);
	}

	inline static int32_t get_offset_of_BeginAcceptReceiveCallback_25() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___BeginAcceptReceiveCallback_25)); }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * get_BeginAcceptReceiveCallback_25() const { return ___BeginAcceptReceiveCallback_25; }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E ** get_address_of_BeginAcceptReceiveCallback_25() { return &___BeginAcceptReceiveCallback_25; }
	inline void set_BeginAcceptReceiveCallback_25(IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * value)
	{
		___BeginAcceptReceiveCallback_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BeginAcceptReceiveCallback_25), (void*)value);
	}

	inline static int32_t get_offset_of_ConnectAsyncCallback_26() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___ConnectAsyncCallback_26)); }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * get_ConnectAsyncCallback_26() const { return ___ConnectAsyncCallback_26; }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA ** get_address_of_ConnectAsyncCallback_26() { return &___ConnectAsyncCallback_26; }
	inline void set_ConnectAsyncCallback_26(AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * value)
	{
		___ConnectAsyncCallback_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ConnectAsyncCallback_26), (void*)value);
	}

	inline static int32_t get_offset_of_BeginConnectCallback_27() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___BeginConnectCallback_27)); }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * get_BeginConnectCallback_27() const { return ___BeginConnectCallback_27; }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E ** get_address_of_BeginConnectCallback_27() { return &___BeginConnectCallback_27; }
	inline void set_BeginConnectCallback_27(IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * value)
	{
		___BeginConnectCallback_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BeginConnectCallback_27), (void*)value);
	}

	inline static int32_t get_offset_of_DisconnectAsyncCallback_28() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___DisconnectAsyncCallback_28)); }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * get_DisconnectAsyncCallback_28() const { return ___DisconnectAsyncCallback_28; }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA ** get_address_of_DisconnectAsyncCallback_28() { return &___DisconnectAsyncCallback_28; }
	inline void set_DisconnectAsyncCallback_28(AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * value)
	{
		___DisconnectAsyncCallback_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DisconnectAsyncCallback_28), (void*)value);
	}

	inline static int32_t get_offset_of_BeginDisconnectCallback_29() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___BeginDisconnectCallback_29)); }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * get_BeginDisconnectCallback_29() const { return ___BeginDisconnectCallback_29; }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E ** get_address_of_BeginDisconnectCallback_29() { return &___BeginDisconnectCallback_29; }
	inline void set_BeginDisconnectCallback_29(IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * value)
	{
		___BeginDisconnectCallback_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BeginDisconnectCallback_29), (void*)value);
	}

	inline static int32_t get_offset_of_ReceiveAsyncCallback_30() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___ReceiveAsyncCallback_30)); }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * get_ReceiveAsyncCallback_30() const { return ___ReceiveAsyncCallback_30; }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA ** get_address_of_ReceiveAsyncCallback_30() { return &___ReceiveAsyncCallback_30; }
	inline void set_ReceiveAsyncCallback_30(AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * value)
	{
		___ReceiveAsyncCallback_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ReceiveAsyncCallback_30), (void*)value);
	}

	inline static int32_t get_offset_of_BeginReceiveCallback_31() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___BeginReceiveCallback_31)); }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * get_BeginReceiveCallback_31() const { return ___BeginReceiveCallback_31; }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E ** get_address_of_BeginReceiveCallback_31() { return &___BeginReceiveCallback_31; }
	inline void set_BeginReceiveCallback_31(IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * value)
	{
		___BeginReceiveCallback_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BeginReceiveCallback_31), (void*)value);
	}

	inline static int32_t get_offset_of_BeginReceiveGenericCallback_32() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___BeginReceiveGenericCallback_32)); }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * get_BeginReceiveGenericCallback_32() const { return ___BeginReceiveGenericCallback_32; }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E ** get_address_of_BeginReceiveGenericCallback_32() { return &___BeginReceiveGenericCallback_32; }
	inline void set_BeginReceiveGenericCallback_32(IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * value)
	{
		___BeginReceiveGenericCallback_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BeginReceiveGenericCallback_32), (void*)value);
	}

	inline static int32_t get_offset_of_ReceiveFromAsyncCallback_33() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___ReceiveFromAsyncCallback_33)); }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * get_ReceiveFromAsyncCallback_33() const { return ___ReceiveFromAsyncCallback_33; }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA ** get_address_of_ReceiveFromAsyncCallback_33() { return &___ReceiveFromAsyncCallback_33; }
	inline void set_ReceiveFromAsyncCallback_33(AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * value)
	{
		___ReceiveFromAsyncCallback_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ReceiveFromAsyncCallback_33), (void*)value);
	}

	inline static int32_t get_offset_of_BeginReceiveFromCallback_34() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___BeginReceiveFromCallback_34)); }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * get_BeginReceiveFromCallback_34() const { return ___BeginReceiveFromCallback_34; }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E ** get_address_of_BeginReceiveFromCallback_34() { return &___BeginReceiveFromCallback_34; }
	inline void set_BeginReceiveFromCallback_34(IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * value)
	{
		___BeginReceiveFromCallback_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BeginReceiveFromCallback_34), (void*)value);
	}

	inline static int32_t get_offset_of_SendAsyncCallback_35() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___SendAsyncCallback_35)); }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * get_SendAsyncCallback_35() const { return ___SendAsyncCallback_35; }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA ** get_address_of_SendAsyncCallback_35() { return &___SendAsyncCallback_35; }
	inline void set_SendAsyncCallback_35(AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * value)
	{
		___SendAsyncCallback_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SendAsyncCallback_35), (void*)value);
	}

	inline static int32_t get_offset_of_BeginSendGenericCallback_36() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___BeginSendGenericCallback_36)); }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * get_BeginSendGenericCallback_36() const { return ___BeginSendGenericCallback_36; }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E ** get_address_of_BeginSendGenericCallback_36() { return &___BeginSendGenericCallback_36; }
	inline void set_BeginSendGenericCallback_36(IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * value)
	{
		___BeginSendGenericCallback_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BeginSendGenericCallback_36), (void*)value);
	}

	inline static int32_t get_offset_of_SendToAsyncCallback_37() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___SendToAsyncCallback_37)); }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * get_SendToAsyncCallback_37() const { return ___SendToAsyncCallback_37; }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA ** get_address_of_SendToAsyncCallback_37() { return &___SendToAsyncCallback_37; }
	inline void set_SendToAsyncCallback_37(AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * value)
	{
		___SendToAsyncCallback_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SendToAsyncCallback_37), (void*)value);
	}
};


// System.SystemException
struct  SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// UnityEngine.Texture
struct  Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.Action`1<Ship>
struct  Action_1_tCCB45E12289F4906FABB0046E40D54208C8DAB5C  : public MulticastDelegate_t
{
public:

public:
};


// System.EventHandler`1<System.Byte[]>
struct  EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8  : public MulticastDelegate_t
{
public:

public:
};


// System.EventHandler`1<System.Exception>
struct  EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct  UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906  : public MulticastDelegate_t
{
public:

public:
};


// System.AsyncCallback
struct  AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// System.IndexOutOfRangeException
struct  IndexOutOfRangeException_tDC9EF7A0346CE39E54DA1083F07BE6DFC3CE2EDD  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// System.NotImplementedException
struct  NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// System.NotSupportedException
struct  NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.Texture2D
struct  Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF  : public Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Events.UnityAction
struct  UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099  : public MulticastDelegate_t
{
public:

public:
};


// Cell/CellClickedEventHandler
struct  CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437  : public MulticastDelegate_t
{
public:

public:
};


// Cell/CellHoverEventHandler
struct  CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Collider2D
struct  Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.RectTransform
struct  RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072  : public Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1
{
public:

public:
};

struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reapplyDrivenProperties_4), (void*)value);
	}
};


// UnityEngine.BoxCollider2D
struct  BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9  : public Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722
{
public:

public:
};


// Cell
struct  Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 Cell::xIndex
	int32_t ___xIndex_4;
	// System.Int32 Cell::yIndex
	int32_t ___yIndex_5;
	// System.Boolean Cell::isShip
	bool ___isShip_6;
	// System.Boolean Cell::isShot
	bool ___isShot_7;
	// System.Boolean Cell::isEnemy
	bool ___isEnemy_8;

public:
	inline static int32_t get_offset_of_xIndex_4() { return static_cast<int32_t>(offsetof(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149, ___xIndex_4)); }
	inline int32_t get_xIndex_4() const { return ___xIndex_4; }
	inline int32_t* get_address_of_xIndex_4() { return &___xIndex_4; }
	inline void set_xIndex_4(int32_t value)
	{
		___xIndex_4 = value;
	}

	inline static int32_t get_offset_of_yIndex_5() { return static_cast<int32_t>(offsetof(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149, ___yIndex_5)); }
	inline int32_t get_yIndex_5() const { return ___yIndex_5; }
	inline int32_t* get_address_of_yIndex_5() { return &___yIndex_5; }
	inline void set_yIndex_5(int32_t value)
	{
		___yIndex_5 = value;
	}

	inline static int32_t get_offset_of_isShip_6() { return static_cast<int32_t>(offsetof(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149, ___isShip_6)); }
	inline bool get_isShip_6() const { return ___isShip_6; }
	inline bool* get_address_of_isShip_6() { return &___isShip_6; }
	inline void set_isShip_6(bool value)
	{
		___isShip_6 = value;
	}

	inline static int32_t get_offset_of_isShot_7() { return static_cast<int32_t>(offsetof(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149, ___isShot_7)); }
	inline bool get_isShot_7() const { return ___isShot_7; }
	inline bool* get_address_of_isShot_7() { return &___isShot_7; }
	inline void set_isShot_7(bool value)
	{
		___isShot_7 = value;
	}

	inline static int32_t get_offset_of_isEnemy_8() { return static_cast<int32_t>(offsetof(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149, ___isEnemy_8)); }
	inline bool get_isEnemy_8() const { return ___isEnemy_8; }
	inline bool* get_address_of_isEnemy_8() { return &___isEnemy_8; }
	inline void set_isEnemy_8(bool value)
	{
		___isEnemy_8 = value;
	}
};

struct Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields
{
public:
	// UnityEngine.Color Cell::standardColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___standardColor_9;
	// UnityEngine.Color Cell::markedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___markedColor_10;
	// Cell/CellHoverEventHandler Cell::CellHoverEvent
	CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * ___CellHoverEvent_11;
	// Cell/CellClickedEventHandler Cell::CellClickedEvent
	CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * ___CellClickedEvent_12;

public:
	inline static int32_t get_offset_of_standardColor_9() { return static_cast<int32_t>(offsetof(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields, ___standardColor_9)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_standardColor_9() const { return ___standardColor_9; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_standardColor_9() { return &___standardColor_9; }
	inline void set_standardColor_9(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___standardColor_9 = value;
	}

	inline static int32_t get_offset_of_markedColor_10() { return static_cast<int32_t>(offsetof(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields, ___markedColor_10)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_markedColor_10() const { return ___markedColor_10; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_markedColor_10() { return &___markedColor_10; }
	inline void set_markedColor_10(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___markedColor_10 = value;
	}

	inline static int32_t get_offset_of_CellHoverEvent_11() { return static_cast<int32_t>(offsetof(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields, ___CellHoverEvent_11)); }
	inline CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * get_CellHoverEvent_11() const { return ___CellHoverEvent_11; }
	inline CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A ** get_address_of_CellHoverEvent_11() { return &___CellHoverEvent_11; }
	inline void set_CellHoverEvent_11(CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * value)
	{
		___CellHoverEvent_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CellHoverEvent_11), (void*)value);
	}

	inline static int32_t get_offset_of_CellClickedEvent_12() { return static_cast<int32_t>(offsetof(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields, ___CellClickedEvent_12)); }
	inline CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * get_CellClickedEvent_12() const { return ___CellClickedEvent_12; }
	inline CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 ** get_address_of_CellClickedEvent_12() { return &___CellClickedEvent_12; }
	inline void set_CellClickedEvent_12(CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * value)
	{
		___CellClickedEvent_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CellClickedEvent_12), (void*)value);
	}
};


// Field
struct  Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 Field::cellwidth
	int32_t ___cellwidth_5;
	// System.Single Field::totalcellwidth
	float ___totalcellwidth_6;
	// System.Single Field::totalcellheight
	float ___totalcellheight_7;
	// System.Single Field::posx0
	float ___posx0_8;
	// System.Single Field::posy0
	float ___posy0_9;
	// UnityEngine.GameObject[0...,0...] Field::cells
	GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* ___cells_10;

public:
	inline static int32_t get_offset_of_cellwidth_5() { return static_cast<int32_t>(offsetof(Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153, ___cellwidth_5)); }
	inline int32_t get_cellwidth_5() const { return ___cellwidth_5; }
	inline int32_t* get_address_of_cellwidth_5() { return &___cellwidth_5; }
	inline void set_cellwidth_5(int32_t value)
	{
		___cellwidth_5 = value;
	}

	inline static int32_t get_offset_of_totalcellwidth_6() { return static_cast<int32_t>(offsetof(Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153, ___totalcellwidth_6)); }
	inline float get_totalcellwidth_6() const { return ___totalcellwidth_6; }
	inline float* get_address_of_totalcellwidth_6() { return &___totalcellwidth_6; }
	inline void set_totalcellwidth_6(float value)
	{
		___totalcellwidth_6 = value;
	}

	inline static int32_t get_offset_of_totalcellheight_7() { return static_cast<int32_t>(offsetof(Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153, ___totalcellheight_7)); }
	inline float get_totalcellheight_7() const { return ___totalcellheight_7; }
	inline float* get_address_of_totalcellheight_7() { return &___totalcellheight_7; }
	inline void set_totalcellheight_7(float value)
	{
		___totalcellheight_7 = value;
	}

	inline static int32_t get_offset_of_posx0_8() { return static_cast<int32_t>(offsetof(Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153, ___posx0_8)); }
	inline float get_posx0_8() const { return ___posx0_8; }
	inline float* get_address_of_posx0_8() { return &___posx0_8; }
	inline void set_posx0_8(float value)
	{
		___posx0_8 = value;
	}

	inline static int32_t get_offset_of_posy0_9() { return static_cast<int32_t>(offsetof(Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153, ___posy0_9)); }
	inline float get_posy0_9() const { return ___posy0_9; }
	inline float* get_address_of_posy0_9() { return &___posy0_9; }
	inline void set_posy0_9(float value)
	{
		___posy0_9 = value;
	}

	inline static int32_t get_offset_of_cells_10() { return static_cast<int32_t>(offsetof(Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153, ___cells_10)); }
	inline GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* get_cells_10() const { return ___cells_10; }
	inline GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9** get_address_of_cells_10() { return &___cells_10; }
	inline void set_cells_10(GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* value)
	{
		___cells_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cells_10), (void*)value);
	}
};

struct Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_StaticFields
{
public:
	// UnityEngine.Texture2D Field::hitImage
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___hitImage_4;

public:
	inline static int32_t get_offset_of_hitImage_4() { return static_cast<int32_t>(offsetof(Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_StaticFields, ___hitImage_4)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_hitImage_4() const { return ___hitImage_4; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_hitImage_4() { return &___hitImage_4; }
	inline void set_hitImage_4(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___hitImage_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitImage_4), (void*)value);
	}
};


// NetworkManager
struct  NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Player NetworkManager::player
	Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * ___player_4;
	// Field NetworkManager::playerField
	Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * ___playerField_5;
	// Field NetworkManager::enemyField
	Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * ___enemyField_6;
	// ProtocolClient.IClient NetworkManager::client
	IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * ___client_7;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606, ___player_4)); }
	inline Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * get_player_4() const { return ___player_4; }
	inline Player_t5689617909B48F7640EA0892D85C92C13CC22C6F ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___player_4), (void*)value);
	}

	inline static int32_t get_offset_of_playerField_5() { return static_cast<int32_t>(offsetof(NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606, ___playerField_5)); }
	inline Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * get_playerField_5() const { return ___playerField_5; }
	inline Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 ** get_address_of_playerField_5() { return &___playerField_5; }
	inline void set_playerField_5(Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * value)
	{
		___playerField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerField_5), (void*)value);
	}

	inline static int32_t get_offset_of_enemyField_6() { return static_cast<int32_t>(offsetof(NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606, ___enemyField_6)); }
	inline Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * get_enemyField_6() const { return ___enemyField_6; }
	inline Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 ** get_address_of_enemyField_6() { return &___enemyField_6; }
	inline void set_enemyField_6(Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * value)
	{
		___enemyField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enemyField_6), (void*)value);
	}

	inline static int32_t get_offset_of_client_7() { return static_cast<int32_t>(offsetof(NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606, ___client_7)); }
	inline IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * get_client_7() const { return ___client_7; }
	inline IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB ** get_address_of_client_7() { return &___client_7; }
	inline void set_client_7(IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * value)
	{
		___client_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___client_7), (void*)value);
	}
};

struct NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606_StaticFields
{
public:
	// UnityEngine.GameObject NetworkManager::instance
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___instance_8;

public:
	inline static int32_t get_offset_of_instance_8() { return static_cast<int32_t>(offsetof(NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606_StaticFields, ___instance_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_instance_8() const { return ___instance_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_instance_8() { return &___instance_8; }
	inline void set_instance_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___instance_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_8), (void*)value);
	}
};


// Player
struct  Player_t5689617909B48F7640EA0892D85C92C13CC22C6F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// NetworkManager Player::networkManager
	NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * ___networkManager_6;
	// System.Int32 Player::shipCountS
	int32_t ___shipCountS_7;
	// System.Int32 Player::shipCountM
	int32_t ___shipCountM_8;
	// System.Int32 Player::shipCountL
	int32_t ___shipCountL_9;
	// System.Int32 Player::shipCountXL
	int32_t ___shipCountXL_10;
	// System.Boolean Player::horizontalPlacement
	bool ___horizontalPlacement_11;
	// Field Player::field
	Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * ___field_12;
	// System.Collections.Generic.List`1<Ship> Player::ships
	List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * ___ships_13;
	// Ship Player::selectedShip
	Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * ___selectedShip_14;
	// UnityEngine.GameObject Player::placementWindow
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___placementWindow_15;
	// UnityEngine.GameObject Player::placementShipS
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___placementShipS_16;
	// UnityEngine.GameObject Player::placementShipM
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___placementShipM_17;
	// UnityEngine.GameObject Player::placementShipL
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___placementShipL_18;
	// UnityEngine.GameObject Player::placementShipXL
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___placementShipXL_19;
	// UnityEngine.GameObject Player::ErrorPopUp
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ErrorPopUp_20;
	// UnityEngine.GameObject Player::undoButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___undoButton_21;
	// UnityEngine.GameObject Player::confirmButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___confirmButton_22;
	// UnityEngine.GameObject Player::rotationState
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___rotationState_23;
	// UnityEngine.GameObject Player::turnIndicator
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___turnIndicator_24;
	// UnityEngine.UI.Text Player::textTurnIndicator
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___textTurnIndicator_25;
	// UnityEngine.GameObject Player::EndScreen
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___EndScreen_26;
	// System.Boolean Player::isPlayersTurn
	bool ___isPlayersTurn_28;

public:
	inline static int32_t get_offset_of_networkManager_6() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___networkManager_6)); }
	inline NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * get_networkManager_6() const { return ___networkManager_6; }
	inline NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 ** get_address_of_networkManager_6() { return &___networkManager_6; }
	inline void set_networkManager_6(NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * value)
	{
		___networkManager_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___networkManager_6), (void*)value);
	}

	inline static int32_t get_offset_of_shipCountS_7() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___shipCountS_7)); }
	inline int32_t get_shipCountS_7() const { return ___shipCountS_7; }
	inline int32_t* get_address_of_shipCountS_7() { return &___shipCountS_7; }
	inline void set_shipCountS_7(int32_t value)
	{
		___shipCountS_7 = value;
	}

	inline static int32_t get_offset_of_shipCountM_8() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___shipCountM_8)); }
	inline int32_t get_shipCountM_8() const { return ___shipCountM_8; }
	inline int32_t* get_address_of_shipCountM_8() { return &___shipCountM_8; }
	inline void set_shipCountM_8(int32_t value)
	{
		___shipCountM_8 = value;
	}

	inline static int32_t get_offset_of_shipCountL_9() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___shipCountL_9)); }
	inline int32_t get_shipCountL_9() const { return ___shipCountL_9; }
	inline int32_t* get_address_of_shipCountL_9() { return &___shipCountL_9; }
	inline void set_shipCountL_9(int32_t value)
	{
		___shipCountL_9 = value;
	}

	inline static int32_t get_offset_of_shipCountXL_10() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___shipCountXL_10)); }
	inline int32_t get_shipCountXL_10() const { return ___shipCountXL_10; }
	inline int32_t* get_address_of_shipCountXL_10() { return &___shipCountXL_10; }
	inline void set_shipCountXL_10(int32_t value)
	{
		___shipCountXL_10 = value;
	}

	inline static int32_t get_offset_of_horizontalPlacement_11() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___horizontalPlacement_11)); }
	inline bool get_horizontalPlacement_11() const { return ___horizontalPlacement_11; }
	inline bool* get_address_of_horizontalPlacement_11() { return &___horizontalPlacement_11; }
	inline void set_horizontalPlacement_11(bool value)
	{
		___horizontalPlacement_11 = value;
	}

	inline static int32_t get_offset_of_field_12() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___field_12)); }
	inline Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * get_field_12() const { return ___field_12; }
	inline Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 ** get_address_of_field_12() { return &___field_12; }
	inline void set_field_12(Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * value)
	{
		___field_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___field_12), (void*)value);
	}

	inline static int32_t get_offset_of_ships_13() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___ships_13)); }
	inline List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * get_ships_13() const { return ___ships_13; }
	inline List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 ** get_address_of_ships_13() { return &___ships_13; }
	inline void set_ships_13(List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * value)
	{
		___ships_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ships_13), (void*)value);
	}

	inline static int32_t get_offset_of_selectedShip_14() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___selectedShip_14)); }
	inline Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * get_selectedShip_14() const { return ___selectedShip_14; }
	inline Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E ** get_address_of_selectedShip_14() { return &___selectedShip_14; }
	inline void set_selectedShip_14(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * value)
	{
		___selectedShip_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selectedShip_14), (void*)value);
	}

	inline static int32_t get_offset_of_placementWindow_15() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___placementWindow_15)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_placementWindow_15() const { return ___placementWindow_15; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_placementWindow_15() { return &___placementWindow_15; }
	inline void set_placementWindow_15(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___placementWindow_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___placementWindow_15), (void*)value);
	}

	inline static int32_t get_offset_of_placementShipS_16() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___placementShipS_16)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_placementShipS_16() const { return ___placementShipS_16; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_placementShipS_16() { return &___placementShipS_16; }
	inline void set_placementShipS_16(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___placementShipS_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___placementShipS_16), (void*)value);
	}

	inline static int32_t get_offset_of_placementShipM_17() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___placementShipM_17)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_placementShipM_17() const { return ___placementShipM_17; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_placementShipM_17() { return &___placementShipM_17; }
	inline void set_placementShipM_17(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___placementShipM_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___placementShipM_17), (void*)value);
	}

	inline static int32_t get_offset_of_placementShipL_18() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___placementShipL_18)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_placementShipL_18() const { return ___placementShipL_18; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_placementShipL_18() { return &___placementShipL_18; }
	inline void set_placementShipL_18(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___placementShipL_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___placementShipL_18), (void*)value);
	}

	inline static int32_t get_offset_of_placementShipXL_19() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___placementShipXL_19)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_placementShipXL_19() const { return ___placementShipXL_19; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_placementShipXL_19() { return &___placementShipXL_19; }
	inline void set_placementShipXL_19(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___placementShipXL_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___placementShipXL_19), (void*)value);
	}

	inline static int32_t get_offset_of_ErrorPopUp_20() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___ErrorPopUp_20)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ErrorPopUp_20() const { return ___ErrorPopUp_20; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ErrorPopUp_20() { return &___ErrorPopUp_20; }
	inline void set_ErrorPopUp_20(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ErrorPopUp_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ErrorPopUp_20), (void*)value);
	}

	inline static int32_t get_offset_of_undoButton_21() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___undoButton_21)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_undoButton_21() const { return ___undoButton_21; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_undoButton_21() { return &___undoButton_21; }
	inline void set_undoButton_21(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___undoButton_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___undoButton_21), (void*)value);
	}

	inline static int32_t get_offset_of_confirmButton_22() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___confirmButton_22)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_confirmButton_22() const { return ___confirmButton_22; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_confirmButton_22() { return &___confirmButton_22; }
	inline void set_confirmButton_22(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___confirmButton_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___confirmButton_22), (void*)value);
	}

	inline static int32_t get_offset_of_rotationState_23() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___rotationState_23)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_rotationState_23() const { return ___rotationState_23; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_rotationState_23() { return &___rotationState_23; }
	inline void set_rotationState_23(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___rotationState_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rotationState_23), (void*)value);
	}

	inline static int32_t get_offset_of_turnIndicator_24() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___turnIndicator_24)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_turnIndicator_24() const { return ___turnIndicator_24; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_turnIndicator_24() { return &___turnIndicator_24; }
	inline void set_turnIndicator_24(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___turnIndicator_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___turnIndicator_24), (void*)value);
	}

	inline static int32_t get_offset_of_textTurnIndicator_25() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___textTurnIndicator_25)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_textTurnIndicator_25() const { return ___textTurnIndicator_25; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_textTurnIndicator_25() { return &___textTurnIndicator_25; }
	inline void set_textTurnIndicator_25(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___textTurnIndicator_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textTurnIndicator_25), (void*)value);
	}

	inline static int32_t get_offset_of_EndScreen_26() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___EndScreen_26)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_EndScreen_26() const { return ___EndScreen_26; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_EndScreen_26() { return &___EndScreen_26; }
	inline void set_EndScreen_26(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___EndScreen_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EndScreen_26), (void*)value);
	}

	inline static int32_t get_offset_of_isPlayersTurn_28() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___isPlayersTurn_28)); }
	inline bool get_isPlayersTurn_28() const { return ___isPlayersTurn_28; }
	inline bool* get_address_of_isPlayersTurn_28() { return &___isPlayersTurn_28; }
	inline void set_isPlayersTurn_28(bool value)
	{
		___isPlayersTurn_28 = value;
	}
};

struct Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields
{
public:
	// UnityEngine.Color Player::disabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___disabledColor_4;
	// UnityEngine.Color Player::enabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___enabledColor_5;
	// System.Boolean Player::playerIsPlacing
	bool ___playerIsPlacing_27;

public:
	inline static int32_t get_offset_of_disabledColor_4() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields, ___disabledColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_disabledColor_4() const { return ___disabledColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_disabledColor_4() { return &___disabledColor_4; }
	inline void set_disabledColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___disabledColor_4 = value;
	}

	inline static int32_t get_offset_of_enabledColor_5() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields, ___enabledColor_5)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_enabledColor_5() const { return ___enabledColor_5; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_enabledColor_5() { return &___enabledColor_5; }
	inline void set_enabledColor_5(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___enabledColor_5 = value;
	}

	inline static int32_t get_offset_of_playerIsPlacing_27() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields, ___playerIsPlacing_27)); }
	inline bool get_playerIsPlacing_27() const { return ___playerIsPlacing_27; }
	inline bool* get_address_of_playerIsPlacing_27() { return &___playerIsPlacing_27; }
	inline void set_playerIsPlacing_27(bool value)
	{
		___playerIsPlacing_27 = value;
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// EnemyField
struct  EnemyField_t935BD6B66CC4595689CD03E7600CAFA8B3893784  : public Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153
{
public:

public:
};


// UnityEngine.UI.Graphic
struct  Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct  Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_EnableCalled_6() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_EnableCalled_6)); }
	inline bool get_m_EnableCalled_6() const { return ___m_EnableCalled_6; }
	inline bool* get_address_of_m_EnableCalled_6() { return &___m_EnableCalled_6; }
	inline void set_m_EnableCalled_6(bool value)
	{
		___m_EnableCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Navigation_7)); }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_5), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Colors_9)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_SpriteState_10)); }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_TargetGraphic_13)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_15() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CurrentIndex_15)); }
	inline int32_t get_m_CurrentIndex_15() const { return ___m_CurrentIndex_15; }
	inline int32_t* get_address_of_m_CurrentIndex_15() { return &___m_CurrentIndex_15; }
	inline void set_m_CurrentIndex_15(int32_t value)
	{
		___m_CurrentIndex_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CanvasGroupCache_19)); }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// UnityEngine.UI.Button
struct  Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * ___m_OnClick_20;

public:
	inline static int32_t get_offset_of_m_OnClick_20() { return static_cast<int32_t>(offsetof(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D, ___m_OnClick_20)); }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * get_m_OnClick_20() const { return ___m_OnClick_20; }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F ** get_address_of_m_OnClick_20() { return &___m_OnClick_20; }
	inline void set_m_OnClick_20(ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * value)
	{
		___m_OnClick_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnClick_20), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Image
struct  Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Sprite_37;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_OverrideSprite_38;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_39;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_40;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_41;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_42;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_43;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_44;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_45;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_46;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_47;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_48;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_49;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_50;

public:
	inline static int32_t get_offset_of_m_Sprite_37() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Sprite_37)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Sprite_37() const { return ___m_Sprite_37; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Sprite_37() { return &___m_Sprite_37; }
	inline void set_m_Sprite_37(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Sprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_38() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_OverrideSprite_38)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_OverrideSprite_38() const { return ___m_OverrideSprite_38; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_OverrideSprite_38() { return &___m_OverrideSprite_38; }
	inline void set_m_OverrideSprite_38(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_OverrideSprite_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_39() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Type_39)); }
	inline int32_t get_m_Type_39() const { return ___m_Type_39; }
	inline int32_t* get_address_of_m_Type_39() { return &___m_Type_39; }
	inline void set_m_Type_39(int32_t value)
	{
		___m_Type_39 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_40() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PreserveAspect_40)); }
	inline bool get_m_PreserveAspect_40() const { return ___m_PreserveAspect_40; }
	inline bool* get_address_of_m_PreserveAspect_40() { return &___m_PreserveAspect_40; }
	inline void set_m_PreserveAspect_40(bool value)
	{
		___m_PreserveAspect_40 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_41() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillCenter_41)); }
	inline bool get_m_FillCenter_41() const { return ___m_FillCenter_41; }
	inline bool* get_address_of_m_FillCenter_41() { return &___m_FillCenter_41; }
	inline void set_m_FillCenter_41(bool value)
	{
		___m_FillCenter_41 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_42() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillMethod_42)); }
	inline int32_t get_m_FillMethod_42() const { return ___m_FillMethod_42; }
	inline int32_t* get_address_of_m_FillMethod_42() { return &___m_FillMethod_42; }
	inline void set_m_FillMethod_42(int32_t value)
	{
		___m_FillMethod_42 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_43() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillAmount_43)); }
	inline float get_m_FillAmount_43() const { return ___m_FillAmount_43; }
	inline float* get_address_of_m_FillAmount_43() { return &___m_FillAmount_43; }
	inline void set_m_FillAmount_43(float value)
	{
		___m_FillAmount_43 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_44() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillClockwise_44)); }
	inline bool get_m_FillClockwise_44() const { return ___m_FillClockwise_44; }
	inline bool* get_address_of_m_FillClockwise_44() { return &___m_FillClockwise_44; }
	inline void set_m_FillClockwise_44(bool value)
	{
		___m_FillClockwise_44 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_45() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillOrigin_45)); }
	inline int32_t get_m_FillOrigin_45() const { return ___m_FillOrigin_45; }
	inline int32_t* get_address_of_m_FillOrigin_45() { return &___m_FillOrigin_45; }
	inline void set_m_FillOrigin_45(int32_t value)
	{
		___m_FillOrigin_45 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_46() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_AlphaHitTestMinimumThreshold_46)); }
	inline float get_m_AlphaHitTestMinimumThreshold_46() const { return ___m_AlphaHitTestMinimumThreshold_46; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_46() { return &___m_AlphaHitTestMinimumThreshold_46; }
	inline void set_m_AlphaHitTestMinimumThreshold_46(float value)
	{
		___m_AlphaHitTestMinimumThreshold_46 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_47() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Tracked_47)); }
	inline bool get_m_Tracked_47() const { return ___m_Tracked_47; }
	inline bool* get_address_of_m_Tracked_47() { return &___m_Tracked_47; }
	inline void set_m_Tracked_47(bool value)
	{
		___m_Tracked_47 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_48() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_UseSpriteMesh_48)); }
	inline bool get_m_UseSpriteMesh_48() const { return ___m_UseSpriteMesh_48; }
	inline bool* get_address_of_m_UseSpriteMesh_48() { return &___m_UseSpriteMesh_48; }
	inline void set_m_UseSpriteMesh_48(bool value)
	{
		___m_UseSpriteMesh_48 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_49() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PixelsPerUnitMultiplier_49)); }
	inline float get_m_PixelsPerUnitMultiplier_49() const { return ___m_PixelsPerUnitMultiplier_49; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_49() { return &___m_PixelsPerUnitMultiplier_49; }
	inline void set_m_PixelsPerUnitMultiplier_49(float value)
	{
		___m_PixelsPerUnitMultiplier_49 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_50() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_CachedReferencePixelsPerUnit_50)); }
	inline float get_m_CachedReferencePixelsPerUnit_50() const { return ___m_CachedReferencePixelsPerUnit_50; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_50() { return &___m_CachedReferencePixelsPerUnit_50; }
	inline void set_m_CachedReferencePixelsPerUnit_50(float value)
	{
		___m_CachedReferencePixelsPerUnit_50 = value;
	}
};

struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_ETC1DefaultUI_36;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_VertScratch_51;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_UVScratch_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Xy_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Uv_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t815A476B0A21E183042059E705F9E505478CD8AE * ___m_TrackedTexturelessImages_55;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_56;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_36() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_ETC1DefaultUI_36)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_ETC1DefaultUI_36() const { return ___s_ETC1DefaultUI_36; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_ETC1DefaultUI_36() { return &___s_ETC1DefaultUI_36; }
	inline void set_s_ETC1DefaultUI_36(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_ETC1DefaultUI_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_51() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_VertScratch_51)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_VertScratch_51() const { return ___s_VertScratch_51; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_VertScratch_51() { return &___s_VertScratch_51; }
	inline void set_s_VertScratch_51(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_VertScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_52() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_UVScratch_52)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_UVScratch_52() const { return ___s_UVScratch_52; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_UVScratch_52() { return &___s_UVScratch_52; }
	inline void set_s_UVScratch_52(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_UVScratch_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_53() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Xy_53)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Xy_53() const { return ___s_Xy_53; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Xy_53() { return &___s_Xy_53; }
	inline void set_s_Xy_53(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Xy_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_53), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_54() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Uv_54)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Uv_54() const { return ___s_Uv_54; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Uv_54() { return &___s_Uv_54; }
	inline void set_s_Uv_54(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Uv_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_54), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_55() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___m_TrackedTexturelessImages_55)); }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE * get_m_TrackedTexturelessImages_55() const { return ___m_TrackedTexturelessImages_55; }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE ** get_address_of_m_TrackedTexturelessImages_55() { return &___m_TrackedTexturelessImages_55; }
	inline void set_m_TrackedTexturelessImages_55(List_1_t815A476B0A21E183042059E705F9E505478CD8AE * value)
	{
		___m_TrackedTexturelessImages_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_55), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_56() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Initialized_56)); }
	inline bool get_s_Initialized_56() const { return ___s_Initialized_56; }
	inline bool* get_address_of_s_Initialized_56() { return &___s_Initialized_56; }
	inline void set_s_Initialized_56(bool value)
	{
		___s_Initialized_56 = value;
	}
};


// UnityEngine.UI.RawImage
struct  RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___m_Texture_36;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___m_UVRect_37;

public:
	inline static int32_t get_offset_of_m_Texture_36() { return static_cast<int32_t>(offsetof(RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A, ___m_Texture_36)); }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * get_m_Texture_36() const { return ___m_Texture_36; }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE ** get_address_of_m_Texture_36() { return &___m_Texture_36; }
	inline void set_m_Texture_36(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * value)
	{
		___m_Texture_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Texture_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_UVRect_37() { return static_cast<int32_t>(offsetof(RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A, ___m_UVRect_37)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get_m_UVRect_37() const { return ___m_UVRect_37; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of_m_UVRect_37() { return &___m_UVRect_37; }
	inline void set_m_UVRect_37(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		___m_UVRect_37 = value;
	}
};


// UnityEngine.UI.Text
struct  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_TempVerts_42;

public:
	inline static int32_t get_offset_of_m_FontData_36() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_FontData_36)); }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * get_m_FontData_36() const { return ___m_FontData_36; }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 ** get_address_of_m_FontData_36() { return &___m_FontData_36; }
	inline void set_m_FontData_36(FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * value)
	{
		___m_FontData_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_37() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_Text_37)); }
	inline String_t* get_m_Text_37() const { return ___m_Text_37; }
	inline String_t** get_address_of_m_Text_37() { return &___m_Text_37; }
	inline void set_m_Text_37(String_t* value)
	{
		___m_Text_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_38() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCache_38)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCache_38() const { return ___m_TextCache_38; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCache_38() { return &___m_TextCache_38; }
	inline void set_m_TextCache_38(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCache_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_39() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCacheForLayout_39)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCacheForLayout_39() const { return ___m_TextCacheForLayout_39; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCacheForLayout_39() { return &___m_TextCacheForLayout_39; }
	inline void set_m_TextCacheForLayout_39(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCacheForLayout_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_41() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_DisableFontTextureRebuiltCallback_41)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_41() const { return ___m_DisableFontTextureRebuiltCallback_41; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_41() { return &___m_DisableFontTextureRebuiltCallback_41; }
	inline void set_m_DisableFontTextureRebuiltCallback_41(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_41 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_42() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TempVerts_42)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_TempVerts_42() const { return ___m_TempVerts_42; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_TempVerts_42() { return &___m_TempVerts_42; }
	inline void set_m_TempVerts_42(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_TempVerts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_42), (void*)value);
	}
};

struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultText_40;

public:
	inline static int32_t get_offset_of_s_DefaultText_40() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields, ___s_DefaultText_40)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultText_40() const { return ___s_DefaultText_40; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultText_40() { return &___s_DefaultText_40; }
	inline void set_s_DefaultText_40(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_40), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.GameObject[0...,0...]
struct GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * m_Items[1];

public:
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * m_Items[1];

public:
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m4039C8E65629D33E1EC84D2505BF1D5DDC936622_gshared (RuntimeObject * ___original0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void System.EventHandler`1<System.Object>::Invoke(System.Object,!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventHandler_1_Invoke_mEAFD7F9E52E7DF356F3C4F0262BCFBA7769C83C0_gshared (EventHandler_1_tFA1C30E54FA1061D79E711F65F9A174BFBD8CDCB * __this, RuntimeObject * ___sender0, RuntimeObject * ___e1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::ContainsKey(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_m4F760255383CE517D97B27C5379EA6578C2FF02F_gshared (Dictionary_2_t7DB3847BFAFC9452C96798C33FA6D63A1DFEBF9A * __this, uint8_t ___key0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_Remove_m19FF3CB3991319A5AAE849FFC4F464FCB40DFA7F_gshared (Dictionary_2_t7DB3847BFAFC9452C96798C33FA6D63A1DFEBF9A * __this, uint8_t ___key0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m04D7B45C09D541A866F16C53B53A0D18A755C947_gshared (Dictionary_2_t7DB3847BFAFC9452C96798C33FA6D63A1DFEBF9A * __this, uint8_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Byte>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_mC92A676A3C9BFEFF1C88BADCAF85070A2DCABED4_gshared (List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Clear_m935994267561A33407112EFB85A4F13BA9D2FE36_gshared (Dictionary_2_t7DB3847BFAFC9452C96798C33FA6D63A1DFEBF9A * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Byte>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_AddRange_m207006733154C20C7F72BFDD693CF79C1B8F4B5F_gshared (List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * __this, RuntimeObject* ___collection0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::TryGetValue(!0,!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_m5441A60FEDEEC843A47D88F1C30C9F1E3F6917FA_gshared (Dictionary_2_t7DB3847BFAFC9452C96798C33FA6D63A1DFEBF9A * __this, uint8_t ___key0, RuntimeObject ** ___value1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Byte>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m1C5E87EEF4B8F8CB985E6C4FD69D863ABAD79F14_gshared_inline (List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Byte>::RemoveRange(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_RemoveRange_m9D8324CBE5E7209DD2BE2F04E5E5513C9D665110_gshared (List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * __this, int32_t ___index0, int32_t ___count1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Byte>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m42BD5721699395D9FC6B6D889CE5A29C4C2F1778_gshared (List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m9F3F1035A447F9664AEA0074FE05B3FBD142A18C_gshared (Dictionary_2_t7DB3847BFAFC9452C96798C33FA6D63A1DFEBF9A * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::get_Item(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Dictionary_2_get_Item_m8EAE235A8AA510698BE04B4EC44DCA6979E58D29_gshared (Dictionary_2_t7DB3847BFAFC9452C96798C33FA6D63A1DFEBF9A * __this, uint8_t ___key0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,System.Int32Enum>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction_2__ctor_m50E7B823E46CB327D49A2D55A761F57472037634_gshared (UnityAction_2_t808E43EBC9AA89CEA5830BD187EC213182A02B50 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.EventHandler`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventHandler_1__ctor_m55B15D6747B269625FC10375E6008AA99BD498B4_gshared (EventHandler_1_tFA1C30E54FA1061D79E711F65F9A174BFBD8CDCB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mA671E933C9D3DAE4E3F71D34FDDA971739618158_gshared (Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::ForEach(System.Action`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_ForEach_m1C08725A39127C32D56B9E8A36D813F08D3BFF4D_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___action0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponentInChildren_TisRuntimeObject_mC8FC6687C66150FA89090C2A7733B2EE2E1315FD_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_RemoveAt_m66148860899ECCAE9B323372032BFC1C255393D2_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 System.Linq.Enumerable::Count<System.Byte>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Enumerable_Count_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m1AFC4338BF90E364F564717BDBFDD9D1217C2980_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// !!0 System.Linq.Enumerable::ElementAt<System.Byte>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_gshared (RuntimeObject* ___source0, int32_t ___index1, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Byte>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* Enumerable_ToArray_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m00F16F6DDED1338D9656B4EDE7F8AE2B2E2251E4_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);

// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55 (Delegate_t * ___a0, Delegate_t * ___b1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4 (Delegate_t * ___source0, Delegate_t * ___value1, const RuntimeMethod* method);
// System.Void CellEventArgs::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellEventArgs__ctor_m42CBF75EDC8AEB81C32D6A6299981BB5BE3481BC (CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * __this, bool ___hovered0, const RuntimeMethod* method);
// System.Void Cell/CellHoverEventHandler::Invoke(System.Object,CellEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellHoverEventHandler_Invoke_m3617B3846462554FF0110231A58370BE5E446409 (CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * __this, RuntimeObject * ___sender0, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * ___e1, const RuntimeMethod* method);
// System.Void CellEventArgs::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellEventArgs__ctor_m77B26E0518E7D0D30CD856D44ED172B4B096F60B (CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * __this, const RuntimeMethod* method);
// System.Void Cell/CellClickedEventHandler::Invoke(System.Object,CellEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellClickedEventHandler_Invoke_mA777B45CCDD94D5BE52E8BC63679ACF954DC4515 (CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * __this, RuntimeObject * ___sender0, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * ___e1, const RuntimeMethod* method);
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78 (String_t* ___str00, String_t* ___str11, String_t* ___str22, String_t* ___str33, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void Cell::RaiseCellClickedEvent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell_RaiseCellClickedEvent_mB23D573B3CF437B447E3329CD6087D445221CB7E (Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * __this, const RuntimeMethod* method);
// System.Void Cell::RaiseCellHoverEvent(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell_RaiseCellHoverEvent_m4AD37176E69A4F5BE3203E49AC1D80C5EA76FB60 (Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * __this, bool ___hovered0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * __this, float ___r0, float ___g1, float ___b2, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void CellEventArgs::set_Hovered(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CellEventArgs_set_Hovered_mED2C25ABB5B9CFDB913D36CE1E569EBB01E19EC0_inline (CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void EnemyField::InitializeCells()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyField_InitializeCells_mDCDC5379AA9F15B92BC0DC7FCE1598BC127743D5 (EnemyField_t935BD6B66CC4595689CD03E7600CAFA8B3893784 * __this, const RuntimeMethod* method);
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * Resources_Load_m011631B3740AFD38D496838F10D3DA635A061120 (String_t* ___path0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___original0, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m4039C8E65629D33E1EC84D2505BF1D5DDC936622_gshared)(___original0, method);
}
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.BoxCollider2D>()
inline BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * GameObject_GetComponent_TisBoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_mAA495A9AAD3FB4D260B3E2B6DDBAFEC18DC49C3C (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, String_t* ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<Cell>()
inline Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void Cell::SetIndexes(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell_SetIndexes_mB97C2F2C7D4CEE6478CD0F5B4A52A8231ED69459 (Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * __this, int32_t ___y0, int32_t ___x1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___p0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_mF7FCDE24496D619F4BB1A0BA44AF17DCB5D697FF_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_sizeDelta_m61943618442E31C6FF0556CDFC70940AE7AD04D0 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.BoxCollider2D::set_size(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxCollider2D_set_size_m8460A38ADDD4BE82BE1F416DE3D7AFB87EBA6760 (BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void Field::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Field__ctor_m850074110EC0DC28CBDCF47344CC06B3C7CA3F03 (Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * __this, const RuntimeMethod* method);
// System.Void Field::InitializeCells()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Field_InitializeCells_m373BF5F93A8819CCD48A7A35E5C81312DA8CC6A7 (Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.RawImage>()
inline RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void UnityEngine.UI.RawImage::set_texture(UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RawImage_set_texture_m1D7BAE6CB629C36894B664D9F5D68CACA88B8D99 (RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * __this, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___value0, const RuntimeMethod* method);
// System.Void System.EventHandler`1<System.Byte[]>::Invoke(System.Object,!0)
inline void EventHandler_1_Invoke_m6A54CC2E93E4E7E38A8E2F4F63FE0DB149028B54 (EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * __this, RuntimeObject * ___sender0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___e1, const RuntimeMethod* method)
{
	((  void (*) (EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *, RuntimeObject *, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, const RuntimeMethod*))EventHandler_1_Invoke_mEAFD7F9E52E7DF356F3C4F0262BCFBA7769C83C0_gshared)(__this, ___sender0, ___e1, method);
}
// System.Void System.EventHandler`1<System.Exception>::Invoke(System.Object,!0)
inline void EventHandler_1_Invoke_m134E3553E044A5F7F21E5C4178920C19082A2400 (EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * __this, RuntimeObject * ___sender0, Exception_t * ___e1, const RuntimeMethod* method)
{
	((  void (*) (EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D *, RuntimeObject *, Exception_t *, const RuntimeMethod*))EventHandler_1_Invoke_mEAFD7F9E52E7DF356F3C4F0262BCFBA7769C83C0_gshared)(__this, ___sender0, ___e1, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,ProtocolClient.NetMessage>::ContainsKey(!0)
inline bool Dictionary_2_ContainsKey_m478627FE9A3A2D2A4FBC3454D861BDDAB5EC22AA (Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * __this, uint8_t ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 *, uint8_t, const RuntimeMethod*))Dictionary_2_ContainsKey_m4F760255383CE517D97B27C5379EA6578C2FF02F_gshared)(__this, ___key0, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,ProtocolClient.NetMessage>::Remove(!0)
inline bool Dictionary_2_Remove_m0CC5596DB8DC24BD58FE8EF0891DC2B267526386 (Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * __this, uint8_t ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 *, uint8_t, const RuntimeMethod*))Dictionary_2_Remove_m19FF3CB3991319A5AAE849FFC4F464FCB40DFA7F_gshared)(__this, ___key0, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,ProtocolClient.NetMessage>::Add(!0,!1)
inline void Dictionary_2_Add_mFFB7D3AB068FCD0390EC514E63DF3B73853D1DBC (Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * __this, uint8_t ___key0, NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 *, uint8_t, NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 *, const RuntimeMethod*))Dictionary_2_Add_m04D7B45C09D541A866F16C53B53A0D18A755C947_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.List`1<System.Byte>::Clear()
inline void List_1_Clear_mC92A676A3C9BFEFF1C88BADCAF85070A2DCABED4 (List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF *, const RuntimeMethod*))List_1_Clear_mC92A676A3C9BFEFF1C88BADCAF85070A2DCABED4_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,ProtocolClient.NetMessage>::Clear()
inline void Dictionary_2_Clear_m6A531EDA6DD13A0BEDC4C0ECB7EA76BE75606AF8 (Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 *, const RuntimeMethod*))Dictionary_2_Clear_m935994267561A33407112EFB85A4F13BA9D2FE36_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Byte>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1_AddRange_m207006733154C20C7F72BFDD693CF79C1B8F4B5F (List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF *, RuntimeObject*, const RuntimeMethod*))List_1_AddRange_m207006733154C20C7F72BFDD693CF79C1B8F4B5F_gshared)(__this, ___collection0, method);
}
// ProtocolClient.NetMessage ProtocolClient.NetReceiveBuffer::SearchForNetMessageInBuffer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * NetReceiveBuffer_SearchForNetMessageInBuffer_mB24D4BC21C300B8BBC9ABA78C3A8F460CB0BB0F8 (NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * __this, const RuntimeMethod* method);
// System.Void ProtocolClient.NetReceiveBuffer::AddNewKeyValue(System.Byte,ProtocolClient.NetMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetReceiveBuffer_AddNewKeyValue_m0E0E250207541900778C2311A7D92044583DCE57 (NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * __this, uint8_t ___messageId0, NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ___newNetMessage1, const RuntimeMethod* method);
// ProtocolClient.NetMessage ProtocolClient.NetReceiveBuffer::GetNetMessageById(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * NetReceiveBuffer_GetNetMessageById_m23DFEC39FE560EC0F2ED5CE316C1F0073FB7E04F (NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * __this, uint8_t ___id0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,ProtocolClient.NetMessage>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_m165079D4EC60902B2BD406C3F965F0A3E93709CF (Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * __this, uint8_t ___key0, NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 ** ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 *, uint8_t, NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m5441A60FEDEEC843A47D88F1C30C9F1E3F6917FA_gshared)(__this, ___key0, ___value1, method);
}
// System.Int32 ProtocolClient.ProtocolTool::GetSizeOfNetMessageInBuffer(System.Collections.Generic.IEnumerable`1<System.Byte>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ProtocolTool_GetSizeOfNetMessageInBuffer_m051D0740337106C72030ED21A975CA266F3C4045 (RuntimeObject* ___rawNetMessage0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Byte>::get_Count()
inline int32_t List_1_get_Count_m1C5E87EEF4B8F8CB985E6C4FD69D863ABAD79F14_inline (List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF *, const RuntimeMethod*))List_1_get_Count_m1C5E87EEF4B8F8CB985E6C4FD69D863ABAD79F14_gshared_inline)(__this, method);
}
// ProtocolClient.NetMessage ProtocolClient.ProtocolTool::GetMessageFromByteArray(System.Collections.Generic.IEnumerable`1<System.Byte>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ProtocolTool_GetMessageFromByteArray_mBD85E536ED14B56570D1CEE94F5CCFEC234121A7 (RuntimeObject* ___rawNetMessage0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Byte>::RemoveRange(System.Int32,System.Int32)
inline void List_1_RemoveRange_m9D8324CBE5E7209DD2BE2F04E5E5513C9D665110 (List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * __this, int32_t ___index0, int32_t ___count1, const RuntimeMethod* method)
{
	((  void (*) (List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF *, int32_t, int32_t, const RuntimeMethod*))List_1_RemoveRange_m9D8324CBE5E7209DD2BE2F04E5E5513C9D665110_gshared)(__this, ___index0, ___count1, method);
}
// System.Void System.Collections.Generic.List`1<System.Byte>::.ctor()
inline void List_1__ctor_m42BD5721699395D9FC6B6D889CE5A29C4C2F1778 (List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF *, const RuntimeMethod*))List_1__ctor_m42BD5721699395D9FC6B6D889CE5A29C4C2F1778_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,ProtocolClient.NetMessage>::.ctor()
inline void Dictionary_2__ctor_m7CDFF19F468AD909A00FDE97E4F7EE902D01ACD5 (Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 *, const RuntimeMethod*))Dictionary_2__ctor_m9F3F1035A447F9664AEA0074FE05B3FBD142A18C_gshared)(__this, method);
}
// !1 System.Collections.Generic.Dictionary`2<System.Byte,ProtocolClient.NetMessage>::get_Item(!0)
inline NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * Dictionary_2_get_Item_m8EF7F2B5D88191D43F306EE9B70BF02239AEEE52 (Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * __this, uint8_t ___key0, const RuntimeMethod* method)
{
	return ((  NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * (*) (Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 *, uint8_t, const RuntimeMethod*))Dictionary_2_get_Item_m8EAE235A8AA510698BE04B4EC44DCA6979E58D29_gshared)(__this, ___key0, method);
}
// System.Byte[] ProtocolClient.ProtocolTool::NetMessageToByteArray(ProtocolClient.NetMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ProtocolTool_NetMessageToByteArray_mCA7A90C8FF3813DC49D9DE9B039FD261C7AFBDFD (NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ___toConvert0, const RuntimeMethod* method);
// System.Void ProtocolClient.TCPConnection::SendAll(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPConnection_SendAll_m5E7C42B504544CA6CFF78C397604D9825F5B5194 (TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___bytes0, const RuntimeMethod* method);
// System.Void ProtocolClient.NetSendBuffer::AddDataToBuffer(ProtocolClient.NetMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetSendBuffer_AddDataToBuffer_m5D25260CCB2126C33732948AE63B963BF55DB169 (NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * __this, NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ___toSend0, const RuntimeMethod* method);
// System.Boolean ProtocolClient.NetSendBuffer::SendMessageById(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NetSendBuffer_SendMessageById_m54A10CDCB7A0F39E5C7DC328531BA25CD94C256E (NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * __this, uint8_t ___id0, const RuntimeMethod* method);
// System.Void NetworkManager::CheckInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_CheckInstance_m81FC480048046FB7E6E353CC73CCB22FF917626D (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___target0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::.ctor(System.Object,System.IntPtr)
inline void UnityAction_2__ctor_mE0417B33CF845A6B9324E67D296ADEA562B91DE0 (UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906 *, RuntimeObject *, intptr_t, const RuntimeMethod*))UnityAction_2__ctor_m50E7B823E46CB327D49A2D55A761F57472037634_gshared)(__this, ___object0, ___method1, method);
}
// System.Void UnityEngine.SceneManagement.SceneManager::add_sceneLoaded(UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_add_sceneLoaded_m54990A485E2E66739E31090BDC3A4C01EF7729BA (UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Application::set_runInBackground(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Application_set_runInBackground_m99DB210D9A5462D08D5FC122FAB971B640D9B636 (bool ___value0, const RuntimeMethod* method);
// System.Void System.EventHandler`1<System.Byte[]>::.ctor(System.Object,System.IntPtr)
inline void EventHandler_1__ctor_mD4C0E5291CBD3101F3D364590F483445B7A27994 (EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *, RuntimeObject *, intptr_t, const RuntimeMethod*))EventHandler_1__ctor_m55B15D6747B269625FC10375E6008AA99BD498B4_gshared)(__this, ___object0, ___method1, method);
}
// System.Void ProtocolClient.IClient::add_ServerRequestEvent(System.EventHandler`1<System.Byte[]>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_add_ServerRequestEvent_m97136FA563855AF87640E7865F21753CD3E10115 (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * ___value0, const RuntimeMethod* method);
// System.Void ProtocolClient.IClient::add_ServerResponseEvent(System.EventHandler`1<System.Byte[]>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_add_ServerResponseEvent_mFF1D827B95AA6BF11F7AB5C97F77340DA5B1692C (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * ___value0, const RuntimeMethod* method);
// System.Void System.EventHandler`1<System.Exception>::.ctor(System.Object,System.IntPtr)
inline void EventHandler_1__ctor_mB90F555001341586440DD1B03C6894D2E0730A26 (EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D *, RuntimeObject *, intptr_t, const RuntimeMethod*))EventHandler_1__ctor_m55B15D6747B269625FC10375E6008AA99BD498B4_gshared)(__this, ___object0, ___method1, method);
}
// System.Void ProtocolClient.IClient::add_ConnectionException(System.EventHandler`1<System.Exception>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_add_ConnectionException_mE43AA0290A2010DBF9451129E56253D50563F0B3 (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * ___value0, const RuntimeMethod* method);
// System.Void ProtocolClient.IClient::add_ServerStatusUpdateEvent(System.EventHandler`1<System.Byte[]>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_add_ServerStatusUpdateEvent_m874656AF3D714B25C12F7CF629134ACF4830CEE0 (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * ___value0, const RuntimeMethod* method);
// System.Collections.IEnumerator NetworkManager::HandleConnection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* NetworkManager_HandleConnection_mA5E86302F2531DC5A2058DEEF9FE738D5243B87B (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.String UnityEngine.SceneManagement.Scene::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Scene_get_name_m38F195D7CA6417FED310C23E4D8E86150C7835B8 (Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GameObject_Find_m20157C941F1A9DA0E33E0ACA1324FAA41C2B199B (String_t* ___name0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<Player>()
inline Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * GameObject_GetComponent_TisPlayer_t5689617909B48F7640EA0892D85C92C13CC22C6F_m82212DD756C5F3A2B4844722FD1EE504E9AAD906 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<Field>()
inline Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * GameObject_GetComponent_TisField_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_m47142A7D5BBB4BD6A40C784E8C4F2DF0D366C708 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m7DAF30213E99396ECBDB1BD40CC34CCF36902092 (String_t* ___sceneName0, const RuntimeMethod* method);
// System.Void System.Exception::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m8ECDE8ACA7F2E0EF1144BD1200FB5DB2870B5F11 (Exception_t * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void Field::OnCellHit(System.Int32,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Field_OnCellHit_mB36A003572E4C64763E34AC8E0CCA3678602967E (Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * __this, int32_t ___y0, int32_t ___x1, bool ___hit2, const RuntimeMethod* method);
// System.Text.Encoding System.Text.Encoding::get_UTF8()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E (const RuntimeMethod* method);
// System.String System.String::CreateString(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_CreateString_mC7F57CE6ED768CF86591160424FE55D5CBA7C344 (String_t* __this, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___val0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method);
// System.String System.String::ToUpper()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_ToUpper_m4BC629F8059C3E0C4E3F7C7E04DB50EBB0C1A05A (String_t* __this, const RuntimeMethod* method);
// System.String[] System.String::Split(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* String_Split_m2C74DC2B85B322998094BEDE787C378822E1F28B (String_t* __this, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___separator0, const RuntimeMethod* method);
// System.Void NetworkManager::StartPlacementPhase()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_StartPlacementPhase_m50F17522A3F45C7D26E4DA3FB359479860843E84 (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, const RuntimeMethod* method);
// System.Void Player::StartPlayingphase()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_StartPlayingphase_m86A50FA9BBB7D0508A33D2929D3659442B4E58A9 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method);
// System.Void Player::OnShipsNotAccepted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_OnShipsNotAccepted_m4FC3F38E952098A8BCC9B71E6244D78773A156B2 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method);
// System.Void NetworkManager::ThrowLogicError()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_ThrowLogicError_mEBCF9F1A963227EC18FB69A35302146F5857A197 (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, const RuntimeMethod* method);
// System.Void Player::SetPlayersTurn(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_SetPlayersTurn_m4BC7801FD622C695EA4953D22BE8A438142F9E66 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, bool ___b0, const RuntimeMethod* method);
// System.Void Player::ShowEndScreen(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_ShowEndScreen_m72605505E37D77917E7CA26DF82CC792A7AD7247 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, bool ___isPlayerWinner0, const RuntimeMethod* method);
// System.Int32 System.Int32::Parse(System.String,System.Globalization.NumberStyles)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Int32_Parse_mE2DF841397B10B947C6729D5B811D0C25D211A56 (String_t* ___s0, int32_t ___style1, const RuntimeMethod* method);
// System.Void NetworkManager::OnCellHit(System.Boolean,System.Int32,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_OnCellHit_mCECB2629807C8147F4E2972B46E0B8A096F63CE5 (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, bool ___isEnemyCell0, int32_t ___x1, int32_t ___y2, bool ___hitShip3, const RuntimeMethod* method);
// System.Void NetworkManager/<>c__DisplayClass14_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass14_0__ctor_m8977A56E78603A833C559237A86A4EE44E5BBC58 (U3CU3Ec__DisplayClass14_0_tCCAB38C50CA018337D1FABDD31482528BBE3F1DF * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<Ship>::get_Count()
inline int32_t List_1_get_Count_m52B8BE5835DE4DEBA361371438815F24116AD4B2_inline (List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Void System.Action`1<Ship>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m3240BECDA2A79E0895E7E9C13C662E65B8BE7709 (Action_1_tCCB45E12289F4906FABB0046E40D54208C8DAB5C * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tCCB45E12289F4906FABB0046E40D54208C8DAB5C *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mA671E933C9D3DAE4E3F71D34FDDA971739618158_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Collections.Generic.List`1<Ship>::ForEach(System.Action`1<!0>)
inline void List_1_ForEach_m0ACE5773DCA4A37490AB5FC2FD86B532EE86251B (List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * __this, Action_1_tCCB45E12289F4906FABB0046E40D54208C8DAB5C * ___action0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 *, Action_1_tCCB45E12289F4906FABB0046E40D54208C8DAB5C *, const RuntimeMethod*))List_1_ForEach_m1C08725A39127C32D56B9E8A36D813F08D3BFF4D_gshared)(__this, ___action0, method);
}
// System.Void NetworkManager/<HandleConnection>d__19::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CHandleConnectionU3Ed__19__ctor_m0B975E74604C0702C0AD73310F88842A78A9B3C1 (U3CHandleConnectionU3Ed__19_t2B2D10AA4C55A2999E347B3F7F12289D2BB5D833 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void ProtocolClient.TCPClient::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient__ctor_m032ED8789EFF986A3548F635F8383BBE08DB858E (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, const RuntimeMethod* method);
// System.Void Player::Init()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_Init_m401115D83CF3607956B7E500383545F8B6588042 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<NetworkManager>()
inline NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * GameObject_GetComponent_TisNetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606_m08D4E52A3ED9AA766CA6959A49D06F448CF3820A (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void Player::UpdatePlacementShips()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_UpdatePlacementShips_m432F1874992B4A70527D0E6EDE75753B57083ECF (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline (Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___call0, const RuntimeMethod* method);
// System.Void Cell/CellHoverEventHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellHoverEventHandler__ctor_mA75DD0C304EFFDF2B7434946DE2A306DFEA4474F (CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void Cell::add_CellHoverEvent(Cell/CellHoverEventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell_add_CellHoverEvent_m62C3C1BCCCDDF49C3D831FDA239E013B6988BE99 (CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * ___value0, const RuntimeMethod* method);
// System.Void Cell/CellClickedEventHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellClickedEventHandler__ctor_m0E624D3592920AD801A5117354902BBC0671D60A (CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void Cell::add_CellClickedEvent(Cell/CellClickedEventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell_add_CellClickedEvent_m2F0AB758089BDF792D350B2DA4B5B6557E78D59F (CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponentInChildren<UnityEngine.UI.Text>()
inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponentInChildren_TisRuntimeObject_mC8FC6687C66150FA89090C2A7733B2EE2E1315FD_gshared)(__this, method);
}
// System.Void UnityEngine.UI.Selectable::set_interactable(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40 (Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.UI.Selectable::get_interactable()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Selectable_get_interactable_m4231515CC3C861B431AA22FE3345F831389B3840_inline (Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * __this, const RuntimeMethod* method);
// System.Void Ship::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ship__ctor_m5734D7D291677BF934981BBF9F7B42B0EC457056 (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, int32_t ___size0, const RuntimeMethod* method);
// System.Boolean CellEventArgs::get_Hovered()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool CellEventArgs_get_Hovered_m2C28642E496CC89649BE2B23DAFAF606407BD189_inline (CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * __this, const RuntimeMethod* method);
// System.Int32 Ship::get_size()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5_inline (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void Player::PlaceShip(System.Object,CellEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_PlaceShip_mF3755A13493938EF34FE4AEB7E5B69801E8D0145 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, RuntimeObject * ___sender0, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * ___e1, const RuntimeMethod* method);
// System.Void Player::ShootCell(System.Object,CellEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_ShootCell_m26EF3D2B0F3339F73B3F4D89C655CAF57C8C60A6 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, RuntimeObject * ___sender0, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * ___e1, const RuntimeMethod* method);
// System.Void Player::ShowErrorPopUp(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_ShowErrorPopUp_mD579E7D01D32349AB1B4EA0F0272FC77B36F2440 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.ctor()
inline void List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8 (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Add(!0)
inline void List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3 (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Item(System.Int32)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.GameObject>::GetEnumerator()
inline Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6 (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, const RuntimeMethod*))List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::get_Current()
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_inline (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 * __this, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.Void Ship::AddCell(UnityEngine.GameObject,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ship_AddCell_mFE93A99E3A478DBDED4B3AA6FBE1D0BEA8408D4D (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cell0, int32_t ___index1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::MoveNext()
inline bool Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7 (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::Dispose()
inline void Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9 (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Ship>::Add(!0)
inline void List_1_Add_m552D9F94492155DFC4DE265624DA1F5E5EBB0319 (List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * __this, Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 *, Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void NetworkManager::SendShootCell(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_SendShootCell_m1C5E85545E665BCF5AFE42903BD71095BBAFF50F (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localEulerAngles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localEulerAngles_m4C442107F523737ADAB54855FDC1777A9B71D545 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localEulerAngles(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localEulerAngles_mB63076996124DC76E6902A81677A6E3C814C693B (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<Ship>::get_Item(System.Int32)
inline Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * List_1_get_Item_m1D3A0CB5B5817CB82948B52D881DD9C0A8F397C5_inline (List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * (*) (List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Void Ship::Delete()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ship_Delete_m7E022DC8EECBAB392587D14CC54C2245D12211AF (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Ship>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_mC9BE02B104DF58BE5358E859E4DFE2C936C4EFAD (List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m66148860899ECCAE9B323372032BFC1C255393D2_gshared)(__this, ___index0, method);
}
// System.Void NetworkManager::SendShips(System.Collections.Generic.List`1<Ship>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_SendShips_mB5DF67D6B2CD0624ED0D71ADF06780EBB288584B (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * ___list0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * GameObject_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16ABC1D7B4E804984517841F8D26E9C769FAE710 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void Cell::remove_CellHoverEvent(Cell/CellHoverEventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell_remove_CellHoverEvent_mCE5ADEEAF176E719EC9545C1D291E37E4FC81C3B (CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Ship>::.ctor()
inline void List_1__ctor_mCCD54CB0212EBDA5809120252A5F13BB00288588 (List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void Player::OnShipSelected(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_OnShipSelected_m07473B2D38372EE1CFBDA985E484A6B8B097AED0 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, int32_t ___size0, const RuntimeMethod* method);
// System.Void System.Console::WriteLine(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Console_WriteLine_mE9EEA95C541D41E36A0F4844153A67EAAA0D12F7 (String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Threading.Thread::Sleep(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Thread_Sleep_m8E61FC80BD38981CB18CA549909710790283DDCC (int32_t ___millisecondsTimeout0, const RuntimeMethod* method);
// System.Char[] System.String::ToCharArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* String_ToCharArray_m33E93AEB7086CBEBDFA5730EAAC49686F144089C (String_t* __this, const RuntimeMethod* method);
// System.ConsoleKeyInfo System.Console::ReadKey()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ConsoleKeyInfo_tDA8AC07839288484FCB167A81B4FBA92ECCEAF88  Console_ReadKey_mFB50402A43E0994EBC4CB125217B21F60F6AA07C (const RuntimeMethod* method);
// System.Type System.Exception::GetType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Exception_GetType_mC5B8B5C944B326B751282AB0E8C25A7F85457D9F (Exception_t * __this, const RuntimeMethod* method);
// System.Void ProtocolClient.NetMessage::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetMessage__ctor_mA4C1F498E1F2D598A9ACD6F14BBF921B3F639C7B (NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * __this, const RuntimeMethod* method);
// System.Byte ProtocolClient.ProtocolTool::GetCRC(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t ProtocolTool_GetCRC_mAAC3331A0C1C131150EE5196ECDFB30353E20590 (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data0, int32_t ___length1, int32_t ___startIndex2, const RuntimeMethod* method);
// System.Void System.Array::ConstrainedCopy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_ConstrainedCopy_mD26D19D1D515C4D884E36327A9B0C2BA79CD7003 (RuntimeArray * ___sourceArray0, int32_t ___sourceIndex1, RuntimeArray * ___destinationArray2, int32_t ___destinationIndex3, int32_t ___length4, const RuntimeMethod* method);
// System.Int32 System.Linq.Enumerable::Count<System.Byte>(System.Collections.Generic.IEnumerable`1<!!0>)
inline int32_t Enumerable_Count_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m1AFC4338BF90E364F564717BDBFDD9D1217C2980 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_Count_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m1AFC4338BF90E364F564717BDBFDD9D1217C2980_gshared)(___source0, method);
}
// !!0 System.Linq.Enumerable::ElementAt<System.Byte>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
inline uint8_t Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4 (RuntimeObject* ___source0, int32_t ___index1, const RuntimeMethod* method)
{
	return ((  uint8_t (*) (RuntimeObject*, int32_t, const RuntimeMethod*))Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_gshared)(___source0, ___index1, method);
}
// !!0[] System.Linq.Enumerable::ToArray<System.Byte>(System.Collections.Generic.IEnumerable`1<!!0>)
inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* Enumerable_ToArray_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m00F16F6DDED1338D9656B4EDE7F8AE2B2E2251E4 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m00F16F6DDED1338D9656B4EDE7F8AE2B2E2251E4_gshared)(___source0, method);
}
// System.Void Ship::set_size(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Ship_set_size_mB69F1B1C31D843999D4B226F7A54D53CCEB8EB11_inline (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void Ship::set_cells(UnityEngine.GameObject[])
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Ship_set_cells_m62750C426A0B1E8A06CAA44EE64B6C00F6070949_inline (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___value0, const RuntimeMethod* method);
// UnityEngine.GameObject[] Ship::get_cells()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* Ship_get_cells_m3B9D3AF56A593555CCF9630DFE9D766BB763A91A_inline (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method);
// System.Boolean ProtocolClient.TCPConnection::IsConnected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TCPConnection_IsConnected_m4D516CEE807E45D142058548598798839D2CD523 (TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * __this, const RuntimeMethod* method);
// System.Void ProtocolClient.NetSendBuffer::SetConnection(ProtocolClient.TCPConnection)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetSendBuffer_SetConnection_mCE9EB7BFBA86CCB47B22217122D58D82C9E595F8_inline (NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * __this, TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * ___newConnection0, const RuntimeMethod* method);
// System.Boolean ProtocolClient.TCPConnection::ConnectToRemoteHost(System.String,System.UInt16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TCPConnection_ConnectToRemoteHost_m48C2352E350516CF0CA667BFD6CC6523392429F0 (TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * __this, String_t* ___ip0, uint16_t ___port1, const RuntimeMethod* method);
// System.DateTime System.DateTime::get_Now()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  DateTime_get_Now_mCAC695993D6E2C57B900C83BEF3F8B18BC4EBC2C (const RuntimeMethod* method);
// System.Void ProtocolClient.NetReceiveBuffer::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetReceiveBuffer_Reset_m54E919D930053D4AF69BBA24FA0EBE4FE5DB1C93 (NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * __this, const RuntimeMethod* method);
// System.Boolean ProtocolClient.TCPConnection::CloseConnection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TCPConnection_CloseConnection_mDFDCFAE5342A9BADB24EFB94979FDB7DAEF6FEB9 (TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * __this, const RuntimeMethod* method);
// System.Void ProtocolClient.IClient::InvokeServerResponseEvent(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_InvokeServerResponseEvent_m9B0164790F2DA73105E5667A06B83270466C9E5D (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___dataToSend0, const RuntimeMethod* method);
// System.Void ProtocolClient.IClient::InvokeServerRequestEvent(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_InvokeServerRequestEvent_m0D9439F86E2BBB0D74AAE21C75DE2F4A9520D328 (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___dataToSend0, const RuntimeMethod* method);
// System.Void ProtocolClient.IClient::InvokeServerStatusUpdateEvent(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_InvokeServerStatusUpdateEvent_mF960B2A19E3CD03F38AE28F4E2A3B35D31F58A6C (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___dataToSend0, const RuntimeMethod* method);
// System.Byte[] System.BitConverter::GetBytes(System.Int16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* BitConverter_GetBytes_m4178DFE7B47F12FAF452AA4742B16FC3BA9618A8 (int16_t ___value0, const RuntimeMethod* method);
// System.Byte[] ProtocolClient.TCPConnection::ReadAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* TCPConnection_ReadAll_m67CC289F1F234B79CF29BBB18578C84B3A9C328A (TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * __this, const RuntimeMethod* method);
// ProtocolClient.NetMessage ProtocolClient.ProtocolTool::CreateMessage(ProtocolClient.NetMessageType,System.Byte[],System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ProtocolTool_CreateMessage_m441288169BBE1CC1C48836E7755683B5A637C2E5 (int32_t ___messageType0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___payload1, uint8_t ___clientId2, const RuntimeMethod* method);
// System.Boolean ProtocolClient.NetSendBuffer::SendNetMessage(ProtocolClient.NetMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NetSendBuffer_SendNetMessage_m1F1D322E9AA41FCA8BC955B3F53F9F0FC2FB0817 (NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * __this, NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ___toSend0, const RuntimeMethod* method);
// ProtocolClient.NetMessage ProtocolClient.NetReceiveBuffer::TakeNextNetMessageById(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * NetReceiveBuffer_TakeNextNetMessageById_m14D6F45C0DC5E5EB6E95C8E7FBF746BE6F4AE24C (NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * __this, uint8_t ___id0, const RuntimeMethod* method);
// System.Void ProtocolClient.NetReceiveBuffer::AddDataToBuffer(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetReceiveBuffer_AddDataToBuffer_mEF538D68769E179CD93D8D5E80144EA57405E06C (NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___bufferData0, const RuntimeMethod* method);
// System.Void ProtocolClient.NetReceiveBuffer::SearchInBuffer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetReceiveBuffer_SearchInBuffer_m825A53363B87F61FB7EE0CA4D76E550C6F2B6690 (NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * __this, const RuntimeMethod* method);
// System.Void ProtocolClient.TCPClient::HandleNewNetMessages()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_HandleNewNetMessages_mC1D0A4F5F587F147D7CE405BEBCD6B40B61E2FFD (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, const RuntimeMethod* method);
// System.Exception ProtocolClient.TCPConnection::GetLastException()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Exception_t * TCPConnection_GetLastException_m4312EE21BDC7253C84B58F1889624532F658C4A6_inline (TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * __this, const RuntimeMethod* method);
// System.Void ProtocolClient.TCPConnection::ResetLastException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPConnection_ResetLastException_m67BB221266DB25CC43F373CB1ED5A4347D08A87C (TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * __this, const RuntimeMethod* method);
// System.Void ProtocolClient.IClient::InvokeExceptionEvent(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_InvokeExceptionEvent_mE37866660EDE33F55DA29AA097356B87AB12B898 (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, Exception_t * ___exceptionToSend0, const RuntimeMethod* method);
// System.TimeSpan System.DateTime::Subtract(System.DateTime)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  DateTime_Subtract_mB3CA7BD87D07F9A98AA7A571D7CA179A4209AE0B (DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * __this, DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___value0, const RuntimeMethod* method);
// System.Double System.TimeSpan::get_TotalMilliseconds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double TimeSpan_get_TotalMilliseconds_m97368AE0609D865EB2A6BAE96AAA97AF8BDBF1C5 (TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * __this, const RuntimeMethod* method);
// System.Void ProtocolClient.TCPClient::IncrementNetMessageId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_IncrementNetMessageId_mBB6ED4FEBC70571FFDEE75AC13C1E16A9A18C4AE (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, const RuntimeMethod* method);
// System.Void System.NotImplementedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotImplementedException__ctor_mA2E9CE7F00CB335581A296D2596082D57E45BA83 (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * __this, const RuntimeMethod* method);
// System.Void ProtocolClient.TCPConnection::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPConnection__ctor_mF343843AB3AD623C131AAF1A9721FE426E8F25DC (TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * __this, const RuntimeMethod* method);
// System.Void ProtocolClient.NetReceiveBuffer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetReceiveBuffer__ctor_mA9D01D5830750AA6550E569EE006BF2D61DE6B17 (NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * __this, const RuntimeMethod* method);
// System.Void ProtocolClient.NetSendBuffer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetSendBuffer__ctor_m450503519157A952342CDA00B9FD06B8C1730814 (NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * __this, const RuntimeMethod* method);
// System.Void ProtocolClient.IClient::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient__ctor_m0D07757F9C99DE9634940B5CC6F78547A53848E0 (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, const RuntimeMethod* method);
// System.Void System.Net.Sockets.Socket::.ctor(System.Net.Sockets.SocketType,System.Net.Sockets.ProtocolType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Socket__ctor_m46AAE859577BBEED1D6246CEB874F2DFDEEE295C (Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * __this, int32_t ___socketType0, int32_t ___protocolType1, const RuntimeMethod* method);
// System.Void System.Net.Sockets.Socket::set_NoDelay(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Socket_set_NoDelay_m34DCB8BF5275A85E1687308D0D016E860F229732 (Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Net.Sockets.Socket::set_SendTimeout(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Socket_set_SendTimeout_mD92AF13D6A05318E6F892F0B145136138E2181E5 (Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Boolean System.Net.Sockets.Socket::get_Connected()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Socket_get_Connected_m6E8C88AC69580EF7782514CD48754D4D180D9CE0_inline (Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * __this, const RuntimeMethod* method);
// System.Void System.Net.Sockets.Socket::Close()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Socket_Close_m24AB78F5DAC1C39BB7FFB30A9620B2B07E01DEEB (Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * __this, const RuntimeMethod* method);
// System.Void System.Net.Sockets.Socket::Connect(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Socket_Connect_m08347703B1E1E29B13D95F2719B76C7B2A51950A (Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * __this, String_t* ___host0, int32_t ___port1, const RuntimeMethod* method);
// System.Int32 System.Net.Sockets.Socket::Receive(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Socket_Receive_mD76EDDF7BFF5A9AB3D7FC197A13B8CF024DEC482 (Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, const RuntimeMethod* method);
// System.Void System.Array::Copy(System.Array,System.Array,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Copy_m40103AA97DC582C557B912CF4BBE86A4D166F803 (RuntimeArray * ___sourceArray0, RuntimeArray * ___destinationArray1, int32_t ___length2, const RuntimeMethod* method);
// System.Int32 System.Net.Sockets.Socket::Send(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Socket_Send_m8E974F1735436F40554635503D4E6C8EF387DA3D (Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Vector2Int::get_x()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector2Int_get_x_mDBEFBCDF9C7924767344ED2CEE1307885AED947B_inline (Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Vector2Int::get_y()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector2Int_get_y_m282591DEB0E70B02F4F4DDFAB90116AEC8E6B86C_inline (Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mFEA7EFA1A6E75B96B1B7BC4526AAC864BFF83CC9 (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___values0, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForFixedUpdate__ctor_mB566493FB99A315BEF348791DE943A89E4B71F9C (WaitForFixedUpdate_t675FCE2AEFAC5C924A4020474C997FF2CDD3F4C5 * __this, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Cell::add_CellHoverEvent(Cell/CellHoverEventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell_add_CellHoverEvent_m62C3C1BCCCDDF49C3D831FDA239E013B6988BE99 (CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * V_0 = NULL;
	CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * V_1 = NULL;
	CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_0 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields*)il2cpp_codegen_static_fields_for(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_CellHoverEvent_11();
		V_0 = L_0;
	}

IL_0006:
	{
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_1 = V_0;
		V_1 = L_1;
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_2 = V_1;
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A *)CastclassSealed((RuntimeObject*)L_4, CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_5 = V_2;
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_6 = V_1;
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_7;
		L_7 = InterlockedCompareExchangeImpl<CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A *>((CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A **)(((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields*)il2cpp_codegen_static_fields_for(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_address_of_CellHoverEvent_11()), L_5, L_6);
		V_0 = L_7;
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_8 = V_0;
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_9 = V_1;
		if ((!(((RuntimeObject*)(CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A *)L_8) == ((RuntimeObject*)(CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Cell::remove_CellHoverEvent(Cell/CellHoverEventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell_remove_CellHoverEvent_mCE5ADEEAF176E719EC9545C1D291E37E4FC81C3B (CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * V_0 = NULL;
	CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * V_1 = NULL;
	CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_0 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields*)il2cpp_codegen_static_fields_for(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_CellHoverEvent_11();
		V_0 = L_0;
	}

IL_0006:
	{
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_1 = V_0;
		V_1 = L_1;
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_2 = V_1;
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A *)CastclassSealed((RuntimeObject*)L_4, CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_5 = V_2;
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_6 = V_1;
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_7;
		L_7 = InterlockedCompareExchangeImpl<CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A *>((CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A **)(((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields*)il2cpp_codegen_static_fields_for(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_address_of_CellHoverEvent_11()), L_5, L_6);
		V_0 = L_7;
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_8 = V_0;
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_9 = V_1;
		if ((!(((RuntimeObject*)(CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A *)L_8) == ((RuntimeObject*)(CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Cell::add_CellClickedEvent(Cell/CellClickedEventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell_add_CellClickedEvent_m2F0AB758089BDF792D350B2DA4B5B6557E78D59F (CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * V_0 = NULL;
	CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * V_1 = NULL;
	CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_0 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields*)il2cpp_codegen_static_fields_for(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_CellClickedEvent_12();
		V_0 = L_0;
	}

IL_0006:
	{
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_1 = V_0;
		V_1 = L_1;
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_2 = V_1;
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 *)CastclassSealed((RuntimeObject*)L_4, CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_5 = V_2;
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_6 = V_1;
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_7;
		L_7 = InterlockedCompareExchangeImpl<CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 *>((CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 **)(((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields*)il2cpp_codegen_static_fields_for(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_address_of_CellClickedEvent_12()), L_5, L_6);
		V_0 = L_7;
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_8 = V_0;
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_9 = V_1;
		if ((!(((RuntimeObject*)(CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 *)L_8) == ((RuntimeObject*)(CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Cell::remove_CellClickedEvent(Cell/CellClickedEventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell_remove_CellClickedEvent_mACB5AB8439AB65417AF009E14D8172E610D9C402 (CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * V_0 = NULL;
	CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * V_1 = NULL;
	CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_0 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields*)il2cpp_codegen_static_fields_for(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_CellClickedEvent_12();
		V_0 = L_0;
	}

IL_0006:
	{
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_1 = V_0;
		V_1 = L_1;
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_2 = V_1;
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 *)CastclassSealed((RuntimeObject*)L_4, CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_5 = V_2;
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_6 = V_1;
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_7;
		L_7 = InterlockedCompareExchangeImpl<CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 *>((CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 **)(((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields*)il2cpp_codegen_static_fields_for(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_address_of_CellClickedEvent_12()), L_5, L_6);
		V_0 = L_7;
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_8 = V_0;
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_9 = V_1;
		if ((!(((RuntimeObject*)(CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 *)L_8) == ((RuntimeObject*)(CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Cell::RaiseCellHoverEvent(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell_RaiseCellHoverEvent_m4AD37176E69A4F5BE3203E49AC1D80C5EA76FB60 (Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * __this, bool ___hovered0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * G_B2_0 = NULL;
	CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * G_B1_0 = NULL;
	{
		// CellHoverEvent?.Invoke(this, new CellEventArgs(hovered));
		IL2CPP_RUNTIME_CLASS_INIT(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_0 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields*)il2cpp_codegen_static_fields_for(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_CellHoverEvent_11();
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000a;
		}
	}
	{
		return;
	}

IL_000a:
	{
		bool L_2 = ___hovered0;
		CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * L_3 = (CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 *)il2cpp_codegen_object_new(CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6_il2cpp_TypeInfo_var);
		CellEventArgs__ctor_m42CBF75EDC8AEB81C32D6A6299981BB5BE3481BC(L_3, L_2, /*hidden argument*/NULL);
		NullCheck(G_B2_0);
		CellHoverEventHandler_Invoke_m3617B3846462554FF0110231A58370BE5E446409(G_B2_0, __this, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Cell::RaiseCellClickedEvent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell_RaiseCellClickedEvent_mB23D573B3CF437B447E3329CD6087D445221CB7E (Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3F178296412650029A46BF49F4A367936D3796A2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74841B32DE49F25B899B48D56A5D35FDED7AD32F);
		s_Il2CppMethodInitialized = true;
	}
	CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * G_B2_0 = NULL;
	CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * G_B1_0 = NULL;
	{
		// CellClickedEvent?.Invoke(this, new CellEventArgs());
		IL2CPP_RUNTIME_CLASS_INIT(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_0 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields*)il2cpp_codegen_static_fields_for(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_CellClickedEvent_12();
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		goto IL_0016;
	}

IL_000b:
	{
		CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * L_2 = (CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 *)il2cpp_codegen_object_new(CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6_il2cpp_TypeInfo_var);
		CellEventArgs__ctor_m77B26E0518E7D0D30CD856D44ED172B4B096F60B(L_2, /*hidden argument*/NULL);
		NullCheck(G_B2_0);
		CellClickedEventHandler_Invoke_mA777B45CCDD94D5BE52E8BC63679ACF954DC4515(G_B2_0, __this, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		// Debug.Log("Cell Clicked x: " + xIndex + " y: " + yIndex);
		int32_t* L_3 = __this->get_address_of_xIndex_4();
		String_t* L_4;
		L_4 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_3, /*hidden argument*/NULL);
		int32_t* L_5 = __this->get_address_of_yIndex_5();
		String_t* L_6;
		L_6 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_5, /*hidden argument*/NULL);
		String_t* L_7;
		L_7 = String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78(_stringLiteral74841B32DE49F25B899B48D56A5D35FDED7AD32F, L_4, _stringLiteral3F178296412650029A46BF49F4A367936D3796A2, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Cell::OnMouseDown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell_OnMouseDown_m3E6D6A41C9F25AE5727461FB513AD77490E35B31 (Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * __this, const RuntimeMethod* method)
{
	{
		// RaiseCellClickedEvent();
		Cell_RaiseCellClickedEvent_mB23D573B3CF437B447E3329CD6087D445221CB7E(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Cell::OnMouseEnter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell_OnMouseEnter_m8FF697CD2B368D468228474E1BC3413F0ED87E0E (Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(Player.playerIsPlacing)
		IL2CPP_RUNTIME_CLASS_INIT(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		bool L_0 = ((Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields*)il2cpp_codegen_static_fields_for(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var))->get_playerIsPlacing_27();
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		// RaiseCellHoverEvent(true);
		Cell_RaiseCellHoverEvent_m4AD37176E69A4F5BE3203E49AC1D80C5EA76FB60(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_000e:
	{
		// }
		return;
	}
}
// System.Void Cell::OnMouseExit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell_OnMouseExit_mF19FDB97E04ABE368255FBA7190F1EE5CBAAF396 (Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Player.playerIsPlacing)
		IL2CPP_RUNTIME_CLASS_INIT(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		bool L_0 = ((Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields*)il2cpp_codegen_static_fields_for(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var))->get_playerIsPlacing_27();
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		// RaiseCellHoverEvent(false);
		Cell_RaiseCellHoverEvent_m4AD37176E69A4F5BE3203E49AC1D80C5EA76FB60(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_000e:
	{
		// }
		return;
	}
}
// System.Void Cell::SetIndexes(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell_SetIndexes_mB97C2F2C7D4CEE6478CD0F5B4A52A8231ED69459 (Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * __this, int32_t ___y0, int32_t ___x1, const RuntimeMethod* method)
{
	{
		// yIndex = y;
		int32_t L_0 = ___y0;
		__this->set_yIndex_5(L_0);
		// xIndex = x;
		int32_t L_1 = ___x1;
		__this->set_xIndex_4(L_1);
		// }
		return;
	}
}
// System.Void Cell::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell__ctor_m8A64B8FBB74A0ECF5EE0871E80CD28E644CBAAF7 (Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell__cctor_m1D14F72D9A407DAAA7C0DD55C80DC5FE02ABC734 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static readonly Color standardColor = new Color(1, 1, 1);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494((&L_0), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields*)il2cpp_codegen_static_fields_for(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->set_standardColor_9(L_0);
		// public static readonly Color markedColor = new Color(1, 131/255f, 122/255f);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494((&L_1), (1.0f), (0.513725519f), (0.478431374f), /*hidden argument*/NULL);
		((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields*)il2cpp_codegen_static_fields_for(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->set_markedColor_10(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CellEventArgs::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellEventArgs__ctor_m77B26E0518E7D0D30CD856D44ED172B4B096F60B (CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * __this, const RuntimeMethod* method)
{
	{
		// public CellEventArgs() { }
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// public CellEventArgs() { }
		return;
	}
}
// System.Void CellEventArgs::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellEventArgs__ctor_m42CBF75EDC8AEB81C32D6A6299981BB5BE3481BC (CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * __this, bool ___hovered0, const RuntimeMethod* method)
{
	{
		// public CellEventArgs(bool hovered)
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// Hovered = hovered;
		bool L_0 = ___hovered0;
		CellEventArgs_set_Hovered_mED2C25ABB5B9CFDB913D36CE1E569EBB01E19EC0_inline(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean CellEventArgs::get_Hovered()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CellEventArgs_get_Hovered_m2C28642E496CC89649BE2B23DAFAF606407BD189 (CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * __this, const RuntimeMethod* method)
{
	{
		// public bool Hovered { get; private set; }
		bool L_0 = __this->get_U3CHoveredU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void CellEventArgs::set_Hovered(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellEventArgs_set_Hovered_mED2C25ABB5B9CFDB913D36CE1E569EBB01E19EC0 (CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool Hovered { get; private set; }
		bool L_0 = ___value0;
		__this->set_U3CHoveredU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnemyField::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyField_Start_mCDC39F0FF180625E36308DE521FCF42567D0A7AA (EnemyField_t935BD6B66CC4595689CD03E7600CAFA8B3893784 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4C1398A837E1F0C532DEE7917D63C248C072132F);
		s_Il2CppMethodInitialized = true;
	}
	{
		// InitializeCells();
		EnemyField_InitializeCells_mDCDC5379AA9F15B92BC0DC7FCE1598BC127743D5(__this, /*hidden argument*/NULL);
		// hitImage = Resources.Load("textures/hit") as Texture2D;
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_0;
		L_0 = Resources_Load_m011631B3740AFD38D496838F10D3DA635A061120(_stringLiteral4C1398A837E1F0C532DEE7917D63C248C072132F, /*hidden argument*/NULL);
		((Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_StaticFields*)il2cpp_codegen_static_fields_for(Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_il2cpp_TypeInfo_var))->set_hitImage_4(((Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *)IsInstSealed((RuntimeObject*)L_0, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var)));
		// }
		return;
	}
}
// System.Void EnemyField::InitializeCells()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyField_InitializeCells_mDCDC5379AA9F15B92BC0DC7FCE1598BC127743D5 (EnemyField_t935BD6B66CC4595689CD03E7600CAFA8B3893784 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisBoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_mAA495A9AAD3FB4D260B3E2B6DDBAFEC18DC49C3C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral50639CAD49418C7B223CC529395C0E2A3892501C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral70BF4C9D283826DF9CDEF1AD58DD09DC37CC109D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral84908AE02AE2F5956C34FD61A53E6762038E2440);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_2 = NULL;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_5;
	memset((&V_5), 0, sizeof(V_5));
	int32_t V_6 = 0;
	{
		// for (int i = 0; i < 10; i++)
		V_0 = 0;
		goto IL_0132;
	}

IL_0007:
	{
		// for (int j = 0; j < 10; j++)
		V_1 = 0;
		goto IL_0126;
	}

IL_000e:
	{
		// GameObject go = Instantiate((GameObject)Resources.Load("prefab/Cell_X_X"));
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_0;
		L_0 = Resources_Load_m011631B3740AFD38D496838F10D3DA635A061120(_stringLiteral84908AE02AE2F5956C34FD61A53E6762038E2440, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(((GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)CastclassSealed((RuntimeObject*)L_0, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var)), /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		V_2 = L_1;
		// BoxCollider2D bc = go.GetComponent<BoxCollider2D>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = V_2;
		NullCheck(L_2);
		BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * L_3;
		L_3 = GameObject_GetComponent_TisBoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_mAA495A9AAD3FB4D260B3E2B6DDBAFEC18DC49C3C(L_2, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_mAA495A9AAD3FB4D260B3E2B6DDBAFEC18DC49C3C_RuntimeMethod_var);
		// go.name = "Cell_" + (i + 1) + "_" + (j + 1);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = V_2;
		int32_t L_5 = V_0;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
		String_t* L_6;
		L_6 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_6), /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
		String_t* L_8;
		L_8 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_6), /*hidden argument*/NULL);
		String_t* L_9;
		L_9 = String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78(_stringLiteral70BF4C9D283826DF9CDEF1AD58DD09DC37CC109D, L_6, _stringLiteral50639CAD49418C7B223CC529395C0E2A3892501C, L_8, /*hidden argument*/NULL);
		NullCheck(L_4);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(L_4, L_9, /*hidden argument*/NULL);
		// Cell c = go.GetComponent<Cell>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10 = V_2;
		NullCheck(L_10);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_11;
		L_11 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_10, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		// c.SetIndexes(i, j);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_12 = L_11;
		int32_t L_13 = V_0;
		int32_t L_14 = V_1;
		NullCheck(L_12);
		Cell_SetIndexes_mB97C2F2C7D4CEE6478CD0F5B4A52A8231ED69459(L_12, L_13, L_14, /*hidden argument*/NULL);
		// c.isEnemy = true;
		NullCheck(L_12);
		L_12->set_isEnemy_8((bool)1);
		// go.transform.SetParent(gameObject.transform);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15 = V_2;
		NullCheck(L_15);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16;
		L_16 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_15, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17;
		L_17 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_18;
		L_18 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E(L_16, L_18, /*hidden argument*/NULL);
		// float xpos = posx0 + j * totalcellwidth;
		float L_19 = ((Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 *)__this)->get_posx0_8();
		int32_t L_20 = V_1;
		float L_21 = ((Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 *)__this)->get_totalcellwidth_6();
		V_3 = ((float)il2cpp_codegen_add((float)L_19, (float)((float)il2cpp_codegen_multiply((float)((float)((float)L_20)), (float)L_21))));
		// float ypos = posy0 - i * totalcellheight;
		float L_22 = ((Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 *)__this)->get_posy0_9();
		int32_t L_23 = V_0;
		float L_24 = ((Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 *)__this)->get_totalcellheight_7();
		V_4 = ((float)il2cpp_codegen_subtract((float)L_22, (float)((float)il2cpp_codegen_multiply((float)((float)((float)L_23)), (float)L_24))));
		// Vector3 position = new Vector3(xpos, ypos);
		float L_25 = V_3;
		float L_26 = V_4;
		Vector3__ctor_mF7FCDE24496D619F4BB1A0BA44AF17DCB5D697FF_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_5), L_25, L_26, /*hidden argument*/NULL);
		// ((RectTransform)go.transform).sizeDelta = new Vector2(cellwidth, cellwidth);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_27 = V_2;
		NullCheck(L_27);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_28;
		L_28 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_27, /*hidden argument*/NULL);
		int32_t L_29 = ((Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 *)__this)->get_cellwidth_5();
		int32_t L_30 = ((Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 *)__this)->get_cellwidth_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_31;
		memset((&L_31), 0, sizeof(L_31));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_31), ((float)((float)L_29)), ((float)((float)L_30)), /*hidden argument*/NULL);
		NullCheck(((RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 *)CastclassSealed((RuntimeObject*)L_28, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var)));
		RectTransform_set_sizeDelta_m61943618442E31C6FF0556CDFC70940AE7AD04D0(((RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 *)CastclassSealed((RuntimeObject*)L_28, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var)), L_31, /*hidden argument*/NULL);
		// ((RectTransform)go.transform).localScale = new Vector3(1, 1);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_32 = V_2;
		NullCheck(L_32);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_33;
		L_33 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_32, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_34;
		memset((&L_34), 0, sizeof(L_34));
		Vector3__ctor_mF7FCDE24496D619F4BB1A0BA44AF17DCB5D697FF_inline((&L_34), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(((RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 *)CastclassSealed((RuntimeObject*)L_33, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var)));
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(((RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 *)CastclassSealed((RuntimeObject*)L_33, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var)), L_34, /*hidden argument*/NULL);
		// bc.size = new Vector2(cellwidth, cellwidth);
		int32_t L_35 = ((Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 *)__this)->get_cellwidth_5();
		int32_t L_36 = ((Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 *)__this)->get_cellwidth_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_37;
		memset((&L_37), 0, sizeof(L_37));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_37), ((float)((float)L_35)), ((float)((float)L_36)), /*hidden argument*/NULL);
		NullCheck(L_3);
		BoxCollider2D_set_size_m8460A38ADDD4BE82BE1F416DE3D7AFB87EBA6760(L_3, L_37, /*hidden argument*/NULL);
		// go.transform.localPosition = position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_38 = V_2;
		NullCheck(L_38);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_39;
		L_39 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_38, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_40 = V_5;
		NullCheck(L_39);
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_39, L_40, /*hidden argument*/NULL);
		// cells[i, j] = go;
		GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_41 = ((Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 *)__this)->get_cells_10();
		int32_t L_42 = V_0;
		int32_t L_43 = V_1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_44 = V_2;
		NullCheck(L_41);
		(L_41)->SetAt(L_42, L_43, L_44);
		// for (int j = 0; j < 10; j++)
		int32_t L_45 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_45, (int32_t)1));
	}

IL_0126:
	{
		// for (int j = 0; j < 10; j++)
		int32_t L_46 = V_1;
		if ((((int32_t)L_46) < ((int32_t)((int32_t)10))))
		{
			goto IL_000e;
		}
	}
	{
		// for (int i = 0; i < 10; i++)
		int32_t L_47 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_47, (int32_t)1));
	}

IL_0132:
	{
		// for (int i = 0; i < 10; i++)
		int32_t L_48 = V_0;
		if ((((int32_t)L_48) < ((int32_t)((int32_t)10))))
		{
			goto IL_0007;
		}
	}
	{
		// }
		return;
	}
}
// System.Void EnemyField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyField__ctor_mF63B12AA5DBF59910F390E93614C11C72B9C6A1D (EnemyField_t935BD6B66CC4595689CD03E7600CAFA8B3893784 * __this, const RuntimeMethod* method)
{
	{
		Field__ctor_m850074110EC0DC28CBDCF47344CC06B3C7CA3F03(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Field::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Field_Start_m8DCCF1A66DB33A7821A590994D8F79043580A310 (Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4C1398A837E1F0C532DEE7917D63C248C072132F);
		s_Il2CppMethodInitialized = true;
	}
	{
		// InitializeCells();
		Field_InitializeCells_m373BF5F93A8819CCD48A7A35E5C81312DA8CC6A7(__this, /*hidden argument*/NULL);
		// hitImage = Resources.Load("textures/hit") as Texture2D;
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_0;
		L_0 = Resources_Load_m011631B3740AFD38D496838F10D3DA635A061120(_stringLiteral4C1398A837E1F0C532DEE7917D63C248C072132F, /*hidden argument*/NULL);
		((Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_StaticFields*)il2cpp_codegen_static_fields_for(Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_il2cpp_TypeInfo_var))->set_hitImage_4(((Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *)IsInstSealed((RuntimeObject*)L_0, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var)));
		// }
		return;
	}
}
// System.Void Field::InitializeCells()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Field_InitializeCells_m373BF5F93A8819CCD48A7A35E5C81312DA8CC6A7 (Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisBoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_mAA495A9AAD3FB4D260B3E2B6DDBAFEC18DC49C3C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral50639CAD49418C7B223CC529395C0E2A3892501C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral70BF4C9D283826DF9CDEF1AD58DD09DC37CC109D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral84908AE02AE2F5956C34FD61A53E6762038E2440);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_2 = NULL;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_5;
	memset((&V_5), 0, sizeof(V_5));
	int32_t V_6 = 0;
	{
		// for(int i = 0; i < 10; i++)
		V_0 = 0;
		goto IL_012b;
	}

IL_0007:
	{
		// for(int j = 0; j < 10; j++)
		V_1 = 0;
		goto IL_011f;
	}

IL_000e:
	{
		// GameObject go = Instantiate((GameObject)Resources.Load("prefab/Cell_X_X"));
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_0;
		L_0 = Resources_Load_m011631B3740AFD38D496838F10D3DA635A061120(_stringLiteral84908AE02AE2F5956C34FD61A53E6762038E2440, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(((GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)CastclassSealed((RuntimeObject*)L_0, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var)), /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		V_2 = L_1;
		// BoxCollider2D bc = go.GetComponent<BoxCollider2D>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = V_2;
		NullCheck(L_2);
		BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * L_3;
		L_3 = GameObject_GetComponent_TisBoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_mAA495A9AAD3FB4D260B3E2B6DDBAFEC18DC49C3C(L_2, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_mAA495A9AAD3FB4D260B3E2B6DDBAFEC18DC49C3C_RuntimeMethod_var);
		// go.name = "Cell_" + (i + 1) + "_" + (j + 1);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = V_2;
		int32_t L_5 = V_0;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
		String_t* L_6;
		L_6 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_6), /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
		String_t* L_8;
		L_8 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_6), /*hidden argument*/NULL);
		String_t* L_9;
		L_9 = String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78(_stringLiteral70BF4C9D283826DF9CDEF1AD58DD09DC37CC109D, L_6, _stringLiteral50639CAD49418C7B223CC529395C0E2A3892501C, L_8, /*hidden argument*/NULL);
		NullCheck(L_4);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(L_4, L_9, /*hidden argument*/NULL);
		// go.GetComponent<Cell>().SetIndexes(i, j);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10 = V_2;
		NullCheck(L_10);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_11;
		L_11 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_10, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		int32_t L_12 = V_0;
		int32_t L_13 = V_1;
		NullCheck(L_11);
		Cell_SetIndexes_mB97C2F2C7D4CEE6478CD0F5B4A52A8231ED69459(L_11, L_12, L_13, /*hidden argument*/NULL);
		// go.transform.SetParent(gameObject.transform);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14 = V_2;
		NullCheck(L_14);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_15;
		L_15 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_14, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_16;
		L_16 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17;
		L_17 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E(L_15, L_17, /*hidden argument*/NULL);
		// float xpos = posx0 + j * totalcellwidth;
		float L_18 = __this->get_posx0_8();
		int32_t L_19 = V_1;
		float L_20 = __this->get_totalcellwidth_6();
		V_3 = ((float)il2cpp_codegen_add((float)L_18, (float)((float)il2cpp_codegen_multiply((float)((float)((float)L_19)), (float)L_20))));
		// float ypos = posy0 - i * totalcellheight;
		float L_21 = __this->get_posy0_9();
		int32_t L_22 = V_0;
		float L_23 = __this->get_totalcellheight_7();
		V_4 = ((float)il2cpp_codegen_subtract((float)L_21, (float)((float)il2cpp_codegen_multiply((float)((float)((float)L_22)), (float)L_23))));
		// Vector3 position = new Vector3(xpos, ypos);
		float L_24 = V_3;
		float L_25 = V_4;
		Vector3__ctor_mF7FCDE24496D619F4BB1A0BA44AF17DCB5D697FF_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_5), L_24, L_25, /*hidden argument*/NULL);
		// ((RectTransform)go.transform).sizeDelta = new Vector2(cellwidth, cellwidth);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_26 = V_2;
		NullCheck(L_26);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_27;
		L_27 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_26, /*hidden argument*/NULL);
		int32_t L_28 = __this->get_cellwidth_5();
		int32_t L_29 = __this->get_cellwidth_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_30;
		memset((&L_30), 0, sizeof(L_30));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_30), ((float)((float)L_28)), ((float)((float)L_29)), /*hidden argument*/NULL);
		NullCheck(((RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 *)CastclassSealed((RuntimeObject*)L_27, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var)));
		RectTransform_set_sizeDelta_m61943618442E31C6FF0556CDFC70940AE7AD04D0(((RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 *)CastclassSealed((RuntimeObject*)L_27, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var)), L_30, /*hidden argument*/NULL);
		// ((RectTransform)go.transform).localScale = new Vector3(1, 1);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_31 = V_2;
		NullCheck(L_31);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_32;
		L_32 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_31, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33;
		memset((&L_33), 0, sizeof(L_33));
		Vector3__ctor_mF7FCDE24496D619F4BB1A0BA44AF17DCB5D697FF_inline((&L_33), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(((RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 *)CastclassSealed((RuntimeObject*)L_32, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var)));
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(((RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 *)CastclassSealed((RuntimeObject*)L_32, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var)), L_33, /*hidden argument*/NULL);
		// bc.size = new Vector2(cellwidth, cellwidth);
		int32_t L_34 = __this->get_cellwidth_5();
		int32_t L_35 = __this->get_cellwidth_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_36;
		memset((&L_36), 0, sizeof(L_36));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_36), ((float)((float)L_34)), ((float)((float)L_35)), /*hidden argument*/NULL);
		NullCheck(L_3);
		BoxCollider2D_set_size_m8460A38ADDD4BE82BE1F416DE3D7AFB87EBA6760(L_3, L_36, /*hidden argument*/NULL);
		// go.transform.localPosition = position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_37 = V_2;
		NullCheck(L_37);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_38;
		L_38 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_37, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_39 = V_5;
		NullCheck(L_38);
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_38, L_39, /*hidden argument*/NULL);
		// cells[i, j] = go;
		GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_40 = __this->get_cells_10();
		int32_t L_41 = V_0;
		int32_t L_42 = V_1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_43 = V_2;
		NullCheck(L_40);
		(L_40)->SetAt(L_41, L_42, L_43);
		// for(int j = 0; j < 10; j++)
		int32_t L_44 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_44, (int32_t)1));
	}

IL_011f:
	{
		// for(int j = 0; j < 10; j++)
		int32_t L_45 = V_1;
		if ((((int32_t)L_45) < ((int32_t)((int32_t)10))))
		{
			goto IL_000e;
		}
	}
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_46 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_46, (int32_t)1));
	}

IL_012b:
	{
		// for(int i = 0; i < 10; i++)
		int32_t L_47 = V_0;
		if ((((int32_t)L_47) < ((int32_t)((int32_t)10))))
		{
			goto IL_0007;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Field::OnCellHit(System.Int32,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Field_OnCellHit_mB36A003572E4C64763E34AC8E0CCA3678602967E (Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * __this, int32_t ___y0, int32_t ___x1, bool ___hit2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * G_B2_0 = NULL;
	RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * G_B1_0 = NULL;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  G_B3_0;
	memset((&G_B3_0), 0, sizeof(G_B3_0));
	RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * G_B3_1 = NULL;
	{
		// RawImage ri = cells[y, x].GetComponent<RawImage>();
		GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_0 = __this->get_cells_10();
		int32_t L_1 = ___y0;
		int32_t L_2 = ___x1;
		NullCheck(L_0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3;
		L_3 = (L_0)->GetAt(L_1, L_2);
		NullCheck(L_3);
		RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * L_4;
		L_4 = GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7(L_3, /*hidden argument*/GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7_RuntimeMethod_var);
		// ri.texture = hitImage;
		RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * L_5 = L_4;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_6 = ((Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_StaticFields*)il2cpp_codegen_static_fields_for(Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_il2cpp_TypeInfo_var))->get_hitImage_4();
		NullCheck(L_5);
		RawImage_set_texture_m1D7BAE6CB629C36894B664D9F5D68CACA88B8D99(L_5, L_6, /*hidden argument*/NULL);
		// ri.color = hit ? Ship.color : Cell.standardColor;
		bool L_7 = ___hit2;
		G_B1_0 = L_5;
		if (L_7)
		{
			G_B2_0 = L_5;
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_8 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields*)il2cpp_codegen_static_fields_for(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_standardColor_9();
		G_B3_0 = L_8;
		G_B3_1 = G_B1_0;
		goto IL_002c;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E_il2cpp_TypeInfo_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_9 = ((Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E_StaticFields*)il2cpp_codegen_static_fields_for(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E_il2cpp_TypeInfo_var))->get_color_0();
		G_B3_0 = L_9;
		G_B3_1 = G_B2_0;
	}

IL_002c:
	{
		NullCheck(G_B3_1);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, G_B3_1, G_B3_0);
		// cells[y, x].GetComponent<Cell>().isShot = true;
		GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_10 = __this->get_cells_10();
		int32_t L_11 = ___y0;
		int32_t L_12 = ___x1;
		NullCheck(L_10);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_13;
		L_13 = (L_10)->GetAt(L_11, L_12);
		NullCheck(L_13);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_14;
		L_14 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_13, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		NullCheck(L_14);
		L_14->set_isShot_7((bool)1);
		// }
		return;
	}
}
// System.Void Field::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Field__ctor_m850074110EC0DC28CBDCF47344CC06B3C7CA3F03 (Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// protected readonly int cellwidth = 77;
		__this->set_cellwidth_5(((int32_t)77));
		// protected readonly float totalcellwidth = 80.58f;
		__this->set_totalcellwidth_6((80.5800018f));
		// protected readonly float totalcellheight = 80.48f;
		__this->set_totalcellheight_7((80.4800034f));
		// protected readonly float posx0 = -323.48f;
		__this->set_posx0_8((-323.480011f));
		// protected readonly float posy0 = 324.48f;
		__this->set_posy0_9((324.480011f));
		// public GameObject[,] cells = new GameObject[10, 10];
		il2cpp_array_size_t L_1[] = { (il2cpp_array_size_t)((int32_t)10), (il2cpp_array_size_t)((int32_t)10) };
		GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_0 = (GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9*)GenArrayNew(GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9_il2cpp_TypeInfo_var, L_1);
		__this->set_cells_10(L_0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ProtocolClient.IClient::add_ServerRequestEvent(System.EventHandler`1<System.Byte[]>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_add_ServerRequestEvent_m97136FA563855AF87640E7865F21753CD3E10115 (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * V_0 = NULL;
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * V_1 = NULL;
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * V_2 = NULL;
	{
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_0 = __this->get_ServerRequestEvent_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_2 = V_1;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var));
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 ** L_5 = __this->get_address_of_ServerRequestEvent_0();
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_6 = V_2;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_7 = V_1;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_8;
		L_8 = InterlockedCompareExchangeImpl<EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *>((EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_9 = V_0;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)L_9) == ((RuntimeObject*)(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ProtocolClient.IClient::remove_ServerRequestEvent(System.EventHandler`1<System.Byte[]>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_remove_ServerRequestEvent_mB4A54D2F46DB957DD2A147A80A70AB92A3347515 (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * V_0 = NULL;
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * V_1 = NULL;
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * V_2 = NULL;
	{
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_0 = __this->get_ServerRequestEvent_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_2 = V_1;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var));
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 ** L_5 = __this->get_address_of_ServerRequestEvent_0();
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_6 = V_2;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_7 = V_1;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_8;
		L_8 = InterlockedCompareExchangeImpl<EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *>((EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_9 = V_0;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)L_9) == ((RuntimeObject*)(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ProtocolClient.IClient::add_ServerResponseEvent(System.EventHandler`1<System.Byte[]>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_add_ServerResponseEvent_mFF1D827B95AA6BF11F7AB5C97F77340DA5B1692C (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * V_0 = NULL;
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * V_1 = NULL;
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * V_2 = NULL;
	{
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_0 = __this->get_ServerResponseEvent_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_2 = V_1;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var));
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 ** L_5 = __this->get_address_of_ServerResponseEvent_1();
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_6 = V_2;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_7 = V_1;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_8;
		L_8 = InterlockedCompareExchangeImpl<EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *>((EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_9 = V_0;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)L_9) == ((RuntimeObject*)(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ProtocolClient.IClient::remove_ServerResponseEvent(System.EventHandler`1<System.Byte[]>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_remove_ServerResponseEvent_mA823C49285E37C297DB68A9FF3C44C3A28B23D33 (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * V_0 = NULL;
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * V_1 = NULL;
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * V_2 = NULL;
	{
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_0 = __this->get_ServerResponseEvent_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_2 = V_1;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var));
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 ** L_5 = __this->get_address_of_ServerResponseEvent_1();
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_6 = V_2;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_7 = V_1;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_8;
		L_8 = InterlockedCompareExchangeImpl<EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *>((EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_9 = V_0;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)L_9) == ((RuntimeObject*)(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ProtocolClient.IClient::add_ServerStatusUpdateEvent(System.EventHandler`1<System.Byte[]>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_add_ServerStatusUpdateEvent_m874656AF3D714B25C12F7CF629134ACF4830CEE0 (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * V_0 = NULL;
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * V_1 = NULL;
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * V_2 = NULL;
	{
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_0 = __this->get_ServerStatusUpdateEvent_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_2 = V_1;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var));
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 ** L_5 = __this->get_address_of_ServerStatusUpdateEvent_2();
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_6 = V_2;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_7 = V_1;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_8;
		L_8 = InterlockedCompareExchangeImpl<EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *>((EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_9 = V_0;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)L_9) == ((RuntimeObject*)(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ProtocolClient.IClient::remove_ServerStatusUpdateEvent(System.EventHandler`1<System.Byte[]>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_remove_ServerStatusUpdateEvent_m82AA4C8B7534F8EF8AEB54F0AE9EADDE5B50E4F3 (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * V_0 = NULL;
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * V_1 = NULL;
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * V_2 = NULL;
	{
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_0 = __this->get_ServerStatusUpdateEvent_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_2 = V_1;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var));
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 ** L_5 = __this->get_address_of_ServerStatusUpdateEvent_2();
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_6 = V_2;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_7 = V_1;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_8;
		L_8 = InterlockedCompareExchangeImpl<EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *>((EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_9 = V_0;
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)L_9) == ((RuntimeObject*)(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ProtocolClient.IClient::add_ConnectionException(System.EventHandler`1<System.Exception>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_add_ConnectionException_mE43AA0290A2010DBF9451129E56253D50563F0B3 (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * V_0 = NULL;
	EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * V_1 = NULL;
	EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * V_2 = NULL;
	{
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_0 = __this->get_ConnectionException_3();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_2 = V_1;
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D_il2cpp_TypeInfo_var));
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D ** L_5 = __this->get_address_of_ConnectionException_3();
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_6 = V_2;
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_7 = V_1;
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_8;
		L_8 = InterlockedCompareExchangeImpl<EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D *>((EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_9 = V_0;
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D *)L_9) == ((RuntimeObject*)(EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ProtocolClient.IClient::remove_ConnectionException(System.EventHandler`1<System.Exception>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_remove_ConnectionException_m59F4E68B067C426B1564F816E26B595CE53462CF (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * V_0 = NULL;
	EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * V_1 = NULL;
	EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * V_2 = NULL;
	{
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_0 = __this->get_ConnectionException_3();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_2 = V_1;
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D_il2cpp_TypeInfo_var));
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D ** L_5 = __this->get_address_of_ConnectionException_3();
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_6 = V_2;
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_7 = V_1;
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_8;
		L_8 = InterlockedCompareExchangeImpl<EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D *>((EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_9 = V_0;
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D *)L_9) == ((RuntimeObject*)(EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ProtocolClient.IClient::InvokeServerRequestEvent(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_InvokeServerRequestEvent_m0D9439F86E2BBB0D74AAE21C75DE2F4A9520D328 (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___dataToSend0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_Invoke_m6A54CC2E93E4E7E38A8E2F4F63FE0DB149028B54_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * G_B2_0 = NULL;
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * G_B1_0 = NULL;
	{
		// ServerRequestEvent?.Invoke(this, dataToSend);
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_0 = __this->get_ServerRequestEvent_0();
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = ___dataToSend0;
		NullCheck(G_B2_0);
		EventHandler_1_Invoke_m6A54CC2E93E4E7E38A8E2F4F63FE0DB149028B54(G_B2_0, __this, L_2, /*hidden argument*/EventHandler_1_Invoke_m6A54CC2E93E4E7E38A8E2F4F63FE0DB149028B54_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void ProtocolClient.IClient::InvokeServerResponseEvent(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_InvokeServerResponseEvent_m9B0164790F2DA73105E5667A06B83270466C9E5D (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___dataToSend0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_Invoke_m6A54CC2E93E4E7E38A8E2F4F63FE0DB149028B54_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * G_B2_0 = NULL;
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * G_B1_0 = NULL;
	{
		// ServerResponseEvent?.Invoke(this, dataToSend);
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_0 = __this->get_ServerResponseEvent_1();
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = ___dataToSend0;
		NullCheck(G_B2_0);
		EventHandler_1_Invoke_m6A54CC2E93E4E7E38A8E2F4F63FE0DB149028B54(G_B2_0, __this, L_2, /*hidden argument*/EventHandler_1_Invoke_m6A54CC2E93E4E7E38A8E2F4F63FE0DB149028B54_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void ProtocolClient.IClient::InvokeServerStatusUpdateEvent(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_InvokeServerStatusUpdateEvent_mF960B2A19E3CD03F38AE28F4E2A3B35D31F58A6C (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___dataToSend0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_Invoke_m6A54CC2E93E4E7E38A8E2F4F63FE0DB149028B54_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * G_B2_0 = NULL;
	EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * G_B1_0 = NULL;
	{
		// ServerStatusUpdateEvent?.Invoke(this, dataToSend);
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_0 = __this->get_ServerStatusUpdateEvent_2();
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = ___dataToSend0;
		NullCheck(G_B2_0);
		EventHandler_1_Invoke_m6A54CC2E93E4E7E38A8E2F4F63FE0DB149028B54(G_B2_0, __this, L_2, /*hidden argument*/EventHandler_1_Invoke_m6A54CC2E93E4E7E38A8E2F4F63FE0DB149028B54_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void ProtocolClient.IClient::InvokeExceptionEvent(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient_InvokeExceptionEvent_mE37866660EDE33F55DA29AA097356B87AB12B898 (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, Exception_t * ___exceptionToSend0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_Invoke_m134E3553E044A5F7F21E5C4178920C19082A2400_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * G_B2_0 = NULL;
	EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * G_B1_0 = NULL;
	{
		// ConnectionException?.Invoke(this, exceptionToSend);
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_0 = __this->get_ConnectionException_3();
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		Exception_t * L_2 = ___exceptionToSend0;
		NullCheck(G_B2_0);
		EventHandler_1_Invoke_m134E3553E044A5F7F21E5C4178920C19082A2400(G_B2_0, __this, L_2, /*hidden argument*/EventHandler_1_Invoke_m134E3553E044A5F7F21E5C4178920C19082A2400_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void ProtocolClient.IClient::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IClient__ctor_m0D07757F9C99DE9634940B5CC6F78547A53848E0 (IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ProtocolClient.NetMessage::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetMessage__ctor_mA4C1F498E1F2D598A9ACD6F14BBF921B3F639C7B (NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ProtocolClient.NetReceiveBuffer::AddNewKeyValue(System.Byte,ProtocolClient.NetMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetReceiveBuffer_AddNewKeyValue_m0E0E250207541900778C2311A7D92044583DCE57 (NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * __this, uint8_t ___messageId0, NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ___newNetMessage1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_mFFB7D3AB068FCD0390EC514E63DF3B73853D1DBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m478627FE9A3A2D2A4FBC3454D861BDDAB5EC22AA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Remove_m0CC5596DB8DC24BD58FE8EF0891DC2B267526386_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (netMessagesPendingList.ContainsKey(messageId) == true) {
		Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * L_0 = __this->get_netMessagesPendingList_1();
		uint8_t L_1 = ___messageId0;
		NullCheck(L_0);
		bool L_2;
		L_2 = Dictionary_2_ContainsKey_m478627FE9A3A2D2A4FBC3454D861BDDAB5EC22AA(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m478627FE9A3A2D2A4FBC3454D861BDDAB5EC22AA_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		// netMessagesPendingList.Remove(messageId);
		Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * L_3 = __this->get_netMessagesPendingList_1();
		uint8_t L_4 = ___messageId0;
		NullCheck(L_3);
		bool L_5;
		L_5 = Dictionary_2_Remove_m0CC5596DB8DC24BD58FE8EF0891DC2B267526386(L_3, L_4, /*hidden argument*/Dictionary_2_Remove_m0CC5596DB8DC24BD58FE8EF0891DC2B267526386_RuntimeMethod_var);
	}

IL_001b:
	{
		// netMessagesPendingList.Add(messageId, newNetMessage);
		Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * L_6 = __this->get_netMessagesPendingList_1();
		uint8_t L_7 = ___messageId0;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_8 = ___newNetMessage1;
		NullCheck(L_6);
		Dictionary_2_Add_mFFB7D3AB068FCD0390EC514E63DF3B73853D1DBC(L_6, L_7, L_8, /*hidden argument*/Dictionary_2_Add_mFFB7D3AB068FCD0390EC514E63DF3B73853D1DBC_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void ProtocolClient.NetReceiveBuffer::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetReceiveBuffer_Reset_m54E919D930053D4AF69BBA24FA0EBE4FE5DB1C93 (NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Clear_m6A531EDA6DD13A0BEDC4C0ECB7EA76BE75606AF8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_mC92A676A3C9BFEFF1C88BADCAF85070A2DCABED4_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// dynamicBuffer.Clear();
		List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * L_0 = __this->get_dynamicBuffer_0();
		NullCheck(L_0);
		List_1_Clear_mC92A676A3C9BFEFF1C88BADCAF85070A2DCABED4(L_0, /*hidden argument*/List_1_Clear_mC92A676A3C9BFEFF1C88BADCAF85070A2DCABED4_RuntimeMethod_var);
		// netMessagesPendingList.Clear();
		Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * L_1 = __this->get_netMessagesPendingList_1();
		NullCheck(L_1);
		Dictionary_2_Clear_m6A531EDA6DD13A0BEDC4C0ECB7EA76BE75606AF8(L_1, /*hidden argument*/Dictionary_2_Clear_m6A531EDA6DD13A0BEDC4C0ECB7EA76BE75606AF8_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void ProtocolClient.NetReceiveBuffer::AddDataToBuffer(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetReceiveBuffer_AddDataToBuffer_mEF538D68769E179CD93D8D5E80144EA57405E06C (NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___bufferData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_AddRange_m207006733154C20C7F72BFDD693CF79C1B8F4B5F_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (bufferData != null)
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ___bufferData0;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		// dynamicBuffer.AddRange(bufferData);
		List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * L_1 = __this->get_dynamicBuffer_0();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = ___bufferData0;
		NullCheck(L_1);
		List_1_AddRange_m207006733154C20C7F72BFDD693CF79C1B8F4B5F(L_1, (RuntimeObject*)(RuntimeObject*)L_2, /*hidden argument*/List_1_AddRange_m207006733154C20C7F72BFDD693CF79C1B8F4B5F_RuntimeMethod_var);
	}

IL_000f:
	{
		// }
		return;
	}
}
// System.Void ProtocolClient.NetReceiveBuffer::SearchInBuffer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetReceiveBuffer_SearchInBuffer_m825A53363B87F61FB7EE0CA4D76E550C6F2B6690 (NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * __this, const RuntimeMethod* method)
{
	NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * V_0 = NULL;
	{
		// NetMessage newNetMessage = null;
		V_0 = (NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 *)NULL;
	}

IL_0002:
	{
		// newNetMessage = SearchForNetMessageInBuffer();
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_0;
		L_0 = NetReceiveBuffer_SearchForNetMessageInBuffer_mB24D4BC21C300B8BBC9ABA78C3A8F460CB0BB0F8(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		// if (newNetMessage != null)
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// AddNewKeyValue(newNetMessage.messageId, newNetMessage);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_2 = V_0;
		NullCheck(L_2);
		uint8_t L_3 = L_2->get_messageId_0();
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_4 = V_0;
		NetReceiveBuffer_AddNewKeyValue_m0E0E250207541900778C2311A7D92044583DCE57(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0019:
	{
		// } while (newNetMessage != null);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_5 = V_0;
		if (L_5)
		{
			goto IL_0002;
		}
	}
	{
		// }
		return;
	}
}
// ProtocolClient.NetMessage ProtocolClient.NetReceiveBuffer::TakeNextNetMessageById(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * NetReceiveBuffer_TakeNextNetMessageById_m14D6F45C0DC5E5EB6E95C8E7FBF746BE6F4AE24C (NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * __this, uint8_t ___id0, const RuntimeMethod* method)
{
	NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * G_B2_0 = NULL;
	NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * G_B1_0 = NULL;
	{
		// NetMessage msg = GetNetMessageById(id);
		uint8_t L_0 = ___id0;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_1;
		L_1 = NetReceiveBuffer_GetNetMessageById_m23DFEC39FE560EC0F2ED5CE316C1F0073FB7E04F(__this, L_0, /*hidden argument*/NULL);
		// if (msg != null) {
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_2 = L_1;
		G_B1_0 = L_2;
		if (!L_2)
		{
			G_B2_0 = L_2;
			goto IL_0012;
		}
	}
	{
		// AddNewKeyValue(id, null);
		uint8_t L_3 = ___id0;
		NetReceiveBuffer_AddNewKeyValue_m0E0E250207541900778C2311A7D92044583DCE57(__this, L_3, (NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 *)NULL, /*hidden argument*/NULL);
		G_B2_0 = G_B1_0;
	}

IL_0012:
	{
		// return msg;
		return G_B2_0;
	}
}
// ProtocolClient.NetMessage ProtocolClient.NetReceiveBuffer::GetNetMessageById(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * NetReceiveBuffer_GetNetMessageById_m23DFEC39FE560EC0F2ED5CE316C1F0073FB7E04F (NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * __this, uint8_t ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m165079D4EC60902B2BD406C3F965F0A3E93709CF_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * V_0 = NULL;
	{
		// if (netMessagesPendingList.TryGetValue(id, out NetMessage value)) {
		Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * L_0 = __this->get_netMessagesPendingList_1();
		uint8_t L_1 = ___id0;
		NullCheck(L_0);
		bool L_2;
		L_2 = Dictionary_2_TryGetValue_m165079D4EC60902B2BD406C3F965F0A3E93709CF(L_0, L_1, (NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 **)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m165079D4EC60902B2BD406C3F965F0A3E93709CF_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		// return value;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_3 = V_0;
		return L_3;
	}

IL_0012:
	{
		// return null;
		return (NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 *)NULL;
	}
}
// ProtocolClient.NetMessage ProtocolClient.NetReceiveBuffer::SearchForNetMessageInBuffer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * NetReceiveBuffer_SearchForNetMessageInBuffer_mB24D4BC21C300B8BBC9ABA78C3A8F460CB0BB0F8 (NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_RemoveRange_m9D8324CBE5E7209DD2BE2F04E5E5513C9D665110_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m1C5E87EEF4B8F8CB985E6C4FD69D863ABAD79F14_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * V_1 = NULL;
	{
		// int neededSize = ProtocolTool.GetSizeOfNetMessageInBuffer(dynamicBuffer);
		List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * L_0 = __this->get_dynamicBuffer_0();
		int32_t L_1;
		L_1 = ProtocolTool_GetSizeOfNetMessageInBuffer_m051D0740337106C72030ED21A975CA266F3C4045(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// NetMessage netMessage = null;
		V_1 = (NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 *)NULL;
		// if (neededSize <= dynamicBuffer.Count)
		int32_t L_2 = V_0;
		List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * L_3 = __this->get_dynamicBuffer_0();
		NullCheck(L_3);
		int32_t L_4;
		L_4 = List_1_get_Count_m1C5E87EEF4B8F8CB985E6C4FD69D863ABAD79F14_inline(L_3, /*hidden argument*/List_1_get_Count_m1C5E87EEF4B8F8CB985E6C4FD69D863ABAD79F14_RuntimeMethod_var);
		if ((((int32_t)L_2) > ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		// netMessage = ProtocolTool.GetMessageFromByteArray(dynamicBuffer);
		List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * L_5 = __this->get_dynamicBuffer_0();
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_6;
		L_6 = ProtocolTool_GetMessageFromByteArray_mBD85E536ED14B56570D1CEE94F5CCFEC234121A7(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		// dynamicBuffer.RemoveRange(0, neededSize);
		List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * L_7 = __this->get_dynamicBuffer_0();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		List_1_RemoveRange_m9D8324CBE5E7209DD2BE2F04E5E5513C9D665110(L_7, 0, L_8, /*hidden argument*/List_1_RemoveRange_m9D8324CBE5E7209DD2BE2F04E5E5513C9D665110_RuntimeMethod_var);
	}

IL_0035:
	{
		// return netMessage;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_9 = V_1;
		return L_9;
	}
}
// System.Void ProtocolClient.NetReceiveBuffer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetReceiveBuffer__ctor_mA9D01D5830750AA6550E569EE006BF2D61DE6B17 (NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m7CDFF19F468AD909A00FDE97E4F7EE902D01ACD5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m42BD5721699395D9FC6B6D889CE5A29C4C2F1778_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<byte> dynamicBuffer = new List<byte>();
		List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * L_0 = (List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF *)il2cpp_codegen_object_new(List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF_il2cpp_TypeInfo_var);
		List_1__ctor_m42BD5721699395D9FC6B6D889CE5A29C4C2F1778(L_0, /*hidden argument*/List_1__ctor_m42BD5721699395D9FC6B6D889CE5A29C4C2F1778_RuntimeMethod_var);
		__this->set_dynamicBuffer_0(L_0);
		// Dictionary<byte, NetMessage> netMessagesPendingList = new Dictionary<byte, NetMessage>();
		Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * L_1 = (Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 *)il2cpp_codegen_object_new(Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m7CDFF19F468AD909A00FDE97E4F7EE902D01ACD5(L_1, /*hidden argument*/Dictionary_2__ctor_m7CDFF19F468AD909A00FDE97E4F7EE902D01ACD5_RuntimeMethod_var);
		__this->set_netMessagesPendingList_1(L_1);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ProtocolClient.NetSendBuffer::SetConnection(ProtocolClient.TCPConnection)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetSendBuffer_SetConnection_mCE9EB7BFBA86CCB47B22217122D58D82C9E595F8 (NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * __this, TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * ___newConnection0, const RuntimeMethod* method)
{
	{
		// connection = newConnection;
		TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * L_0 = ___newConnection0;
		__this->set_connection_1(L_0);
		// }
		return;
	}
}
// System.Void ProtocolClient.NetSendBuffer::AddDataToBuffer(ProtocolClient.NetMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetSendBuffer_AddDataToBuffer_m5D25260CCB2126C33732948AE63B963BF55DB169 (NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * __this, NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ___toSend0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_mFFB7D3AB068FCD0390EC514E63DF3B73853D1DBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m478627FE9A3A2D2A4FBC3454D861BDDAB5EC22AA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Remove_m0CC5596DB8DC24BD58FE8EF0891DC2B267526386_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (netMessagesPendingList.ContainsKey(currentAddMessageId))
		Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * L_0 = __this->get_netMessagesPendingList_0();
		uint8_t L_1 = __this->get_currentAddMessageId_2();
		NullCheck(L_0);
		bool L_2;
		L_2 = Dictionary_2_ContainsKey_m478627FE9A3A2D2A4FBC3454D861BDDAB5EC22AA(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m478627FE9A3A2D2A4FBC3454D861BDDAB5EC22AA_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		// netMessagesPendingList.Remove(currentAddMessageId);
		Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * L_3 = __this->get_netMessagesPendingList_0();
		uint8_t L_4 = __this->get_currentAddMessageId_2();
		NullCheck(L_3);
		bool L_5;
		L_5 = Dictionary_2_Remove_m0CC5596DB8DC24BD58FE8EF0891DC2B267526386(L_3, L_4, /*hidden argument*/Dictionary_2_Remove_m0CC5596DB8DC24BD58FE8EF0891DC2B267526386_RuntimeMethod_var);
	}

IL_0025:
	{
		// toSend.messageId = currentAddMessageId;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_6 = ___toSend0;
		uint8_t L_7 = __this->get_currentAddMessageId_2();
		NullCheck(L_6);
		L_6->set_messageId_0(L_7);
		// netMessagesPendingList.Add(currentAddMessageId, toSend);
		Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * L_8 = __this->get_netMessagesPendingList_0();
		uint8_t L_9 = __this->get_currentAddMessageId_2();
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_10 = ___toSend0;
		NullCheck(L_8);
		Dictionary_2_Add_mFFB7D3AB068FCD0390EC514E63DF3B73853D1DBC(L_8, L_9, L_10, /*hidden argument*/Dictionary_2_Add_mFFB7D3AB068FCD0390EC514E63DF3B73853D1DBC_RuntimeMethod_var);
		// lastAddedMessageId = currentAddMessageId;
		uint8_t L_11 = __this->get_currentAddMessageId_2();
		__this->set_lastAddedMessageId_3(L_11);
		// currentAddMessageId++;
		uint8_t L_12 = __this->get_currentAddMessageId_2();
		__this->set_currentAddMessageId_2((uint8_t)((int32_t)((uint8_t)((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1)))));
		// }
		return;
	}
}
// System.Void ProtocolClient.NetSendBuffer::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetSendBuffer_Reset_mB35E52BD9238AC87BFE12EEB39845411ADCA5F7D (NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Clear_m6A531EDA6DD13A0BEDC4C0ECB7EA76BE75606AF8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// currentAddMessageId = 0;
		__this->set_currentAddMessageId_2((uint8_t)0);
		// lastAddedMessageId = 0;
		__this->set_lastAddedMessageId_3((uint8_t)0);
		// netMessagesPendingList.Clear();
		Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * L_0 = __this->get_netMessagesPendingList_0();
		NullCheck(L_0);
		Dictionary_2_Clear_m6A531EDA6DD13A0BEDC4C0ECB7EA76BE75606AF8(L_0, /*hidden argument*/Dictionary_2_Clear_m6A531EDA6DD13A0BEDC4C0ECB7EA76BE75606AF8_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Boolean ProtocolClient.NetSendBuffer::SendMessageById(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NetSendBuffer_SendMessageById_m54A10CDCB7A0F39E5C7DC328531BA25CD94C256E (NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * __this, uint8_t ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m478627FE9A3A2D2A4FBC3454D861BDDAB5EC22AA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m8EF7F2B5D88191D43F306EE9B70BF02239AEEE52_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * V_0 = NULL;
	{
		// if (!netMessagesPendingList.ContainsKey(id)) {
		Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * L_0 = __this->get_netMessagesPendingList_0();
		uint8_t L_1 = ___id0;
		NullCheck(L_0);
		bool L_2;
		L_2 = Dictionary_2_ContainsKey_m478627FE9A3A2D2A4FBC3454D861BDDAB5EC22AA(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m478627FE9A3A2D2A4FBC3454D861BDDAB5EC22AA_RuntimeMethod_var);
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_0010:
	{
		// NetMessage nmToSend = netMessagesPendingList[id];
		Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * L_3 = __this->get_netMessagesPendingList_0();
		uint8_t L_4 = ___id0;
		NullCheck(L_3);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_5;
		L_5 = Dictionary_2_get_Item_m8EF7F2B5D88191D43F306EE9B70BF02239AEEE52(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m8EF7F2B5D88191D43F306EE9B70BF02239AEEE52_RuntimeMethod_var);
		V_0 = L_5;
		// connection.SendAll(ProtocolTool.NetMessageToByteArray(nmToSend));
		TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * L_6 = __this->get_connection_1();
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_7 = V_0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_8;
		L_8 = ProtocolTool_NetMessageToByteArray_mCA7A90C8FF3813DC49D9DE9B039FD261C7AFBDFD(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		TCPConnection_SendAll_m5E7C42B504544CA6CFF78C397604D9825F5B5194(L_6, L_8, /*hidden argument*/NULL);
		// return true;
		return (bool)1;
	}
}
// System.Boolean ProtocolClient.NetSendBuffer::SendNetMessage(ProtocolClient.NetMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NetSendBuffer_SendNetMessage_m1F1D322E9AA41FCA8BC955B3F53F9F0FC2FB0817 (NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * __this, NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ___toSend0, const RuntimeMethod* method)
{
	{
		// AddDataToBuffer(toSend);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_0 = ___toSend0;
		NetSendBuffer_AddDataToBuffer_m5D25260CCB2126C33732948AE63B963BF55DB169(__this, L_0, /*hidden argument*/NULL);
		// return SendMessageById(lastAddedMessageId);
		uint8_t L_1 = __this->get_lastAddedMessageId_3();
		bool L_2;
		L_2 = NetSendBuffer_SendMessageById_m54A10CDCB7A0F39E5C7DC328531BA25CD94C256E(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void ProtocolClient.NetSendBuffer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetSendBuffer__ctor_m450503519157A952342CDA00B9FD06B8C1730814 (NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m7CDFF19F468AD909A00FDE97E4F7EE902D01ACD5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Dictionary<byte, NetMessage> netMessagesPendingList = new Dictionary<byte, NetMessage>();
		Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 * L_0 = (Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262 *)il2cpp_codegen_object_new(Dictionary_2_tA05DF92DA3F98A461521A166DE96CC4C4A41E262_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m7CDFF19F468AD909A00FDE97E4F7EE902D01ACD5(L_0, /*hidden argument*/Dictionary_2__ctor_m7CDFF19F468AD909A00FDE97E4F7EE902D01ACD5_RuntimeMethod_var);
		__this->set_netMessagesPendingList_0(L_0);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void NetworkManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_Awake_m264F69270EE58EA94BD639B7CFC5DBB6CEAD7E2F (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// CheckInstance();
		NetworkManager_CheckInstance_m81FC480048046FB7E6E353CC73CCB22FF917626D(__this, /*hidden argument*/NULL);
		// DontDestroyOnLoad(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void NetworkManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_Start_mF62F7D5D531194AFEF4C3FFAC8BFE07E10F72365 (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1__ctor_mB90F555001341586440DD1B03C6894D2E0730A26_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1__ctor_mD4C0E5291CBD3101F3D364590F483445B7A27994_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkManager_OnConnectionException_mFE229675BCBDD084EE00546034929AA6AEC8702F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkManager_OnSceneLoaded_mC352C4F66FD7C5E2CA0498FE9AF8C58ED2324C4B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkManager_OnServerRequest_m61A6A7F2EA27F355998FCCB66EA5B56AD54F3209_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkManager_OnServerResponse_mFDD6F10C6BD01FD06FD36F43D47CB21BDD5D732F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkManager_OnServerStatusUpdate_m7B66E753A550AEA74B48ECA487105D8A8F26A3E4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_2__ctor_mE0417B33CF845A6B9324E67D296ADEA562B91DE0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.sceneLoaded += OnSceneLoaded;
		UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906 * L_0 = (UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906 *)il2cpp_codegen_object_new(UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906_il2cpp_TypeInfo_var);
		UnityAction_2__ctor_mE0417B33CF845A6B9324E67D296ADEA562B91DE0(L_0, __this, (intptr_t)((intptr_t)NetworkManager_OnSceneLoaded_mC352C4F66FD7C5E2CA0498FE9AF8C58ED2324C4B_RuntimeMethod_var), /*hidden argument*/UnityAction_2__ctor_mE0417B33CF845A6B9324E67D296ADEA562B91DE0_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_add_sceneLoaded_m54990A485E2E66739E31090BDC3A4C01EF7729BA(L_0, /*hidden argument*/NULL);
		// Application.runInBackground = true;
		Application_set_runInBackground_m99DB210D9A5462D08D5FC122FAB971B640D9B636((bool)1, /*hidden argument*/NULL);
		// client.ServerRequestEvent += OnServerRequest;
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_1 = __this->get_client_7();
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_2 = (EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)il2cpp_codegen_object_new(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mD4C0E5291CBD3101F3D364590F483445B7A27994(L_2, __this, (intptr_t)((intptr_t)NetworkManager_OnServerRequest_m61A6A7F2EA27F355998FCCB66EA5B56AD54F3209_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mD4C0E5291CBD3101F3D364590F483445B7A27994_RuntimeMethod_var);
		NullCheck(L_1);
		IClient_add_ServerRequestEvent_m97136FA563855AF87640E7865F21753CD3E10115(L_1, L_2, /*hidden argument*/NULL);
		// client.ServerResponseEvent += OnServerResponse;
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_3 = __this->get_client_7();
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_4 = (EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)il2cpp_codegen_object_new(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mD4C0E5291CBD3101F3D364590F483445B7A27994(L_4, __this, (intptr_t)((intptr_t)NetworkManager_OnServerResponse_mFDD6F10C6BD01FD06FD36F43D47CB21BDD5D732F_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mD4C0E5291CBD3101F3D364590F483445B7A27994_RuntimeMethod_var);
		NullCheck(L_3);
		IClient_add_ServerResponseEvent_mFF1D827B95AA6BF11F7AB5C97F77340DA5B1692C(L_3, L_4, /*hidden argument*/NULL);
		// client.ConnectionException += OnConnectionException;
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_5 = __this->get_client_7();
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_6 = (EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D *)il2cpp_codegen_object_new(EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mB90F555001341586440DD1B03C6894D2E0730A26(L_6, __this, (intptr_t)((intptr_t)NetworkManager_OnConnectionException_mFE229675BCBDD084EE00546034929AA6AEC8702F_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mB90F555001341586440DD1B03C6894D2E0730A26_RuntimeMethod_var);
		NullCheck(L_5);
		IClient_add_ConnectionException_mE43AA0290A2010DBF9451129E56253D50563F0B3(L_5, L_6, /*hidden argument*/NULL);
		// client.ServerStatusUpdateEvent += OnServerStatusUpdate;
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_7 = __this->get_client_7();
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_8 = (EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)il2cpp_codegen_object_new(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mD4C0E5291CBD3101F3D364590F483445B7A27994(L_8, __this, (intptr_t)((intptr_t)NetworkManager_OnServerStatusUpdate_m7B66E753A550AEA74B48ECA487105D8A8F26A3E4_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mD4C0E5291CBD3101F3D364590F483445B7A27994_RuntimeMethod_var);
		NullCheck(L_7);
		IClient_add_ServerStatusUpdateEvent_m874656AF3D714B25C12F7CF629134ACF4830CEE0(L_7, L_8, /*hidden argument*/NULL);
		// StartCoroutine(HandleConnection());
		RuntimeObject* L_9;
		L_9 = NetworkManager_HandleConnection_mA5E86302F2531DC5A2058DEEF9FE738D5243B87B(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_10;
		L_10 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_9, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void NetworkManager::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_OnSceneLoaded_mC352C4F66FD7C5E2CA0498FE9AF8C58ED2324C4B (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  ___scene0, int32_t ___loadSceneMode1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisField_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_m47142A7D5BBB4BD6A40C784E8C4F2DF0D366C708_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisPlayer_t5689617909B48F7640EA0892D85C92C13CC22C6F_m82212DD756C5F3A2B4844722FD1EE504E9AAD906_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral079998E3393B6BDC1FAFFA63A54F724488AE5306);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral10D6F9AEBD4F517BBCDCBFC3E369C08377331E12);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC79460AD6EC7DAD42BBDE9AD92EA622183A394A4);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_0 = NULL;
	{
		// if (scene.name == "Game")
		String_t* L_0;
		L_0 = Scene_get_name_m38F195D7CA6417FED310C23E4D8E86150C7835B8((Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE *)(&___scene0), /*hidden argument*/NULL);
		bool L_1;
		L_1 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_0, _stringLiteral079998E3393B6BDC1FAFFA63A54F724488AE5306, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004b;
		}
	}
	{
		// GameObject go = GameObject.Find("PlayerField");
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = GameObject_Find_m20157C941F1A9DA0E33E0ACA1324FAA41C2B199B(_stringLiteralC79460AD6EC7DAD42BBDE9AD92EA622183A394A4, /*hidden argument*/NULL);
		V_0 = L_2;
		// player = go.GetComponent<Player>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = V_0;
		NullCheck(L_3);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_4;
		L_4 = GameObject_GetComponent_TisPlayer_t5689617909B48F7640EA0892D85C92C13CC22C6F_m82212DD756C5F3A2B4844722FD1EE504E9AAD906(L_3, /*hidden argument*/GameObject_GetComponent_TisPlayer_t5689617909B48F7640EA0892D85C92C13CC22C6F_m82212DD756C5F3A2B4844722FD1EE504E9AAD906_RuntimeMethod_var);
		__this->set_player_4(L_4);
		// playerField = go.GetComponent<Field>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = V_0;
		NullCheck(L_5);
		Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_6;
		L_6 = GameObject_GetComponent_TisField_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_m47142A7D5BBB4BD6A40C784E8C4F2DF0D366C708(L_5, /*hidden argument*/GameObject_GetComponent_TisField_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_m47142A7D5BBB4BD6A40C784E8C4F2DF0D366C708_RuntimeMethod_var);
		__this->set_playerField_5(L_6);
		// enemyField = GameObject.Find("EnemyField").GetComponent<Field>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7;
		L_7 = GameObject_Find_m20157C941F1A9DA0E33E0ACA1324FAA41C2B199B(_stringLiteral10D6F9AEBD4F517BBCDCBFC3E369C08377331E12, /*hidden argument*/NULL);
		NullCheck(L_7);
		Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_8;
		L_8 = GameObject_GetComponent_TisField_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_m47142A7D5BBB4BD6A40C784E8C4F2DF0D366C708(L_7, /*hidden argument*/GameObject_GetComponent_TisField_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_m47142A7D5BBB4BD6A40C784E8C4F2DF0D366C708_RuntimeMethod_var);
		__this->set_enemyField_6(L_8);
	}

IL_004b:
	{
		// }
		return;
	}
}
// System.Void NetworkManager::CheckInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_CheckInstance_m81FC480048046FB7E6E353CC73CCB22FF917626D (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (instance == null)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ((NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606_StaticFields*)il2cpp_codegen_static_fields_for(NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606_il2cpp_TypeInfo_var))->get_instance_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// instance = gameObject;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		((NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606_StaticFields*)il2cpp_codegen_static_fields_for(NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606_il2cpp_TypeInfo_var))->set_instance_8(L_2);
		// }
		return;
	}

IL_0019:
	{
		// Destroy(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3;
		L_3 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void NetworkManager::StartPlacementPhase()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_StartPlacementPhase_m50F17522A3F45C7D26E4DA3FB359479860843E84 (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral079998E3393B6BDC1FAFFA63A54F724488AE5306);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Game");
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m7DAF30213E99396ECBDB1BD40CC34CCF36902092(_stringLiteral079998E3393B6BDC1FAFFA63A54F724488AE5306, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void NetworkManager::ThrowLogicError()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_ThrowLogicError_mEBCF9F1A963227EC18FB69A35302146F5857A197 (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, const RuntimeMethod* method)
{
	{
		// throw new Exception("logic error");
		Exception_t * L_0 = (Exception_t *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		Exception__ctor_m8ECDE8ACA7F2E0EF1144BD1200FB5DB2870B5F11(L_0, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral7C5EF74B0595BB6BD48A25BD0580CC74406F83C8)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NetworkManager_ThrowLogicError_mEBCF9F1A963227EC18FB69A35302146F5857A197_RuntimeMethod_var)));
	}
}
// System.Void NetworkManager::OnCellHit(System.Boolean,System.Int32,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_OnCellHit_mCECB2629807C8147F4E2972B46E0B8A096F63CE5 (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, bool ___isEnemyCell0, int32_t ___x1, int32_t ___y2, bool ___hitShip3, const RuntimeMethod* method)
{
	Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * G_B3_0 = NULL;
	{
		// (isEnemyCell ? enemyField : playerField).OnCellHit(y, x, hitShip);
		bool L_0 = ___isEnemyCell0;
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_1 = __this->get_playerField_5();
		G_B3_0 = L_1;
		goto IL_0011;
	}

IL_000b:
	{
		Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_2 = __this->get_enemyField_6();
		G_B3_0 = L_2;
	}

IL_0011:
	{
		int32_t L_3 = ___y2;
		int32_t L_4 = ___x1;
		bool L_5 = ___hitShip3;
		NullCheck(G_B3_0);
		Field_OnCellHit_mB36A003572E4C64763E34AC8E0CCA3678602967E(G_B3_0, L_3, L_4, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void NetworkManager::OnServerResponse(System.Object,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_OnServerResponse_mFDD6F10C6BD01FD06FD36F43D47CB21BDD5D732F (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, RuntimeObject * ___clientRef0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral13F2FF26B335B8BC9BBB7A01E7CBA33A3A93FDE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3ACC4CC1ADEC59220C31AAE3AEFE4D604CB500A9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4442A2F11FF7A9FB25557A681CB38556A3CA1E15);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral579A50C67ADB00AC3925859C23590322B2D0BE9D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CB05FD18E12F98F81A204339D25DD82BC993FDD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAEF11DB910A8B41A49B693B7DA8885A63EC450E4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC5CB235FDF341E57B3A3E3D289810AD3382B4E8B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD7461C99FE0AF610527A1F4273DBC4696AB5F17);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE42E8BB820D4F7550A0F04619F4E15FDC56943B9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE91FE173F59B063D620A934CE1A010F2B114C1F3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFCA440D9A4F7E2D10772A726D6FB427BAE128D34);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	bool V_4 = false;
	bool V_5 = false;
	{
		// string received = new string(Encoding.UTF8.GetChars(data));
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_0;
		L_0 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = ___data1;
		NullCheck(L_0);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_2;
		L_2 = VirtFuncInvoker1< CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(23 /* System.Char[] System.Text.Encoding::GetChars(System.Byte[]) */, L_0, L_1);
		String_t* L_3;
		L_3 = String_CreateString_mC7F57CE6ED768CF86591160424FE55D5CBA7C344(NULL, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// Debug.Log("response: " + received);
		String_t* L_4 = V_0;
		String_t* L_5;
		L_5 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteralAEF11DB910A8B41A49B693B7DA8885A63EC450E4, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_5, /*hidden argument*/NULL);
		// if (received.Length == 0)
		String_t* L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0034;
		}
	}
	{
		// Debug.Log("something went wrong!");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteral13F2FF26B335B8BC9BBB7A01E7CBA33A3A93FDE9, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_0034:
	{
		// var command = received.ToUpper().Split(':');
		String_t* L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9;
		L_9 = String_ToUpper_m4BC629F8059C3E0C4E3F7C7E04DB50EBB0C1A05A(L_8, /*hidden argument*/NULL);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_10 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_11 = L_10;
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)58));
		NullCheck(L_9);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_12;
		L_12 = String_Split_m2C74DC2B85B322998094BEDE787C378822E1F28B(L_9, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		// if(command[0] == "X")
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = V_1;
		NullCheck(L_13);
		int32_t L_14 = 0;
		String_t* L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		bool L_16;
		L_16 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_15, _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0061;
		}
	}
	{
		// StartPlacementPhase();
		NetworkManager_StartPlacementPhase_m50F17522A3F45C7D26E4DA3FB359479860843E84(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0061:
	{
		// else if (command[0] == "I")
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_17 = V_1;
		NullCheck(L_17);
		int32_t L_18 = 0;
		String_t* L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		bool L_20;
		L_20 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_19, _stringLiteral6CB05FD18E12F98F81A204339D25DD82BC993FDD, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00ad;
		}
	}
	{
		// if (command[1] == "Y")
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_21 = V_1;
		NullCheck(L_21);
		int32_t L_22 = 1;
		String_t* L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		bool L_24;
		L_24 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_23, _stringLiteralC5CB235FDF341E57B3A3E3D289810AD3382B4E8B, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_008b;
		}
	}
	{
		// player.StartPlayingphase();
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_25 = __this->get_player_4();
		NullCheck(L_25);
		Player_StartPlayingphase_m86A50FA9BBB7D0508A33D2929D3659442B4E58A9(L_25, /*hidden argument*/NULL);
		// }
		return;
	}

IL_008b:
	{
		// else if (command[1] == "N")
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_26 = V_1;
		NullCheck(L_26);
		int32_t L_27 = 1;
		String_t* L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		bool L_29;
		L_29 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_28, _stringLiteralE42E8BB820D4F7550A0F04619F4E15FDC56943B9, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00a6;
		}
	}
	{
		// player.OnShipsNotAccepted();
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_30 = __this->get_player_4();
		NullCheck(L_30);
		Player_OnShipsNotAccepted_m4FC3F38E952098A8BCC9B71E6244D78773A156B2(L_30, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00a6:
	{
		// ThrowLogicError();
		NetworkManager_ThrowLogicError_mEBCF9F1A963227EC18FB69A35302146F5857A197(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00ad:
	{
		// else if (command[0] == "P")
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_31 = V_1;
		NullCheck(L_31);
		int32_t L_32 = 0;
		String_t* L_33 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		bool L_34;
		L_34 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_33, _stringLiteralFCA440D9A4F7E2D10772A726D6FB427BAE128D34, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_00fb;
		}
	}
	{
		// if(command[1] == "1")
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_35 = V_1;
		NullCheck(L_35);
		int32_t L_36 = 1;
		String_t* L_37 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		bool L_38;
		L_38 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_37, _stringLiteralE91FE173F59B063D620A934CE1A010F2B114C1F3, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_00d8;
		}
	}
	{
		// player.SetPlayersTurn(true);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_39 = __this->get_player_4();
		NullCheck(L_39);
		Player_SetPlayersTurn_m4BC7801FD622C695EA4953D22BE8A438142F9E66(L_39, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00d8:
	{
		// else if(command[1] == "0")
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_40 = V_1;
		NullCheck(L_40);
		int32_t L_41 = 1;
		String_t* L_42 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		bool L_43;
		L_43 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_42, _stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_00f4;
		}
	}
	{
		// player.SetPlayersTurn(false);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_44 = __this->get_player_4();
		NullCheck(L_44);
		Player_SetPlayersTurn_m4BC7801FD622C695EA4953D22BE8A438142F9E66(L_44, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00f4:
	{
		// ThrowLogicError();
		NetworkManager_ThrowLogicError_mEBCF9F1A963227EC18FB69A35302146F5857A197(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00fb:
	{
		// else if (command[0] == "W")
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_45 = V_1;
		NullCheck(L_45);
		int32_t L_46 = 0;
		String_t* L_47 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		bool L_48;
		L_48 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_47, _stringLiteral3ACC4CC1ADEC59220C31AAE3AEFE4D604CB500A9, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_0149;
		}
	}
	{
		// if (command[1] == "1")
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_49 = V_1;
		NullCheck(L_49);
		int32_t L_50 = 1;
		String_t* L_51 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		bool L_52;
		L_52 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_51, _stringLiteralE91FE173F59B063D620A934CE1A010F2B114C1F3, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_0126;
		}
	}
	{
		// player.ShowEndScreen(true);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_53 = __this->get_player_4();
		NullCheck(L_53);
		Player_ShowEndScreen_m72605505E37D77917E7CA26DF82CC792A7AD7247(L_53, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0126:
	{
		// else if (command[1] == "0")
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_54 = V_1;
		NullCheck(L_54);
		int32_t L_55 = 1;
		String_t* L_56 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		bool L_57;
		L_57 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_56, _stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_0142;
		}
	}
	{
		// player.ShowEndScreen(false);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_58 = __this->get_player_4();
		NullCheck(L_58);
		Player_ShowEndScreen_m72605505E37D77917E7CA26DF82CC792A7AD7247(L_58, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0142:
	{
		// ThrowLogicError();
		NetworkManager_ThrowLogicError_mEBCF9F1A963227EC18FB69A35302146F5857A197(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0149:
	{
		// else if (command[0] == "E" || command[0] == "M")
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_59 = V_1;
		NullCheck(L_59);
		int32_t L_60 = 0;
		String_t* L_61 = (L_59)->GetAt(static_cast<il2cpp_array_size_t>(L_60));
		bool L_62;
		L_62 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_61, _stringLiteral579A50C67ADB00AC3925859C23590322B2D0BE9D, /*hidden argument*/NULL);
		if (L_62)
		{
			goto IL_0167;
		}
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_63 = V_1;
		NullCheck(L_63);
		int32_t L_64 = 0;
		String_t* L_65 = (L_63)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		bool L_66;
		L_66 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_65, _stringLiteralDD7461C99FE0AF610527A1F4273DBC4696AB5F17, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_01ae;
		}
	}

IL_0167:
	{
		// int x = int.Parse(command[1], System.Globalization.NumberStyles.HexNumber);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_67 = V_1;
		NullCheck(L_67);
		int32_t L_68 = 1;
		String_t* L_69 = (L_67)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		int32_t L_70;
		L_70 = Int32_Parse_mE2DF841397B10B947C6729D5B811D0C25D211A56(L_69, ((int32_t)515), /*hidden argument*/NULL);
		V_2 = L_70;
		// int y = int.Parse(command[2], System.Globalization.NumberStyles.HexNumber);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_71 = V_1;
		NullCheck(L_71);
		int32_t L_72 = 2;
		String_t* L_73 = (L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_72));
		int32_t L_74;
		L_74 = Int32_Parse_mE2DF841397B10B947C6729D5B811D0C25D211A56(L_73, ((int32_t)515), /*hidden argument*/NULL);
		V_3 = L_74;
		// bool hitShip = command[3] == "1";
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_75 = V_1;
		NullCheck(L_75);
		int32_t L_76 = 3;
		String_t* L_77 = (L_75)->GetAt(static_cast<il2cpp_array_size_t>(L_76));
		bool L_78;
		L_78 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_77, _stringLiteralE91FE173F59B063D620A934CE1A010F2B114C1F3, /*hidden argument*/NULL);
		V_4 = L_78;
		// bool isEnemyField = command[0] == "E";
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_79 = V_1;
		NullCheck(L_79);
		int32_t L_80 = 0;
		String_t* L_81 = (L_79)->GetAt(static_cast<il2cpp_array_size_t>(L_80));
		bool L_82;
		L_82 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_81, _stringLiteral579A50C67ADB00AC3925859C23590322B2D0BE9D, /*hidden argument*/NULL);
		V_5 = L_82;
		// OnCellHit(isEnemyField, x, y, hitShip);
		bool L_83 = V_5;
		int32_t L_84 = V_2;
		int32_t L_85 = V_3;
		bool L_86 = V_4;
		NetworkManager_OnCellHit_mCECB2629807C8147F4E2972B46E0B8A096F63CE5(__this, L_83, L_84, L_85, L_86, /*hidden argument*/NULL);
		// }
		return;
	}

IL_01ae:
	{
		// else if (command[0] == "N")
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_87 = V_1;
		NullCheck(L_87);
		int32_t L_88 = 0;
		String_t* L_89 = (L_87)->GetAt(static_cast<il2cpp_array_size_t>(L_88));
		bool L_90;
		L_90 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_89, _stringLiteralE42E8BB820D4F7550A0F04619F4E15FDC56943B9, /*hidden argument*/NULL);
		if (!L_90)
		{
			goto IL_01c8;
		}
	}
	{
		// Debug.Log("invalid request");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteral4442A2F11FF7A9FB25557A681CB38556A3CA1E15, /*hidden argument*/NULL);
		// }
		return;
	}

IL_01c8:
	{
		// ThrowLogicError();
		NetworkManager_ThrowLogicError_mEBCF9F1A963227EC18FB69A35302146F5857A197(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void NetworkManager::SendShootCell(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_SendShootCell_m1C5E85545E665BCF5AFE42903BD71095BBAFF50F (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral35FA7EC05BA14E9CC38AC240741774D8D99798AD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// string message = "H:" + x + ":" + y;
		String_t* L_0;
		L_0 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___x0), /*hidden argument*/NULL);
		String_t* L_1;
		L_1 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___y1), /*hidden argument*/NULL);
		String_t* L_2;
		L_2 = String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78(_stringLiteral35FA7EC05BA14E9CC38AC240741774D8D99798AD, L_0, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// client.SendClientRequestToServer(Encoding.UTF8.GetBytes(message));
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_3 = __this->get_client_7();
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_4;
		L_4 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		String_t* L_5 = V_0;
		NullCheck(L_4);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6;
		L_6 = VirtFuncInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, String_t* >::Invoke(16 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_4, L_5);
		NullCheck(L_3);
		VirtActionInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(6 /* System.Void ProtocolClient.IClient::SendClientRequestToServer(System.Byte[]) */, L_3, L_6);
		// }
		return;
	}
}
// System.Void NetworkManager::SendShips(System.Collections.Generic.List`1<Ship>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_SendShips_mB5DF67D6B2CD0624ED0D71ADF06780EBB288584B (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * ___list0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_m3240BECDA2A79E0895E7E9C13C662E65B8BE7709_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tCCB45E12289F4906FABB0046E40D54208C8DAB5C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_ForEach_m0ACE5773DCA4A37490AB5FC2FD86B532EE86251B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m52B8BE5835DE4DEBA361371438815F24116AD4B2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass14_0_U3CSendShipsU3Eb__0_mA5DBBC94404A3AC08A32CA6247039C842132B91A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass14_0_tCCAB38C50CA018337D1FABDD31482528BBE3F1DF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6E3E236877E32C8AA063023C2D8DFDB6F4D06132);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass14_0_tCCAB38C50CA018337D1FABDD31482528BBE3F1DF * V_0 = NULL;
	int32_t V_1 = 0;
	{
		U3CU3Ec__DisplayClass14_0_tCCAB38C50CA018337D1FABDD31482528BBE3F1DF * L_0 = (U3CU3Ec__DisplayClass14_0_tCCAB38C50CA018337D1FABDD31482528BBE3F1DF *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass14_0_tCCAB38C50CA018337D1FABDD31482528BBE3F1DF_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass14_0__ctor_m8977A56E78603A833C559237A86A4EE44E5BBC58(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		// string message = "I:" + list.Count;
		U3CU3Ec__DisplayClass14_0_tCCAB38C50CA018337D1FABDD31482528BBE3F1DF * L_1 = V_0;
		List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * L_2 = ___list0;
		NullCheck(L_2);
		int32_t L_3;
		L_3 = List_1_get_Count_m52B8BE5835DE4DEBA361371438815F24116AD4B2_inline(L_2, /*hidden argument*/List_1_get_Count_m52B8BE5835DE4DEBA361371438815F24116AD4B2_RuntimeMethod_var);
		V_1 = L_3;
		String_t* L_4;
		L_4 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_1), /*hidden argument*/NULL);
		String_t* L_5;
		L_5 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral6E3E236877E32C8AA063023C2D8DFDB6F4D06132, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_message_0(L_5);
		// list.ForEach(ship => {
		//     message += ":" + ship.size + ":" + ship.startPos.x + ":" + ship.startPos.y + ":" + (ship.isHorizontal ? "H" : "V");
		// });
		List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * L_6 = ___list0;
		U3CU3Ec__DisplayClass14_0_tCCAB38C50CA018337D1FABDD31482528BBE3F1DF * L_7 = V_0;
		Action_1_tCCB45E12289F4906FABB0046E40D54208C8DAB5C * L_8 = (Action_1_tCCB45E12289F4906FABB0046E40D54208C8DAB5C *)il2cpp_codegen_object_new(Action_1_tCCB45E12289F4906FABB0046E40D54208C8DAB5C_il2cpp_TypeInfo_var);
		Action_1__ctor_m3240BECDA2A79E0895E7E9C13C662E65B8BE7709(L_8, L_7, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass14_0_U3CSendShipsU3Eb__0_mA5DBBC94404A3AC08A32CA6247039C842132B91A_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m3240BECDA2A79E0895E7E9C13C662E65B8BE7709_RuntimeMethod_var);
		NullCheck(L_6);
		List_1_ForEach_m0ACE5773DCA4A37490AB5FC2FD86B532EE86251B(L_6, L_8, /*hidden argument*/List_1_ForEach_m0ACE5773DCA4A37490AB5FC2FD86B532EE86251B_RuntimeMethod_var);
		// client.SendClientRequestToServer(Encoding.UTF8.GetBytes(message));
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_9 = __this->get_client_7();
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_10;
		L_10 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		U3CU3Ec__DisplayClass14_0_tCCAB38C50CA018337D1FABDD31482528BBE3F1DF * L_11 = V_0;
		NullCheck(L_11);
		String_t* L_12 = L_11->get_message_0();
		NullCheck(L_10);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_13;
		L_13 = VirtFuncInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, String_t* >::Invoke(16 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_10, L_12);
		NullCheck(L_9);
		VirtActionInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(6 /* System.Void ProtocolClient.IClient::SendClientRequestToServer(System.Byte[]) */, L_9, L_13);
		// }
		return;
	}
}
// System.Void NetworkManager::SendMatchmakingRequest()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_SendMatchmakingRequest_m6A3DA0D4DBFF14A179AD7A81F2D6FA6763FA2022 (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEF420ABFDDBDA7B9EE665D85EF62E4A437554003);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// string message = "A";
		V_0 = _stringLiteralEF420ABFDDBDA7B9EE665D85EF62E4A437554003;
		// client.SendClientRequestToServer(Encoding.UTF8.GetBytes(message));
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_0 = __this->get_client_7();
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_1;
		L_1 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		String_t* L_2 = V_0;
		NullCheck(L_1);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3;
		L_3 = VirtFuncInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, String_t* >::Invoke(16 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_1, L_2);
		NullCheck(L_0);
		VirtActionInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(6 /* System.Void ProtocolClient.IClient::SendClientRequestToServer(System.Byte[]) */, L_0, L_3);
		// }
		return;
	}
}
// System.Void NetworkManager::OnServerRequest(System.Object,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_OnServerRequest_m61A6A7F2EA27F355998FCCB66EA5B56AD54F3209 (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, RuntimeObject * ___clientRef0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA9BB20C09F4F520E63DB862B4C15AEBB8F186DD8);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// string received = new string(Encoding.UTF8.GetChars(data));
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_0;
		L_0 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = ___data1;
		NullCheck(L_0);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_2;
		L_2 = VirtFuncInvoker1< CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(23 /* System.Char[] System.Text.Encoding::GetChars(System.Byte[]) */, L_0, L_1);
		String_t* L_3;
		L_3 = String_CreateString_mC7F57CE6ED768CF86591160424FE55D5CBA7C344(NULL, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// Debug.Log("request: " + received);
		String_t* L_4 = V_0;
		String_t* L_5;
		L_5 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteralA9BB20C09F4F520E63DB862B4C15AEBB8F186DD8, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void NetworkManager::OnServerStatusUpdate(System.Object,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_OnServerStatusUpdate_m7B66E753A550AEA74B48ECA487105D8A8F26A3E4 (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, RuntimeObject * ___clientRef0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9F7324E0CFE9755D866091DF20AC370998BCB0CE);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// string received = new string(Encoding.UTF8.GetChars(data));
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_0;
		L_0 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = ___data1;
		NullCheck(L_0);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_2;
		L_2 = VirtFuncInvoker1< CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(23 /* System.Char[] System.Text.Encoding::GetChars(System.Byte[]) */, L_0, L_1);
		String_t* L_3;
		L_3 = String_CreateString_mC7F57CE6ED768CF86591160424FE55D5CBA7C344(NULL, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// Debug.Log("status update: " + received);
		String_t* L_4 = V_0;
		String_t* L_5;
		L_5 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral9F7324E0CFE9755D866091DF20AC370998BCB0CE, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void NetworkManager::OnConnectionException(System.Object,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_OnConnectionException_mFE229675BCBDD084EE00546034929AA6AEC8702F (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, RuntimeObject * ___clientRef0, Exception_t * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralED18E4874205E7C94B9863D6E8A3DCC112F2FA7D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("OnClientException: " + e.ToString());
		Exception_t * L_0 = ___e1;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteralED18E4874205E7C94B9863D6E8A3DCC112F2FA7D, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator NetworkManager::HandleConnection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* NetworkManager_HandleConnection_mA5E86302F2531DC5A2058DEEF9FE738D5243B87B (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CHandleConnectionU3Ed__19_t2B2D10AA4C55A2999E347B3F7F12289D2BB5D833_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CHandleConnectionU3Ed__19_t2B2D10AA4C55A2999E347B3F7F12289D2BB5D833 * L_0 = (U3CHandleConnectionU3Ed__19_t2B2D10AA4C55A2999E347B3F7F12289D2BB5D833 *)il2cpp_codegen_object_new(U3CHandleConnectionU3Ed__19_t2B2D10AA4C55A2999E347B3F7F12289D2BB5D833_il2cpp_TypeInfo_var);
		U3CHandleConnectionU3Ed__19__ctor_m0B975E74604C0702C0AD73310F88842A78A9B3C1(L_0, 0, /*hidden argument*/NULL);
		U3CHandleConnectionU3Ed__19_t2B2D10AA4C55A2999E347B3F7F12289D2BB5D833 * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void NetworkManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager__ctor_m1C3DBB165C04540C090C4D74FEDBD83DEFC5C62F (NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private IClient client = new TCPClient();
		TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * L_0 = (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E *)il2cpp_codegen_object_new(TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E_il2cpp_TypeInfo_var);
		TCPClient__ctor_m032ED8789EFF986A3548F635F8383BBE08DB858E(L_0, /*hidden argument*/NULL);
		__this->set_client_7(L_0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Player::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	{
		// Init();
		Player_Init_m401115D83CF3607956B7E500383545F8B6588042(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Player::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_OnDestroy_m2A962F7BBA90A997ED723C07BF86A0681AA24F6E (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// playerIsPlacing = true;
		IL2CPP_RUNTIME_CLASS_INIT(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		((Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields*)il2cpp_codegen_static_fields_for(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var))->set_playerIsPlacing_27((bool)1);
		// }
		return;
	}
}
// System.Void Player::Init()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_Init_m401115D83CF3607956B7E500383545F8B6588042 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisField_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_m47142A7D5BBB4BD6A40C784E8C4F2DF0D366C708_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisNetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606_m08D4E52A3ED9AA766CA6959A49D06F448CF3820A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Player_OnCellClicked_mA6B6B1109EE15590E94170CE40E8611B3517FDAF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Player_OnCellHovered_mAE7B9A82B03E6BB91C8D787CA947A5357BE3ECF5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Player_U3CInitU3Eb__27_0_m8D6827D28CAB82B07877F797AA211BB18E56B3BD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Player_U3CInitU3Eb__27_1_mCDD656E8EF18AA918150F421711C319ABB31A4F2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Player_U3CInitU3Eb__27_2_m3CB994442E169D8C9F468B27B339CB5FA2AAFFE8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Player_U3CInitU3Eb__27_3_mC230339C8E8E63933ECC2C90B23875FCC7F736D3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral82C9E73181CA72F47C910A7EF02BE15659453D35);
		s_Il2CppMethodInitialized = true;
	}
	{
		// networkManager = GameObject.Find("NetworkManager").GetComponent<NetworkManager>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = GameObject_Find_m20157C941F1A9DA0E33E0ACA1324FAA41C2B199B(_stringLiteral82C9E73181CA72F47C910A7EF02BE15659453D35, /*hidden argument*/NULL);
		NullCheck(L_0);
		NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * L_1;
		L_1 = GameObject_GetComponent_TisNetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606_m08D4E52A3ED9AA766CA6959A49D06F448CF3820A(L_0, /*hidden argument*/GameObject_GetComponent_TisNetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606_m08D4E52A3ED9AA766CA6959A49D06F448CF3820A_RuntimeMethod_var);
		__this->set_networkManager_6(L_1);
		// textTurnIndicator = turnIndicator.GetComponent<Text>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_turnIndicator_24();
		NullCheck(L_2);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_3;
		L_3 = GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65(L_2, /*hidden argument*/GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		__this->set_textTurnIndicator_25(L_3);
		// field = gameObject.GetComponent<Field>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_5;
		L_5 = GameObject_GetComponent_TisField_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_m47142A7D5BBB4BD6A40C784E8C4F2DF0D366C708(L_4, /*hidden argument*/GameObject_GetComponent_TisField_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153_m47142A7D5BBB4BD6A40C784E8C4F2DF0D366C708_RuntimeMethod_var);
		__this->set_field_12(L_5);
		// UpdatePlacementShips();
		Player_UpdatePlacementShips_m432F1874992B4A70527D0E6EDE75753B57083ECF(__this, /*hidden argument*/NULL);
		// placementShipS.GetComponent<Button>().onClick.AddListener(delegate { OnShipSelected(2); });
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = __this->get_placementShipS_16();
		NullCheck(L_6);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_7;
		L_7 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_6, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_7);
		ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * L_8;
		L_8 = Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline(L_7, /*hidden argument*/NULL);
		UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * L_9 = (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 *)il2cpp_codegen_object_new(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA(L_9, __this, (intptr_t)((intptr_t)Player_U3CInitU3Eb__27_0_m8D6827D28CAB82B07877F797AA211BB18E56B3BD_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_8);
		UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D(L_8, L_9, /*hidden argument*/NULL);
		// placementShipM.GetComponent<Button>().onClick.AddListener(delegate { OnShipSelected(3); });
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10 = __this->get_placementShipM_17();
		NullCheck(L_10);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_11;
		L_11 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_10, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_11);
		ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * L_12;
		L_12 = Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline(L_11, /*hidden argument*/NULL);
		UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * L_13 = (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 *)il2cpp_codegen_object_new(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA(L_13, __this, (intptr_t)((intptr_t)Player_U3CInitU3Eb__27_1_mCDD656E8EF18AA918150F421711C319ABB31A4F2_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_12);
		UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D(L_12, L_13, /*hidden argument*/NULL);
		// placementShipL.GetComponent<Button>().onClick.AddListener(delegate { OnShipSelected(4); });
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14 = __this->get_placementShipL_18();
		NullCheck(L_14);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_15;
		L_15 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_14, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_15);
		ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * L_16;
		L_16 = Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline(L_15, /*hidden argument*/NULL);
		UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * L_17 = (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 *)il2cpp_codegen_object_new(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA(L_17, __this, (intptr_t)((intptr_t)Player_U3CInitU3Eb__27_2_m3CB994442E169D8C9F468B27B339CB5FA2AAFFE8_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_16);
		UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D(L_16, L_17, /*hidden argument*/NULL);
		// placementShipXL.GetComponent<Button>().onClick.AddListener(delegate { OnShipSelected(5); });
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_18 = __this->get_placementShipXL_19();
		NullCheck(L_18);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_19;
		L_19 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_18, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_19);
		ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * L_20;
		L_20 = Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline(L_19, /*hidden argument*/NULL);
		UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * L_21 = (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 *)il2cpp_codegen_object_new(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA(L_21, __this, (intptr_t)((intptr_t)Player_U3CInitU3Eb__27_3_mC230339C8E8E63933ECC2C90B23875FCC7F736D3_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_20);
		UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D(L_20, L_21, /*hidden argument*/NULL);
		// Cell.CellHoverEvent += OnCellHovered;
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_22 = (CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A *)il2cpp_codegen_object_new(CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A_il2cpp_TypeInfo_var);
		CellHoverEventHandler__ctor_mA75DD0C304EFFDF2B7434946DE2A306DFEA4474F(L_22, __this, (intptr_t)((intptr_t)Player_OnCellHovered_mAE7B9A82B03E6BB91C8D787CA947A5357BE3ECF5_RuntimeMethod_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		Cell_add_CellHoverEvent_m62C3C1BCCCDDF49C3D831FDA239E013B6988BE99(L_22, /*hidden argument*/NULL);
		// Cell.CellClickedEvent += OnCellClicked;
		CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * L_23 = (CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 *)il2cpp_codegen_object_new(CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437_il2cpp_TypeInfo_var);
		CellClickedEventHandler__ctor_m0E624D3592920AD801A5117354902BBC0671D60A(L_23, __this, (intptr_t)((intptr_t)Player_OnCellClicked_mA6B6B1109EE15590E94170CE40E8611B3517FDAF_RuntimeMethod_var), /*hidden argument*/NULL);
		Cell_add_CellClickedEvent_m2F0AB758089BDF792D350B2DA4B5B6557E78D59F(L_23, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Player::UpdatePlacementShips()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_UpdatePlacementShips_m432F1874992B4A70527D0E6EDE75753B57083ECF (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m52B8BE5835DE4DEBA361371438815F24116AD4B2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral062DB096C728515E033CF8C48A1C1F0B9A79384B);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// placementShipS.GetComponentInChildren<Text>().text = shipCountS + "x";
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_placementShipS_16();
		NullCheck(L_0);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_1;
		L_1 = GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		int32_t* L_2 = __this->get_address_of_shipCountS_7();
		String_t* L_3;
		L_3 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_2, /*hidden argument*/NULL);
		String_t* L_4;
		L_4 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_3, _stringLiteral062DB096C728515E033CF8C48A1C1F0B9A79384B, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_4);
		// if (shipCountS == 0)
		int32_t L_5 = __this->get_shipCountS_7();
		if (L_5)
		{
			goto IL_0055;
		}
	}
	{
		// placementShipS.GetComponentInChildren<Text>().color = disabledColor;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = __this->get_placementShipS_16();
		NullCheck(L_6);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_7;
		L_7 = GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881(L_6, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_8 = ((Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields*)il2cpp_codegen_static_fields_for(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var))->get_disabledColor_4();
		NullCheck(L_7);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_7, L_8);
		// placementShipS.GetComponent<Button>().interactable = false;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_9 = __this->get_placementShipS_16();
		NullCheck(L_9);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_10;
		L_10 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_9, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_10);
		Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40(L_10, (bool)0, /*hidden argument*/NULL);
		// }
		goto IL_008d;
	}

IL_0055:
	{
		// else if(placementShipS.GetComponent<Button>().interactable == false)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11 = __this->get_placementShipS_16();
		NullCheck(L_11);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_12;
		L_12 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_11, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_12);
		bool L_13;
		L_13 = Selectable_get_interactable_m4231515CC3C861B431AA22FE3345F831389B3840_inline(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_008d;
		}
	}
	{
		// placementShipS.GetComponentInChildren<Text>().color = enabledColor;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14 = __this->get_placementShipS_16();
		NullCheck(L_14);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_15;
		L_15 = GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881(L_14, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_16 = ((Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields*)il2cpp_codegen_static_fields_for(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var))->get_enabledColor_5();
		NullCheck(L_15);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_15, L_16);
		// placementShipS.GetComponent<Button>().interactable = true;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17 = __this->get_placementShipS_16();
		NullCheck(L_17);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_18;
		L_18 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_17, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_18);
		Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40(L_18, (bool)1, /*hidden argument*/NULL);
	}

IL_008d:
	{
		// placementShipM.GetComponentInChildren<Text>().text = shipCountM + "x";
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_19 = __this->get_placementShipM_17();
		NullCheck(L_19);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_20;
		L_20 = GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881(L_19, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		int32_t* L_21 = __this->get_address_of_shipCountM_8();
		String_t* L_22;
		L_22 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_21, /*hidden argument*/NULL);
		String_t* L_23;
		L_23 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_22, _stringLiteral062DB096C728515E033CF8C48A1C1F0B9A79384B, /*hidden argument*/NULL);
		NullCheck(L_20);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_20, L_23);
		// if (shipCountM == 0)
		int32_t L_24 = __this->get_shipCountM_8();
		if (L_24)
		{
			goto IL_00e2;
		}
	}
	{
		// placementShipM.GetComponentInChildren<Text>().color = disabledColor;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_25 = __this->get_placementShipM_17();
		NullCheck(L_25);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_26;
		L_26 = GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881(L_25, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_27 = ((Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields*)il2cpp_codegen_static_fields_for(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var))->get_disabledColor_4();
		NullCheck(L_26);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_26, L_27);
		// placementShipM.GetComponent<Button>().interactable = false;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_28 = __this->get_placementShipM_17();
		NullCheck(L_28);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_29;
		L_29 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_28, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_29);
		Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40(L_29, (bool)0, /*hidden argument*/NULL);
		// }
		goto IL_011a;
	}

IL_00e2:
	{
		// else if (placementShipM.GetComponent<Button>().interactable == false)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_30 = __this->get_placementShipM_17();
		NullCheck(L_30);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_31;
		L_31 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_30, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_31);
		bool L_32;
		L_32 = Selectable_get_interactable_m4231515CC3C861B431AA22FE3345F831389B3840_inline(L_31, /*hidden argument*/NULL);
		if (L_32)
		{
			goto IL_011a;
		}
	}
	{
		// placementShipM.GetComponentInChildren<Text>().color = enabledColor;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_33 = __this->get_placementShipM_17();
		NullCheck(L_33);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_34;
		L_34 = GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881(L_33, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_35 = ((Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields*)il2cpp_codegen_static_fields_for(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var))->get_enabledColor_5();
		NullCheck(L_34);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_34, L_35);
		// placementShipM.GetComponent<Button>().interactable = true;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_36 = __this->get_placementShipM_17();
		NullCheck(L_36);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_37;
		L_37 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_36, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_37);
		Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40(L_37, (bool)1, /*hidden argument*/NULL);
	}

IL_011a:
	{
		// placementShipL.GetComponentInChildren<Text>().text = shipCountL + "x";
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_38 = __this->get_placementShipL_18();
		NullCheck(L_38);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_39;
		L_39 = GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881(L_38, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		int32_t* L_40 = __this->get_address_of_shipCountL_9();
		String_t* L_41;
		L_41 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_40, /*hidden argument*/NULL);
		String_t* L_42;
		L_42 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_41, _stringLiteral062DB096C728515E033CF8C48A1C1F0B9A79384B, /*hidden argument*/NULL);
		NullCheck(L_39);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_39, L_42);
		// if (shipCountL == 0)
		int32_t L_43 = __this->get_shipCountL_9();
		if (L_43)
		{
			goto IL_016f;
		}
	}
	{
		// placementShipL.GetComponentInChildren<Text>().color = disabledColor;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_44 = __this->get_placementShipL_18();
		NullCheck(L_44);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_45;
		L_45 = GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881(L_44, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_46 = ((Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields*)il2cpp_codegen_static_fields_for(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var))->get_disabledColor_4();
		NullCheck(L_45);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_45, L_46);
		// placementShipL.GetComponent<Button>().interactable = false;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_47 = __this->get_placementShipL_18();
		NullCheck(L_47);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_48;
		L_48 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_47, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_48);
		Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40(L_48, (bool)0, /*hidden argument*/NULL);
		// }
		goto IL_01a7;
	}

IL_016f:
	{
		// else if (placementShipL.GetComponent<Button>().interactable == false)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_49 = __this->get_placementShipL_18();
		NullCheck(L_49);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_50;
		L_50 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_49, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_50);
		bool L_51;
		L_51 = Selectable_get_interactable_m4231515CC3C861B431AA22FE3345F831389B3840_inline(L_50, /*hidden argument*/NULL);
		if (L_51)
		{
			goto IL_01a7;
		}
	}
	{
		// placementShipL.GetComponentInChildren<Text>().color = enabledColor;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_52 = __this->get_placementShipL_18();
		NullCheck(L_52);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_53;
		L_53 = GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881(L_52, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_54 = ((Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields*)il2cpp_codegen_static_fields_for(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var))->get_enabledColor_5();
		NullCheck(L_53);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_53, L_54);
		// placementShipL.GetComponent<Button>().interactable = true;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_55 = __this->get_placementShipL_18();
		NullCheck(L_55);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_56;
		L_56 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_55, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_56);
		Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40(L_56, (bool)1, /*hidden argument*/NULL);
	}

IL_01a7:
	{
		// placementShipXL.GetComponentInChildren<Text>().text = shipCountXL + "x";
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_57 = __this->get_placementShipXL_19();
		NullCheck(L_57);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_58;
		L_58 = GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881(L_57, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		int32_t* L_59 = __this->get_address_of_shipCountXL_10();
		String_t* L_60;
		L_60 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_59, /*hidden argument*/NULL);
		String_t* L_61;
		L_61 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_60, _stringLiteral062DB096C728515E033CF8C48A1C1F0B9A79384B, /*hidden argument*/NULL);
		NullCheck(L_58);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_58, L_61);
		// if (shipCountXL == 0)
		int32_t L_62 = __this->get_shipCountXL_10();
		if (L_62)
		{
			goto IL_01fc;
		}
	}
	{
		// placementShipXL.GetComponentInChildren<Text>().color = disabledColor;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_63 = __this->get_placementShipXL_19();
		NullCheck(L_63);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_64;
		L_64 = GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881(L_63, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_65 = ((Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields*)il2cpp_codegen_static_fields_for(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var))->get_disabledColor_4();
		NullCheck(L_64);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_64, L_65);
		// placementShipXL.GetComponent<Button>().interactable = false;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_66 = __this->get_placementShipXL_19();
		NullCheck(L_66);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_67;
		L_67 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_66, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_67);
		Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40(L_67, (bool)0, /*hidden argument*/NULL);
		// }
		goto IL_0234;
	}

IL_01fc:
	{
		// else if (placementShipXL.GetComponent<Button>().interactable == false)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_68 = __this->get_placementShipXL_19();
		NullCheck(L_68);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_69;
		L_69 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_68, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_69);
		bool L_70;
		L_70 = Selectable_get_interactable_m4231515CC3C861B431AA22FE3345F831389B3840_inline(L_69, /*hidden argument*/NULL);
		if (L_70)
		{
			goto IL_0234;
		}
	}
	{
		// placementShipXL.GetComponentInChildren<Text>().color = enabledColor;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_71 = __this->get_placementShipXL_19();
		NullCheck(L_71);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_72;
		L_72 = GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881(L_71, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_73 = ((Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields*)il2cpp_codegen_static_fields_for(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var))->get_enabledColor_5();
		NullCheck(L_72);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_72, L_73);
		// placementShipXL.GetComponent<Button>().interactable = true;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_74 = __this->get_placementShipXL_19();
		NullCheck(L_74);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_75;
		L_75 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_74, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_75);
		Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40(L_75, (bool)1, /*hidden argument*/NULL);
	}

IL_0234:
	{
		// if (ships.Count == 0)
		List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * L_76 = __this->get_ships_13();
		NullCheck(L_76);
		int32_t L_77;
		L_77 = List_1_get_Count_m52B8BE5835DE4DEBA361371438815F24116AD4B2_inline(L_76, /*hidden argument*/List_1_get_Count_m52B8BE5835DE4DEBA361371438815F24116AD4B2_RuntimeMethod_var);
		if (L_77)
		{
			goto IL_0254;
		}
	}
	{
		// undoButton.GetComponent<Button>().interactable = false;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_78 = __this->get_undoButton_21();
		NullCheck(L_78);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_79;
		L_79 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_78, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_79);
		Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40(L_79, (bool)0, /*hidden argument*/NULL);
		goto IL_0273;
	}

IL_0254:
	{
		// else if (ships.Count == 1)
		List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * L_80 = __this->get_ships_13();
		NullCheck(L_80);
		int32_t L_81;
		L_81 = List_1_get_Count_m52B8BE5835DE4DEBA361371438815F24116AD4B2_inline(L_80, /*hidden argument*/List_1_get_Count_m52B8BE5835DE4DEBA361371438815F24116AD4B2_RuntimeMethod_var);
		if ((!(((uint32_t)L_81) == ((uint32_t)1))))
		{
			goto IL_0273;
		}
	}
	{
		// undoButton.GetComponent<Button>().interactable = true;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_82 = __this->get_undoButton_21();
		NullCheck(L_82);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_83;
		L_83 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_82, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_83);
		Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40(L_83, (bool)1, /*hidden argument*/NULL);
	}

IL_0273:
	{
		// switch (ships.Count)
		List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * L_84 = __this->get_ships_13();
		NullCheck(L_84);
		int32_t L_85;
		L_85 = List_1_get_Count_m52B8BE5835DE4DEBA361371438815F24116AD4B2_inline(L_84, /*hidden argument*/List_1_get_Count_m52B8BE5835DE4DEBA361371438815F24116AD4B2_RuntimeMethod_var);
		V_0 = L_85;
		int32_t L_86 = V_0;
		if (!L_86)
		{
			goto IL_0289;
		}
	}
	{
		int32_t L_87 = V_0;
		if ((((int32_t)L_87) == ((int32_t)((int32_t)10))))
		{
			goto IL_029b;
		}
	}
	{
		goto IL_02ad;
	}

IL_0289:
	{
		// undoButton.GetComponent<Button>().interactable = false;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_88 = __this->get_undoButton_21();
		NullCheck(L_88);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_89;
		L_89 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_88, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_89);
		Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40(L_89, (bool)0, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_029b:
	{
		// confirmButton.GetComponent<Button>().interactable = true;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_90 = __this->get_confirmButton_22();
		NullCheck(L_90);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_91;
		L_91 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_90, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_91);
		Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40(L_91, (bool)1, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_02ad:
	{
		// bool confirmButtonState = confirmButton.GetComponent<Button>().interactable;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_92 = __this->get_confirmButton_22();
		NullCheck(L_92);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_93;
		L_93 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_92, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_93);
		bool L_94;
		L_94 = Selectable_get_interactable_m4231515CC3C861B431AA22FE3345F831389B3840_inline(L_93, /*hidden argument*/NULL);
		// if (confirmButtonState)
		// bool undoButtonState = undoButton.GetComponent<Button>().interactable;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_95 = __this->get_undoButton_21();
		NullCheck(L_95);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_96;
		L_96 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_95, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		NullCheck(L_96);
		bool L_97;
		L_97 = Selectable_get_interactable_m4231515CC3C861B431AA22FE3345F831389B3840_inline(L_96, /*hidden argument*/NULL);
		// if (!undoButtonState)
		// }
		return;
	}
}
// System.Void Player::OnShipSelected(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_OnShipSelected_m07473B2D38372EE1CFBDA985E484A6B8B097AED0 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, int32_t ___size0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// selectedShip = new Ship(size);
		int32_t L_0 = ___size0;
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_1 = (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E *)il2cpp_codegen_object_new(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E_il2cpp_TypeInfo_var);
		Ship__ctor_m5734D7D291677BF934981BBF9F7B42B0EC457056(L_1, L_0, /*hidden argument*/NULL);
		__this->set_selectedShip_14(L_1);
		// }
		return;
	}
}
// System.Void Player::OnCellHovered(System.Object,CellEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_OnCellHovered_mAE7B9A82B03E6BB91C8D787CA947A5357BE3ECF5 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, RuntimeObject * ___sender0, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	bool V_3 = false;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 4);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * G_B10_0 = NULL;
	RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * G_B9_0 = NULL;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  G_B11_0;
	memset((&G_B11_0), 0, sizeof(G_B11_0));
	RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * G_B11_1 = NULL;
	RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * G_B24_0 = NULL;
	RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * G_B23_0 = NULL;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  G_B25_0;
	memset((&G_B25_0), 0, sizeof(G_B25_0));
	RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * G_B25_1 = NULL;
	{
		// int borderoffset = 1;
		V_0 = 1;
		// int hoveredCellX = (sender as Cell).xIndex;
		RuntimeObject * L_0 = ___sender0;
		NullCheck(((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 *)IsInstClass((RuntimeObject*)L_0, Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var)));
		int32_t L_1 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 *)IsInstClass((RuntimeObject*)L_0, Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_xIndex_4();
		V_1 = L_1;
		// int hoveredCellY = (sender as Cell).yIndex;
		RuntimeObject * L_2 = ___sender0;
		NullCheck(((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 *)IsInstClass((RuntimeObject*)L_2, Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var)));
		int32_t L_3 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 *)IsInstClass((RuntimeObject*)L_2, Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_yIndex_5();
		V_2 = L_3;
		// bool hoverstate = e.Hovered;
		CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * L_4 = ___e1;
		NullCheck(L_4);
		bool L_5;
		L_5 = CellEventArgs_get_Hovered_m2C28642E496CC89649BE2B23DAFAF606407BD189_inline(L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		// if (selectedShip != null)
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_6 = __this->get_selectedShip_14();
		if (!L_6)
		{
			goto IL_0149;
		}
	}
	{
		// GameObject cell = null;
		V_4 = (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL;
		// if (horizontalPlacement)
		bool L_7 = __this->get_horizontalPlacement_11();
		if (!L_7)
		{
			goto IL_00c2;
		}
	}
	{
		// for (int i = selectedShip.size - 1; i >= 0; i--)
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_8 = __this->get_selectedShip_14();
		NullCheck(L_8);
		int32_t L_9;
		L_9 = Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5_inline(L_8, /*hidden argument*/NULL);
		V_5 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)1));
		goto IL_00bc;
	}

IL_004b:
	{
	}

IL_004c:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			// cell = field.cells[hoveredCellY, hoveredCellX - i];
			Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_10 = __this->get_field_12();
			NullCheck(L_10);
			GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_11 = L_10->get_cells_10();
			int32_t L_12 = V_2;
			int32_t L_13 = V_1;
			int32_t L_14 = V_5;
			NullCheck(L_11);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15;
			L_15 = (L_11)->GetAt(L_12, ((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)L_14)));
			V_4 = L_15;
			// }
			IL2CPP_LEAVE(0xB6, FINALLY_0082);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&IndexOutOfRangeException_tDC9EF7A0346CE39E54DA1083F07BE6DFC3CE2EDD_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
				goto CATCH_0065;
			throw e;
		}

CATCH_0065:
		{ // begin catch(System.IndexOutOfRangeException)
			// catch (IndexOutOfRangeException)
			// cell = field.cells[hoveredCellY, hoveredCellX + borderoffset++];
			Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_16 = __this->get_field_12();
			NullCheck(L_16);
			GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_17 = L_16->get_cells_10();
			int32_t L_18 = V_2;
			int32_t L_19 = V_1;
			int32_t L_20 = V_0;
			int32_t L_21 = L_20;
			V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
			NullCheck(L_17);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_22;
			L_22 = (L_17)->GetAt(L_18, ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)L_21)));
			V_4 = L_22;
			// }
			IL2CPP_LEAVE(0xB6, FINALLY_0082);
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0082;
	}

FINALLY_0082:
	{ // begin finally (depth: 1)
		{
			// if(cell != null && !cell.GetComponent<Cell>().isShip)
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_23 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
			bool L_24;
			L_24 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_23, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
			if (!L_24)
			{
				goto IL_00b5;
			}
		}

IL_008c:
		{
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_25 = V_4;
			NullCheck(L_25);
			Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_26;
			L_26 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_25, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
			NullCheck(L_26);
			bool L_27 = L_26->get_isShip_6();
			if (L_27)
			{
				goto IL_00b5;
			}
		}

IL_009a:
		{
			// cell.GetComponent<RawImage>().color = hoverstate ? Cell.markedColor : Cell.standardColor;
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_28 = V_4;
			NullCheck(L_28);
			RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * L_29;
			L_29 = GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7(L_28, /*hidden argument*/GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7_RuntimeMethod_var);
			bool L_30 = V_3;
			G_B9_0 = L_29;
			if (L_30)
			{
				G_B10_0 = L_29;
				goto IL_00ab;
			}
		}

IL_00a4:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
			Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_31 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields*)il2cpp_codegen_static_fields_for(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_standardColor_9();
			G_B11_0 = L_31;
			G_B11_1 = G_B9_0;
			goto IL_00b0;
		}

IL_00ab:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
			Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_32 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields*)il2cpp_codegen_static_fields_for(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_markedColor_10();
			G_B11_0 = L_32;
			G_B11_1 = G_B10_0;
		}

IL_00b0:
		{
			NullCheck(G_B11_1);
			VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, G_B11_1, G_B11_0);
		}

IL_00b5:
		{
			// }
			IL2CPP_END_FINALLY(130)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(130)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0xB6, IL_00b6)
	}

IL_00b6:
	{
		// for (int i = selectedShip.size - 1; i >= 0; i--)
		int32_t L_33 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_33, (int32_t)1));
	}

IL_00bc:
	{
		// for (int i = selectedShip.size - 1; i >= 0; i--)
		int32_t L_34 = V_5;
		if ((((int32_t)L_34) >= ((int32_t)0)))
		{
			goto IL_004b;
		}
	}
	{
		// }
		return;
	}

IL_00c2:
	{
		// for (int i = selectedShip.size-1; i >= 0; i--)
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_35 = __this->get_selectedShip_14();
		NullCheck(L_35);
		int32_t L_36;
		L_36 = Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5_inline(L_35, /*hidden argument*/NULL);
		V_6 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_36, (int32_t)1));
		goto IL_0144;
	}

IL_00d3:
	{
	}

IL_00d4:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			// cell = field.cells[hoveredCellY - i, hoveredCellX];
			Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_37 = __this->get_field_12();
			NullCheck(L_37);
			GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_38 = L_37->get_cells_10();
			int32_t L_39 = V_2;
			int32_t L_40 = V_6;
			int32_t L_41 = V_1;
			NullCheck(L_38);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_42;
			L_42 = (L_38)->GetAt(((int32_t)il2cpp_codegen_subtract((int32_t)L_39, (int32_t)L_40)), L_41);
			V_4 = L_42;
			// }
			IL2CPP_LEAVE(0x13E, FINALLY_010a);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&IndexOutOfRangeException_tDC9EF7A0346CE39E54DA1083F07BE6DFC3CE2EDD_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
				goto CATCH_00ed;
			throw e;
		}

CATCH_00ed:
		{ // begin catch(System.IndexOutOfRangeException)
			// catch (IndexOutOfRangeException)
			// cell = field.cells[hoveredCellY + borderoffset++, hoveredCellX];
			Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_43 = __this->get_field_12();
			NullCheck(L_43);
			GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_44 = L_43->get_cells_10();
			int32_t L_45 = V_2;
			int32_t L_46 = V_0;
			int32_t L_47 = L_46;
			V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_47, (int32_t)1));
			int32_t L_48 = V_1;
			NullCheck(L_44);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_49;
			L_49 = (L_44)->GetAt(((int32_t)il2cpp_codegen_add((int32_t)L_45, (int32_t)L_47)), L_48);
			V_4 = L_49;
			// }
			IL2CPP_LEAVE(0x13E, FINALLY_010a);
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_010a;
	}

FINALLY_010a:
	{ // begin finally (depth: 1)
		{
			// if( cell != null && !cell.GetComponent<Cell>().isShip)
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_50 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
			bool L_51;
			L_51 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_50, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
			if (!L_51)
			{
				goto IL_013d;
			}
		}

IL_0114:
		{
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_52 = V_4;
			NullCheck(L_52);
			Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_53;
			L_53 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_52, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
			NullCheck(L_53);
			bool L_54 = L_53->get_isShip_6();
			if (L_54)
			{
				goto IL_013d;
			}
		}

IL_0122:
		{
			// cell.GetComponent<RawImage>().color = hoverstate ? Cell.markedColor : Cell.standardColor;
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_55 = V_4;
			NullCheck(L_55);
			RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * L_56;
			L_56 = GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7(L_55, /*hidden argument*/GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7_RuntimeMethod_var);
			bool L_57 = V_3;
			G_B23_0 = L_56;
			if (L_57)
			{
				G_B24_0 = L_56;
				goto IL_0133;
			}
		}

IL_012c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
			Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_58 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields*)il2cpp_codegen_static_fields_for(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_standardColor_9();
			G_B25_0 = L_58;
			G_B25_1 = G_B23_0;
			goto IL_0138;
		}

IL_0133:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
			Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_59 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields*)il2cpp_codegen_static_fields_for(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_markedColor_10();
			G_B25_0 = L_59;
			G_B25_1 = G_B24_0;
		}

IL_0138:
		{
			NullCheck(G_B25_1);
			VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, G_B25_1, G_B25_0);
		}

IL_013d:
		{
			// }
			IL2CPP_END_FINALLY(266)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(266)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x13E, IL_013e)
	}

IL_013e:
	{
		// for (int i = selectedShip.size-1; i >= 0; i--)
		int32_t L_60 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_60, (int32_t)1));
	}

IL_0144:
	{
		// for (int i = selectedShip.size-1; i >= 0; i--)
		int32_t L_61 = V_6;
		if ((((int32_t)L_61) >= ((int32_t)0)))
		{
			goto IL_00d3;
		}
	}

IL_0149:
	{
		// }
		return;
	}
}
// System.Void Player::OnCellClicked(System.Object,CellEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_OnCellClicked_mA6B6B1109EE15590E94170CE40E8611B3517FDAF (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, RuntimeObject * ___sender0, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral505B0F1B5A3487FE7058988A9F84A1A850B987A4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF6E1A632EC5908721EE7EB7EFE8CF41B4C1C2E69);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (playerIsPlacing)
		IL2CPP_RUNTIME_CLASS_INIT(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		bool L_0 = ((Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields*)il2cpp_codegen_static_fields_for(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var))->get_playerIsPlacing_27();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		// PlaceShip(sender, e);
		RuntimeObject * L_1 = ___sender0;
		CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * L_2 = ___e1;
		Player_PlaceShip_mF3755A13493938EF34FE4AEB7E5B69801E8D0145(__this, L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0010:
	{
		// else if (isPlayersTurn)
		bool L_3 = __this->get_isPlayersTurn_28();
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		// ShootCell(sender, e);
		RuntimeObject * L_4 = ___sender0;
		CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * L_5 = ___e1;
		Player_ShootCell_m26EF3D2B0F3339F73B3F4D89C655CAF57C8C60A6(__this, L_4, L_5, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0021:
	{
		// ShowErrorPopUp("not your turn!");
		Player_ShowErrorPopUp_mD579E7D01D32349AB1B4EA0F0272FC77B36F2440(__this, _stringLiteralF6E1A632EC5908721EE7EB7EFE8CF41B4C1C2E69, /*hidden argument*/NULL);
		// Debug.Log("not your turn");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteral505B0F1B5A3487FE7058988A9F84A1A850B987A4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Player::PlaceShip(System.Object,CellEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_PlaceShip_mF3755A13493938EF34FE4AEB7E5B69801E8D0145 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, RuntimeObject * ___sender0, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m552D9F94492155DFC4DE265624DA1F5E5EBB0319_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF0D7280F6BAA958E5EB0B700A4D9AE5A0E790473);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * V_6 = NULL;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_7 = NULL;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  V_20;
	memset((&V_20), 0, sizeof(V_20));
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_21 = NULL;
	int32_t V_22 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 5);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// bool iscollision = false;
		V_0 = (bool)0;
		// bool placementValid = true;
		V_1 = (bool)1;
		// int borderoffset = 1;
		V_2 = 1;
		// int shipindex = 0;
		V_3 = 0;
		// int hoveredCellX = (sender as Cell).xIndex;
		RuntimeObject * L_0 = ___sender0;
		NullCheck(((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 *)IsInstClass((RuntimeObject*)L_0, Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var)));
		int32_t L_1 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 *)IsInstClass((RuntimeObject*)L_0, Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_xIndex_4();
		V_4 = L_1;
		// int hoveredCellY = (sender as Cell).yIndex;
		RuntimeObject * L_2 = ___sender0;
		NullCheck(((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 *)IsInstClass((RuntimeObject*)L_2, Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var)));
		int32_t L_3 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 *)IsInstClass((RuntimeObject*)L_2, Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_yIndex_5();
		V_5 = L_3;
		// if (selectedShip != null)
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_4 = __this->get_selectedShip_14();
		if (!L_4)
		{
			goto IL_04f3;
		}
	}
	{
		// List<GameObject> selectedCells = new List<GameObject>();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_5 = (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *)il2cpp_codegen_object_new(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8(L_5, /*hidden argument*/List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var);
		V_6 = L_5;
		// GameObject cell = null;
		V_7 = (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL;
		// if (horizontalPlacement)
		bool L_6 = __this->get_horizontalPlacement_11();
		if (!L_6)
		{
			goto IL_00c7;
		}
	}
	{
		// for (int i = selectedShip.size - 1; i >= 0; i--)
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_7 = __this->get_selectedShip_14();
		NullCheck(L_7);
		int32_t L_8;
		L_8 = Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5_inline(L_7, /*hidden argument*/NULL);
		V_8 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)1));
		goto IL_00c0;
	}

IL_0053:
	{
	}

IL_0054:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			// cell = field.cells[hoveredCellY, hoveredCellX - i];
			Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_9 = __this->get_field_12();
			NullCheck(L_9);
			GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_10 = L_9->get_cells_10();
			int32_t L_11 = V_5;
			int32_t L_12 = V_4;
			int32_t L_13 = V_8;
			NullCheck(L_10);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14;
			L_14 = (L_10)->GetAt(L_11, ((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)L_13)));
			V_7 = L_14;
			// }
			IL2CPP_LEAVE(0xB4, FINALLY_008e);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&IndexOutOfRangeException_tDC9EF7A0346CE39E54DA1083F07BE6DFC3CE2EDD_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
				goto CATCH_006f;
			throw e;
		}

CATCH_006f:
		{ // begin catch(System.IndexOutOfRangeException)
			// catch (IndexOutOfRangeException)
			// cell = field.cells[hoveredCellY, hoveredCellX + borderoffset++];
			Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_15 = __this->get_field_12();
			NullCheck(L_15);
			GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_16 = L_15->get_cells_10();
			int32_t L_17 = V_5;
			int32_t L_18 = V_4;
			int32_t L_19 = V_2;
			int32_t L_20 = L_19;
			V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1));
			NullCheck(L_16);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_21;
			L_21 = (L_16)->GetAt(L_17, ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)L_20)));
			V_7 = L_21;
			// }
			IL2CPP_LEAVE(0xB4, FINALLY_008e);
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_008e;
	}

FINALLY_008e:
	{ // begin finally (depth: 1)
		{
			// if (cell != null)
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_22 = V_7;
			IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
			bool L_23;
			L_23 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_22, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
			if (!L_23)
			{
				goto IL_00b3;
			}
		}

IL_0098:
		{
			// if (cell.GetComponent<Cell>().isShip)
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_24 = V_7;
			NullCheck(L_24);
			Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_25;
			L_25 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_24, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
			NullCheck(L_25);
			bool L_26 = L_25->get_isShip_6();
			if (!L_26)
			{
				goto IL_00aa;
			}
		}

IL_00a6:
		{
			// iscollision = true;
			V_0 = (bool)1;
			// }
			goto IL_00b3;
		}

IL_00aa:
		{
			// selectedCells.Add(cell);
			List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_27 = V_6;
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_28 = V_7;
			NullCheck(L_27);
			List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3(L_27, L_28, /*hidden argument*/List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var);
		}

IL_00b3:
		{
			// }
			IL2CPP_END_FINALLY(142)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(142)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0xB4, IL_00b4)
	}

IL_00b4:
	{
		// if (iscollision)
		bool L_29 = V_0;
		if (L_29)
		{
			goto IL_013d;
		}
	}
	{
		// for (int i = selectedShip.size - 1; i >= 0; i--)
		int32_t L_30 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_30, (int32_t)1));
	}

IL_00c0:
	{
		// for (int i = selectedShip.size - 1; i >= 0; i--)
		int32_t L_31 = V_8;
		if ((((int32_t)L_31) >= ((int32_t)0)))
		{
			goto IL_0053;
		}
	}
	{
		// }
		goto IL_013d;
	}

IL_00c7:
	{
		// for (int i = selectedShip.size - 1; i >= 0; i--)
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_32 = __this->get_selectedShip_14();
		NullCheck(L_32);
		int32_t L_33;
		L_33 = Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5_inline(L_32, /*hidden argument*/NULL);
		V_9 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_33, (int32_t)1));
		goto IL_0138;
	}

IL_00d8:
	{
	}

IL_00d9:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			// cell = field.cells[hoveredCellY - i, hoveredCellX];
			Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_34 = __this->get_field_12();
			NullCheck(L_34);
			GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_35 = L_34->get_cells_10();
			int32_t L_36 = V_5;
			int32_t L_37 = V_9;
			int32_t L_38 = V_4;
			NullCheck(L_35);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_39;
			L_39 = (L_35)->GetAt(((int32_t)il2cpp_codegen_subtract((int32_t)L_36, (int32_t)L_37)), L_38);
			V_7 = L_39;
			// }
			IL2CPP_LEAVE(0x12F, FINALLY_0113);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&IndexOutOfRangeException_tDC9EF7A0346CE39E54DA1083F07BE6DFC3CE2EDD_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
				goto CATCH_00f4;
			throw e;
		}

CATCH_00f4:
		{ // begin catch(System.IndexOutOfRangeException)
			// catch (IndexOutOfRangeException)
			// cell = field.cells[hoveredCellY + borderoffset++, hoveredCellX];
			Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_40 = __this->get_field_12();
			NullCheck(L_40);
			GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_41 = L_40->get_cells_10();
			int32_t L_42 = V_5;
			int32_t L_43 = V_2;
			int32_t L_44 = L_43;
			V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_44, (int32_t)1));
			int32_t L_45 = V_4;
			NullCheck(L_41);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_46;
			L_46 = (L_41)->GetAt(((int32_t)il2cpp_codegen_add((int32_t)L_42, (int32_t)L_44)), L_45);
			V_7 = L_46;
			// }
			IL2CPP_LEAVE(0x12F, FINALLY_0113);
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0113;
	}

FINALLY_0113:
	{ // begin finally (depth: 1)
		{
			// if (cell.GetComponent<Cell>().isShip)
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_47 = V_7;
			NullCheck(L_47);
			Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_48;
			L_48 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_47, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
			NullCheck(L_48);
			bool L_49 = L_48->get_isShip_6();
			if (!L_49)
			{
				goto IL_0125;
			}
		}

IL_0121:
		{
			// iscollision = true;
			V_0 = (bool)1;
			// }
			goto IL_012e;
		}

IL_0125:
		{
			// selectedCells.Add(cell);
			List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_50 = V_6;
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_51 = V_7;
			NullCheck(L_50);
			List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3(L_50, L_51, /*hidden argument*/List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var);
		}

IL_012e:
		{
			// }
			IL2CPP_END_FINALLY(275)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(275)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x12F, IL_012f)
	}

IL_012f:
	{
		// if (iscollision)
		bool L_52 = V_0;
		if (L_52)
		{
			goto IL_013d;
		}
	}
	{
		// for (int i = selectedShip.size - 1; i >= 0; i--)
		int32_t L_53 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_53, (int32_t)1));
	}

IL_0138:
	{
		// for (int i = selectedShip.size - 1; i >= 0; i--)
		int32_t L_54 = V_9;
		if ((((int32_t)L_54) >= ((int32_t)0)))
		{
			goto IL_00d8;
		}
	}

IL_013d:
	{
		// if (iscollision)
		bool L_55 = V_0;
		if (!L_55)
		{
			goto IL_0147;
		}
	}
	{
		// placementValid = false;
		V_1 = (bool)0;
		// }
		goto IL_040d;
	}

IL_0147:
	{
		// if (horizontalPlacement)
		bool L_56 = __this->get_horizontalPlacement_11();
		if (!L_56)
		{
			goto IL_02b2;
		}
	}
	{
		// int y = selectedCells[0].GetComponent<Cell>().yIndex;
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_57 = V_6;
		NullCheck(L_57);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_58;
		L_58 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_57, 0, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		NullCheck(L_58);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_59;
		L_59 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_58, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		NullCheck(L_59);
		int32_t L_60 = L_59->get_yIndex_5();
		V_10 = L_60;
		// int x = selectedCells[0].GetComponent<Cell>().xIndex;
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_61 = V_6;
		NullCheck(L_61);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_62;
		L_62 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_61, 0, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		NullCheck(L_62);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_63;
		L_63 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_62, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		NullCheck(L_63);
		int32_t L_64 = L_63->get_xIndex_4();
		V_11 = L_64;
		// if (x != 0)
		int32_t L_65 = V_11;
		if (!L_65)
		{
			goto IL_01b7;
		}
	}
	{
		// for (int i = 1; i < selectedShip.size; i++)
		V_12 = 1;
		goto IL_01a8;
	}

IL_0183:
	{
		// int j = selectedCells[i].GetComponent<Cell>().xIndex;
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_66 = V_6;
		int32_t L_67 = V_12;
		NullCheck(L_66);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_68;
		L_68 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_66, L_67, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		NullCheck(L_68);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_69;
		L_69 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_68, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		NullCheck(L_69);
		int32_t L_70 = L_69->get_xIndex_4();
		V_13 = L_70;
		// if (j < x)
		int32_t L_71 = V_13;
		int32_t L_72 = V_11;
		if ((((int32_t)L_71) >= ((int32_t)L_72)))
		{
			goto IL_01a2;
		}
	}
	{
		// x = j;
		int32_t L_73 = V_13;
		V_11 = L_73;
	}

IL_01a2:
	{
		// for (int i = 1; i < selectedShip.size; i++)
		int32_t L_74 = V_12;
		V_12 = ((int32_t)il2cpp_codegen_add((int32_t)L_74, (int32_t)1));
	}

IL_01a8:
	{
		// for (int i = 1; i < selectedShip.size; i++)
		int32_t L_75 = V_12;
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_76 = __this->get_selectedShip_14();
		NullCheck(L_76);
		int32_t L_77;
		L_77 = Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5_inline(L_76, /*hidden argument*/NULL);
		if ((((int32_t)L_75) < ((int32_t)L_77)))
		{
			goto IL_0183;
		}
	}

IL_01b7:
	{
		// if (x > 0 && field.cells[y, x - 1].GetComponent<Cell>().isShip)
		int32_t L_78 = V_11;
		if ((((int32_t)L_78) <= ((int32_t)0)))
		{
			goto IL_01e0;
		}
	}
	{
		Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_79 = __this->get_field_12();
		NullCheck(L_79);
		GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_80 = L_79->get_cells_10();
		int32_t L_81 = V_10;
		int32_t L_82 = V_11;
		NullCheck(L_80);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_83;
		L_83 = (L_80)->GetAt(L_81, ((int32_t)il2cpp_codegen_subtract((int32_t)L_82, (int32_t)1)));
		NullCheck(L_83);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_84;
		L_84 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_83, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		NullCheck(L_84);
		bool L_85 = L_84->get_isShip_6();
		if (!L_85)
		{
			goto IL_01e0;
		}
	}
	{
		// placementValid = false;
		V_1 = (bool)0;
	}

IL_01e0:
	{
		// if (placementValid)
		bool L_86 = V_1;
		if (!L_86)
		{
			goto IL_0223;
		}
	}
	{
		// if (x + selectedShip.size <= 9 && field.cells[y, x + selectedShip.size].GetComponent<Cell>().isShip)
		int32_t L_87 = V_11;
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_88 = __this->get_selectedShip_14();
		NullCheck(L_88);
		int32_t L_89;
		L_89 = Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5_inline(L_88, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_87, (int32_t)L_89))) > ((int32_t)((int32_t)9))))
		{
			goto IL_0223;
		}
	}
	{
		Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_90 = __this->get_field_12();
		NullCheck(L_90);
		GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_91 = L_90->get_cells_10();
		int32_t L_92 = V_10;
		int32_t L_93 = V_11;
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_94 = __this->get_selectedShip_14();
		NullCheck(L_94);
		int32_t L_95;
		L_95 = Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5_inline(L_94, /*hidden argument*/NULL);
		NullCheck(L_91);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_96;
		L_96 = (L_91)->GetAt(L_92, ((int32_t)il2cpp_codegen_add((int32_t)L_93, (int32_t)L_95)));
		NullCheck(L_96);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_97;
		L_97 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_96, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		NullCheck(L_97);
		bool L_98 = L_97->get_isShip_6();
		if (!L_98)
		{
			goto IL_0223;
		}
	}
	{
		// placementValid = false;
		V_1 = (bool)0;
	}

IL_0223:
	{
		// if (placementValid)
		bool L_99 = V_1;
		if (!L_99)
		{
			goto IL_040d;
		}
	}
	{
		// for (int i = -1; i <= selectedShip.size; i++)
		V_14 = (-1);
		goto IL_029e;
	}

IL_022e:
	{
		// if (x + i >= 0 && x + i <= 9)
		int32_t L_100 = V_11;
		int32_t L_101 = V_14;
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_100, (int32_t)L_101))) < ((int32_t)0)))
		{
			goto IL_0298;
		}
	}
	{
		int32_t L_102 = V_11;
		int32_t L_103 = V_14;
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_102, (int32_t)L_103))) > ((int32_t)((int32_t)9))))
		{
			goto IL_0298;
		}
	}
	{
		// if (y < 9 && field.cells[y + 1, x + i].GetComponent<Cell>().isShip)
		int32_t L_104 = V_10;
		if ((((int32_t)L_104) >= ((int32_t)((int32_t)9))))
		{
			goto IL_026c;
		}
	}
	{
		Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_105 = __this->get_field_12();
		NullCheck(L_105);
		GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_106 = L_105->get_cells_10();
		int32_t L_107 = V_10;
		int32_t L_108 = V_11;
		int32_t L_109 = V_14;
		NullCheck(L_106);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_110;
		L_110 = (L_106)->GetAt(((int32_t)il2cpp_codegen_add((int32_t)L_107, (int32_t)1)), ((int32_t)il2cpp_codegen_add((int32_t)L_108, (int32_t)L_109)));
		NullCheck(L_110);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_111;
		L_111 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_110, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		NullCheck(L_111);
		bool L_112 = L_111->get_isShip_6();
		if (!L_112)
		{
			goto IL_026c;
		}
	}
	{
		// placementValid = false;
		V_1 = (bool)0;
	}

IL_026c:
	{
		// if (y > 0 && field.cells[y - 1, x + i].GetComponent<Cell>().isShip)
		int32_t L_113 = V_10;
		if ((((int32_t)L_113) <= ((int32_t)0)))
		{
			goto IL_0298;
		}
	}
	{
		Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_114 = __this->get_field_12();
		NullCheck(L_114);
		GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_115 = L_114->get_cells_10();
		int32_t L_116 = V_10;
		int32_t L_117 = V_11;
		int32_t L_118 = V_14;
		NullCheck(L_115);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_119;
		L_119 = (L_115)->GetAt(((int32_t)il2cpp_codegen_subtract((int32_t)L_116, (int32_t)1)), ((int32_t)il2cpp_codegen_add((int32_t)L_117, (int32_t)L_118)));
		NullCheck(L_119);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_120;
		L_120 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_119, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		NullCheck(L_120);
		bool L_121 = L_120->get_isShip_6();
		if (!L_121)
		{
			goto IL_0298;
		}
	}
	{
		// placementValid = false;
		V_1 = (bool)0;
	}

IL_0298:
	{
		// for (int i = -1; i <= selectedShip.size; i++)
		int32_t L_122 = V_14;
		V_14 = ((int32_t)il2cpp_codegen_add((int32_t)L_122, (int32_t)1));
	}

IL_029e:
	{
		// for (int i = -1; i <= selectedShip.size; i++)
		int32_t L_123 = V_14;
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_124 = __this->get_selectedShip_14();
		NullCheck(L_124);
		int32_t L_125;
		L_125 = Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5_inline(L_124, /*hidden argument*/NULL);
		if ((((int32_t)L_123) <= ((int32_t)L_125)))
		{
			goto IL_022e;
		}
	}
	{
		// }
		goto IL_040d;
	}

IL_02b2:
	{
		// int y = selectedCells[0].GetComponent<Cell>().yIndex;
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_126 = V_6;
		NullCheck(L_126);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_127;
		L_127 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_126, 0, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		NullCheck(L_127);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_128;
		L_128 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_127, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		NullCheck(L_128);
		int32_t L_129 = L_128->get_yIndex_5();
		V_15 = L_129;
		// int x = selectedCells[0].GetComponent<Cell>().xIndex;
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_130 = V_6;
		NullCheck(L_130);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_131;
		L_131 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_130, 0, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		NullCheck(L_131);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_132;
		L_132 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_131, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		NullCheck(L_132);
		int32_t L_133 = L_132->get_xIndex_4();
		V_16 = L_133;
		// if (y != 0)
		int32_t L_134 = V_15;
		if (!L_134)
		{
			goto IL_0317;
		}
	}
	{
		// for (int i = 1; i < selectedShip.size; i++)
		V_17 = 1;
		goto IL_0308;
	}

IL_02e3:
	{
		// int j = selectedCells[i].GetComponent<Cell>().yIndex;
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_135 = V_6;
		int32_t L_136 = V_17;
		NullCheck(L_135);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_137;
		L_137 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_135, L_136, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		NullCheck(L_137);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_138;
		L_138 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_137, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		NullCheck(L_138);
		int32_t L_139 = L_138->get_yIndex_5();
		V_18 = L_139;
		// if (j < y)
		int32_t L_140 = V_18;
		int32_t L_141 = V_15;
		if ((((int32_t)L_140) >= ((int32_t)L_141)))
		{
			goto IL_0302;
		}
	}
	{
		// y = j;
		int32_t L_142 = V_18;
		V_15 = L_142;
	}

IL_0302:
	{
		// for (int i = 1; i < selectedShip.size; i++)
		int32_t L_143 = V_17;
		V_17 = ((int32_t)il2cpp_codegen_add((int32_t)L_143, (int32_t)1));
	}

IL_0308:
	{
		// for (int i = 1; i < selectedShip.size; i++)
		int32_t L_144 = V_17;
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_145 = __this->get_selectedShip_14();
		NullCheck(L_145);
		int32_t L_146;
		L_146 = Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5_inline(L_145, /*hidden argument*/NULL);
		if ((((int32_t)L_144) < ((int32_t)L_146)))
		{
			goto IL_02e3;
		}
	}

IL_0317:
	{
		// if (y > 0 && field.cells[y - 1, x].GetComponent<Cell>().isShip)
		int32_t L_147 = V_15;
		if ((((int32_t)L_147) <= ((int32_t)0)))
		{
			goto IL_0340;
		}
	}
	{
		Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_148 = __this->get_field_12();
		NullCheck(L_148);
		GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_149 = L_148->get_cells_10();
		int32_t L_150 = V_15;
		int32_t L_151 = V_16;
		NullCheck(L_149);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_152;
		L_152 = (L_149)->GetAt(((int32_t)il2cpp_codegen_subtract((int32_t)L_150, (int32_t)1)), L_151);
		NullCheck(L_152);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_153;
		L_153 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_152, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		NullCheck(L_153);
		bool L_154 = L_153->get_isShip_6();
		if (!L_154)
		{
			goto IL_0340;
		}
	}
	{
		// placementValid = false;
		V_1 = (bool)0;
	}

IL_0340:
	{
		// if (placementValid)
		bool L_155 = V_1;
		if (!L_155)
		{
			goto IL_0383;
		}
	}
	{
		// if (y + selectedShip.size <= 9 && field.cells[y + selectedShip.size, x].GetComponent<Cell>().isShip)
		int32_t L_156 = V_15;
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_157 = __this->get_selectedShip_14();
		NullCheck(L_157);
		int32_t L_158;
		L_158 = Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5_inline(L_157, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_156, (int32_t)L_158))) > ((int32_t)((int32_t)9))))
		{
			goto IL_0383;
		}
	}
	{
		Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_159 = __this->get_field_12();
		NullCheck(L_159);
		GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_160 = L_159->get_cells_10();
		int32_t L_161 = V_15;
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_162 = __this->get_selectedShip_14();
		NullCheck(L_162);
		int32_t L_163;
		L_163 = Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5_inline(L_162, /*hidden argument*/NULL);
		int32_t L_164 = V_16;
		NullCheck(L_160);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_165;
		L_165 = (L_160)->GetAt(((int32_t)il2cpp_codegen_add((int32_t)L_161, (int32_t)L_163)), L_164);
		NullCheck(L_165);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_166;
		L_166 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_165, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		NullCheck(L_166);
		bool L_167 = L_166->get_isShip_6();
		if (!L_167)
		{
			goto IL_0383;
		}
	}
	{
		// placementValid = false;
		V_1 = (bool)0;
	}

IL_0383:
	{
		// if (placementValid)
		bool L_168 = V_1;
		if (!L_168)
		{
			goto IL_040d;
		}
	}
	{
		// for (int i = -1; i <= selectedShip.size; i++)
		V_19 = (-1);
		goto IL_03fe;
	}

IL_038e:
	{
		// if (y + i >= 0 && y + i <= 9)
		int32_t L_169 = V_15;
		int32_t L_170 = V_19;
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_169, (int32_t)L_170))) < ((int32_t)0)))
		{
			goto IL_03f8;
		}
	}
	{
		int32_t L_171 = V_15;
		int32_t L_172 = V_19;
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_171, (int32_t)L_172))) > ((int32_t)((int32_t)9))))
		{
			goto IL_03f8;
		}
	}
	{
		// if (x < 9 && field.cells[y + i, x + 1].GetComponent<Cell>().isShip)
		int32_t L_173 = V_16;
		if ((((int32_t)L_173) >= ((int32_t)((int32_t)9))))
		{
			goto IL_03cc;
		}
	}
	{
		Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_174 = __this->get_field_12();
		NullCheck(L_174);
		GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_175 = L_174->get_cells_10();
		int32_t L_176 = V_15;
		int32_t L_177 = V_19;
		int32_t L_178 = V_16;
		NullCheck(L_175);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_179;
		L_179 = (L_175)->GetAt(((int32_t)il2cpp_codegen_add((int32_t)L_176, (int32_t)L_177)), ((int32_t)il2cpp_codegen_add((int32_t)L_178, (int32_t)1)));
		NullCheck(L_179);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_180;
		L_180 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_179, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		NullCheck(L_180);
		bool L_181 = L_180->get_isShip_6();
		if (!L_181)
		{
			goto IL_03cc;
		}
	}
	{
		// placementValid = false;
		V_1 = (bool)0;
	}

IL_03cc:
	{
		// if (x > 0 && field.cells[y + i, x - 1].GetComponent<Cell>().isShip)
		int32_t L_182 = V_16;
		if ((((int32_t)L_182) <= ((int32_t)0)))
		{
			goto IL_03f8;
		}
	}
	{
		Field_tC6B3BDE43EF1B7CE3258A84CB186FE2711431153 * L_183 = __this->get_field_12();
		NullCheck(L_183);
		GameObjectU5BU2CU5D_t21BFB7BF24ABCC4325F4CD839223D6CA713750D9* L_184 = L_183->get_cells_10();
		int32_t L_185 = V_15;
		int32_t L_186 = V_19;
		int32_t L_187 = V_16;
		NullCheck(L_184);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_188;
		L_188 = (L_184)->GetAt(((int32_t)il2cpp_codegen_add((int32_t)L_185, (int32_t)L_186)), ((int32_t)il2cpp_codegen_subtract((int32_t)L_187, (int32_t)1)));
		NullCheck(L_188);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_189;
		L_189 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_188, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		NullCheck(L_189);
		bool L_190 = L_189->get_isShip_6();
		if (!L_190)
		{
			goto IL_03f8;
		}
	}
	{
		// placementValid = false;
		V_1 = (bool)0;
	}

IL_03f8:
	{
		// for (int i = -1; i <= selectedShip.size; i++)
		int32_t L_191 = V_19;
		V_19 = ((int32_t)il2cpp_codegen_add((int32_t)L_191, (int32_t)1));
	}

IL_03fe:
	{
		// for (int i = -1; i <= selectedShip.size; i++)
		int32_t L_192 = V_19;
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_193 = __this->get_selectedShip_14();
		NullCheck(L_193);
		int32_t L_194;
		L_194 = Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5_inline(L_193, /*hidden argument*/NULL);
		if ((((int32_t)L_192) <= ((int32_t)L_194)))
		{
			goto IL_038e;
		}
	}

IL_040d:
	{
		// if (placementValid)
		bool L_195 = V_1;
		if (!L_195)
		{
			goto IL_04e8;
		}
	}
	{
		// foreach (GameObject c in selectedCells)
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_196 = V_6;
		NullCheck(L_196);
		Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  L_197;
		L_197 = List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6(L_196, /*hidden argument*/List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6_RuntimeMethod_var);
		V_20 = L_197;
	}

IL_041c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0439;
		}

IL_041e:
		{
			// foreach (GameObject c in selectedCells)
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_198;
			L_198 = Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_inline((Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *)(&V_20), /*hidden argument*/Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_RuntimeMethod_var);
			V_21 = L_198;
			// selectedShip.AddCell(c, shipindex++);
			Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_199 = __this->get_selectedShip_14();
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_200 = V_21;
			int32_t L_201 = V_3;
			int32_t L_202 = L_201;
			V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_202, (int32_t)1));
			NullCheck(L_199);
			Ship_AddCell_mFE93A99E3A478DBDED4B3AA6FBE1D0BEA8408D4D(L_199, L_200, L_202, /*hidden argument*/NULL);
		}

IL_0439:
		{
			// foreach (GameObject c in selectedCells)
			bool L_203;
			L_203 = Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7((Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *)(&V_20), /*hidden argument*/Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7_RuntimeMethod_var);
			if (L_203)
			{
				goto IL_041e;
			}
		}

IL_0442:
		{
			IL2CPP_LEAVE(0x452, FINALLY_0444);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0444;
	}

FINALLY_0444:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9((Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *)(&V_20), /*hidden argument*/Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9_RuntimeMethod_var);
		IL2CPP_END_FINALLY(1092)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1092)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x452, IL_0452)
	}

IL_0452:
	{
		// selectedShip.isHorizontal = horizontalPlacement;
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_204 = __this->get_selectedShip_14();
		bool L_205 = __this->get_horizontalPlacement_11();
		NullCheck(L_204);
		L_204->set_isHorizontal_1(L_205);
		// ships.Add(selectedShip);
		List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * L_206 = __this->get_ships_13();
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_207 = __this->get_selectedShip_14();
		NullCheck(L_206);
		List_1_Add_m552D9F94492155DFC4DE265624DA1F5E5EBB0319(L_206, L_207, /*hidden argument*/List_1_Add_m552D9F94492155DFC4DE265624DA1F5E5EBB0319_RuntimeMethod_var);
		// switch (selectedShip.size)
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_208 = __this->get_selectedShip_14();
		NullCheck(L_208);
		int32_t L_209;
		L_209 = Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5_inline(L_208, /*hidden argument*/NULL);
		V_22 = L_209;
		int32_t L_210 = V_22;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_210, (int32_t)2)))
		{
			case 0:
			{
				goto IL_049c;
			}
			case 1:
			{
				goto IL_04ac;
			}
			case 2:
			{
				goto IL_04bc;
			}
			case 3:
			{
				goto IL_04cc;
			}
		}
	}
	{
		goto IL_04da;
	}

IL_049c:
	{
		// shipCountS--;
		int32_t L_211 = __this->get_shipCountS_7();
		__this->set_shipCountS_7(((int32_t)il2cpp_codegen_subtract((int32_t)L_211, (int32_t)1)));
		// break;
		goto IL_04da;
	}

IL_04ac:
	{
		// shipCountM--;
		int32_t L_212 = __this->get_shipCountM_8();
		__this->set_shipCountM_8(((int32_t)il2cpp_codegen_subtract((int32_t)L_212, (int32_t)1)));
		// break;
		goto IL_04da;
	}

IL_04bc:
	{
		// shipCountL--;
		int32_t L_213 = __this->get_shipCountL_9();
		__this->set_shipCountL_9(((int32_t)il2cpp_codegen_subtract((int32_t)L_213, (int32_t)1)));
		// break;
		goto IL_04da;
	}

IL_04cc:
	{
		// shipCountXL--;
		int32_t L_214 = __this->get_shipCountXL_10();
		__this->set_shipCountXL_10(((int32_t)il2cpp_codegen_subtract((int32_t)L_214, (int32_t)1)));
	}

IL_04da:
	{
		// UpdatePlacementShips();
		Player_UpdatePlacementShips_m432F1874992B4A70527D0E6EDE75753B57083ECF(__this, /*hidden argument*/NULL);
		// selectedShip = null;
		__this->set_selectedShip_14((Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E *)NULL);
		// }
		return;
	}

IL_04e8:
	{
		// ShowErrorPopUp("invalid placement!");
		Player_ShowErrorPopUp_mD579E7D01D32349AB1B4EA0F0272FC77B36F2440(__this, _stringLiteralF0D7280F6BAA958E5EB0B700A4D9AE5A0E790473, /*hidden argument*/NULL);
	}

IL_04f3:
	{
		// }
		return;
	}
}
// System.Void Player::ShowErrorPopUp(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_ShowErrorPopUp_mD579E7D01D32349AB1B4EA0F0272FC77B36F2440 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ErrorPopUp.GetComponentInChildren<Text>().text = message;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_ErrorPopUp_20();
		NullCheck(L_0);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_1;
		L_1 = GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		String_t* L_2 = ___message0;
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_2);
		// ErrorPopUp.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_ErrorPopUp_20();
		NullCheck(L_3);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Player::ShootCell(System.Object,CellEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_ShootCell_m26EF3D2B0F3339F73B3F4D89C655CAF57C8C60A6 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, RuntimeObject * ___sender0, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE5DD6B874CDA2CC3D2A0AABF1B63977712E9BD29);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if((sender as Cell).isShot)
		RuntimeObject * L_0 = ___sender0;
		NullCheck(((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 *)IsInstClass((RuntimeObject*)L_0, Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var)));
		bool L_1 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 *)IsInstClass((RuntimeObject*)L_0, Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_isShot_7();
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// ShowErrorPopUp("invalid hit!");
		Player_ShowErrorPopUp_mD579E7D01D32349AB1B4EA0F0272FC77B36F2440(__this, _stringLiteralE5DD6B874CDA2CC3D2A0AABF1B63977712E9BD29, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0019:
	{
		// networkManager.SendShootCell((sender as Cell).xIndex, (sender as Cell).yIndex);
		NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * L_2 = __this->get_networkManager_6();
		RuntimeObject * L_3 = ___sender0;
		NullCheck(((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 *)IsInstClass((RuntimeObject*)L_3, Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var)));
		int32_t L_4 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 *)IsInstClass((RuntimeObject*)L_3, Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_xIndex_4();
		RuntimeObject * L_5 = ___sender0;
		NullCheck(((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 *)IsInstClass((RuntimeObject*)L_5, Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var)));
		int32_t L_6 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 *)IsInstClass((RuntimeObject*)L_5, Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_yIndex_5();
		NullCheck(L_2);
		NetworkManager_SendShootCell_m1C5E85545E665BCF5AFE42903BD71095BBAFF50F(L_2, L_4, L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Player::ToggleOrientation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_ToggleOrientation_m53CF237E2F37D86948EAEA3991B86C7BA7E8AB96 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float G_B2_0 = 0.0f;
	float* G_B2_1 = NULL;
	float G_B1_0 = 0.0f;
	float* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	float G_B3_1 = 0.0f;
	float* G_B3_2 = NULL;
	{
		// horizontalPlacement = !horizontalPlacement;
		bool L_0 = __this->get_horizontalPlacement_11();
		__this->set_horizontalPlacement_11((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		// Vector3 rotation = rotationState.transform.localEulerAngles;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_rotationState_23();
		NullCheck(L_1);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_localEulerAngles_m4C442107F523737ADAB54855FDC1777A9B71D545(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// rotation.z += horizontalPlacement ? 90 : -90;
		float* L_4 = (&V_0)->get_address_of_z_4();
		float* L_5 = L_4;
		float L_6 = *((float*)L_5);
		bool L_7 = __this->get_horizontalPlacement_11();
		G_B1_0 = L_6;
		G_B1_1 = L_5;
		if (L_7)
		{
			G_B2_0 = L_6;
			G_B2_1 = L_5;
			goto IL_0035;
		}
	}
	{
		G_B3_0 = ((int32_t)-90);
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0037;
	}

IL_0035:
	{
		G_B3_0 = ((int32_t)90);
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0037:
	{
		*((float*)G_B3_2) = (float)((float)il2cpp_codegen_add((float)G_B3_1, (float)((float)((float)G_B3_0))));
		// rotationState.transform.localEulerAngles = rotation;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = __this->get_rotationState_23();
		NullCheck(L_8);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9;
		L_9 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_8, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		NullCheck(L_9);
		Transform_set_localEulerAngles_mB63076996124DC76E6902A81677A6E3C814C693B(L_9, L_10, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Player::DeleteLastShip()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_DeleteLastShip_m3881E67CC897B2B09A7DB7348D5E569732B74FD0 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_RemoveAt_mC9BE02B104DF58BE5358E859E4DFE2C936C4EFAD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m52B8BE5835DE4DEBA361371438815F24116AD4B2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1D3A0CB5B5817CB82948B52D881DD9C0A8F397C5_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// int i = ships.Count - 1;
		List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * L_0 = __this->get_ships_13();
		NullCheck(L_0);
		int32_t L_1;
		L_1 = List_1_get_Count_m52B8BE5835DE4DEBA361371438815F24116AD4B2_inline(L_0, /*hidden argument*/List_1_get_Count_m52B8BE5835DE4DEBA361371438815F24116AD4B2_RuntimeMethod_var);
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1));
		// switch (ships[i].size)
		List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * L_2 = __this->get_ships_13();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_4;
		L_4 = List_1_get_Item_m1D3A0CB5B5817CB82948B52D881DD9C0A8F397C5_inline(L_2, L_3, /*hidden argument*/List_1_get_Item_m1D3A0CB5B5817CB82948B52D881DD9C0A8F397C5_RuntimeMethod_var);
		NullCheck(L_4);
		int32_t L_5;
		L_5 = Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5_inline(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_1;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)2)))
		{
			case 0:
			{
				goto IL_003a;
			}
			case 1:
			{
				goto IL_004a;
			}
			case 2:
			{
				goto IL_005a;
			}
			case 3:
			{
				goto IL_006a;
			}
		}
	}
	{
		goto IL_0078;
	}

IL_003a:
	{
		// shipCountS++;
		int32_t L_7 = __this->get_shipCountS_7();
		__this->set_shipCountS_7(((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1)));
		// break;
		goto IL_0078;
	}

IL_004a:
	{
		// shipCountM++;
		int32_t L_8 = __this->get_shipCountM_8();
		__this->set_shipCountM_8(((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1)));
		// break;
		goto IL_0078;
	}

IL_005a:
	{
		// shipCountL++;
		int32_t L_9 = __this->get_shipCountL_9();
		__this->set_shipCountL_9(((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1)));
		// break;
		goto IL_0078;
	}

IL_006a:
	{
		// shipCountXL++;
		int32_t L_10 = __this->get_shipCountXL_10();
		__this->set_shipCountXL_10(((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1)));
	}

IL_0078:
	{
		// ships[i].Delete();
		List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * L_11 = __this->get_ships_13();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_13;
		L_13 = List_1_get_Item_m1D3A0CB5B5817CB82948B52D881DD9C0A8F397C5_inline(L_11, L_12, /*hidden argument*/List_1_get_Item_m1D3A0CB5B5817CB82948B52D881DD9C0A8F397C5_RuntimeMethod_var);
		NullCheck(L_13);
		Ship_Delete_m7E022DC8EECBAB392587D14CC54C2245D12211AF(L_13, /*hidden argument*/NULL);
		// ships.RemoveAt(i);
		List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * L_14 = __this->get_ships_13();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		List_1_RemoveAt_mC9BE02B104DF58BE5358E859E4DFE2C936C4EFAD(L_14, L_15, /*hidden argument*/List_1_RemoveAt_mC9BE02B104DF58BE5358E859E4DFE2C936C4EFAD_RuntimeMethod_var);
		// UpdatePlacementShips();
		Player_UpdatePlacementShips_m432F1874992B4A70527D0E6EDE75753B57083ECF(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Player::ConfirmPlacements()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_ConfirmPlacements_m2B819263E472A19CA9850D66BF45D9407B8A7369 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	{
		// networkManager.SendShips(ships);
		NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * L_0 = __this->get_networkManager_6();
		List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * L_1 = __this->get_ships_13();
		NullCheck(L_0);
		NetworkManager_SendShips_mB5DF67D6B2CD0624ED0D71ADF06780EBB288584B(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Player::OnShipsNotAccepted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_OnShipsNotAccepted_m4FC3F38E952098A8BCC9B71E6244D78773A156B2 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCD98A9DD263E7154BD50DF9B4E84377553129BB9);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ShowErrorPopUp("invalid placements!");
		Player_ShowErrorPopUp_mD579E7D01D32349AB1B4EA0F0272FC77B36F2440(__this, _stringLiteralCD98A9DD263E7154BD50DF9B4E84377553129BB9, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Player::SetPlayersTurn(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_SetPlayersTurn_m4BC7801FD622C695EA4953D22BE8A438142F9E66 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, bool ___b0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral16A950F8ED51C142DED24CF66F507945DFE09106);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral92AA5B2BF2682814D726B425F6401630816FCADB);
		s_Il2CppMethodInitialized = true;
	}
	{
		// isPlayersTurn = b;
		bool L_0 = ___b0;
		__this->set_isPlayersTurn_28(L_0);
		// if (b)
		bool L_1 = ___b0;
		if (!L_1)
		{
			goto IL_003a;
		}
	}
	{
		// textTurnIndicator.text = "your turn!";
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_2 = __this->get_textTurnIndicator_25();
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, _stringLiteral16A950F8ED51C142DED24CF66F507945DFE09106);
		// textTurnIndicator.color = new Color(0, 1, 0);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_3 = __this->get_textTurnIndicator_25();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494((&L_4), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_3, L_4);
		// }
		return;
	}

IL_003a:
	{
		// textTurnIndicator.text = "enemy turn!";
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_5 = __this->get_textTurnIndicator_25();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteral92AA5B2BF2682814D726B425F6401630816FCADB);
		// textTurnIndicator.color = new Color(1, 0, 0);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_6 = __this->get_textTurnIndicator_25();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_7;
		memset((&L_7), 0, sizeof(L_7));
		Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494((&L_7), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_6, L_7);
		// }
		return;
	}
}
// System.Void Player::ShowEndScreen(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_ShowEndScreen_m72605505E37D77917E7CA26DF82CC792A7AD7247 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, bool ___isPlayerWinner0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16ABC1D7B4E804984517841F8D26E9C769FAE710_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral457A7B275BF30EDA4D263CB412F36AF1888D0988);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral59FA9E3480810057109834B562A27C1C58E3CE26);
		s_Il2CppMethodInitialized = true;
	}
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * G_B2_0 = NULL;
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * G_B1_0 = NULL;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  G_B3_0;
	memset((&G_B3_0), 0, sizeof(G_B3_0));
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * G_B3_1 = NULL;
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * G_B5_0 = NULL;
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * G_B4_0 = NULL;
	String_t* G_B6_0 = NULL;
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * G_B6_1 = NULL;
	{
		// EndScreen.GetComponent<Image>().color = isPlayerWinner ? new Color(0, 1, 0) : new Color(1, 0, 0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_EndScreen_26();
		NullCheck(L_0);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_1;
		L_1 = GameObject_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16ABC1D7B4E804984517841F8D26E9C769FAE710(L_0, /*hidden argument*/GameObject_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16ABC1D7B4E804984517841F8D26E9C769FAE710_RuntimeMethod_var);
		bool L_2 = ___isPlayerWinner0;
		G_B1_0 = L_1;
		if (L_2)
		{
			G_B2_0 = L_1;
			goto IL_0024;
		}
	}
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494((&L_3), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_0038;
	}

IL_0024:
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494((&L_4), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
	}

IL_0038:
	{
		NullCheck(G_B3_1);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, G_B3_1, G_B3_0);
		// EndScreen.GetComponentInChildren<Text>().text = isPlayerWinner ? "YOU WON" : "YOU LOST";
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = __this->get_EndScreen_26();
		NullCheck(L_5);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_6;
		L_6 = GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881(L_5, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		bool L_7 = ___isPlayerWinner0;
		G_B4_0 = L_6;
		if (L_7)
		{
			G_B5_0 = L_6;
			goto IL_0052;
		}
	}
	{
		G_B6_0 = _stringLiteral457A7B275BF30EDA4D263CB412F36AF1888D0988;
		G_B6_1 = G_B4_0;
		goto IL_0057;
	}

IL_0052:
	{
		G_B6_0 = _stringLiteral59FA9E3480810057109834B562A27C1C58E3CE26;
		G_B6_1 = G_B5_0;
	}

IL_0057:
	{
		NullCheck(G_B6_1);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, G_B6_1, G_B6_0);
		// EndScreen.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = __this->get_EndScreen_26();
		NullCheck(L_8);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_8, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Player::StartPlayingphase()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_StartPlayingphase_m86A50FA9BBB7D0508A33D2929D3659442B4E58A9 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Player_OnCellHovered_mAE7B9A82B03E6BB91C8D787CA947A5357BE3ECF5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Cell.CellHoverEvent -= OnCellHovered;
		CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * L_0 = (CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A *)il2cpp_codegen_object_new(CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A_il2cpp_TypeInfo_var);
		CellHoverEventHandler__ctor_mA75DD0C304EFFDF2B7434946DE2A306DFEA4474F(L_0, __this, (intptr_t)((intptr_t)Player_OnCellHovered_mAE7B9A82B03E6BB91C8D787CA947A5357BE3ECF5_RuntimeMethod_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		Cell_remove_CellHoverEvent_mCE5ADEEAF176E719EC9545C1D291E37E4FC81C3B(L_0, /*hidden argument*/NULL);
		// playerIsPlacing = false;
		IL2CPP_RUNTIME_CLASS_INIT(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		((Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields*)il2cpp_codegen_static_fields_for(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var))->set_playerIsPlacing_27((bool)0);
		// placementWindow.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_placementWindow_15();
		NullCheck(L_1);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)0, /*hidden argument*/NULL);
		// turnIndicator.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_turnIndicator_24();
		NullCheck(L_2);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Player::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCCD54CB0212EBDA5809120252A5F13BB00288588_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private int shipCountS = 4;
		__this->set_shipCountS_7(4);
		// private int shipCountM = 3;
		__this->set_shipCountM_8(3);
		// private int shipCountL = 2;
		__this->set_shipCountL_9(2);
		// private int shipCountXL = 1;
		__this->set_shipCountXL_10(1);
		// private List<Ship> ships = new List<Ship>();
		List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 * L_0 = (List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835 *)il2cpp_codegen_object_new(List_1_t8799C6247F5C296ACFFF1D7F8CAA2F49ADBD8835_il2cpp_TypeInfo_var);
		List_1__ctor_mCCD54CB0212EBDA5809120252A5F13BB00288588(L_0, /*hidden argument*/List_1__ctor_mCCD54CB0212EBDA5809120252A5F13BB00288588_RuntimeMethod_var);
		__this->set_ships_13(L_0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Player::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player__cctor_m38DB9DEEDAE88802EC9823ECFF4A17859AABF96F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static Color disabledColor = new Color(102 / 255f, 102 / 255f, 102 / 255f);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494((&L_0), (0.400000006f), (0.400000006f), (0.400000006f), /*hidden argument*/NULL);
		((Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields*)il2cpp_codegen_static_fields_for(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var))->set_disabledColor_4(L_0);
		// private static Color enabledColor = new Color(1, 1, 1);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494((&L_1), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields*)il2cpp_codegen_static_fields_for(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var))->set_enabledColor_5(L_1);
		// public static bool playerIsPlacing = true;
		((Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_StaticFields*)il2cpp_codegen_static_fields_for(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var))->set_playerIsPlacing_27((bool)1);
		return;
	}
}
// System.Void Player::<Init>b__27_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_U3CInitU3Eb__27_0_m8D6827D28CAB82B07877F797AA211BB18E56B3BD (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	{
		// placementShipS.GetComponent<Button>().onClick.AddListener(delegate { OnShipSelected(2); });
		Player_OnShipSelected_m07473B2D38372EE1CFBDA985E484A6B8B097AED0(__this, 2, /*hidden argument*/NULL);
		// placementShipS.GetComponent<Button>().onClick.AddListener(delegate { OnShipSelected(2); });
		return;
	}
}
// System.Void Player::<Init>b__27_1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_U3CInitU3Eb__27_1_mCDD656E8EF18AA918150F421711C319ABB31A4F2 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	{
		// placementShipM.GetComponent<Button>().onClick.AddListener(delegate { OnShipSelected(3); });
		Player_OnShipSelected_m07473B2D38372EE1CFBDA985E484A6B8B097AED0(__this, 3, /*hidden argument*/NULL);
		// placementShipM.GetComponent<Button>().onClick.AddListener(delegate { OnShipSelected(3); });
		return;
	}
}
// System.Void Player::<Init>b__27_2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_U3CInitU3Eb__27_2_m3CB994442E169D8C9F468B27B339CB5FA2AAFFE8 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	{
		// placementShipL.GetComponent<Button>().onClick.AddListener(delegate { OnShipSelected(4); });
		Player_OnShipSelected_m07473B2D38372EE1CFBDA985E484A6B8B097AED0(__this, 4, /*hidden argument*/NULL);
		// placementShipL.GetComponent<Button>().onClick.AddListener(delegate { OnShipSelected(4); });
		return;
	}
}
// System.Void Player::<Init>b__27_3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_U3CInitU3Eb__27_3_mC230339C8E8E63933ECC2C90B23875FCC7F736D3 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	{
		// placementShipXL.GetComponent<Button>().onClick.AddListener(delegate { OnShipSelected(5); });
		Player_OnShipSelected_m07473B2D38372EE1CFBDA985E484A6B8B097AED0(__this, 5, /*hidden argument*/NULL);
		// placementShipXL.GetComponent<Button>().onClick.AddListener(delegate { OnShipSelected(5); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ProtocolClient.Program::OnServerResponse(System.Object,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Program_OnServerResponse_mC8C36D4F38846D1C686CFE44D14D42C4B4B04054 (RuntimeObject * ___a0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Console_t79987B1B5914E76054A8CBE506B9E11936A8BC07_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral18E02A56A35A0E2B7B0C47F4586B2C4F19B38FFD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F24B739AA45780EA08FF218BC64454668DC866A);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// string received = new string(Encoding.UTF8.GetChars(data));
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_0;
		L_0 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = ___data1;
		NullCheck(L_0);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_2;
		L_2 = VirtFuncInvoker1< CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(23 /* System.Char[] System.Text.Encoding::GetChars(System.Byte[]) */, L_0, L_1);
		String_t* L_3;
		L_3 = String_CreateString_mC7F57CE6ED768CF86591160424FE55D5CBA7C344(NULL, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// Console.WriteLine("OnClientResponse: " + a.ToString() + "; Text/Data:" + received);
		RuntimeObject * L_4 = ___a0;
		NullCheck(L_4);
		String_t* L_5;
		L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		String_t* L_6 = V_0;
		String_t* L_7;
		L_7 = String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78(_stringLiteral18E02A56A35A0E2B7B0C47F4586B2C4F19B38FFD, L_5, _stringLiteral7F24B739AA45780EA08FF218BC64454668DC866A, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t79987B1B5914E76054A8CBE506B9E11936A8BC07_il2cpp_TypeInfo_var);
		Console_WriteLine_mE9EEA95C541D41E36A0F4844153A67EAAA0D12F7(L_7, /*hidden argument*/NULL);
		// client.SendClientRequestToServer(data);
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_8 = ((Program_t84DC950B444F8124814710510D84842D4A59581C_StaticFields*)il2cpp_codegen_static_fields_for(Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var))->get_client_0();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_9 = ___data1;
		NullCheck(L_8);
		VirtActionInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(6 /* System.Void ProtocolClient.IClient::SendClientRequestToServer(System.Byte[]) */, L_8, L_9);
		// }
		return;
	}
}
// System.Void ProtocolClient.Program::OnServerRequest(System.Object,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Program_OnServerRequest_m2D0A0E1642BA74E65B0BE9E897A5B50A64544415 (RuntimeObject * ___a0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Console_t79987B1B5914E76054A8CBE506B9E11936A8BC07_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4340D3686DEB126422F53A38EF24AD59BFAEEDEF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F24B739AA45780EA08FF218BC64454668DC866A);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// string received = new string(Encoding.UTF8.GetChars(data));
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_0;
		L_0 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = ___data1;
		NullCheck(L_0);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_2;
		L_2 = VirtFuncInvoker1< CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(23 /* System.Char[] System.Text.Encoding::GetChars(System.Byte[]) */, L_0, L_1);
		String_t* L_3;
		L_3 = String_CreateString_mC7F57CE6ED768CF86591160424FE55D5CBA7C344(NULL, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// Console.WriteLine("OnClientRequest: " + a.ToString() + "; Text/Data:" + received);
		RuntimeObject * L_4 = ___a0;
		NullCheck(L_4);
		String_t* L_5;
		L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		String_t* L_6 = V_0;
		String_t* L_7;
		L_7 = String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78(_stringLiteral4340D3686DEB126422F53A38EF24AD59BFAEEDEF, L_5, _stringLiteral7F24B739AA45780EA08FF218BC64454668DC866A, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t79987B1B5914E76054A8CBE506B9E11936A8BC07_il2cpp_TypeInfo_var);
		Console_WriteLine_mE9EEA95C541D41E36A0F4844153A67EAAA0D12F7(L_7, /*hidden argument*/NULL);
		// client.SendClientResponseToServer(data);
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_8 = ((Program_t84DC950B444F8124814710510D84842D4A59581C_StaticFields*)il2cpp_codegen_static_fields_for(Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var))->get_client_0();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_9 = ___data1;
		NullCheck(L_8);
		VirtActionInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(7 /* System.Void ProtocolClient.IClient::SendClientResponseToServer(System.Byte[]) */, L_8, L_9);
		// }
		return;
	}
}
// System.Void ProtocolClient.Program::OnServerStatusUpdate(System.Object,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Program_OnServerStatusUpdate_mCB1063529DE2E3330D8915545C4859A935172FCC (RuntimeObject * ___a0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Console_t79987B1B5914E76054A8CBE506B9E11936A8BC07_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F24B739AA45780EA08FF218BC64454668DC866A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE64AFDAF0040B375D3BD8A9F4E8E8065E273167F);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// string received = new string(Encoding.UTF8.GetChars(data));
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_0;
		L_0 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = ___data1;
		NullCheck(L_0);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_2;
		L_2 = VirtFuncInvoker1< CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(23 /* System.Char[] System.Text.Encoding::GetChars(System.Byte[]) */, L_0, L_1);
		String_t* L_3;
		L_3 = String_CreateString_mC7F57CE6ED768CF86591160424FE55D5CBA7C344(NULL, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// Console.WriteLine("OnServerStatusUpdate: " + a.ToString() + "; Text/Data:" + received);
		RuntimeObject * L_4 = ___a0;
		NullCheck(L_4);
		String_t* L_5;
		L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		String_t* L_6 = V_0;
		String_t* L_7;
		L_7 = String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78(_stringLiteralE64AFDAF0040B375D3BD8A9F4E8E8065E273167F, L_5, _stringLiteral7F24B739AA45780EA08FF218BC64454668DC866A, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t79987B1B5914E76054A8CBE506B9E11936A8BC07_il2cpp_TypeInfo_var);
		Console_WriteLine_mE9EEA95C541D41E36A0F4844153A67EAAA0D12F7(L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ProtocolClient.Program::OnConnectionException(System.Object,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Program_OnConnectionException_m63BF7B0C6CDAFC553DA9EE126D7C14E802191003 (RuntimeObject * ___a0, Exception_t * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Console_t79987B1B5914E76054A8CBE506B9E11936A8BC07_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralED18E4874205E7C94B9863D6E8A3DCC112F2FA7D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Console.WriteLine("OnClientException: " + e.ToString());
		Exception_t * L_0 = ___e1;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteralED18E4874205E7C94B9863D6E8A3DCC112F2FA7D, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t79987B1B5914E76054A8CBE506B9E11936A8BC07_il2cpp_TypeInfo_var);
		Console_WriteLine_mE9EEA95C541D41E36A0F4844153A67EAAA0D12F7(L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ProtocolClient.Program::Main(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Program_Main_m1B3FC9CF9690BECCD200565DEBA25978D8E14899 (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Console_t79987B1B5914E76054A8CBE506B9E11936A8BC07_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1__ctor_mB90F555001341586440DD1B03C6894D2E0730A26_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1__ctor_mD4C0E5291CBD3101F3D364590F483445B7A27994_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Program_OnConnectionException_m63BF7B0C6CDAFC553DA9EE126D7C14E802191003_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Program_OnServerRequest_m2D0A0E1642BA74E65B0BE9E897A5B50A64544415_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Program_OnServerResponse_mC8C36D4F38846D1C686CFE44D14D42C4B4B04054_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Program_OnServerStatusUpdate_mCB1063529DE2E3330D8915545C4859A935172FCC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0275001C02F73E21B4F7E80C7B45DE35755D42CF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral086ACF708E659BABAC341FF91E92F3393010FE37);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral58549ED8B8D9F9B518B419295904911D7280D5B6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral76C3D4024DE9EE847070E35CC5A197DC21F66FEE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB2F5E43AE5BCE860EA9E569C97183101099BEAD3);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_0 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_1 = NULL;
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* V_2 = NULL;
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* V_3 = NULL;
	NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * V_4 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 3);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// client = new TCPClient();
		TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * L_0 = (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E *)il2cpp_codegen_object_new(TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E_il2cpp_TypeInfo_var);
		TCPClient__ctor_m032ED8789EFF986A3548F635F8383BBE08DB858E(L_0, /*hidden argument*/NULL);
		((Program_t84DC950B444F8124814710510D84842D4A59581C_StaticFields*)il2cpp_codegen_static_fields_for(Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var))->set_client_0(L_0);
		// client.ServerRequestEvent  += OnServerRequest;
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_1 = ((Program_t84DC950B444F8124814710510D84842D4A59581C_StaticFields*)il2cpp_codegen_static_fields_for(Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var))->get_client_0();
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_2 = (EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)il2cpp_codegen_object_new(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mD4C0E5291CBD3101F3D364590F483445B7A27994(L_2, NULL, (intptr_t)((intptr_t)Program_OnServerRequest_m2D0A0E1642BA74E65B0BE9E897A5B50A64544415_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mD4C0E5291CBD3101F3D364590F483445B7A27994_RuntimeMethod_var);
		NullCheck(L_1);
		IClient_add_ServerRequestEvent_m97136FA563855AF87640E7865F21753CD3E10115(L_1, L_2, /*hidden argument*/NULL);
		// client.ServerResponseEvent += OnServerResponse;
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_3 = ((Program_t84DC950B444F8124814710510D84842D4A59581C_StaticFields*)il2cpp_codegen_static_fields_for(Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var))->get_client_0();
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_4 = (EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)il2cpp_codegen_object_new(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mD4C0E5291CBD3101F3D364590F483445B7A27994(L_4, NULL, (intptr_t)((intptr_t)Program_OnServerResponse_mC8C36D4F38846D1C686CFE44D14D42C4B4B04054_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mD4C0E5291CBD3101F3D364590F483445B7A27994_RuntimeMethod_var);
		NullCheck(L_3);
		IClient_add_ServerResponseEvent_mFF1D827B95AA6BF11F7AB5C97F77340DA5B1692C(L_3, L_4, /*hidden argument*/NULL);
		// client.ConnectionException += OnConnectionException;
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_5 = ((Program_t84DC950B444F8124814710510D84842D4A59581C_StaticFields*)il2cpp_codegen_static_fields_for(Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var))->get_client_0();
		EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D * L_6 = (EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D *)il2cpp_codegen_object_new(EventHandler_1_tF35C29479A47541F59B60DF11C6FA06006A1559D_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mB90F555001341586440DD1B03C6894D2E0730A26(L_6, NULL, (intptr_t)((intptr_t)Program_OnConnectionException_m63BF7B0C6CDAFC553DA9EE126D7C14E802191003_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mB90F555001341586440DD1B03C6894D2E0730A26_RuntimeMethod_var);
		NullCheck(L_5);
		IClient_add_ConnectionException_mE43AA0290A2010DBF9451129E56253D50563F0B3(L_5, L_6, /*hidden argument*/NULL);
		// client.ServerStatusUpdateEvent += OnServerStatusUpdate;
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_7 = ((Program_t84DC950B444F8124814710510D84842D4A59581C_StaticFields*)il2cpp_codegen_static_fields_for(Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var))->get_client_0();
		EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 * L_8 = (EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8 *)il2cpp_codegen_object_new(EventHandler_1_tE4F7E439316B9E28301DB57F560AE8A101C2F5B8_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mD4C0E5291CBD3101F3D364590F483445B7A27994(L_8, NULL, (intptr_t)((intptr_t)Program_OnServerStatusUpdate_mCB1063529DE2E3330D8915545C4859A935172FCC_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mD4C0E5291CBD3101F3D364590F483445B7A27994_RuntimeMethod_var);
		NullCheck(L_7);
		IClient_add_ServerStatusUpdateEvent_m874656AF3D714B25C12F7CF629134ACF4830CEE0(L_7, L_8, /*hidden argument*/NULL);
		// client.ConnectToServer("127.0.0.1", 1338);
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_9 = ((Program_t84DC950B444F8124814710510D84842D4A59581C_StaticFields*)il2cpp_codegen_static_fields_for(Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var))->get_client_0();
		NullCheck(L_9);
		VirtActionInvoker2< String_t*, uint16_t >::Invoke(4 /* System.Void ProtocolClient.IClient::ConnectToServer(System.String,System.UInt16) */, L_9, _stringLiteral76C3D4024DE9EE847070E35CC5A197DC21F66FEE, (uint16_t)((int32_t)1338));
		// if (client.GetConnectionState() == ConnectionState.NOT_CONNECTED) {
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_10 = ((Program_t84DC950B444F8124814710510D84842D4A59581C_StaticFields*)il2cpp_codegen_static_fields_for(Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var))->get_client_0();
		NullCheck(L_10);
		int32_t L_11;
		L_11 = VirtFuncInvoker0< int32_t >::Invoke(25 /* ProtocolClient.ConnectionState ProtocolClient.IClient::GetConnectionState() */, L_10);
		if (L_11)
		{
			goto IL_009e;
		}
	}
	{
		// Console.WriteLine("Not Connected!");
		IL2CPP_RUNTIME_CLASS_INIT(Console_t79987B1B5914E76054A8CBE506B9E11936A8BC07_il2cpp_TypeInfo_var);
		Console_WriteLine_mE9EEA95C541D41E36A0F4844153A67EAAA0D12F7(_stringLiteral0275001C02F73E21B4F7E80C7B45DE35755D42CF, /*hidden argument*/NULL);
		goto IL_009e;
	}

IL_008e:
	{
		// client.Update();
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_12 = ((Program_t84DC950B444F8124814710510D84842D4A59581C_StaticFields*)il2cpp_codegen_static_fields_for(Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var))->get_client_0();
		NullCheck(L_12);
		VirtActionInvoker0::Invoke(8 /* System.Void ProtocolClient.IClient::Update() */, L_12);
		// Thread.Sleep(1);
		Thread_Sleep_m8E61FC80BD38981CB18CA549909710790283DDCC(1, /*hidden argument*/NULL);
	}

IL_009e:
	{
		// while (client.GetConnectionState() == ConnectionState.CONNECTING)
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_13 = ((Program_t84DC950B444F8124814710510D84842D4A59581C_StaticFields*)il2cpp_codegen_static_fields_for(Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var))->get_client_0();
		NullCheck(L_13);
		int32_t L_14;
		L_14 = VirtFuncInvoker0< int32_t >::Invoke(25 /* ProtocolClient.ConnectionState ProtocolClient.IClient::GetConnectionState() */, L_13);
		if ((((int32_t)L_14) == ((int32_t)1)))
		{
			goto IL_008e;
		}
	}
	{
		// byte[] requestByteArray = null;
		V_0 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)NULL;
		// var msgAsCharArray = "Client->Request Server->Response".ToCharArray();
		NullCheck(_stringLiteral58549ED8B8D9F9B518B419295904911D7280D5B6);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_15;
		L_15 = String_ToCharArray_m33E93AEB7086CBEBDFA5730EAAC49686F144089C(_stringLiteral58549ED8B8D9F9B518B419295904911D7280D5B6, /*hidden argument*/NULL);
		V_2 = L_15;
		// requestByteArray = Encoding.UTF8.GetBytes(msgAsCharArray);
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_16;
		L_16 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_17 = V_2;
		NullCheck(L_16);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_18;
		L_18 = VirtFuncInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* >::Invoke(13 /* System.Byte[] System.Text.Encoding::GetBytes(System.Char[]) */, L_16, L_17);
		V_0 = L_18;
		// byte[] responseByteArray = null;
		V_1 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)NULL;
		// var msgAsCharArray = "Server->Request Client->Response!".ToCharArray();
		NullCheck(_stringLiteral086ACF708E659BABAC341FF91E92F3393010FE37);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_19;
		L_19 = String_ToCharArray_m33E93AEB7086CBEBDFA5730EAAC49686F144089C(_stringLiteral086ACF708E659BABAC341FF91E92F3393010FE37, /*hidden argument*/NULL);
		V_3 = L_19;
		// responseByteArray = Encoding.UTF8.GetBytes(msgAsCharArray);
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_20;
		L_20 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_21 = V_3;
		NullCheck(L_20);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_22;
		L_22 = VirtFuncInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* >::Invoke(13 /* System.Byte[] System.Text.Encoding::GetBytes(System.Char[]) */, L_20, L_21);
		V_1 = L_22;
		// client.SendClientRequestToServer(requestByteArray);
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_23 = ((Program_t84DC950B444F8124814710510D84842D4A59581C_StaticFields*)il2cpp_codegen_static_fields_for(Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var))->get_client_0();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_24 = V_0;
		NullCheck(L_23);
		VirtActionInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(6 /* System.Void ProtocolClient.IClient::SendClientRequestToServer(System.Byte[]) */, L_23, L_24);
		// client.SendClientResponseToServer(responseByteArray);
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_25 = ((Program_t84DC950B444F8124814710510D84842D4A59581C_StaticFields*)il2cpp_codegen_static_fields_for(Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var))->get_client_0();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_26 = V_1;
		NullCheck(L_25);
		VirtActionInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(7 /* System.Void ProtocolClient.IClient::SendClientResponseToServer(System.Byte[]) */, L_25, L_26);
	}

IL_00f3:
	{
	}

IL_00f4:
	try
	{ // begin try (depth: 1)
		{
			// client.Update();
			IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_27 = ((Program_t84DC950B444F8124814710510D84842D4A59581C_StaticFields*)il2cpp_codegen_static_fields_for(Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var))->get_client_0();
			NullCheck(L_27);
			VirtActionInvoker0::Invoke(8 /* System.Void ProtocolClient.IClient::Update() */, L_27);
			// if (client.GetConnectionState() != ConnectionState.CONNECTED)
			IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_28 = ((Program_t84DC950B444F8124814710510D84842D4A59581C_StaticFields*)il2cpp_codegen_static_fields_for(Program_t84DC950B444F8124814710510D84842D4A59581C_il2cpp_TypeInfo_var))->get_client_0();
			NullCheck(L_28);
			int32_t L_29;
			L_29 = VirtFuncInvoker0< int32_t >::Invoke(25 /* ProtocolClient.ConnectionState ProtocolClient.IClient::GetConnectionState() */, L_28);
			if ((((int32_t)L_29) == ((int32_t)2)))
			{
				goto IL_011d;
			}
		}

IL_010b:
		{
			// Console.WriteLine("Connection lost");
			IL2CPP_RUNTIME_CLASS_INIT(Console_t79987B1B5914E76054A8CBE506B9E11936A8BC07_il2cpp_TypeInfo_var);
			Console_WriteLine_mE9EEA95C541D41E36A0F4844153A67EAAA0D12F7(_stringLiteralB2F5E43AE5BCE860EA9E569C97183101099BEAD3, /*hidden argument*/NULL);
			// Console.ReadKey();
			ConsoleKeyInfo_tDA8AC07839288484FCB167A81B4FBA92ECCEAF88  L_30;
			L_30 = Console_ReadKey_mFB50402A43E0994EBC4CB125217B21F60F6AA07C(/*hidden argument*/NULL);
			// return;
			goto IL_014a;
		}

IL_011d:
		{
			// }
			goto IL_013e;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
			goto CATCH_011f;
		throw e;
	}

CATCH_011f:
	{ // begin catch(System.NotImplementedException)
		// catch (NotImplementedException exception) {
		V_4 = ((NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 *)__exception_local);
		// Console.WriteLine("An Exception Occoured: "  + exception.GetType().ToString());
		NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * L_31 = V_4;
		NullCheck(L_31);
		Type_t * L_32;
		L_32 = Exception_GetType_mC5B8B5C944B326B751282AB0E8C25A7F85457D9F(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		String_t* L_33;
		L_33 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_32);
		String_t* L_34;
		L_34 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralA387B4B1E5AE081CE48BC1B1CAF6D2442E00297B)), L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Console_t79987B1B5914E76054A8CBE506B9E11936A8BC07_il2cpp_TypeInfo_var)));
		Console_WriteLine_mE9EEA95C541D41E36A0F4844153A67EAAA0D12F7(L_34, /*hidden argument*/NULL);
		// }
		goto IL_013e;
	} // end catch (depth: 1)

IL_013e:
	{
		// Thread.Sleep(1000);
		Thread_Sleep_m8E61FC80BD38981CB18CA549909710790283DDCC(((int32_t)1000), /*hidden argument*/NULL);
		// while (true) {
		goto IL_00f3;
	}

IL_014a:
	{
		// }
		return;
	}
}
// System.Void ProtocolClient.Program::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Program__ctor_m1FF83F114A9435990F39E63E8A396F697DD5A6F8 (Program_t84DC950B444F8124814710510D84842D4A59581C * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// ProtocolClient.NetMessage ProtocolClient.ProtocolTool::CreateMessage(ProtocolClient.NetMessageType,System.Byte[],System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ProtocolTool_CreateMessage_m441288169BBE1CC1C48836E7755683B5A637C2E5 (int32_t ___messageType0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___payload1, uint8_t ___clientId2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * V_0 = NULL;
	{
		// NetMessage newMessage = new NetMessage
		// {
		//     type = (byte)messageType,
		//     payload = payload,
		//     clientId = clientId
		// };
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_0 = (NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 *)il2cpp_codegen_object_new(NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670_il2cpp_TypeInfo_var);
		NetMessage__ctor_mA4C1F498E1F2D598A9ACD6F14BBF921B3F639C7B(L_0, /*hidden argument*/NULL);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_1 = L_0;
		int32_t L_2 = ___messageType0;
		NullCheck(L_1);
		L_1->set_type_2((uint8_t)((int32_t)((uint8_t)L_2)));
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_3 = L_1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4 = ___payload1;
		NullCheck(L_3);
		L_3->set_payload_5(L_4);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_5 = L_3;
		uint8_t L_6 = ___clientId2;
		NullCheck(L_5);
		L_5->set_clientId_1(L_6);
		V_0 = L_5;
		// if (payload != null)
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_7 = ___payload1;
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		// newMessage.payloadSize = (ushort)payload.Length;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_8 = V_0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_9 = ___payload1;
		NullCheck(L_9);
		NullCheck(L_8);
		L_8->set_payloadSize_4((uint16_t)((int32_t)((uint16_t)((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length))))));
		// newMessage.crc = GetCRC(payload, newMessage.payloadSize, 0);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_10 = V_0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11 = ___payload1;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_12 = V_0;
		NullCheck(L_12);
		uint16_t L_13 = L_12->get_payloadSize_4();
		uint8_t L_14;
		L_14 = ProtocolTool_GetCRC_mAAC3331A0C1C131150EE5196ECDFB30353E20590(L_11, L_13, 0, /*hidden argument*/NULL);
		NullCheck(L_10);
		L_10->set_crc_3(L_14);
	}

IL_003c:
	{
		// return newMessage;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_15 = V_0;
		return L_15;
	}
}
// System.Byte ProtocolClient.ProtocolTool::GetCRC(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t ProtocolTool_GetCRC_mAAC3331A0C1C131150EE5196ECDFB30353E20590 (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data0, int32_t ___length1, int32_t ___startIndex2, const RuntimeMethod* method)
{
	uint8_t V_0 = 0x0;
	uint8_t V_1 = 0x0;
	int32_t V_2 = 0;
	{
		// byte crc = 0;        
		V_1 = (uint8_t)0;
		// int dataIndex = startIndex;
		int32_t L_0 = ___startIndex2;
		V_2 = L_0;
		goto IL_003a;
	}

IL_0006:
	{
		// length--;
		int32_t L_1 = ___length1;
		___length1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1));
		// crc ^= data[dataIndex];        
		uint8_t L_2 = V_1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = ___data0;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		uint8_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_1 = (uint8_t)((int32_t)((uint8_t)((int32_t)((int32_t)L_2^(int32_t)L_6))));
		// dataIndex++;
		int32_t L_7 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
		// for (i = 0; i < 8; i++)
		V_0 = (uint8_t)0;
		goto IL_0036;
	}

IL_001a:
	{
		// if ((crc & 0x80) != 0)
		uint8_t L_8 = V_1;
		if (!((int32_t)((int32_t)L_8&(int32_t)((int32_t)128))))
		{
			goto IL_002c;
		}
	}
	{
		// crc = (byte)((crc << 1) ^ 0x07);
		uint8_t L_9 = V_1;
		V_1 = (uint8_t)((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_9<<(int32_t)1))^(int32_t)7))));
		goto IL_0031;
	}

IL_002c:
	{
		// crc <<= 1;
		uint8_t L_10 = V_1;
		V_1 = (uint8_t)((int32_t)((uint8_t)((int32_t)((int32_t)L_10<<(int32_t)1))));
	}

IL_0031:
	{
		// for (i = 0; i < 8; i++)
		uint8_t L_11 = V_0;
		V_0 = (uint8_t)((int32_t)((uint8_t)((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1))));
	}

IL_0036:
	{
		// for (i = 0; i < 8; i++)
		uint8_t L_12 = V_0;
		if ((((int32_t)L_12) < ((int32_t)8)))
		{
			goto IL_001a;
		}
	}

IL_003a:
	{
		// while (length > startIndex)
		int32_t L_13 = ___length1;
		int32_t L_14 = ___startIndex2;
		if ((((int32_t)L_13) > ((int32_t)L_14)))
		{
			goto IL_0006;
		}
	}
	{
		// return crc;
		uint8_t L_15 = V_1;
		return L_15;
	}
}
// System.Byte[] ProtocolClient.ProtocolTool::NetMessageToByteArray(ProtocolClient.NetMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ProtocolTool_NetMessageToByteArray_mCA7A90C8FF3813DC49D9DE9B039FD261C7AFBDFD (NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ___toConvert0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_0 = NULL;
	{
		// byte[] rawNetMessage = new byte[toConvert.payloadSize + HeaderSizeOfNetMessage];
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_0 = ___toConvert0;
		NullCheck(L_0);
		uint16_t L_1 = L_0->get_payloadSize_4();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)6)));
		V_0 = L_2;
		// rawNetMessage[0] = toConvert.messageId;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = V_0;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_4 = ___toConvert0;
		NullCheck(L_4);
		uint8_t L_5 = L_4->get_messageId_0();
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)L_5);
		// rawNetMessage[1] = toConvert.clientId;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6 = V_0;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_7 = ___toConvert0;
		NullCheck(L_7);
		uint8_t L_8 = L_7->get_clientId_1();
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)L_8);
		// rawNetMessage[2] = toConvert.type;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_9 = V_0;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_10 = ___toConvert0;
		NullCheck(L_10);
		uint8_t L_11 = L_10->get_type_2();
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint8_t)L_11);
		// rawNetMessage[4] = (byte)toConvert.payloadSize;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_12 = V_0;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_13 = ___toConvert0;
		NullCheck(L_13);
		uint16_t L_14 = L_13->get_payloadSize_4();
		NullCheck(L_12);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (uint8_t)((int32_t)((uint8_t)L_14)));
		// rawNetMessage[5] = (byte)(toConvert.payloadSize >> 8);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_15 = V_0;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_16 = ___toConvert0;
		NullCheck(L_16);
		uint16_t L_17 = L_16->get_payloadSize_4();
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(5), (uint8_t)((int32_t)((uint8_t)((int32_t)((int32_t)L_17>>(int32_t)8)))));
		// rawNetMessage[3] = GetCRC(toConvert.payload, toConvert.payloadSize);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_18 = V_0;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_19 = ___toConvert0;
		NullCheck(L_19);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_20 = L_19->get_payload_5();
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_21 = ___toConvert0;
		NullCheck(L_21);
		uint16_t L_22 = L_21->get_payloadSize_4();
		uint8_t L_23;
		L_23 = ProtocolTool_GetCRC_mAAC3331A0C1C131150EE5196ECDFB30353E20590(L_20, L_22, 0, /*hidden argument*/NULL);
		NullCheck(L_18);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(3), (uint8_t)L_23);
		// if(toConvert.payload != null) {
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_24 = ___toConvert0;
		NullCheck(L_24);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_25 = L_24->get_payload_5();
		if (!L_25)
		{
			goto IL_0070;
		}
	}
	{
		// Array.ConstrainedCopy(toConvert.payload, 0, rawNetMessage, HeaderSizeOfNetMessage, toConvert.payloadSize);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_26 = ___toConvert0;
		NullCheck(L_26);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_27 = L_26->get_payload_5();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_28 = V_0;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_29 = ___toConvert0;
		NullCheck(L_29);
		uint16_t L_30 = L_29->get_payloadSize_4();
		Array_ConstrainedCopy_mD26D19D1D515C4D884E36327A9B0C2BA79CD7003((RuntimeArray *)(RuntimeArray *)L_27, 0, (RuntimeArray *)(RuntimeArray *)L_28, 6, L_30, /*hidden argument*/NULL);
	}

IL_0070:
	{
		// return rawNetMessage;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_31 = V_0;
		return L_31;
	}
}
// System.Int32 ProtocolClient.ProtocolTool::GetSizeOfNetMessageInBuffer(System.Collections.Generic.IEnumerable`1<System.Byte>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ProtocolTool_GetSizeOfNetMessageInBuffer_m051D0740337106C72030ED21A975CA266F3C4045 (RuntimeObject* ___rawNetMessage0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Count_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m1AFC4338BF90E364F564717BDBFDD9D1217C2980_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	uint16_t V_0 = 0;
	{
		// if (rawNetMessage.Count() < HeaderSizeOfNetMessage) return HeaderSizeOfNetMessage;
		RuntimeObject* L_0 = ___rawNetMessage0;
		int32_t L_1;
		L_1 = Enumerable_Count_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m1AFC4338BF90E364F564717BDBFDD9D1217C2980(L_0, /*hidden argument*/Enumerable_Count_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m1AFC4338BF90E364F564717BDBFDD9D1217C2980_RuntimeMethod_var);
		if ((((int32_t)L_1) >= ((int32_t)6)))
		{
			goto IL_000b;
		}
	}
	{
		// if (rawNetMessage.Count() < HeaderSizeOfNetMessage) return HeaderSizeOfNetMessage;
		return 6;
	}

IL_000b:
	{
		// ushort sizeOfPayload = (ushort)(rawNetMessage.ElementAt(4) | rawNetMessage.ElementAt(5) << 8);
		RuntimeObject* L_2 = ___rawNetMessage0;
		uint8_t L_3;
		L_3 = Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4(L_2, 4, /*hidden argument*/Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_RuntimeMethod_var);
		RuntimeObject* L_4 = ___rawNetMessage0;
		uint8_t L_5;
		L_5 = Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4(L_4, 5, /*hidden argument*/Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_RuntimeMethod_var);
		V_0 = (uint16_t)((int32_t)((uint16_t)((int32_t)((int32_t)L_3|(int32_t)((int32_t)((int32_t)L_5<<(int32_t)8))))));
		// return HeaderSizeOfNetMessage + sizeOfPayload;
		uint16_t L_6 = V_0;
		return ((int32_t)il2cpp_codegen_add((int32_t)6, (int32_t)L_6));
	}
}
// ProtocolClient.NetMessage ProtocolClient.ProtocolTool::GetNetMessageHeaderFromByteArray(System.Collections.Generic.IEnumerable`1<System.Byte>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ProtocolTool_GetNetMessageHeaderFromByteArray_mD826FE9DD204E15BCDBEC861D88A1746269CCDF8 (RuntimeObject* ___rawNetMessage0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Count_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m1AFC4338BF90E364F564717BDBFDD9D1217C2980_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (HeaderSizeOfNetMessage > rawNetMessage.Count()) return null;
		RuntimeObject* L_0 = ___rawNetMessage0;
		int32_t L_1;
		L_1 = Enumerable_Count_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m1AFC4338BF90E364F564717BDBFDD9D1217C2980(L_0, /*hidden argument*/Enumerable_Count_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m1AFC4338BF90E364F564717BDBFDD9D1217C2980_RuntimeMethod_var);
		if ((((int32_t)6) <= ((int32_t)L_1)))
		{
			goto IL_000b;
		}
	}
	{
		// if (HeaderSizeOfNetMessage > rawNetMessage.Count()) return null;
		return (NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 *)NULL;
	}

IL_000b:
	{
		// NetMessage result = new NetMessage();
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_2 = (NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 *)il2cpp_codegen_object_new(NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670_il2cpp_TypeInfo_var);
		NetMessage__ctor_mA4C1F498E1F2D598A9ACD6F14BBF921B3F639C7B(L_2, /*hidden argument*/NULL);
		// result.messageId = rawNetMessage.ElementAt(0);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_3 = L_2;
		RuntimeObject* L_4 = ___rawNetMessage0;
		uint8_t L_5;
		L_5 = Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4(L_4, 0, /*hidden argument*/Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_RuntimeMethod_var);
		NullCheck(L_3);
		L_3->set_messageId_0(L_5);
		// result.clientId = rawNetMessage.ElementAt(1);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_6 = L_3;
		RuntimeObject* L_7 = ___rawNetMessage0;
		uint8_t L_8;
		L_8 = Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4(L_7, 1, /*hidden argument*/Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_RuntimeMethod_var);
		NullCheck(L_6);
		L_6->set_clientId_1(L_8);
		// result.type = rawNetMessage.ElementAt(2);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_9 = L_6;
		RuntimeObject* L_10 = ___rawNetMessage0;
		uint8_t L_11;
		L_11 = Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4(L_10, 2, /*hidden argument*/Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_RuntimeMethod_var);
		NullCheck(L_9);
		L_9->set_type_2(L_11);
		// result.crc = rawNetMessage.ElementAt(3);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_12 = L_9;
		RuntimeObject* L_13 = ___rawNetMessage0;
		uint8_t L_14;
		L_14 = Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4(L_13, 3, /*hidden argument*/Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_RuntimeMethod_var);
		NullCheck(L_12);
		L_12->set_crc_3(L_14);
		// result.payloadSize = (ushort)(rawNetMessage.ElementAt(4) | rawNetMessage.ElementAt(5) << 8);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_15 = L_12;
		RuntimeObject* L_16 = ___rawNetMessage0;
		uint8_t L_17;
		L_17 = Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4(L_16, 4, /*hidden argument*/Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_RuntimeMethod_var);
		RuntimeObject* L_18 = ___rawNetMessage0;
		uint8_t L_19;
		L_19 = Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4(L_18, 5, /*hidden argument*/Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_RuntimeMethod_var);
		NullCheck(L_15);
		L_15->set_payloadSize_4((uint16_t)((int32_t)((uint16_t)((int32_t)((int32_t)L_17|(int32_t)((int32_t)((int32_t)L_19<<(int32_t)8)))))));
		// return result;
		return L_15;
	}
}
// ProtocolClient.NetMessage ProtocolClient.ProtocolTool::GetMessageFromByteArray(System.Collections.Generic.IEnumerable`1<System.Byte>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ProtocolTool_GetMessageFromByteArray_mBD85E536ED14B56570D1CEE94F5CCFEC234121A7 (RuntimeObject* ___rawNetMessage0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Count_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m1AFC4338BF90E364F564717BDBFDD9D1217C2980_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToArray_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m00F16F6DDED1338D9656B4EDE7F8AE2B2E2251E4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * V_0 = NULL;
	{
		// if (HeaderSizeOfNetMessage > rawNetMessage.Count()) return null;
		RuntimeObject* L_0 = ___rawNetMessage0;
		int32_t L_1;
		L_1 = Enumerable_Count_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m1AFC4338BF90E364F564717BDBFDD9D1217C2980(L_0, /*hidden argument*/Enumerable_Count_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m1AFC4338BF90E364F564717BDBFDD9D1217C2980_RuntimeMethod_var);
		if ((((int32_t)6) <= ((int32_t)L_1)))
		{
			goto IL_000b;
		}
	}
	{
		// if (HeaderSizeOfNetMessage > rawNetMessage.Count()) return null;
		return (NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 *)NULL;
	}

IL_000b:
	{
		// NetMessage result = new NetMessage();
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_2 = (NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 *)il2cpp_codegen_object_new(NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670_il2cpp_TypeInfo_var);
		NetMessage__ctor_mA4C1F498E1F2D598A9ACD6F14BBF921B3F639C7B(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		// result.messageId = rawNetMessage.ElementAt(0);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_3 = V_0;
		RuntimeObject* L_4 = ___rawNetMessage0;
		uint8_t L_5;
		L_5 = Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4(L_4, 0, /*hidden argument*/Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_RuntimeMethod_var);
		NullCheck(L_3);
		L_3->set_messageId_0(L_5);
		// result.clientId = rawNetMessage.ElementAt(1);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_6 = V_0;
		RuntimeObject* L_7 = ___rawNetMessage0;
		uint8_t L_8;
		L_8 = Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4(L_7, 1, /*hidden argument*/Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_RuntimeMethod_var);
		NullCheck(L_6);
		L_6->set_clientId_1(L_8);
		// result.type = rawNetMessage.ElementAt(2);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_9 = V_0;
		RuntimeObject* L_10 = ___rawNetMessage0;
		uint8_t L_11;
		L_11 = Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4(L_10, 2, /*hidden argument*/Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_RuntimeMethod_var);
		NullCheck(L_9);
		L_9->set_type_2(L_11);
		// result.crc = rawNetMessage.ElementAt(3);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_12 = V_0;
		RuntimeObject* L_13 = ___rawNetMessage0;
		uint8_t L_14;
		L_14 = Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4(L_13, 3, /*hidden argument*/Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_RuntimeMethod_var);
		NullCheck(L_12);
		L_12->set_crc_3(L_14);
		// result.payloadSize = (ushort)(rawNetMessage.ElementAt(4) | rawNetMessage.ElementAt(5) << 8);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_15 = V_0;
		RuntimeObject* L_16 = ___rawNetMessage0;
		uint8_t L_17;
		L_17 = Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4(L_16, 4, /*hidden argument*/Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_RuntimeMethod_var);
		RuntimeObject* L_18 = ___rawNetMessage0;
		uint8_t L_19;
		L_19 = Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4(L_18, 5, /*hidden argument*/Enumerable_ElementAt_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m126D43395576B016FA3FA3F86C6CFF150AFA8FA4_RuntimeMethod_var);
		NullCheck(L_15);
		L_15->set_payloadSize_4((uint16_t)((int32_t)((uint16_t)((int32_t)((int32_t)L_17|(int32_t)((int32_t)((int32_t)L_19<<(int32_t)8)))))));
		// result.payload = new byte[result.payloadSize];
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_20 = V_0;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_21 = V_0;
		NullCheck(L_21);
		uint16_t L_22 = L_21->get_payloadSize_4();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_23 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)L_22);
		NullCheck(L_20);
		L_20->set_payload_5(L_23);
		// if(result.payloadSize > 0)
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_24 = V_0;
		NullCheck(L_24);
		uint16_t L_25 = L_24->get_payloadSize_4();
		if ((((int32_t)L_25) <= ((int32_t)0)))
		{
			goto IL_0090;
		}
	}
	{
		// Array.ConstrainedCopy(rawNetMessage.ToArray(), HeaderSizeOfNetMessage, result.payload, 0, result.payloadSize);
		RuntimeObject* L_26 = ___rawNetMessage0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_27;
		L_27 = Enumerable_ToArray_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m00F16F6DDED1338D9656B4EDE7F8AE2B2E2251E4(L_26, /*hidden argument*/Enumerable_ToArray_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m00F16F6DDED1338D9656B4EDE7F8AE2B2E2251E4_RuntimeMethod_var);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_28 = V_0;
		NullCheck(L_28);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_29 = L_28->get_payload_5();
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_30 = V_0;
		NullCheck(L_30);
		uint16_t L_31 = L_30->get_payloadSize_4();
		Array_ConstrainedCopy_mD26D19D1D515C4D884E36327A9B0C2BA79CD7003((RuntimeArray *)(RuntimeArray *)L_27, 6, (RuntimeArray *)(RuntimeArray *)L_29, 0, L_31, /*hidden argument*/NULL);
	}

IL_0090:
	{
		// return result;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_32 = V_0;
		return L_32;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 Ship::get_size()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5 (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method)
{
	{
		// public int size { get; private set; }
		int32_t L_0 = __this->get_U3CsizeU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void Ship::set_size(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ship_set_size_mB69F1B1C31D843999D4B226F7A54D53CCEB8EB11 (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int size { get; private set; }
		int32_t L_0 = ___value0;
		__this->set_U3CsizeU3Ek__BackingField_3(L_0);
		return;
	}
}
// UnityEngine.GameObject[] Ship::get_cells()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* Ship_get_cells_m3B9D3AF56A593555CCF9630DFE9D766BB763A91A (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method)
{
	{
		// public GameObject[] cells { get; private set; }
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_0 = __this->get_U3CcellsU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void Ship::set_cells(UnityEngine.GameObject[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ship_set_cells_m62750C426A0B1E8A06CAA44EE64B6C00F6070949 (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___value0, const RuntimeMethod* method)
{
	{
		// public GameObject[] cells { get; private set; }
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_0 = ___value0;
		__this->set_U3CcellsU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void Ship::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ship__ctor_m5734D7D291677BF934981BBF9F7B42B0EC457056 (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, int32_t ___size0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public Ship(int size)
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// this.size = size;
		int32_t L_0 = ___size0;
		Ship_set_size_mB69F1B1C31D843999D4B226F7A54D53CCEB8EB11_inline(__this, L_0, /*hidden argument*/NULL);
		// this.cells = new GameObject[size];
		int32_t L_1 = ___size0;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_2 = (GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642*)(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642*)SZArrayNew(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642_il2cpp_TypeInfo_var, (uint32_t)L_1);
		Ship_set_cells_m62750C426A0B1E8A06CAA44EE64B6C00F6070949_inline(__this, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Ship::AddCell(UnityEngine.GameObject,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ship_AddCell_mFE93A99E3A478DBDED4B3AA6FBE1D0BEA8408D4D (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cell0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// cells[index] = cell;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_0;
		L_0 = Ship_get_cells_m3B9D3AF56A593555CCF9630DFE9D766BB763A91A_inline(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___index1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = ___cell0;
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)L_2);
		// cell.GetComponent<Cell>().isShip = true;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = ___cell0;
		NullCheck(L_3);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_4;
		L_4 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_3, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		NullCheck(L_4);
		L_4->set_isShip_6((bool)1);
		// cell.GetComponent<RawImage>().color = color;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = ___cell0;
		NullCheck(L_5);
		RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * L_6;
		L_6 = GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7(L_5, /*hidden argument*/GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E_il2cpp_TypeInfo_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_7 = ((Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E_StaticFields*)il2cpp_codegen_static_fields_for(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E_il2cpp_TypeInfo_var))->get_color_0();
		NullCheck(L_6);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_6, L_7);
		// }
		return;
	}
}
// System.Void Ship::Delete()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ship_Delete_m7E022DC8EECBAB392587D14CC54C2245D12211AF (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// foreach (GameObject c in this.cells)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_0;
		L_0 = Ship_get_cells_m3B9D3AF56A593555CCF9630DFE9D766BB763A91A_inline(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_002d;
	}

IL_000b:
	{
		// foreach (GameObject c in this.cells)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		// c.GetComponent<Cell>().isShip = false;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = L_4;
		NullCheck(L_5);
		Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149 * L_6;
		L_6 = GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2(L_5, /*hidden argument*/GameObject_GetComponent_TisCell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_m354622F5DA296C2BD10B3372BA2A2841D7A111E2_RuntimeMethod_var);
		NullCheck(L_6);
		L_6->set_isShip_6((bool)0);
		// c.GetComponent<RawImage>().color = Cell.standardColor;
		NullCheck(L_5);
		RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * L_7;
		L_7 = GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7(L_5, /*hidden argument*/GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_8 = ((Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_StaticFields*)il2cpp_codegen_static_fields_for(Cell_t577CC64507C54E1575DDC4DAC3545D2A42CE9149_il2cpp_TypeInfo_var))->get_standardColor_9();
		NullCheck(L_7);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_7, L_8);
		int32_t L_9 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_002d:
	{
		// foreach (GameObject c in this.cells)
		int32_t L_10 = V_1;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_11 = V_0;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Ship::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ship__cctor_m75C4224EC2259358122532BE23FF2DAB441EBF39 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static readonly Color color = new Color(1, 0, 0);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494((&L_0), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		((Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E_StaticFields*)il2cpp_codegen_static_fields_for(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E_il2cpp_TypeInfo_var))->set_color_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ProtocolClient.TCPClient::IncrementNetMessageId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_IncrementNetMessageId_mBB6ED4FEBC70571FFDEE75AC13C1E16A9A18C4AE (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, const RuntimeMethod* method)
{
	{
		// currentMessageId++;
		uint8_t L_0 = __this->get_currentMessageId_12();
		__this->set_currentMessageId_12((uint8_t)((int32_t)((uint8_t)((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1)))));
		// }
		return;
	}
}
// ProtocolClient.ConnectionState ProtocolClient.TCPClient::GetConnectionState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TCPClient_GetConnectionState_m5DD3E995295BD398FF0217A553B044D551F35601 (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, const RuntimeMethod* method)
{
	{
		// bool isSocketConnected = connection.IsConnected();
		TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * L_0 = __this->get_connection_5();
		NullCheck(L_0);
		bool L_1;
		L_1 = TCPConnection_IsConnected_m4D516CEE807E45D142058548598798839D2CD523(L_0, /*hidden argument*/NULL);
		// if (isSocketConnected) {
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// if (IsHandshakeComplete() == false)
		bool L_2;
		L_2 = VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean ProtocolClient.IClient::IsHandshakeComplete() */, __this);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		// return ConnectionState.CONNECTING;
		return (int32_t)(1);
	}

IL_0017:
	{
		// return ConnectionState.CONNECTED;
		return (int32_t)(2);
	}

IL_0019:
	{
		// return ConnectionState.NOT_CONNECTED;
		return (int32_t)(0);
	}
}
// System.Boolean ProtocolClient.TCPClient::IsHandshakeComplete()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TCPClient_IsHandshakeComplete_m8426F9E0E6ABBA575F777C0049E24B28ACCA4EAB (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, const RuntimeMethod* method)
{
	{
		// return clientId != 0;
		uint8_t L_0 = __this->get_clientId_4();
		return (bool)((!(((uint32_t)L_0) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Void ProtocolClient.TCPClient::ConnectToServer(System.String,System.UInt16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_ConnectToServer_m482B17ACD66A39EB20CB1DBC7025ECC21E11B346 (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, String_t* ___host0, uint16_t ___port1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// lastHost = host;
		String_t* L_0 = ___host0;
		__this->set_lastHost_8(L_0);
		// lastPort = port;
		uint16_t L_1 = ___port1;
		__this->set_lastPort_9(L_1);
		// sendBuffer.SetConnection(connection);
		NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * L_2 = __this->get_sendBuffer_7();
		TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * L_3 = __this->get_connection_5();
		NullCheck(L_2);
		NetSendBuffer_SetConnection_mCE9EB7BFBA86CCB47B22217122D58D82C9E595F8_inline(L_2, L_3, /*hidden argument*/NULL);
		// if (connection.ConnectToRemoteHost(host, port))
		TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * L_4 = __this->get_connection_5();
		String_t* L_5 = ___host0;
		uint16_t L_6 = ___port1;
		NullCheck(L_4);
		bool L_7;
		L_7 = TCPConnection_ConnectToRemoteHost_m48C2352E350516CF0CA667BFD6CC6523392429F0(L_4, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0040;
		}
	}
	{
		// lastResponse = DateTime.Now;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_il2cpp_TypeInfo_var);
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_8;
		L_8 = DateTime_get_Now_mCAC695993D6E2C57B900C83BEF3F8B18BC4EBC2C(/*hidden argument*/NULL);
		__this->set_lastResponse_10(L_8);
		// SendRegisterClient();
		VirtActionInvoker0::Invoke(9 /* System.Void ProtocolClient.IClient::SendRegisterClient() */, __this);
		// }
		return;
	}

IL_0040:
	{
		// OnConnectionClosed();
		VirtActionInvoker0::Invoke(14 /* System.Void ProtocolClient.IClient::OnConnectionClosed() */, __this);
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::OnConnectionClosed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_OnConnectionClosed_mBA3B3BBC6E498FB41843B96DA29DB58B52D57EF2 (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, const RuntimeMethod* method)
{
	{
		// clientId = 0;
		__this->set_clientId_4((uint8_t)0);
		// currentMessageId = 0;
		__this->set_currentMessageId_12((uint8_t)0);
		// recVBuffer.Reset();
		NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * L_0 = __this->get_recVBuffer_6();
		NullCheck(L_0);
		NetReceiveBuffer_Reset_m54E919D930053D4AF69BBA24FA0EBE4FE5DB1C93(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::ResetConnection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_ResetConnection_mC3FF01B88B24DD066D9661D27515FA9174D84131 (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, const RuntimeMethod* method)
{
	{
		// connection.CloseConnection();
		TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * L_0 = __this->get_connection_5();
		NullCheck(L_0);
		bool L_1;
		L_1 = TCPConnection_CloseConnection_mDFDCFAE5342A9BADB24EFB94979FDB7DAEF6FEB9(L_0, /*hidden argument*/NULL);
		// connection.ConnectToRemoteHost(lastHost, lastPort);
		TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * L_2 = __this->get_connection_5();
		String_t* L_3 = __this->get_lastHost_8();
		uint16_t L_4 = __this->get_lastPort_9();
		NullCheck(L_2);
		bool L_5;
		L_5 = TCPConnection_ConnectToRemoteHost_m48C2352E350516CF0CA667BFD6CC6523392429F0(L_2, L_3, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::SendClientRequestToServer(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_SendClientRequestToServer_m869CF66785F9717FA1F1DE863289CD04D5EA9F8D (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___request0, const RuntimeMethod* method)
{
	{
		// BuildAndSendNetMessage(NetMessageType.ClientRequest, request);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ___request0;
		VirtActionInvoker2< int32_t, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(23 /* System.Void ProtocolClient.IClient::BuildAndSendNetMessage(ProtocolClient.NetMessageType,System.Byte[]) */, __this, 5, L_0);
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::OnServerResponse(ProtocolClient.NetMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_OnServerResponse_m1E3B2B85F307579FBEDD1AF33490C07FE21A18DE (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ___netMsg0, const RuntimeMethod* method)
{
	{
		// InvokeServerResponseEvent(netMsg.payload);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_0 = ___netMsg0;
		NullCheck(L_0);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = L_0->get_payload_5();
		IClient_InvokeServerResponseEvent_m9B0164790F2DA73105E5667A06B83270466C9E5D(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::OnServerRequest(ProtocolClient.NetMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_OnServerRequest_mCF3D19889EDBF627C97B282E7DC9512A3F4C1456 (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ___netMsg0, const RuntimeMethod* method)
{
	{
		// InvokeServerRequestEvent(netMsg.payload);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_0 = ___netMsg0;
		NullCheck(L_0);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = L_0->get_payload_5();
		IClient_InvokeServerRequestEvent_m0D9439F86E2BBB0D74AAE21C75DE2F4A9520D328(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::SendClientResponseToServer(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_SendClientResponseToServer_m2FCF8084F6F36DD1785628B3722F1CB64D059025 (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___request0, const RuntimeMethod* method)
{
	{
		// BuildAndSendNetMessage(NetMessageType.ClientResponse, request);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ___request0;
		VirtActionInvoker2< int32_t, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(23 /* System.Void ProtocolClient.IClient::BuildAndSendNetMessage(ProtocolClient.NetMessageType,System.Byte[]) */, __this, 6, L_0);
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::OnClientMessageWasCorrupted(ProtocolClient.NetMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_OnClientMessageWasCorrupted_mEB98637188773289641783D5D8432F7A9AA174B0 (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ___netMsg0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Count_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m1AFC4338BF90E364F564717BDBFDD9D1217C2980_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	uint8_t V_0 = 0x0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* G_B2_0 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	{
		// byte corruptedId = 0xFF;
		V_0 = (uint8_t)((int32_t)255);
		// if (netMsg.payload?.Count() > 0) {
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_0 = ___netMsg0;
		NullCheck(L_0);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = L_0->get_payload_5();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0013;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_001b;
	}

IL_0013:
	{
		int32_t L_3;
		L_3 = Enumerable_Count_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m1AFC4338BF90E364F564717BDBFDD9D1217C2980((RuntimeObject*)(RuntimeObject*)G_B2_0, /*hidden argument*/Enumerable_Count_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m1AFC4338BF90E364F564717BDBFDD9D1217C2980_RuntimeMethod_var);
		G_B3_0 = ((((int32_t)L_3) > ((int32_t)0))? 1 : 0);
	}

IL_001b:
	{
		if (!G_B3_0)
		{
			goto IL_0033;
		}
	}
	{
		// corruptedId = netMsg.payload[0];
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_4 = ___netMsg0;
		NullCheck(L_4);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = L_4->get_payload_5();
		NullCheck(L_5);
		int32_t L_6 = 0;
		uint8_t L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = L_7;
		// sendBuffer.SendMessageById(corruptedId);
		NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * L_8 = __this->get_sendBuffer_7();
		uint8_t L_9 = V_0;
		NullCheck(L_8);
		bool L_10;
		L_10 = NetSendBuffer_SendMessageById_m54A10CDCB7A0F39E5C7DC328531BA25CD94C256E(L_8, L_9, /*hidden argument*/NULL);
	}

IL_0033:
	{
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::OnServerStatusUpdate(ProtocolClient.NetMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_OnServerStatusUpdate_mF8FFABEA850ACAA1369A0E9EE6A85006472A53E2 (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ___netMsg0, const RuntimeMethod* method)
{
	{
		// InvokeServerStatusUpdateEvent(netMsg.payload);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_0 = ___netMsg0;
		NullCheck(L_0);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = L_0->get_payload_5();
		IClient_InvokeServerStatusUpdateEvent_mF960B2A19E3CD03F38AE28F4E2A3B35D31F58A6C(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::SendCloseConnection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_SendCloseConnection_mF69A8364BD2B4A99A53B9028633D8A81A722EB8F (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, const RuntimeMethod* method)
{
	{
		// BuildAndSendNetMessage(NetMessageType.CloseConnection, null);
		VirtActionInvoker2< int32_t, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(23 /* System.Void ProtocolClient.IClient::BuildAndSendNetMessage(ProtocolClient.NetMessageType,System.Byte[]) */, __this, ((int32_t)10), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)NULL);
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::SendKeepAlive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_SendKeepAlive_m8B9AB748AF11B32E631457696A7075A64757F340 (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, const RuntimeMethod* method)
{
	{
		// BuildAndSendNetMessage(NetMessageType.KeepAlive, null);
		VirtActionInvoker2< int32_t, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(23 /* System.Void ProtocolClient.IClient::BuildAndSendNetMessage(ProtocolClient.NetMessageType,System.Byte[]) */, __this, 3, (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)NULL);
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::SendServerMessageWasCorrupted(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_SendServerMessageWasCorrupted_m9A56635B3F8DA16AB5BCCC7C365C59D4D3823CC6 (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, uint8_t ___messageId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// BuildAndSendNetMessage(NetMessageType.MessageError, BitConverter.GetBytes(messageId));
		uint8_t L_0 = ___messageId0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1;
		L_1 = BitConverter_GetBytes_m4178DFE7B47F12FAF452AA4742B16FC3BA9618A8(L_0, /*hidden argument*/NULL);
		VirtActionInvoker2< int32_t, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(23 /* System.Void ProtocolClient.IClient::BuildAndSendNetMessage(ProtocolClient.NetMessageType,System.Byte[]) */, __this, ((int32_t)9), L_1);
		// }
		return;
	}
}
// System.Byte[] ProtocolClient.TCPClient::ReadData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* TCPClient_ReadData_mAE5DC4C255E03C400C43731E36EC7A3BA050A6FA (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, const RuntimeMethod* method)
{
	{
		// return connection.ReadAll();
		TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * L_0 = __this->get_connection_5();
		NullCheck(L_0);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1;
		L_1 = TCPConnection_ReadAll_m67CC289F1F234B79CF29BBB18578C84B3A9C328A(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void ProtocolClient.TCPClient::SendRegisterClient()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_SendRegisterClient_mB97E379DD079177034D4B9CC186B8906A43217B1 (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, const RuntimeMethod* method)
{
	{
		// BuildAndSendNetMessage(NetMessageType.RegisterClient, null);
		VirtActionInvoker2< int32_t, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(23 /* System.Void ProtocolClient.IClient::BuildAndSendNetMessage(ProtocolClient.NetMessageType,System.Byte[]) */, __this, 1, (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)NULL);
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::OnClientRegistered(ProtocolClient.NetMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_OnClientRegistered_mEA4CD57E24E074F2F95AB6ABCE935C95B804C83E (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ___netMsg0, const RuntimeMethod* method)
{
	{
		// clientId = netMsg.clientId;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_0 = ___netMsg0;
		NullCheck(L_0);
		uint8_t L_1 = L_0->get_clientId_1();
		__this->set_clientId_4(L_1);
		// UpdateTimeOut();
		VirtActionInvoker0::Invoke(19 /* System.Void ProtocolClient.IClient::UpdateTimeOut() */, __this);
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::UpdateTimeOut()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_UpdateTimeOut_m98DC9616111657B768763ABEDD8CF2D397DB5C27 (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// lastResponse = DateTime.Now;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_il2cpp_TypeInfo_var);
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_0;
		L_0 = DateTime_get_Now_mCAC695993D6E2C57B900C83BEF3F8B18BC4EBC2C(/*hidden argument*/NULL);
		__this->set_lastResponse_10(L_0);
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::BuildAndSendNetMessage(ProtocolClient.NetMessageType,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_BuildAndSendNetMessage_mF1A8A4F4C48821475627AF77FE50D4C01CD776C4 (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, int32_t ___type0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___dataToSend1, const RuntimeMethod* method)
{
	NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * V_0 = NULL;
	{
		// var netMessage = ProtocolTool.CreateMessage(type, dataToSend, clientId);
		int32_t L_0 = ___type0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = ___dataToSend1;
		uint8_t L_2 = __this->get_clientId_4();
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_3;
		L_3 = ProtocolTool_CreateMessage_m441288169BBE1CC1C48836E7755683B5A637C2E5(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// if (type != NetMessageType.RegisterClient) {
		int32_t L_4 = ___type0;
		if ((((int32_t)L_4) == ((int32_t)1)))
		{
			goto IL_001e;
		}
	}
	{
		// netMessage.clientId = this.clientId;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_5 = V_0;
		uint8_t L_6 = __this->get_clientId_4();
		NullCheck(L_5);
		L_5->set_clientId_1(L_6);
	}

IL_001e:
	{
		// sendBuffer.SendNetMessage(netMessage);
		NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * L_7 = __this->get_sendBuffer_7();
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_8 = V_0;
		NullCheck(L_7);
		bool L_9;
		L_9 = NetSendBuffer_SendNetMessage_m1F1D322E9AA41FCA8BC955B3F53F9F0FC2FB0817(L_7, L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::HandleNewNetMessages()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_HandleNewNetMessages_mC1D0A4F5F587F147D7CE405BEBCD6B40B61E2FFD (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, const RuntimeMethod* method)
{
	NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * V_0 = NULL;
	{
		// NetMessage netMessage = null;
		V_0 = (NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 *)NULL;
	}

IL_0002:
	{
		// netMessage = recVBuffer.TakeNextNetMessageById(currentMessageId);
		NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * L_0 = __this->get_recVBuffer_6();
		uint8_t L_1 = __this->get_currentMessageId_12();
		NullCheck(L_0);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_2;
		L_2 = NetReceiveBuffer_TakeNextNetMessageById_m14D6F45C0DC5E5EB6E95C8E7FBF746BE6F4AE24C(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// if (netMessage != null)
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		// OnNewNetMessageReceived(netMessage);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_4 = V_0;
		VirtActionInvoker1< NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * >::Invoke(21 /* System.Void ProtocolClient.IClient::OnNewNetMessageReceived(ProtocolClient.NetMessage) */, __this, L_4);
	}

IL_001e:
	{
		// } while (netMessage != null);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_5 = V_0;
		if (L_5)
		{
			goto IL_0002;
		}
	}
	{
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_Update_mDFFF455DA91EF9842289A1CE47C5A66E60A3D408 (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, const RuntimeMethod* method)
{
	Exception_t * V_0 = NULL;
	{
		// recVBuffer.AddDataToBuffer(ReadData());
		NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * L_0 = __this->get_recVBuffer_6();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1;
		L_1 = VirtFuncInvoker0< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(26 /* System.Byte[] ProtocolClient.TCPClient::ReadData() */, __this);
		NullCheck(L_0);
		NetReceiveBuffer_AddDataToBuffer_mEF538D68769E179CD93D8D5E80144EA57405E06C(L_0, L_1, /*hidden argument*/NULL);
		// SendKeepAlive();
		VirtActionInvoker0::Invoke(10 /* System.Void ProtocolClient.IClient::SendKeepAlive() */, __this);
		// recVBuffer.SearchInBuffer();
		NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * L_2 = __this->get_recVBuffer_6();
		NullCheck(L_2);
		NetReceiveBuffer_SearchInBuffer_m825A53363B87F61FB7EE0CA4D76E550C6F2B6690(L_2, /*hidden argument*/NULL);
		// HandleNewNetMessages();
		TCPClient_HandleNewNetMessages_mC1D0A4F5F587F147D7CE405BEBCD6B40B61E2FFD(__this, /*hidden argument*/NULL);
		// Exception exceptionFound = connection.GetLastException();
		TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * L_3 = __this->get_connection_5();
		NullCheck(L_3);
		Exception_t * L_4;
		L_4 = TCPConnection_GetLastException_m4312EE21BDC7253C84B58F1889624532F658C4A6_inline(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// if (exceptionFound != null) {
		Exception_t * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0049;
		}
	}
	{
		// connection.ResetLastException();
		TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * L_6 = __this->get_connection_5();
		NullCheck(L_6);
		TCPConnection_ResetLastException_m67BB221266DB25CC43F373CB1ED5A4347D08A87C(L_6, /*hidden argument*/NULL);
		// InvokeExceptionEvent(exceptionFound);
		Exception_t * L_7 = V_0;
		IClient_InvokeExceptionEvent_mE37866660EDE33F55DA29AA097356B87AB12B898(__this, L_7, /*hidden argument*/NULL);
	}

IL_0049:
	{
		// CheckForTimeOut();
		VirtActionInvoker0::Invoke(20 /* System.Void ProtocolClient.IClient::CheckForTimeOut() */, __this);
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::CheckForTimeOut()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_CheckForTimeOut_m5055E62D1E62E7E9106D5137B54EADB7F759E451 (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  V_0;
	memset((&V_0), 0, sizeof(V_0));
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// var diffTime = (DateTime.Now.Subtract(lastResponse));
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_il2cpp_TypeInfo_var);
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_0;
		L_0 = DateTime_get_Now_mCAC695993D6E2C57B900C83BEF3F8B18BC4EBC2C(/*hidden argument*/NULL);
		V_1 = L_0;
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_1 = __this->get_lastResponse_10();
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  L_2;
		L_2 = DateTime_Subtract_mB3CA7BD87D07F9A98AA7A571D7CA179A4209AE0B((DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 *)(&V_1), L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// if (diffTime.TotalMilliseconds > timeOutMilliseconds) {
		double L_3;
		L_3 = TimeSpan_get_TotalMilliseconds_m97368AE0609D865EB2A6BAE96AAA97AF8BDBF1C5((TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 *)(&V_0), /*hidden argument*/NULL);
		uint32_t L_4 = __this->get_timeOutMilliseconds_11();
		if ((!(((double)L_3) > ((double)((double)((double)((double)((uint32_t)L_4))))))))
		{
			goto IL_0030;
		}
	}
	{
		// throw new Exception("Timeout!");
		Exception_t * L_5 = (Exception_t *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		Exception__ctor_m8ECDE8ACA7F2E0EF1144BD1200FB5DB2870B5F11(L_5, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral8BD159C3D7F4D6D6F5C894061300DC61F9604327)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&TCPClient_CheckForTimeOut_m5055E62D1E62E7E9106D5137B54EADB7F759E451_RuntimeMethod_var)));
	}

IL_0030:
	{
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::OnNewNetMessageReceived(ProtocolClient.NetMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_OnNewNetMessageReceived_m9357397956DB11F8F05FF4DD8C3CB8E1534BDE99 (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * ___nM0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// byte crcValue = ProtocolTool.GetCRC(nM.payload, (int)nM.payloadSize, 0);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_0 = ___nM0;
		NullCheck(L_0);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = L_0->get_payload_5();
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_2 = ___nM0;
		NullCheck(L_2);
		uint16_t L_3 = L_2->get_payloadSize_4();
		uint8_t L_4;
		L_4 = ProtocolTool_GetCRC_mAAC3331A0C1C131150EE5196ECDFB30353E20590(L_1, L_3, 0, /*hidden argument*/NULL);
		// bool isSame = (crcValue == nM.crc);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_5 = ___nM0;
		NullCheck(L_5);
		uint8_t L_6 = L_5->get_crc_3();
		// if (isSame == false)
		if (((((int32_t)L_4) == ((int32_t)L_6))? 1 : 0))
		{
			goto IL_0034;
		}
	}
	{
		// OnClientMessageWasCorrupted(nM);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_7 = ___nM0;
		VirtActionInvoker1< NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * >::Invoke(17 /* System.Void ProtocolClient.IClient::OnClientMessageWasCorrupted(ProtocolClient.NetMessage) */, __this, L_7);
		// UpdateTimeOut();
		VirtActionInvoker0::Invoke(19 /* System.Void ProtocolClient.IClient::UpdateTimeOut() */, __this);
		// throw new Exception("CRC mismatch handling Not Yet Implemented!");
		Exception_t * L_8 = (Exception_t *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		Exception__ctor_m8ECDE8ACA7F2E0EF1144BD1200FB5DB2870B5F11(L_8, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral0C2606B489C63F0DF336C88CA02DAA3D51AC64CC)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&TCPClient_OnNewNetMessageReceived_m9357397956DB11F8F05FF4DD8C3CB8E1534BDE99_RuntimeMethod_var)));
	}

IL_0034:
	{
		// IncrementNetMessageId();
		TCPClient_IncrementNetMessageId_mBB6ED4FEBC70571FFDEE75AC13C1E16A9A18C4AE(__this, /*hidden argument*/NULL);
		// NetMessageType nMType = (NetMessageType)nM.type;
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_9 = ___nM0;
		NullCheck(L_9);
		uint8_t L_10 = L_9->get_type_2();
		V_0 = L_10;
		int32_t L_11 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_11, (int32_t)1)))
		{
			case 0:
			{
				goto IL_0097;
			}
			case 1:
			{
				goto IL_00a8;
			}
			case 2:
			{
				goto IL_00ae;
			}
			case 3:
			{
				goto IL_0073;
			}
			case 4:
			{
				goto IL_00ae;
			}
			case 5:
			{
				goto IL_00ae;
			}
			case 6:
			{
				goto IL_007c;
			}
			case 7:
			{
				goto IL_0085;
			}
			case 8:
			{
				goto IL_008e;
			}
			case 9:
			{
				goto IL_00a0;
			}
		}
	}
	{
		goto IL_00ae;
	}

IL_0073:
	{
		// this.OnServerStatusUpdate(nM);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_12 = ___nM0;
		VirtActionInvoker1< NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * >::Invoke(18 /* System.Void ProtocolClient.IClient::OnServerStatusUpdate(ProtocolClient.NetMessage) */, __this, L_12);
		// break;
		goto IL_00ae;
	}

IL_007c:
	{
		// this.OnServerRequest(nM);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_13 = ___nM0;
		VirtActionInvoker1< NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * >::Invoke(15 /* System.Void ProtocolClient.IClient::OnServerRequest(ProtocolClient.NetMessage) */, __this, L_13);
		// break;
		goto IL_00ae;
	}

IL_0085:
	{
		// this.OnServerResponse(nM);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_14 = ___nM0;
		VirtActionInvoker1< NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * >::Invoke(16 /* System.Void ProtocolClient.IClient::OnServerResponse(ProtocolClient.NetMessage) */, __this, L_14);
		// break;
		goto IL_00ae;
	}

IL_008e:
	{
		// this.OnClientMessageWasCorrupted(nM);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_15 = ___nM0;
		VirtActionInvoker1< NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * >::Invoke(17 /* System.Void ProtocolClient.IClient::OnClientMessageWasCorrupted(ProtocolClient.NetMessage) */, __this, L_15);
		// break;
		goto IL_00ae;
	}

IL_0097:
	{
		// this.OnClientRegistered(nM);
		NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * L_16 = ___nM0;
		VirtActionInvoker1< NetMessage_tB90B7CB77EA78E45CB4635B6527ED4E68A5A3670 * >::Invoke(13 /* System.Void ProtocolClient.IClient::OnClientRegistered(ProtocolClient.NetMessage) */, __this, L_16);
		// break;
		goto IL_00ae;
	}

IL_00a0:
	{
		// this.OnConnectionClosed();
		VirtActionInvoker0::Invoke(14 /* System.Void ProtocolClient.IClient::OnConnectionClosed() */, __this);
		// break;
		goto IL_00ae;
	}

IL_00a8:
	{
		// this.OnConnectionResetReceived();
		VirtActionInvoker0::Invoke(22 /* System.Void ProtocolClient.IClient::OnConnectionResetReceived() */, __this);
	}

IL_00ae:
	{
		// UpdateTimeOut();
		VirtActionInvoker0::Invoke(19 /* System.Void ProtocolClient.IClient::UpdateTimeOut() */, __this);
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPClient::OnConnectionResetReceived()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient_OnConnectionResetReceived_mA3AAF6AFD258C5B91AA92C435BEC4C304FFD63EB (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, const RuntimeMethod* method)
{
	{
		// ResetConnection();
		VirtActionInvoker0::Invoke(5 /* System.Void ProtocolClient.IClient::ResetConnection() */, __this);
		// throw new NotImplementedException();
		NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * L_0 = (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var)));
		NotImplementedException__ctor_mA2E9CE7F00CB335581A296D2596082D57E45BA83(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&TCPClient_OnConnectionResetReceived_mA3AAF6AFD258C5B91AA92C435BEC4C304FFD63EB_RuntimeMethod_var)));
	}
}
// System.Void ProtocolClient.TCPClient::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPClient__ctor_m032ED8789EFF986A3548F635F8383BBE08DB858E (TCPClient_t3A21F4597D0FEA9163D3C370B0B5EDBC5DF4117E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// protected TCPConnection connection = new TCPConnection();
		TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * L_0 = (TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C *)il2cpp_codegen_object_new(TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C_il2cpp_TypeInfo_var);
		TCPConnection__ctor_mF343843AB3AD623C131AAF1A9721FE426E8F25DC(L_0, /*hidden argument*/NULL);
		__this->set_connection_5(L_0);
		// protected NetReceiveBuffer recVBuffer = new NetReceiveBuffer();
		NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF * L_1 = (NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF *)il2cpp_codegen_object_new(NetReceiveBuffer_t9A5E4856C9A7280969822FF4B95DF8A28799DDEF_il2cpp_TypeInfo_var);
		NetReceiveBuffer__ctor_mA9D01D5830750AA6550E569EE006BF2D61DE6B17(L_1, /*hidden argument*/NULL);
		__this->set_recVBuffer_6(L_1);
		// protected NetSendBuffer sendBuffer = new NetSendBuffer();
		NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * L_2 = (NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 *)il2cpp_codegen_object_new(NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3_il2cpp_TypeInfo_var);
		NetSendBuffer__ctor_m450503519157A952342CDA00B9FD06B8C1730814(L_2, /*hidden argument*/NULL);
		__this->set_sendBuffer_7(L_2);
		// private UInt32 timeOutMilliseconds = 5000;
		__this->set_timeOutMilliseconds_11(((int32_t)5000));
		IClient__ctor_m0D07757F9C99DE9634940B5CC6F78547A53848E0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Exception ProtocolClient.TCPConnection::GetLastException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * TCPConnection_GetLastException_m4312EE21BDC7253C84B58F1889624532F658C4A6 (TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * __this, const RuntimeMethod* method)
{
	{
		// return lastException;
		Exception_t * L_0 = __this->get_lastException_1();
		return L_0;
	}
}
// System.Void ProtocolClient.TCPConnection::ResetLastException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPConnection_ResetLastException_m67BB221266DB25CC43F373CB1ED5A4347D08A87C (TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * __this, const RuntimeMethod* method)
{
	{
		// lastException = null;
		__this->set_lastException_1((Exception_t *)NULL);
		// }
		return;
	}
}
// System.Void ProtocolClient.TCPConnection::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPConnection__ctor_mF343843AB3AD623C131AAF1A9721FE426E8F25DC (TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// protected Socket tcpSocket= new Socket(SocketType.Stream, ProtocolType.Tcp);
		Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * L_0 = (Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 *)il2cpp_codegen_object_new(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_il2cpp_TypeInfo_var);
		Socket__ctor_m46AAE859577BBEED1D6246CEB874F2DFDEEE295C(L_0, 1, 6, /*hidden argument*/NULL);
		__this->set_tcpSocket_0(L_0);
		// public TCPConnection(){
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// tcpSocket.NoDelay = true;
		Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * L_1 = __this->get_tcpSocket_0();
		NullCheck(L_1);
		Socket_set_NoDelay_m34DCB8BF5275A85E1687308D0D016E860F229732(L_1, (bool)1, /*hidden argument*/NULL);
		// tcpSocket.SendTimeout = 5000;
		Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * L_2 = __this->get_tcpSocket_0();
		NullCheck(L_2);
		Socket_set_SendTimeout_mD92AF13D6A05318E6F892F0B145136138E2181E5(L_2, ((int32_t)5000), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean ProtocolClient.TCPConnection::CloseConnection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TCPConnection_CloseConnection_mDFDCFAE5342A9BADB24EFB94979FDB7DAEF6FEB9 (TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * __this, const RuntimeMethod* method)
{
	Exception_t * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// if (tcpSocket.Connected)
		Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * L_0 = __this->get_tcpSocket_0();
		NullCheck(L_0);
		bool L_1;
		L_1 = Socket_get_Connected_m6E8C88AC69580EF7782514CD48754D4D180D9CE0_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		// tcpSocket.Close();
		Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * L_2 = __this->get_tcpSocket_0();
		NullCheck(L_2);
		Socket_Close_m24AB78F5DAC1C39BB7FFB30A9620B2B07E01DEEB(L_2, /*hidden argument*/NULL);
		// }
		goto IL_0024;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001a;
		throw e;
	}

CATCH_001a:
	{ // begin catch(System.Exception)
		// catch (Exception catchedException)
		V_0 = ((Exception_t *)__exception_local);
		// this.lastException = catchedException;
		Exception_t * L_3 = V_0;
		__this->set_lastException_1(L_3);
		// }
		goto IL_0024;
	} // end catch (depth: 1)

IL_0024:
	{
		// return tcpSocket.Connected == false;
		Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * L_4 = __this->get_tcpSocket_0();
		NullCheck(L_4);
		bool L_5;
		L_5 = Socket_get_Connected_m6E8C88AC69580EF7782514CD48754D4D180D9CE0_inline(L_4, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_5) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean ProtocolClient.TCPConnection::ConnectToRemoteHost(System.String,System.UInt16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TCPConnection_ConnectToRemoteHost_m48C2352E350516CF0CA667BFD6CC6523392429F0 (TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * __this, String_t* ___ip0, uint16_t ___port1, const RuntimeMethod* method)
{
	Exception_t * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// tcpSocket.Connect(ip, port);
		Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * L_0 = __this->get_tcpSocket_0();
		String_t* L_1 = ___ip0;
		uint16_t L_2 = ___port1;
		NullCheck(L_0);
		Socket_Connect_m08347703B1E1E29B13D95F2719B76C7B2A51950A(L_0, L_1, L_2, /*hidden argument*/NULL);
		// }
		goto IL_0019;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
			goto CATCH_000f;
		throw e;
	}

CATCH_000f:
	{ // begin catch(System.Exception)
		// catch (Exception catchedException) {
		V_0 = ((Exception_t *)__exception_local);
		// this.lastException = catchedException;
		Exception_t * L_3 = V_0;
		__this->set_lastException_1(L_3);
		// }
		goto IL_0019;
	} // end catch (depth: 1)

IL_0019:
	{
		// return tcpSocket.Connected;
		Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * L_4 = __this->get_tcpSocket_0();
		NullCheck(L_4);
		bool L_5;
		L_5 = Socket_get_Connected_m6E8C88AC69580EF7782514CD48754D4D180D9CE0_inline(L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Byte[] ProtocolClient.TCPConnection::ReadAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* TCPConnection_ReadAll_m67CC289F1F234B79CF29BBB18578C84B3A9C328A (TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_0 = NULL;
	int32_t V_1 = 0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_2 = NULL;
	Exception_t * V_3 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_4 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// byte[] readBuffer = new byte[1024];
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024));
		V_0 = L_0;
		// int bytesRead = -1;
		V_1 = (-1);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		// bytesRead = tcpSocket.Receive(readBuffer);
		Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * L_1 = __this->get_tcpSocket_0();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3;
		L_3 = Socket_Receive_mD76EDDF7BFF5A9AB3D7FC197A13B8CF024DEC482(L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		// }
		goto IL_0029;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001c;
		throw e;
	}

CATCH_001c:
	{ // begin catch(System.Exception)
		// catch (Exception catchedException) {
		V_3 = ((Exception_t *)__exception_local);
		// this.lastException = catchedException;
		Exception_t * L_4 = V_3;
		__this->set_lastException_1(L_4);
		// return null;
		V_4 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)NULL;
		goto IL_003a;
	} // end catch (depth: 1)

IL_0029:
	{
		// byte[] recVData = new byte[bytesRead];
		int32_t L_5 = V_1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)L_5);
		V_2 = L_6;
		// Array.Copy(readBuffer, recVData, bytesRead);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_7 = V_0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_8 = V_2;
		int32_t L_9 = V_1;
		Array_Copy_m40103AA97DC582C557B912CF4BBE86A4D166F803((RuntimeArray *)(RuntimeArray *)L_7, (RuntimeArray *)(RuntimeArray *)L_8, L_9, /*hidden argument*/NULL);
		// return recVData;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10 = V_2;
		return L_10;
	}

IL_003a:
	{
		// }
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11 = V_4;
		return L_11;
	}
}
// System.Void ProtocolClient.TCPConnection::SendAll(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TCPConnection_SendAll_m5E7C42B504544CA6CFF78C397604D9825F5B5194 (TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___bytes0, const RuntimeMethod* method)
{
	Exception_t * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// tcpSocket.Send(bytes);
		Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * L_0 = __this->get_tcpSocket_0();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = ___bytes0;
		NullCheck(L_0);
		int32_t L_2;
		L_2 = Socket_Send_m8E974F1735436F40554635503D4E6C8EF387DA3D(L_0, L_1, /*hidden argument*/NULL);
		// }
		goto IL_0019;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
			goto CATCH_000f;
		throw e;
	}

CATCH_000f:
	{ // begin catch(System.Exception)
		// catch (Exception catchedException) {
		V_0 = ((Exception_t *)__exception_local);
		// this.lastException = catchedException;
		Exception_t * L_3 = V_0;
		__this->set_lastException_1(L_3);
		// }
		goto IL_0019;
	} // end catch (depth: 1)

IL_0019:
	{
		// }
		return;
	}
}
// System.Boolean ProtocolClient.TCPConnection::IsConnected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TCPConnection_IsConnected_m4D516CEE807E45D142058548598798839D2CD523 (TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * __this, const RuntimeMethod* method)
{
	{
		// return this.tcpSocket.Connected;
		Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * L_0 = __this->get_tcpSocket_0();
		NullCheck(L_0);
		bool L_1;
		L_1 = Socket_get_Connected_m6E8C88AC69580EF7782514CD48754D4D180D9CE0_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Cell/CellClickedEventHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellClickedEventHandler__ctor_m0E624D3592920AD801A5117354902BBC0671D60A (CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Cell/CellClickedEventHandler::Invoke(System.Object,CellEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellClickedEventHandler_Invoke_mA777B45CCDD94D5BE52E8BC63679ACF954DC4515 (CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * __this, RuntimeObject * ___sender0, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * ___e1, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject *, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___sender0, ___e1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___sender0, ___e1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * >::Invoke(targetMethod, ___sender0, ___e1);
					else
						GenericVirtActionInvoker1< CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * >::Invoke(targetMethod, ___sender0, ___e1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___sender0, ___e1);
					else
						VirtActionInvoker1< CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___sender0, ___e1);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___sender0, ___e1, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< RuntimeObject *, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * >::Invoke(targetMethod, targetThis, ___sender0, ___e1);
					else
						GenericVirtActionInvoker2< RuntimeObject *, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * >::Invoke(targetMethod, targetThis, ___sender0, ___e1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< RuntimeObject *, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___sender0, ___e1);
					else
						VirtActionInvoker2< RuntimeObject *, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___sender0, ___e1);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___sender0, ___e1, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, RuntimeObject *, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___sender0, ___e1, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult Cell/CellClickedEventHandler::BeginInvoke(System.Object,CellEventArgs,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CellClickedEventHandler_BeginInvoke_m612BDEDC33D7EDE53D6250C1C4638EF19C3A2BC9 (CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * __this, RuntimeObject * ___sender0, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * ___e1, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___sender0;
	__d_args[1] = ___e1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);;
}
// System.Void Cell/CellClickedEventHandler::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellClickedEventHandler_EndInvoke_m03B4956DBE6C776C7121299AF0CF01414D44BC45 (CellClickedEventHandler_t3A0CA73A81C5E055DCD54B4EADB4506B7E78A437 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Cell/CellHoverEventHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellHoverEventHandler__ctor_mA75DD0C304EFFDF2B7434946DE2A306DFEA4474F (CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Cell/CellHoverEventHandler::Invoke(System.Object,CellEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellHoverEventHandler_Invoke_m3617B3846462554FF0110231A58370BE5E446409 (CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * __this, RuntimeObject * ___sender0, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * ___e1, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject *, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___sender0, ___e1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___sender0, ___e1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * >::Invoke(targetMethod, ___sender0, ___e1);
					else
						GenericVirtActionInvoker1< CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * >::Invoke(targetMethod, ___sender0, ___e1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___sender0, ___e1);
					else
						VirtActionInvoker1< CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___sender0, ___e1);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___sender0, ___e1, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< RuntimeObject *, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * >::Invoke(targetMethod, targetThis, ___sender0, ___e1);
					else
						GenericVirtActionInvoker2< RuntimeObject *, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * >::Invoke(targetMethod, targetThis, ___sender0, ___e1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< RuntimeObject *, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___sender0, ___e1);
					else
						VirtActionInvoker2< RuntimeObject *, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___sender0, ___e1);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___sender0, ___e1, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, RuntimeObject *, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___sender0, ___e1, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult Cell/CellHoverEventHandler::BeginInvoke(System.Object,CellEventArgs,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CellHoverEventHandler_BeginInvoke_m7CF7D6D3415DD7441AAF0FE42F80EF95592D6204 (CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * __this, RuntimeObject * ___sender0, CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * ___e1, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___sender0;
	__d_args[1] = ___e1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);;
}
// System.Void Cell/CellHoverEventHandler::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellHoverEventHandler_EndInvoke_m747B67D81DC5CEA0585EA8276D7D82724DCC4381 (CellHoverEventHandler_t019FEC8EA464C866548D255DB6A9B4695CBB932A * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void NetworkManager/<>c__DisplayClass14_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass14_0__ctor_m8977A56E78603A833C559237A86A4EE44E5BBC58 (U3CU3Ec__DisplayClass14_0_tCCAB38C50CA018337D1FABDD31482528BBE3F1DF * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NetworkManager/<>c__DisplayClass14_0::<SendShips>b__0(Ship)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass14_0_U3CSendShipsU3Eb__0_mA5DBBC94404A3AC08A32CA6247039C842132B91A (U3CU3Ec__DisplayClass14_0_tCCAB38C50CA018337D1FABDD31482528BBE3F1DF * __this, Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * ___ship0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral410E5346BCA8EE150FFD507311DD85789F2E171E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5377EFB7D3540F7856D33807A13661041629AB2F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* G_B2_1 = NULL;
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* G_B2_2 = NULL;
	U3CU3Ec__DisplayClass14_0_tCCAB38C50CA018337D1FABDD31482528BBE3F1DF * G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* G_B1_1 = NULL;
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* G_B1_2 = NULL;
	U3CU3Ec__DisplayClass14_0_tCCAB38C50CA018337D1FABDD31482528BBE3F1DF * G_B1_3 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* G_B3_2 = NULL;
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* G_B3_3 = NULL;
	U3CU3Ec__DisplayClass14_0_tCCAB38C50CA018337D1FABDD31482528BBE3F1DF * G_B3_4 = NULL;
	{
		// message += ":" + ship.size + ":" + ship.startPos.x + ":" + ship.startPos.y + ":" + (ship.isHorizontal ? "H" : "V");
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9));
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = L_0;
		String_t* L_2 = __this->get_message_0();
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_2);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = L_1;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_4 = L_3;
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_5 = ___ship0;
		NullCheck(L_5);
		int32_t L_6;
		L_6 = Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5_inline(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		String_t* L_7;
		L_7 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_0), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_7);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_8 = L_4;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = L_8;
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_10 = ___ship0;
		NullCheck(L_10);
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_11 = L_10->get_address_of_startPos_2();
		int32_t L_12;
		L_12 = Vector2Int_get_x_mDBEFBCDF9C7924767344ED2CEE1307885AED947B_inline((Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 *)L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		String_t* L_13;
		L_13 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_0), /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_13);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_13);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14 = L_9;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_15 = L_14;
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_16 = ___ship0;
		NullCheck(L_16);
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_17 = L_16->get_address_of_startPos_2();
		int32_t L_18;
		L_18 = Vector2Int_get_y_m282591DEB0E70B02F4F4DDFAB90116AEC8E6B86C_inline((Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 *)L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		String_t* L_19;
		L_19 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_0), /*hidden argument*/NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_19);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)L_19);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_20 = L_15;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_21 = L_20;
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_22 = ___ship0;
		NullCheck(L_22);
		bool L_23 = L_22->get_isHorizontal_1();
		G_B1_0 = 8;
		G_B1_1 = L_21;
		G_B1_2 = L_21;
		G_B1_3 = __this;
		if (L_23)
		{
			G_B2_0 = 8;
			G_B2_1 = L_21;
			G_B2_2 = L_21;
			G_B2_3 = __this;
			goto IL_007f;
		}
	}
	{
		G_B3_0 = _stringLiteral5377EFB7D3540F7856D33807A13661041629AB2F;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_0084;
	}

IL_007f:
	{
		G_B3_0 = _stringLiteral410E5346BCA8EE150FFD507311DD85789F2E171E;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_0084:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		String_t* L_24;
		L_24 = String_Concat_mFEA7EFA1A6E75B96B1B7BC4526AAC864BFF83CC9(G_B3_3, /*hidden argument*/NULL);
		NullCheck(G_B3_4);
		G_B3_4->set_message_0(L_24);
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void NetworkManager/<HandleConnection>d__19::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CHandleConnectionU3Ed__19__ctor_m0B975E74604C0702C0AD73310F88842A78A9B3C1 (U3CHandleConnectionU3Ed__19_t2B2D10AA4C55A2999E347B3F7F12289D2BB5D833 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void NetworkManager/<HandleConnection>d__19::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CHandleConnectionU3Ed__19_System_IDisposable_Dispose_mC4F11A17118E3616415623F816761F66938BC246 (U3CHandleConnectionU3Ed__19_t2B2D10AA4C55A2999E347B3F7F12289D2BB5D833 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean NetworkManager/<HandleConnection>d__19::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CHandleConnectionU3Ed__19_MoveNext_mF1E5CB4AD3088A07508DD744A9C130B8274B3AE4 (U3CHandleConnectionU3Ed__19_t2B2D10AA4C55A2999E347B3F7F12289D2BB5D833 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForFixedUpdate_t675FCE2AEFAC5C924A4020474C997FF2CDD3F4C5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0275001C02F73E21B4F7E80C7B45DE35755D42CF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral58549ED8B8D9F9B518B419295904911D7280D5B6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6FE2772A25510BC2C002B4FCD5E9C590A0DF1DDE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral76C3D4024DE9EE847070E35CC5A197DC21F66FEE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB2F5E43AE5BCE860EA9E569C97183101099BEAD3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB76C73E87558DB3053EA9968982CB95A2747BB5C);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * V_1 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_2 = NULL;
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* V_3 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_0080;
			}
			case 2:
			{
				goto IL_00ef;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		__this->set_U3CU3E1__state_0((-1));
		// Debug.Log("HandleConnection started");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteralB76C73E87558DB3053EA9968982CB95A2747BB5C, /*hidden argument*/NULL);
		// client.ConnectToServer("127.0.0.1", 1338);
		NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * L_3 = V_1;
		NullCheck(L_3);
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_4 = L_3->get_client_7();
		NullCheck(L_4);
		VirtActionInvoker2< String_t*, uint16_t >::Invoke(4 /* System.Void ProtocolClient.IClient::ConnectToServer(System.String,System.UInt16) */, L_4, _stringLiteral76C3D4024DE9EE847070E35CC5A197DC21F66FEE, (uint16_t)((int32_t)1338));
		// if (client.GetConnectionState() == ConnectionState.NOT_CONNECTED)
		NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * L_5 = V_1;
		NullCheck(L_5);
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_6 = L_5->get_client_7();
		NullCheck(L_6);
		int32_t L_7;
		L_7 = VirtFuncInvoker0< int32_t >::Invoke(25 /* ProtocolClient.ConnectionState ProtocolClient.IClient::GetConnectionState() */, L_6);
		if (L_7)
		{
			goto IL_0087;
		}
	}
	{
		// Debug.Log("Not Connected!");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteral0275001C02F73E21B4F7E80C7B45DE35755D42CF, /*hidden argument*/NULL);
		// yield break;
		return (bool)0;
	}

IL_0061:
	{
		// client.Update();
		NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * L_8 = V_1;
		NullCheck(L_8);
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_9 = L_8->get_client_7();
		NullCheck(L_9);
		VirtActionInvoker0::Invoke(8 /* System.Void ProtocolClient.IClient::Update() */, L_9);
		// yield return new WaitForFixedUpdate();
		WaitForFixedUpdate_t675FCE2AEFAC5C924A4020474C997FF2CDD3F4C5 * L_10 = (WaitForFixedUpdate_t675FCE2AEFAC5C924A4020474C997FF2CDD3F4C5 *)il2cpp_codegen_object_new(WaitForFixedUpdate_t675FCE2AEFAC5C924A4020474C997FF2CDD3F4C5_il2cpp_TypeInfo_var);
		WaitForFixedUpdate__ctor_mB566493FB99A315BEF348791DE943A89E4B71F9C(L_10, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_10);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0080:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0087:
	{
		// while (client.GetConnectionState() == ConnectionState.CONNECTING)
		NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * L_11 = V_1;
		NullCheck(L_11);
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_12 = L_11->get_client_7();
		NullCheck(L_12);
		int32_t L_13;
		L_13 = VirtFuncInvoker0< int32_t >::Invoke(25 /* ProtocolClient.ConnectionState ProtocolClient.IClient::GetConnectionState() */, L_12);
		if ((((int32_t)L_13) == ((int32_t)1)))
		{
			goto IL_0061;
		}
	}
	{
		// byte[] requestByteArray = null;
		V_2 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)NULL;
		// var msgAsCharArray = "Client->Request Server->Response".ToCharArray();
		NullCheck(_stringLiteral58549ED8B8D9F9B518B419295904911D7280D5B6);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_14;
		L_14 = String_ToCharArray_m33E93AEB7086CBEBDFA5730EAAC49686F144089C(_stringLiteral58549ED8B8D9F9B518B419295904911D7280D5B6, /*hidden argument*/NULL);
		V_3 = L_14;
		// requestByteArray = Encoding.UTF8.GetBytes(msgAsCharArray);
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_15;
		L_15 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_16 = V_3;
		NullCheck(L_15);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_17;
		L_17 = VirtFuncInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* >::Invoke(13 /* System.Byte[] System.Text.Encoding::GetBytes(System.Char[]) */, L_15, L_16);
		V_2 = L_17;
		// client.SendClientRequestToServer(requestByteArray);
		NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * L_18 = V_1;
		NullCheck(L_18);
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_19 = L_18->get_client_7();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_20 = V_2;
		NullCheck(L_19);
		VirtActionInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(6 /* System.Void ProtocolClient.IClient::SendClientRequestToServer(System.Byte[]) */, L_19, L_20);
	}

IL_00ba:
	{
		// client.Update();
		NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * L_21 = V_1;
		NullCheck(L_21);
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_22 = L_21->get_client_7();
		NullCheck(L_22);
		VirtActionInvoker0::Invoke(8 /* System.Void ProtocolClient.IClient::Update() */, L_22);
		// if (client.GetConnectionState() != ConnectionState.CONNECTED)
		NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * L_23 = V_1;
		NullCheck(L_23);
		IClient_tD14F3222687B2DF278C1AE3131E91B8F73BD62CB * L_24 = L_23->get_client_7();
		NullCheck(L_24);
		int32_t L_25;
		L_25 = VirtFuncInvoker0< int32_t >::Invoke(25 /* ProtocolClient.ConnectionState ProtocolClient.IClient::GetConnectionState() */, L_24);
		if ((((int32_t)L_25) == ((int32_t)2)))
		{
			goto IL_00df;
		}
	}
	{
		// Debug.Log("Connection lost");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteralB2F5E43AE5BCE860EA9E569C97183101099BEAD3, /*hidden argument*/NULL);
		// break;
		goto IL_00f8;
	}

IL_00df:
	{
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_00ef:
	{
		__this->set_U3CU3E1__state_0((-1));
		// while (true)
		goto IL_00ba;
	}

IL_00f8:
	{
		// Debug.Log("HandleConnection finished");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteral6FE2772A25510BC2C002B4FCD5E9C590A0DF1DDE, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object NetworkManager/<HandleConnection>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CHandleConnectionU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60F3F861D4AF9D9D153D3ED56092E76F13CD33B3 (U3CHandleConnectionU3Ed__19_t2B2D10AA4C55A2999E347B3F7F12289D2BB5D833 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void NetworkManager/<HandleConnection>d__19::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CHandleConnectionU3Ed__19_System_Collections_IEnumerator_Reset_mC513FF3D1591032404B4F3DA1967D46B0C6C2430 (U3CHandleConnectionU3Ed__19_t2B2D10AA4C55A2999E347B3F7F12289D2BB5D833 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CHandleConnectionU3Ed__19_System_Collections_IEnumerator_Reset_mC513FF3D1591032404B4F3DA1967D46B0C6C2430_RuntimeMethod_var)));
	}
}
// System.Object NetworkManager/<HandleConnection>d__19::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CHandleConnectionU3Ed__19_System_Collections_IEnumerator_get_Current_m6351AEBEE9885118D26A214B3E29CC71D86AFF35 (U3CHandleConnectionU3Ed__19_t2B2D10AA4C55A2999E347B3F7F12289D2BB5D833 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CellEventArgs_set_Hovered_mED2C25ABB5B9CFDB913D36CE1E569EBB01E19EC0_inline (CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool Hovered { get; private set; }
		bool L_0 = ___value0;
		__this->set_U3CHoveredU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_mF7FCDE24496D619F4BB1A0BA44AF17DCB5D697FF_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		__this->set_z_4((0.0f));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_stringLength_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline (Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * __this, const RuntimeMethod* method)
{
	{
		// get { return m_OnClick; }
		ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * L_0 = __this->get_m_OnClick_20();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Selectable_get_interactable_m4231515CC3C861B431AA22FE3345F831389B3840_inline (Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Interactable; }
		bool L_0 = __this->get_m_Interactable_12();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool CellEventArgs_get_Hovered_m2C28642E496CC89649BE2B23DAFAF606407BD189_inline (CellEventArgs_tF6A8E4F068898819970440CC131894B71B2758E6 * __this, const RuntimeMethod* method)
{
	{
		// public bool Hovered { get; private set; }
		bool L_0 = __this->get_U3CHoveredU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Ship_get_size_mCB389A60C9338A2267F6357CBA2839134C7C79E5_inline (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method)
{
	{
		// public int size { get; private set; }
		int32_t L_0 = __this->get_U3CsizeU3Ek__BackingField_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Ship_set_size_mB69F1B1C31D843999D4B226F7A54D53CCEB8EB11_inline (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int size { get; private set; }
		int32_t L_0 = ___value0;
		__this->set_U3CsizeU3Ek__BackingField_3(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Ship_set_cells_m62750C426A0B1E8A06CAA44EE64B6C00F6070949_inline (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___value0, const RuntimeMethod* method)
{
	{
		// public GameObject[] cells { get; private set; }
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_0 = ___value0;
		__this->set_U3CcellsU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* Ship_get_cells_m3B9D3AF56A593555CCF9630DFE9D766BB763A91A_inline (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method)
{
	{
		// public GameObject[] cells { get; private set; }
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_0 = __this->get_U3CcellsU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetSendBuffer_SetConnection_mCE9EB7BFBA86CCB47B22217122D58D82C9E595F8_inline (NetSendBuffer_tA1B9AA549DC3A2A7F30C66EDBB756524AD88E9F3 * __this, TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * ___newConnection0, const RuntimeMethod* method)
{
	{
		// connection = newConnection;
		TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * L_0 = ___newConnection0;
		__this->set_connection_1(L_0);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Exception_t * TCPConnection_GetLastException_m4312EE21BDC7253C84B58F1889624532F658C4A6_inline (TCPConnection_tD058A030693BDA469660F4B6C9BD22703E921D7C * __this, const RuntimeMethod* method)
{
	{
		// return lastException;
		Exception_t * L_0 = __this->get_lastException_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Socket_get_Connected_m6E8C88AC69580EF7782514CD48754D4D180D9CE0_inline (Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_is_connected_19();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector2Int_get_x_mDBEFBCDF9C7924767344ED2CEE1307885AED947B_inline (Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_X_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector2Int_get_y_m282591DEB0E70B02F4F4DDFAB90116AEC8E6B86C_inline (Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Y_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m1C5E87EEF4B8F8CB985E6C4FD69D863ABAD79F14_gshared_inline (List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
