using UnityEngine;
using UnityEngine.UI;

///Class for representing a ship
public class Ship
{
    ///Predefined ship color
    public static readonly Color color = new Color(1, 0, 0);

    /** @name Properties of a ship
     * 
     */
    ///@{
    public bool isHorizontal;
    public Vector2Int startPos
    { get
        {
            int xmin = 9;
            int ymin = 9;
            foreach (GameObject c in cells)
            {
                int cx = c.GetComponent<Cell>().xIndex;
                int cy = c.GetComponent<Cell>().yIndex;
                if (cx < xmin)
                    xmin = cx;
                if (cy < ymin)
                    ymin = cy;
            }
            return new Vector2Int(xmin, ymin);
        }
    }
    public int size { get; private set; }
    public GameObject[] cells { get; private set; }
    ///@}

    public Ship(int size)
    {
        this.size = size;
        this.cells = new GameObject[size];
    }

    /// <summary>
    /// Adding a cell to the ship
    /// </summary>
    /// <param name="cell">Gameobject which represents the cell</param>
    /// <param name="index">Index that the cell should have inside the Array</param>
    public void AddCell(GameObject cell, int index)
    {
        cells[index] = cell;
        cell.GetComponent<Cell>().isShip = true;
        cell.GetComponent<RawImage>().color = color;
    }

    ///Deleting a ship and resetting the corresponding cells
    public void Delete()
    {
        foreach (GameObject c in this.cells)
        {
            c.GetComponent<Cell>().isShip = false;
            c.GetComponent<RawImage>().color = Cell.standardColor;
        }
    }
}
