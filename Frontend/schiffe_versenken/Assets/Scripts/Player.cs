using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

///Class that handles the player related game actions
public class Player : MonoBehaviour
{
    /** @name Predefined Colors
     * 
     */
    ///@{
    /** Color for disabled elements*/
    private static Color disabledColor = new Color(102 / 255f, 102 / 255f, 102 / 255f);
    /** Color for enabled elements*/
    private static Color enabledColor = new Color(1, 1, 1);
    ///@}

    ///Reference to the Network Manager
    private NetworkManager networkManager;

    /** @name Presets determined by ruleset
     * 
     */
    ///@{
    private int shipCountS = 4;
    private int shipCountM = 3;
    private int shipCountL = 2;
    private int shipCountXL = 1;
    ///@}

    ///Flag to determine the orientation of the ship placement
    private bool horizontalPlacement = false;

    /** @name Game related data
     * 
     */
    ///@{
    private Field field;
    private List<Ship> ships = new List<Ship>();
    private Ship selectedShip;
    ///@}

    /** @name UI Elements
     * 
     */
    ///@{
    /**Window for placement related Elements*/
    public GameObject placementWindow;
    /**Gameobject which contains a button component to select the ship with size s*/
    public GameObject placementShipS;
    /**Gameobject which contains a button component to select the ship with size m*/
    public GameObject placementShipM;
    /**Gameobject which contains a button component to select the ship with size l*/
    public GameObject placementShipL;
    /**Gameobject which contains a button component to select the ship with size xl*/
    public GameObject placementShipXL;
    /**Popup for displaying rule violations*/
    public GameObject ErrorPopUp;
    public GameObject undoButton;
    public GameObject confirmButton;
    /**Gameobject which indicates the rotation state*/
    public GameObject rotationState;
    /**Gameobject which indicates whether its the players or the enemy turn*/
    public GameObject turnIndicator;
    /**Text compontent of the turnIndicator gameobject*/
    private Text textTurnIndicator;
    public GameObject EndScreen;
    ///@}

    ///Flag to differ betweeen placing state and play state
    public static bool playerIsPlacing = true;

    ///Flag to determine if it's the players turn
    private bool isPlayersTurn;

    void Start()
    {
        Init();
    }

    private void OnDestroy()
    {
        if (!playerIsPlacing)
            playerIsPlacing = true;
        else
            Cell.CellHoverEvent -= OnCellHovered;
        Cell.CellClickedEvent -= OnCellClicked;
    }

    ///Initial operations which are executed when the script is loaded
    void Init()
    {
        networkManager = GameObject.Find("NetworkManager").GetComponent<NetworkManager>();

        textTurnIndicator = turnIndicator.GetComponent<Text>();

        field = gameObject.GetComponent<Field>();

        UpdatePlacementShips();

        //Linking ship selector event to corresponding button
        placementShipS.GetComponent<Button>().onClick.AddListener(delegate { OnShipSelected(2); });
        placementShipM.GetComponent<Button>().onClick.AddListener(delegate { OnShipSelected(3); });
        placementShipL.GetComponent<Button>().onClick.AddListener(delegate { OnShipSelected(4); });
        placementShipXL.GetComponent<Button>().onClick.AddListener(delegate { OnShipSelected(5); });

        //subscribing the hovered and clicked events
        Cell.CellHoverEvent += OnCellHovered;
        Cell.CellClickedEvent += OnCellClicked;
    }

    /**Updating the selection buttons and the corresponding texts \n
    *Updating the confirm and the undo button*/
    private void UpdatePlacementShips()
    {
        placementShipS.GetComponentInChildren<Text>().text = shipCountS + "x";
        if (shipCountS == 0)
        {
            placementShipS.GetComponentInChildren<Text>().color = disabledColor;
            placementShipS.GetComponent<Button>().interactable = false;
        }
        else if(placementShipS.GetComponent<Button>().interactable == false)
        {
            placementShipS.GetComponentInChildren<Text>().color = enabledColor;
            placementShipS.GetComponent<Button>().interactable = true;
        }
        placementShipM.GetComponentInChildren<Text>().text = shipCountM + "x";
        if (shipCountM == 0)
        {
            placementShipM.GetComponentInChildren<Text>().color = disabledColor;
            placementShipM.GetComponent<Button>().interactable = false;
        }
        else if (placementShipM.GetComponent<Button>().interactable == false)
        {
            placementShipM.GetComponentInChildren<Text>().color = enabledColor;
            placementShipM.GetComponent<Button>().interactable = true;
        }
        placementShipL.GetComponentInChildren<Text>().text = shipCountL + "x";
        if (shipCountL == 0)
        {
            placementShipL.GetComponentInChildren<Text>().color = disabledColor;
            placementShipL.GetComponent<Button>().interactable = false;
        }
        else if (placementShipL.GetComponent<Button>().interactable == false)
        {
            placementShipL.GetComponentInChildren<Text>().color = enabledColor;
            placementShipL.GetComponent<Button>().interactable = true;
        }
        placementShipXL.GetComponentInChildren<Text>().text = shipCountXL + "x";
        if (shipCountXL == 0)
        {
            placementShipXL.GetComponentInChildren<Text>().color = disabledColor;
            placementShipXL.GetComponent<Button>().interactable = false;
        }
        else if (placementShipXL.GetComponent<Button>().interactable == false)
        {
            placementShipXL.GetComponentInChildren<Text>().color = enabledColor;
            placementShipXL.GetComponent<Button>().interactable = true;
        }

        //deactivate undo button if no ships are placed anymore:
        if (ships.Count == 0)
            undoButton.GetComponent<Button>().interactable = false;
        //activate the undo button on first placement
        else if (ships.Count == 1)
            undoButton.GetComponent<Button>().interactable = true;

        switch (ships.Count)
        {
            case 0:
                //deactivate undo button if no ships are placed anymore:
                undoButton.GetComponent<Button>().interactable = false;
                break;
            case 10:
                //activate the confirm button if all ships are placed
                confirmButton.GetComponent<Button>().interactable = true;
                break;
            default:
                bool confirmButtonState = confirmButton.GetComponent<Button>().interactable;
                //deactivate the confirm button if a placement was undone
                if (confirmButtonState)
                    confirmButtonState = false;
                bool undoButtonState = undoButton.GetComponent<Button>().interactable;
                //activate the undo button on first placement
                if (!undoButtonState)
                    undoButtonState = true;
                break;
        }
    }

    ///Assigning the static selected ship (called by selection event)
    private void OnShipSelected(int size)
    {
        selectedShip = new Ship(size);
    }

    ///Marking the cells which will be set as ships if the player is clicking
    private void OnCellHovered(object sender, CellEventArgs e)
    {
        int borderoffset = 1;
        int hoveredCellX = (sender as Cell).xIndex;
        int hoveredCellY = (sender as Cell).yIndex;
        bool hoverstate = e.Hovered;
        if (selectedShip != null)
        {
            GameObject cell = null;
            if (horizontalPlacement)
            {
                //for each cell in ship
                for (int i = selectedShip.size - 1; i >= 0; i--)
                {
                    //selecting the cell based on increments of the initial cell
                    try
                    {
                        cell = field.cells[hoveredCellY, hoveredCellX - i];
                    }
                    //if calculated cell position is not within the fields borders, selecting the cell at the opposite side with an incrementing index based on the initial cell
                    catch (IndexOutOfRangeException)
                    {
                        cell = field.cells[hoveredCellY, hoveredCellX + borderoffset++];
                    }
                    //marking or unmarking the selected cells based on the event which called the function
                    finally
                    {
                        if(cell != null && !cell.GetComponent<Cell>().isShip)
                        {
                            cell.GetComponent<RawImage>().color = hoverstate ? Cell.markedColor : Cell.standardColor;
                        }
                    }
                }
            }
            //equivalent to horizontal placement with switched index increments for vertical placement
            else
            {
                for (int i = selectedShip.size-1; i >= 0; i--)
                {
                    try
                    {
                        cell = field.cells[hoveredCellY - i, hoveredCellX];
                    }
                    catch (IndexOutOfRangeException)
                    {
                        cell = field.cells[hoveredCellY + borderoffset++, hoveredCellX];
                    }
                    finally
                    {
                        if( cell != null && !cell.GetComponent<Cell>().isShip)
                        {
                            cell.GetComponent<RawImage>().color = hoverstate ? Cell.markedColor : Cell.standardColor;
                        }
                    }
                }
            }
        }
    }

    ///Reacting to the clicked event depending on the current gamestate
    private void OnCellClicked(object sender, CellEventArgs e)
    {
        if (playerIsPlacing)
        {
            PlaceShip(sender, e);
        }
        else if ((sender as Cell).isEnemy)
        {
            if (isPlayersTurn)
            {
                ShootCell(sender, e);
            }
            else
            {
                ShowErrorPopUp("not your turn!");
                Debug.Log("not your turn");
            }
        }
    }

    ///Checking if the placement is valid \n
    ///Setting the selected cells as ship and adding the ship to the ships list if the placement is valid
    private void PlaceShip(object sender, CellEventArgs e)
    {
        bool iscollision = false;
        bool placementValid = true;
        int borderoffset = 1;
        int shipindex = 0;
        int hoveredCellX = (sender as Cell).xIndex;
        int hoveredCellY = (sender as Cell).yIndex;
        if (selectedShip != null)
        {
            List<GameObject> selectedCells = new List<GameObject>();
            GameObject cell = null;
            //selecting the cells the same way as they are marked as hovered
            if (horizontalPlacement)
            {
                for (int i = selectedShip.size - 1; i >= 0; i--)
                {
                    try
                    {
                        cell = field.cells[hoveredCellY, hoveredCellX - i];
                    }
                    catch (IndexOutOfRangeException)
                    {
                        cell = field.cells[hoveredCellY, hoveredCellX + borderoffset++];
                    }
                    finally
                    {
                        if (cell != null)
                        {
                            //checking if one of the selected cells is already a part of a ship
                            if (cell.GetComponent<Cell>().isShip)
                            {
                                iscollision = true;
                            }
                            else
                            {
                                selectedCells.Add(cell);
                            }
                        }
                    }
                    //breaking out of the loop if a collision was detected
                    if (iscollision)
                    {
                        break;
                    }
                }
            }
            else
            {
                for (int i = selectedShip.size - 1; i >= 0; i--)
                {
                    try
                    {
                        cell = field.cells[hoveredCellY - i, hoveredCellX];
                    }
                    catch (IndexOutOfRangeException)
                    {
                        cell = field.cells[hoveredCellY + borderoffset++, hoveredCellX];
                    }
                    finally
                    {
                        if (cell.GetComponent<Cell>().isShip)
                        {
                            iscollision = true;
                        }
                        else
                        {
                            selectedCells.Add(cell);
                        }
                    }
                    if (iscollision)
                    {
                        break;
                    }
                }
            }
            //checking if a collision was detected
            if (iscollision)
            {
                placementValid = false;
            }
            else
            {
                if (horizontalPlacement)
                {
                    //searching for the smallest cellindex
                    int y = selectedCells[0].GetComponent<Cell>().yIndex;
                    int x = selectedCells[0].GetComponent<Cell>().xIndex;
                    if (x != 0)
                    {
                        for (int i = 1; i < selectedShip.size; i++)
                        {
                            int j = selectedCells[i].GetComponent<Cell>().xIndex;
                            if (j < x)
                                x = j;
                        }
                    }
                    //check if the cell to the left of the initial cell is a ship
                    if (x > 0 && field.cells[y, x - 1].GetComponent<Cell>().isShip)
                    {
                        placementValid = false;
                    }
                    //checking if placement Valid is still true to avoid unnecessary code execution
                    if (placementValid)
                    {
                        //check if the cell to the right of the ship is a ship
                        if (x + selectedShip.size <= 9 && field.cells[y, x + selectedShip.size].GetComponent<Cell>().isShip)
                        {
                            placementValid = false;
                        }
                    }
                    if (placementValid)
                    {
                        //check if one of the cells above or below the ship cells are ship cells
                        for (int i = -1; i <= selectedShip.size; i++)
                        {
                            if (x + i >= 0 && x + i <= 9)
                            {
                                if (y < 9 && field.cells[y + 1, x + i].GetComponent<Cell>().isShip)
                                {
                                    placementValid = false;
                                }
                                if (y > 0 && field.cells[y - 1, x + i].GetComponent<Cell>().isShip)
                                {
                                    placementValid = false;
                                }
                            }
                        }
                    }
                }
                //equivalent to the block above but with switched index increments for vertical placements
                else
                {
                    int y = selectedCells[0].GetComponent<Cell>().yIndex;
                    int x = selectedCells[0].GetComponent<Cell>().xIndex;
                    if (y != 0)
                    {
                        for (int i = 1; i < selectedShip.size; i++)
                        {
                            int j = selectedCells[i].GetComponent<Cell>().yIndex;
                            if (j < y)
                                y = j;
                        }
                    }
                    if (y > 0 && field.cells[y - 1, x].GetComponent<Cell>().isShip)
                    {
                        placementValid = false;
                    }
                    if (placementValid)
                    {
                        if (y + selectedShip.size <= 9 && field.cells[y + selectedShip.size, x].GetComponent<Cell>().isShip)
                        {
                            placementValid = false;
                        }
                    }
                    if (placementValid)
                    {
                        for (int i = -1; i <= selectedShip.size; i++)
                        {
                            if (y + i >= 0 && y + i <= 9)
                            {
                                if (x < 9 && field.cells[y + i, x + 1].GetComponent<Cell>().isShip)
                                {
                                    placementValid = false;
                                }
                                if (x > 0 && field.cells[y + i, x - 1].GetComponent<Cell>().isShip)
                                {
                                    placementValid = false;
                                }
                            }
                        }
                    }
                }
            }
            //if placementValid is still true, every placement rule was obeyed
            if (placementValid)
            {
                //setting the cell colors to the ship color which is defined as a static variable in the ship class
                foreach (GameObject c in selectedCells)
                {
                    //c.GetComponent<Image>().color = Ship.color;
                    selectedShip.AddCell(c, shipindex++);
                }
                selectedShip.isHorizontal = horizontalPlacement;
                //adding the placed ship to the ships list
                ships.Add(selectedShip);
                //decrementing the shipcount depending on the shipsize
                switch (selectedShip.size)
                {
                    case 2:
                        shipCountS--;
                        break;
                    case 3:
                        shipCountM--;
                        break;
                    case 4:
                        shipCountL--;
                        break;
                    case 5:
                        shipCountXL--;
                        break;
                    default:
                        break;
                }
                UpdatePlacementShips();
                //reset the ship selection
                selectedShip = null;
            }
            //if placementValid is false the warning popup is getting activated
            else
            {
                ShowErrorPopUp("invalid placement!");
            }
        }
    }

    /// <summary>
    /// Displaying an error message in a popup
    /// </summary>
    /// <param name="message">Message which will be displayed in the popup</param>
    private void ShowErrorPopUp(string message)
    {
        ErrorPopUp.GetComponentInChildren<Text>().text = message;
        ErrorPopUp.SetActive(true);
    }

    ///Checking wether the shot was valid \n
    ///Sending a request to the server to shoot an enemy cell
    private void ShootCell(object sender, CellEventArgs e)
    {
        if((sender as Cell).isShot)
        {
            ShowErrorPopUp("invalid hit!");
        }
        else
        {
            networkManager.SendShootCell((sender as Cell).xIndex, (sender as Cell).yIndex);
        }
    }

    ///Toggling the orientation of the ship placement
    public void ToggleOrientation()
    {
        horizontalPlacement = !horizontalPlacement;
        Vector3 rotation = rotationState.transform.localEulerAngles;
        rotation.z += horizontalPlacement ? 90 : -90;
        rotationState.transform.localEulerAngles = rotation;
    }

    ///Deleting the last placed ship
    public void DeleteLastShip()
    {
        int i = ships.Count - 1;
        switch (ships[i].size)
        {
            case 2:
                shipCountS++;
                break;
            case 3:
                shipCountM++;
                break;
            case 4:
                shipCountL++;
                break;
            case 5:
                shipCountXL++;
                break;
            default:
                break;
        }
        ships[i].Delete();
        ships.RemoveAt(i);
        UpdatePlacementShips();
    }

    ///Sending a server request to confirm the ship placements
    public void ConfirmPlacements()
    {
        networkManager.SendShips(ships);
    }

    ///Showing an error popup if the server did not accept the ship placements
    public void OnShipsNotAccepted()
    {
        ShowErrorPopUp("invalid placements!");
    }

    /// <summary>
    /// Setting the turn indicator
    /// </summary>
    /// <param name="b">Flag which indicates whether it is the players or the enemy turn</param>
    public void SetPlayersTurn(bool b)
    {
        isPlayersTurn = b;
        if (b)
        {
            textTurnIndicator.text = "your turn!";
            textTurnIndicator.color = new Color(0, 1, 0);
        }
        else
        {
            textTurnIndicator.text = "enemy turn!";
            textTurnIndicator.color = new Color(1, 0, 0);
        }
    }

    /// <summary>
    /// Displaying the end screen
    /// </summary>
    /// <param name="isPlayerWinner">Flag that indicates whether the player or the enemy has won</param>
    public void ShowEndScreen(bool isPlayerWinner)
    {
        EndScreen.GetComponent<Image>().color = isPlayerWinner ? new Color(0, 1, 0) : new Color(1, 0, 0);
        EndScreen.GetComponentInChildren<Text>().text = isPlayerWinner ? "YOU WON" : "YOU LOST";
        EndScreen.SetActive(true);
    }

    ///Transition from the placement phase to the playing phase
    public void StartPlayingphase()
    {
        Cell.CellHoverEvent -= OnCellHovered;
        playerIsPlacing = false;
        placementWindow.SetActive(false);
        turnIndicator.SetActive(true);
    }

    ///Sending a server request to quit the session \n
    ///Loading the main menu
    public void QuitGame()
    {
        networkManager.SendGameQuitRequest();
        LoadScene("MainMenu");
    }

    ///Sending a server request to search for a new lobby
    public void PlayAgain()
    {
        networkManager.SendMatchmakingRequest();
    }

    /// <summary>
    /// Loading a game scene
    /// </summary>
    /// <param name="name">Name of the scene</param>
    public void LoadScene(string name)
    {
        SceneManager.LoadScene(name);
    }
}
