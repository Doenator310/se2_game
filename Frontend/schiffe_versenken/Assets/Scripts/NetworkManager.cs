using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using UnityEngine;
using ProtocolClient;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

///Class that handles the interaction with the server
public class NetworkManager : MonoBehaviour
{
    ///Reference to the player instance
    private Player player;

    /** @name Field references
     * 
     */
    ///@{
    private Field playerField;
    private Field enemyField;
    ///@}

    private IClient client = new TCPClient();

    ///Gameobject which displays 
    private GameObject statusIndicator;

    ///Static instance which is used to prevent multiple instances
    private static GameObject instance;

    private void Awake()
    {
        CheckInstance();
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        InitStatusIndicator();
        Application.runInBackground = true;
        client.ServerRequestEvent += OnServerRequest;
        client.ServerResponseEvent += OnServerResponse;
        client.ConnectionException += OnConnectionException;
        client.ServerStatusUpdateEvent += OnServerStatusUpdate;
        StartCoroutine(HandleConnection());
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {
        if (scene.name == "Game")
        {
            GameObject go = GameObject.Find("PlayerField");
            player = go.GetComponent<Player>();
            playerField = go.GetComponent<Field>();
            enemyField = GameObject.Find("EnemyField").GetComponent<Field>();
        }
        InitStatusIndicator();
    }

    ///Instantiating the status indicator object
    private void InitStatusIndicator()
    {
        statusIndicator = new GameObject("StatusIndicator");
        statusIndicator.SetActive(false);
        statusIndicator.transform.SetParent(GameObject.Find("Screen").transform);
        statusIndicator.AddComponent<RectTransform>().sizeDelta = new Vector2(430, 100);
        statusIndicator.transform.localScale = new Vector3(1, 1, 1);
        statusIndicator.transform.localPosition = new Vector2(0, -280);
        Text tmp = statusIndicator.AddComponent<Text>();
        tmp.alignment = TextAnchor.MiddleLeft;
        tmp.fontSize = 40;
        tmp.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
    }

    ///Coroutine that changes the text of the status indicator every second
    private IEnumerator ShowSearchStatusIndicator()
    {
        statusIndicator.SetActive(true);
        Text text = statusIndicator.GetComponent<Text>();
        string t = "searching for player";
        text.text = t;
        float time = 0f;
        int i = 0;
        while (text != null)
        {
            time += Time.deltaTime;
            if(time >= 1)
            {
                time = 0;
                switch (++i % 4)
                {
                    case 1:
                        text.text = t + " .";
                        break;
                    case 2:
                        text.text = t + " . .";
                        break;
                    case 3:
                        text.text = t + " . . .";
                        break;
                    case 0:
                        text.text = t;
                        break;
                    default:
                        break;
                }
            }
            yield return null;
        }
        Debug.Log("coroutine stopped");
    }

    ///Coroutine to show a status indicator if the opponnent has left the game
    private IEnumerator ShowGameAbortionStatusIndicator()
    {
        yield return null;
        statusIndicator.SetActive(true);
        statusIndicator.GetComponent<Text>().text = "The opponent has left!";
        statusIndicator.GetComponent<Text>().color = new Color(1, 0, 0);
        yield return new WaitForSeconds(3);
        statusIndicator.GetComponent<Text>().color = new Color(1, 1, 1);
        statusIndicator.SetActive(false);
    }

    ///Checking if there is an instance already to prevent multiple instantiation
    private void CheckInstance()
    {
        if (instance == null)
        {
            instance = gameObject;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    ///Loading the game scene
    private void StartPlacementPhase()
    {
        SceneManager.LoadScene("Game");
    }

    private void ThrowLogicError()
    {
        throw new Exception("logic error");
    }

    /// <summary>
    /// Calling the OnCellHit() function of either the player or the enemy field
    /// </summary>
    /// <param name="isEnemyCell">Flag to determine if the cell is on the player or the enemy field</param>
    /// <param name="x">X index of the hit cell</param>
    /// <param name="y">Y index of the hit cell</param>
    /// <param name="hitShip">Flag to determine if a ship has been hit</param>
    private void OnCellHit(bool isEnemyCell, int x, int y, bool hitShip)
    {
        (isEnemyCell ? enemyField : playerField).OnCellHit(y, x, hitShip);
    }

    ///Reacting to the different kinds of server responses that occur during the connection
    private void OnServerResponse(object clientRef, byte[] data)
    {
        string received = new string(Encoding.UTF8.GetChars(data));
        Debug.Log("response: " + received);
        if (received.Length == 0)
        {
            Debug.Log("something went wrong!");
            return;
        }
        var command = received.ToUpper().Split(':');
        if(command[0] == "X")
        {
            //placement phase starts
            StartPlacementPhase();
        }
        else if (command[0] == "I")
        {
            if (command[1] == "Y")
            {
                //ships accepted
                player.StartPlayingphase();
            }
            else if (command[1] == "N")
            {
                //ships not accepted
                player.OnShipsNotAccepted();
            }
            else
                ThrowLogicError();
        }
        else if (command[0] == "P")
        {
            if(command[1] == "1")
            {
                //players turn
                player.SetPlayersTurn(true);
            }
            else if(command[1] == "0")
            {
                //enemies turn
                player.SetPlayersTurn(false);
            }
            else
                ThrowLogicError();
        }
        else if (command[0] == "W")
        {
            if (command[1] == "1")
            {
                //player wins
                player.ShowEndScreen(true);
            }
            else if (command[1] == "0")
            {
                //enemy wins
                player.ShowEndScreen(false);
            }
            else
                ThrowLogicError();
        }
        else if (command[0] == "E" || command[0] == "M")
        {
            //cell hit
            int x = int.Parse(command[1], System.Globalization.NumberStyles.HexNumber);
            int y = int.Parse(command[2], System.Globalization.NumberStyles.HexNumber);
            bool hitShip = command[3] == "1";
            bool isEnemyField = command[0] == "E";
            OnCellHit(isEnemyField, x, y, hitShip);
        }
        else if (command[0] == "N")
        {
            Debug.Log("invalid request");
        }
        else if (command[0] == "F")
        {
            SceneManager.LoadScene("MainMenu");
            StartCoroutine(ShowGameAbortionStatusIndicator());
        }
        else
            ThrowLogicError();
    }

    /// <summary>
    /// Sending a server request to shoot a cell
    /// </summary>
    /// <param name="x">X index of the cell</param>
    /// <param name="y">Y index of the cell</param>
    public void SendShootCell(int x, int y)
    {
        string message = "H:" + x + ":" + y;
        client.SendClientRequestToServer(Encoding.UTF8.GetBytes(message));
    }

    /// <summary>
    /// Sending a server request with the needed information of the placed ships
    /// </summary>
    /// <param name="list">List of the players placed ships</param>
    public void SendShips(List<Ship> list)
    {
        string message = "I:" + list.Count;
        list.ForEach(ship => {
            message += ":" + ship.size + ":" + ship.startPos.x + ":" + ship.startPos.y + ":" + (ship.isHorizontal ? "H" : "V");
        });
        client.SendClientRequestToServer(Encoding.UTF8.GetBytes(message));
    }

    ///Sending a matchmaking request to the server
    public void SendMatchmakingRequest()
    {
        string message = "A";
        client.SendClientRequestToServer(Encoding.UTF8.GetBytes(message));

        StartCoroutine(ShowSearchStatusIndicator());
    }

    ///Requesting the server to close the game lobby
    public void SendGameQuitRequest()
    {
        string message = "R";
        client.SendClientRequestToServer(Encoding.UTF8.GetBytes(message));
    }

    private void OnServerRequest(object clientRef, byte[] data)
    {
        string received = new string(Encoding.UTF8.GetChars(data));
        Debug.Log("request: " + received);
    }

    private void OnServerStatusUpdate(object clientRef, byte[] data)
    {
        string received = new string(Encoding.UTF8.GetChars(data));
        Debug.Log("status update: " + received);
    }

    private void OnConnectionException(object clientRef, Exception e)
    {
        Debug.Log("OnClientException: " + e.ToString());
    }

    ///Coroutine to handle the server connection
    private IEnumerator HandleConnection()
    {
        Debug.Log("HandleConnection started");

        client.ConnectToServer("127.0.0.1", 1338);

        if (client.GetConnectionState() == ConnectionState.NOT_CONNECTED)
        {
            Debug.Log("Not Connected!");
            yield break;
        }
        while (client.GetConnectionState() == ConnectionState.CONNECTING)
        {
            client.Update();
            yield return new WaitForFixedUpdate();
        }
        byte[] requestByteArray = null;
        {
            var msgAsCharArray = "Client->Request Server->Response".ToCharArray();
            requestByteArray = Encoding.UTF8.GetBytes(msgAsCharArray);
        }
        client.SendClientRequestToServer(requestByteArray);
        while (true)
        {
            client.Update();
            if (client.GetConnectionState() != ConnectionState.CONNECTED)
            {
                Debug.Log("Connection lost");
                break;
            }
            yield return new WaitForSecondsRealtime(0.05f);
        }
        Debug.Log("HandleConnection finished");
    }
}
