using UnityEngine;

///Specialization of the Field class for enemy fields
public class EnemyField : Field
{
    private void Start()
    {
        InitializeCells();
        hitImage = Resources.Load("textures/hit") as Texture2D;
    }

    ///Instantiating the gameobjects which represent the field cells and marking the cells as enemy cells
    private void InitializeCells()
    {
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                //instantiating a gameobject based on a prefab
                GameObject go = Instantiate((GameObject)Resources.Load("prefab/Cell_X_X"));
                //accessing the BoxCollider2D component of the gameobject
                BoxCollider2D bc = go.GetComponent<BoxCollider2D>();
                //setting the name of the cell based on the indexes
                go.name = "Cell_" + (i + 1) + "_" + (j + 1);
                //setting the indexes of the cell component
                Cell c = go.GetComponent<Cell>();
                c.SetIndexes(i, j);
                c.isEnemy = true;
                //setting the field as parent object
                go.transform.SetParent(gameObject.transform);
                //calculating the x and y coordinates based on the cells indexes
                float xpos = posx0 + j * totalcellwidth;
                float ypos = posy0 - i * totalcellheight;
                Vector3 position = new Vector3(xpos, ypos);
                //setting the size and scale of the gameobject
                ((RectTransform)go.transform).sizeDelta = new Vector2(cellwidth, cellwidth);
                ((RectTransform)go.transform).localScale = new Vector3(1, 1);
                //scaling the size of the collider based on the cellsize
                bc.size = new Vector2(cellwidth, cellwidth);
                //setting the gameobjects coordinates to the calculated coordinates (in local coordinate system)
                go.transform.localPosition = position;
                //adding the gameobject to the cells array
                cells[i, j] = go;
            }
        }
    }
}
