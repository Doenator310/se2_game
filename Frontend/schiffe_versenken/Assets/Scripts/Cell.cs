using UnityEngine;

///Custom event arguments for hovered and clicked event.
public class CellEventArgs
{
    public CellEventArgs() { }
    public CellEventArgs(bool hovered)
    {
        Hovered = hovered;
    }
    public bool Hovered { get; private set; }
}

///Class for representing a cell
public class Cell : MonoBehaviour
{
    /** @name Cell indices
     * 
     */
    ///@{
    public int xIndex;
    public int yIndex;
    ///@}
    ///Flag showing whether a cell is part of a ship
    public bool isShip;
    ///Flag showing whether a cell was shot
    public bool isShot;
    ///Flag showing wheter a cell is an enemy cell
    public bool isEnemy;

    /** @name Predefined color values
     * 
     */
    ///@{
    ///Standard color of a cell
    public static readonly Color standardColor = new Color(1, 1, 1);
    ///Color of a marked cell
    public static readonly Color markedColor = new Color(1, 131/255f, 122/255f);
    ///@}

    //events with custom EventHandler which takes the custom EventArgs "CellEventArgs":
    //hover event
    public delegate void CellHoverEventHandler(object sender, CellEventArgs e);
    public static event CellHoverEventHandler CellHoverEvent;
    //clicked event
    public delegate void CellClickedEventHandler(object sender, CellEventArgs e);
    public static event CellClickedEventHandler CellClickedEvent;

    ///Function to raise hover event:
    private void RaiseCellHoverEvent(bool hovered)
    {
        CellHoverEvent?.Invoke(this, new CellEventArgs(hovered));
    }

    ///Function to raise clicked event:
    private void RaiseCellClickedEvent()
    {
        CellClickedEvent?.Invoke(this, new CellEventArgs());
        Debug.Log("Cell Clicked x: " + xIndex + " y: " + yIndex);
    }

    ///OnMouseDown is called if the gameobject is clicked:
    private void OnMouseDown()
    {
        RaiseCellClickedEvent();
    }

    ///OnMouseEnter is called if the mouse pointer enters the gameobject's collider
    private void OnMouseEnter()
    {
        if(Player.playerIsPlacing)
            RaiseCellHoverEvent(true);
    }

    ///OnMouseExit is called if the mouse pointer exits the gameobject's collider
    private void OnMouseExit()
    {
        if (Player.playerIsPlacing)
            RaiseCellHoverEvent(false);
    }

    ///Function to set indexes of a cell
    public void SetIndexes(int y, int x)
    {
        yIndex = y;
        xIndex = x;
    }
}
