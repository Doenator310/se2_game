using UnityEngine;
using UnityEngine.UI;

///Class for representing a field
public class Field : MonoBehaviour
{
    ///Image texture for hit fields
    protected static Texture2D hitImage;
    ///Width of the clickable cell
    protected readonly int cellwidth = 77;
    ///Width of a cell + width of the border
    protected readonly float totalcellwidth = 80.58f;
    ///Height of a cell + height of the border
    protected readonly float totalcellheight = 80.48f;
    ///X position of cell_0_0 (relative to parent object)
    protected readonly float posx0 = -323.48f;
    ///Y position of cell_0_0 (relative to parent object)
    protected readonly float posy0 = 324.48f;
    ///2D Array of cells
    public GameObject[,] cells = new GameObject[10, 10];

    private void Start()
    {
        InitializeCells();
        //loading the hit texture from resources
        hitImage = Resources.Load("textures/hit") as Texture2D;
    }

    ///Instantiating the gameobjects which represent the field cells
    private void InitializeCells()
    {
        for(int i = 0; i < 10; i++)
        {
            for(int j = 0; j < 10; j++)
            {
                //instantiating a gameobject based on a prefab
                GameObject go = Instantiate((GameObject)Resources.Load("prefab/Cell_X_X"));
                //accessing the BoxCollider2D component of the gameobject
                BoxCollider2D bc = go.GetComponent<BoxCollider2D>();
                //setting the name of the cell based on the indexes
                go.name = "Cell_" + (i + 1) + "_" + (j + 1);
                //setting the indexes of the cell component
                go.GetComponent<Cell>().SetIndexes(i, j);
                //setting the field as parent object
                go.transform.SetParent(gameObject.transform);
                //calculating the x and y coordinates based on the cells indexes
                float xpos = posx0 + j * totalcellwidth;
                float ypos = posy0 - i * totalcellheight;
                Vector3 position = new Vector3(xpos, ypos);
                //setting the size and scale of the gameobject
                ((RectTransform)go.transform).sizeDelta = new Vector2(cellwidth, cellwidth);
                ((RectTransform)go.transform).localScale = new Vector3(1, 1);
                //scaling the size of the collider based on the cellsize
                bc.size = new Vector2(cellwidth, cellwidth);
                //setting the gameobjects coordinates to the calculated coordinates (in local coordinate system)
                go.transform.localPosition = position;
                //adding the gameobject to the cells array
                cells[i, j] = go;
            }
        }
    }

    /// <summary>
    /// Setting a cell as hit and changing its image and its color depending on wether there was a ship
    /// </summary>
    /// <param name="y">X index of the destination cell</param>
    /// <param name="x">Y index of the destination cell</param>
    /// <param name="hit">Flag which indicates wether a ship has been hit</param>
    public void OnCellHit(int y, int x, bool hit)
    {
        RawImage ri = cells[y, x].GetComponent<RawImage>();
        ri.texture = hitImage;
        ri.color = hit ? Ship.color : Cell.standardColor;
        cells[y, x].GetComponent<Cell>().isShot = true;
    }
}
