using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

///Class to automatically add the SendMatchmakingRequest function of the NetworkManager to the corresponding button
public class NetRefAdder : MonoBehaviour
{
    void Start()
    {
        this.GetComponent<Button>().onClick.AddListener(FindObjectOfType<NetworkManager>().SendMatchmakingRequest);
    }
}
