var searchData=
[
  ['sendgamequitrequest_164',['SendGameQuitRequest',['../class_network_manager.html#ab68e9d06e8cc3c88d0c01d423edc00db',1,'NetworkManager']]],
  ['sendmatchmakingrequest_165',['SendMatchmakingRequest',['../class_network_manager.html#a6c298ae5d06d442e099c6087f8279314',1,'NetworkManager']]],
  ['sendships_166',['SendShips',['../class_network_manager.html#a49a7cf951d9ea06c2dd973f158daf530',1,'NetworkManager']]],
  ['sendshootcell_167',['SendShootCell',['../class_network_manager.html#a6dc90eff1030a6d774c5447c57b1693d',1,'NetworkManager']]],
  ['setindexes_168',['SetIndexes',['../class_cell.html#a0ed7eabe6981c4bed93446c285e5140d',1,'Cell']]],
  ['setplayersturn_169',['SetPlayersTurn',['../class_player.html#aedafddfafe75c83f47b47b46194557b6',1,'Player']]],
  ['ship_170',['Ship',['../class_ship.html#a07b2fbd2490822d6084390130728c2f3',1,'Ship']]],
  ['shootcell_171',['ShootCell',['../class_player.html#a3d3af1a33d468a9f9830ced6885610ab',1,'Player']]],
  ['showendscreen_172',['ShowEndScreen',['../class_player.html#a199698aaeece740e0734864ca6d36e04',1,'Player']]],
  ['showerrorpopup_173',['ShowErrorPopUp',['../class_player.html#ac6be25e0288b9353581218dac8d54747',1,'Player']]],
  ['showgameabortionstatusindicator_174',['ShowGameAbortionStatusIndicator',['../class_network_manager.html#afd389c520e3626b0314978f3c30a119d',1,'NetworkManager']]],
  ['showsearchstatusindicator_175',['ShowSearchStatusIndicator',['../class_network_manager.html#a55c3ded26c49d7a0775334b52a50522e',1,'NetworkManager']]],
  ['start_176',['Start',['../class_enemy_field.html#aaa7fd6b10e3fce6e52d588f99c1f0c3e',1,'EnemyField.Start()'],['../class_field.html#a62714f93c9766ac839f498e377ee3838',1,'Field.Start()'],['../class_net_ref_adder.html#a4a617d5c26defefa526d1b3235bd5bd6',1,'NetRefAdder.Start()'],['../class_network_manager.html#a3e9146c4e5bda229850acb04f80383cd',1,'NetworkManager.Start()'],['../class_player.html#a1a09a3ded16ac1646f6bdd4f25fe0ddd',1,'Player.Start()']]],
  ['startplacementphase_177',['StartPlacementPhase',['../class_network_manager.html#abaa55d01a06c9126761858b1eb6ebee3',1,'NetworkManager']]],
  ['startplayingphase_178',['StartPlayingphase',['../class_player.html#a40e3df439077acf934f4148f3d4db328',1,'Player']]]
];
