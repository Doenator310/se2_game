var searchData=
[
  ['placementshipl_203',['placementShipL',['../class_player.html#a684e35f781a290a09708ea2b9f79d95c',1,'Player']]],
  ['placementshipm_204',['placementShipM',['../class_player.html#a45892f7c98e74f59c5f11866618bcc3c',1,'Player']]],
  ['placementships_205',['placementShipS',['../class_player.html#a8e8a323bea96f1205554bff6af4992ad',1,'Player']]],
  ['placementshipxl_206',['placementShipXL',['../class_player.html#a333d3d33c99645a06606d671ff6a47c6',1,'Player']]],
  ['placementwindow_207',['placementWindow',['../class_player.html#a48e3d5ecc1a2af6a2e01b95f222ff14e',1,'Player']]],
  ['player_208',['player',['../class_network_manager.html#a49ddfc1ba00cb6f9f20b00ed3198b87e',1,'NetworkManager']]],
  ['playerfield_209',['playerField',['../class_network_manager.html#ae850c367cb1f61cff326aa34d5593146',1,'NetworkManager']]],
  ['playerisplacing_210',['playerIsPlacing',['../class_player.html#a2368adf154304e33bf2d3ddc701d8213',1,'Player']]],
  ['posx0_211',['posx0',['../class_field.html#add085632497a5df612f2658aa074d047',1,'Field']]],
  ['posy0_212',['posy0',['../class_field.html#a45e93bf1858e93c7d8cf6dcca3a741e8',1,'Field']]]
];
