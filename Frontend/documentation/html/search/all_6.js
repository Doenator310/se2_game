var searchData=
[
  ['init_32',['Init',['../class_player.html#a2294de05251594c53c41c4ed0fdeb618',1,'Player']]],
  ['initializecells_33',['InitializeCells',['../class_enemy_field.html#a39aad617cc60e32520066a93a469b87f',1,'EnemyField.InitializeCells()'],['../class_field.html#abcd1ee1e487956d40d1c17b6b94ee6b2',1,'Field.InitializeCells()']]],
  ['initstatusindicator_34',['InitStatusIndicator',['../class_network_manager.html#a728930d6d5a54f5632a50b896769491e',1,'NetworkManager']]],
  ['instance_35',['instance',['../class_network_manager.html#ae261663465c10b2b0a45aa41bc1cfe33',1,'NetworkManager']]],
  ['isenemy_36',['isEnemy',['../class_cell.html#a9afb742bd36ef5a7be0819df1e5b1486',1,'Cell']]],
  ['ishorizontal_37',['isHorizontal',['../class_ship.html#a84c4417f945e97c65ea9083e5aa7fcc3',1,'Ship']]],
  ['isplayersturn_38',['isPlayersTurn',['../class_player.html#a7c2667b0c4d3a98aeaa02388b8f75020',1,'Player']]],
  ['isship_39',['isShip',['../class_cell.html#a6544a714ee1ae6b6cba50a5aeaf582f4',1,'Cell']]],
  ['isshot_40',['isShot',['../class_cell.html#a584f81b9115e56d19f5db33b0dec542d',1,'Cell']]]
];
