var searchData=
[
  ['cell_2',['Cell',['../class_cell.html',1,'']]],
  ['cell_2ecs_3',['Cell.cs',['../_cell_8cs.html',1,'']]],
  ['cellclickedevent_4',['CellClickedEvent',['../class_cell.html#ab476ab6f54ac492ca626789974c4c0cb',1,'Cell']]],
  ['cellclickedeventhandler_5',['CellClickedEventHandler',['../class_cell.html#a14bc4838ca18a94bfc1d9b0ddc3c1800',1,'Cell']]],
  ['celleventargs_6',['CellEventArgs',['../class_cell_event_args.html',1,'CellEventArgs'],['../class_cell_event_args.html#aea36e10da0ef34195fd2647db4b7ad2d',1,'CellEventArgs.CellEventArgs()'],['../class_cell_event_args.html#a5a12bd17ab90cb47c84abdb4911f8e80',1,'CellEventArgs.CellEventArgs(bool hovered)']]],
  ['cellhoverevent_7',['CellHoverEvent',['../class_cell.html#a4b528c18046d1fb399e95ff27aa9b67a',1,'Cell']]],
  ['cellhovereventhandler_8',['CellHoverEventHandler',['../class_cell.html#aea23b0b78cc26885757be1b94cdddb32',1,'Cell']]],
  ['cells_9',['cells',['../class_field.html#a2bdaa63666fc4173e2c3af10e3aa150d',1,'Field.cells()'],['../class_ship.html#ad066dc5d31a2fd47f7129ab5ffcc0a89',1,'Ship.cells()']]],
  ['cellwidth_10',['cellwidth',['../class_field.html#a205e13e91d649be585c2fc86cf55851c',1,'Field']]],
  ['checkinstance_11',['CheckInstance',['../class_network_manager.html#ade41509c192e626e0a84be659bb377cd',1,'NetworkManager']]],
  ['client_12',['client',['../class_network_manager.html#a9324eb8e4c030f5026e8ec9654bde437',1,'NetworkManager']]],
  ['color_13',['color',['../class_ship.html#ab7438d34a9d286cdea866136131fc54a',1,'Ship']]],
  ['confirmbutton_14',['confirmButton',['../class_player.html#a7306813935ceb363d8e9a1cb3c9663f2',1,'Player']]],
  ['confirmplacements_15',['ConfirmPlacements',['../class_player.html#af833adaff1aad86e150dd6598978277f',1,'Player']]]
];
