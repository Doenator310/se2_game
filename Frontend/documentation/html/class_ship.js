var class_ship =
[
    [ "Ship", "class_ship.html#a07b2fbd2490822d6084390130728c2f3", null ],
    [ "AddCell", "class_ship.html#a9e6ac1b588566b7d6085c401b37c8ddd", null ],
    [ "Delete", "class_ship.html#a7a555db319f560bfd63995d93827efc0", null ],
    [ "color", "class_ship.html#ab7438d34a9d286cdea866136131fc54a", null ],
    [ "isHorizontal", "class_ship.html#a84c4417f945e97c65ea9083e5aa7fcc3", null ],
    [ "cells", "class_ship.html#ad066dc5d31a2fd47f7129ab5ffcc0a89", null ],
    [ "size", "class_ship.html#ad8575fd1cb63b05ba05e938d856f7f97", null ],
    [ "startPos", "class_ship.html#a1461c0930615f1d5be61478d7713f8ae", null ]
];