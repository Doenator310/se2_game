var dir_cf5b459b0baeaf8e2eb611f859934421 =
[
    [ "Client.cs", "_client_8cs.html", [
      [ "TCPClient", "class_protocol_client_1_1_t_c_p_client.html", "class_protocol_client_1_1_t_c_p_client" ]
    ] ],
    [ "IClient.cs", "_i_client_8cs.html", "_i_client_8cs" ],
    [ "INetConnection.cs", "_i_net_connection_8cs.html", [
      [ "INetConnection", "interface_protocol_client_1_1_i_net_connection.html", "interface_protocol_client_1_1_i_net_connection" ],
      [ "INetChannel", "interface_protocol_client_1_1_i_net_channel.html", "interface_protocol_client_1_1_i_net_channel" ]
    ] ],
    [ "NetReceiveBuffer.cs", "_net_receive_buffer_8cs.html", [
      [ "NetReceiveBuffer", "class_protocol_client_1_1_net_receive_buffer.html", "class_protocol_client_1_1_net_receive_buffer" ]
    ] ],
    [ "NetSendBuffer.cs", "_net_send_buffer_8cs.html", [
      [ "NetSendBuffer", "class_protocol_client_1_1_net_send_buffer.html", "class_protocol_client_1_1_net_send_buffer" ]
    ] ],
    [ "Program.cs", "_program_8cs.html", [
      [ "Program", "class_protocol_client_1_1_program.html", null ]
    ] ],
    [ "Protocol.cs", "_protocol_8cs.html", "_protocol_8cs" ],
    [ "TCPConnection.cs", "_t_c_p_connection_8cs.html", [
      [ "TCPConnection", "class_protocol_client_1_1_t_c_p_connection.html", "class_protocol_client_1_1_t_c_p_connection" ]
    ] ]
];