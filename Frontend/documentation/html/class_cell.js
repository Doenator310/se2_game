var class_cell =
[
    [ "CellClickedEventHandler", "class_cell.html#a14bc4838ca18a94bfc1d9b0ddc3c1800", null ],
    [ "CellHoverEventHandler", "class_cell.html#aea23b0b78cc26885757be1b94cdddb32", null ],
    [ "OnMouseDown", "class_cell.html#aa66004a7b67b83c38d37a041ad8a9504", null ],
    [ "OnMouseEnter", "class_cell.html#a11d20a70f32f2f1d71a50fc2291c0613", null ],
    [ "OnMouseExit", "class_cell.html#aef370b2171c21a535dbd6e42f36976f1", null ],
    [ "RaiseCellClickedEvent", "class_cell.html#ac8b997e51912c2711cb1fe152809a8bc", null ],
    [ "RaiseCellHoverEvent", "class_cell.html#ab0b1ffbdeea0b87e974ab0c04afe66f3", null ],
    [ "SetIndexes", "class_cell.html#a0ed7eabe6981c4bed93446c285e5140d", null ],
    [ "isEnemy", "class_cell.html#a9afb742bd36ef5a7be0819df1e5b1486", null ],
    [ "isShip", "class_cell.html#a6544a714ee1ae6b6cba50a5aeaf582f4", null ],
    [ "isShot", "class_cell.html#a584f81b9115e56d19f5db33b0dec542d", null ],
    [ "markedColor", "class_cell.html#ad439ac99d151bfee65533472f275fd7a", null ],
    [ "standardColor", "class_cell.html#ac01aaba6b698e574cffe11f3719dc4fe", null ],
    [ "xIndex", "class_cell.html#af346249ad55ac18c4c036080d8cb5ac1", null ],
    [ "yIndex", "class_cell.html#a872a43683726ba71867caa62230bf3c5", null ],
    [ "CellClickedEvent", "class_cell.html#ab476ab6f54ac492ca626789974c4c0cb", null ],
    [ "CellHoverEvent", "class_cell.html#a4b528c18046d1fb399e95ff27aa9b67a", null ]
];