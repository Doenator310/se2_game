\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\contentsline {chapter}{\numberline {1}Hierarchical Index}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Class Hierarchy}{1}{section.1.1}%
\contentsline {chapter}{\numberline {2}Class Index}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Class List}{3}{section.2.1}%
\contentsline {chapter}{\numberline {3}File Index}{5}{chapter.3}%
\contentsline {section}{\numberline {3.1}File List}{5}{section.3.1}%
\contentsline {chapter}{\numberline {4}Class Documentation}{7}{chapter.4}%
\contentsline {section}{\numberline {4.1}Cell Class Reference}{7}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Detailed Description}{8}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Member Function Documentation}{8}{subsection.4.1.2}%
\contentsline {subsubsection}{\numberline {4.1.2.1}CellClickedEventHandler()}{8}{subsubsection.4.1.2.1}%
\contentsline {subsubsection}{\numberline {4.1.2.2}CellHoverEventHandler()}{8}{subsubsection.4.1.2.2}%
\contentsline {subsubsection}{\numberline {4.1.2.3}OnMouseDown()}{9}{subsubsection.4.1.2.3}%
\contentsline {subsubsection}{\numberline {4.1.2.4}OnMouseEnter()}{9}{subsubsection.4.1.2.4}%
\contentsline {subsubsection}{\numberline {4.1.2.5}OnMouseExit()}{9}{subsubsection.4.1.2.5}%
\contentsline {subsubsection}{\numberline {4.1.2.6}RaiseCellClickedEvent()}{9}{subsubsection.4.1.2.6}%
\contentsline {subsubsection}{\numberline {4.1.2.7}RaiseCellHoverEvent()}{9}{subsubsection.4.1.2.7}%
\contentsline {subsubsection}{\numberline {4.1.2.8}SetIndexes()}{9}{subsubsection.4.1.2.8}%
\contentsline {subsection}{\numberline {4.1.3}Member Data Documentation}{10}{subsection.4.1.3}%
\contentsline {subsubsection}{\numberline {4.1.3.1}isEnemy}{10}{subsubsection.4.1.3.1}%
\contentsline {subsubsection}{\numberline {4.1.3.2}isShip}{10}{subsubsection.4.1.3.2}%
\contentsline {subsubsection}{\numberline {4.1.3.3}isShot}{10}{subsubsection.4.1.3.3}%
\contentsline {subsubsection}{\numberline {4.1.3.4}markedColor}{10}{subsubsection.4.1.3.4}%
\contentsline {subsubsection}{\numberline {4.1.3.5}standardColor}{10}{subsubsection.4.1.3.5}%
\contentsline {subsubsection}{\numberline {4.1.3.6}xIndex}{10}{subsubsection.4.1.3.6}%
\contentsline {subsubsection}{\numberline {4.1.3.7}yIndex}{11}{subsubsection.4.1.3.7}%
\contentsline {subsection}{\numberline {4.1.4}Event Documentation}{11}{subsection.4.1.4}%
\contentsline {subsubsection}{\numberline {4.1.4.1}CellClickedEvent}{11}{subsubsection.4.1.4.1}%
\contentsline {subsubsection}{\numberline {4.1.4.2}CellHoverEvent}{11}{subsubsection.4.1.4.2}%
\contentsline {section}{\numberline {4.2}Cell\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Event\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Args Class Reference}{11}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Detailed Description}{11}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Constructor \& Destructor Documentation}{11}{subsection.4.2.2}%
\contentsline {subsubsection}{\numberline {4.2.2.1}CellEventArgs()\hspace {0.1cm}{\footnotesize \ttfamily [1/2]}}{12}{subsubsection.4.2.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2.2}CellEventArgs()\hspace {0.1cm}{\footnotesize \ttfamily [2/2]}}{12}{subsubsection.4.2.2.2}%
\contentsline {subsection}{\numberline {4.2.3}Property Documentation}{12}{subsection.4.2.3}%
\contentsline {subsubsection}{\numberline {4.2.3.1}Hovered}{12}{subsubsection.4.2.3.1}%
\contentsline {section}{\numberline {4.3}Enemy\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Field Class Reference}{12}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Detailed Description}{13}{subsection.4.3.1}%
\contentsline {subsection}{\numberline {4.3.2}Member Function Documentation}{13}{subsection.4.3.2}%
\contentsline {subsubsection}{\numberline {4.3.2.1}InitializeCells()}{13}{subsubsection.4.3.2.1}%
\contentsline {subsubsection}{\numberline {4.3.2.2}Start()}{13}{subsubsection.4.3.2.2}%
\contentsline {section}{\numberline {4.4}Field Class Reference}{13}{section.4.4}%
\contentsline {subsection}{\numberline {4.4.1}Detailed Description}{14}{subsection.4.4.1}%
\contentsline {subsection}{\numberline {4.4.2}Member Function Documentation}{14}{subsection.4.4.2}%
\contentsline {subsubsection}{\numberline {4.4.2.1}InitializeCells()}{14}{subsubsection.4.4.2.1}%
\contentsline {subsubsection}{\numberline {4.4.2.2}OnCellHit()}{14}{subsubsection.4.4.2.2}%
\contentsline {subsubsection}{\numberline {4.4.2.3}Start()}{15}{subsubsection.4.4.2.3}%
\contentsline {subsection}{\numberline {4.4.3}Member Data Documentation}{15}{subsection.4.4.3}%
\contentsline {subsubsection}{\numberline {4.4.3.1}cells}{15}{subsubsection.4.4.3.1}%
\contentsline {subsubsection}{\numberline {4.4.3.2}cellwidth}{15}{subsubsection.4.4.3.2}%
\contentsline {subsubsection}{\numberline {4.4.3.3}hitImage}{15}{subsubsection.4.4.3.3}%
\contentsline {subsubsection}{\numberline {4.4.3.4}posx0}{15}{subsubsection.4.4.3.4}%
\contentsline {subsubsection}{\numberline {4.4.3.5}posy0}{16}{subsubsection.4.4.3.5}%
\contentsline {subsubsection}{\numberline {4.4.3.6}totalcellheight}{16}{subsubsection.4.4.3.6}%
\contentsline {subsubsection}{\numberline {4.4.3.7}totalcellwidth}{16}{subsubsection.4.4.3.7}%
\contentsline {section}{\numberline {4.5}Network\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Manager Class Reference}{16}{section.4.5}%
\contentsline {subsection}{\numberline {4.5.1}Detailed Description}{17}{subsection.4.5.1}%
\contentsline {subsection}{\numberline {4.5.2}Member Function Documentation}{17}{subsection.4.5.2}%
\contentsline {subsubsection}{\numberline {4.5.2.1}Awake()}{18}{subsubsection.4.5.2.1}%
\contentsline {subsubsection}{\numberline {4.5.2.2}CheckInstance()}{18}{subsubsection.4.5.2.2}%
\contentsline {subsubsection}{\numberline {4.5.2.3}HandleConnection()}{18}{subsubsection.4.5.2.3}%
\contentsline {subsubsection}{\numberline {4.5.2.4}InitStatusIndicator()}{18}{subsubsection.4.5.2.4}%
\contentsline {subsubsection}{\numberline {4.5.2.5}OnCellHit()}{18}{subsubsection.4.5.2.5}%
\contentsline {subsubsection}{\numberline {4.5.2.6}OnConnectionException()}{19}{subsubsection.4.5.2.6}%
\contentsline {subsubsection}{\numberline {4.5.2.7}OnSceneLoaded()}{19}{subsubsection.4.5.2.7}%
\contentsline {subsubsection}{\numberline {4.5.2.8}OnServerRequest()}{19}{subsubsection.4.5.2.8}%
\contentsline {subsubsection}{\numberline {4.5.2.9}OnServerResponse()}{19}{subsubsection.4.5.2.9}%
\contentsline {subsubsection}{\numberline {4.5.2.10}OnServerStatusUpdate()}{19}{subsubsection.4.5.2.10}%
\contentsline {subsubsection}{\numberline {4.5.2.11}SendGameQuitRequest()}{19}{subsubsection.4.5.2.11}%
\contentsline {subsubsection}{\numberline {4.5.2.12}SendMatchmakingRequest()}{20}{subsubsection.4.5.2.12}%
\contentsline {subsubsection}{\numberline {4.5.2.13}SendShips()}{20}{subsubsection.4.5.2.13}%
\contentsline {subsubsection}{\numberline {4.5.2.14}SendShootCell()}{20}{subsubsection.4.5.2.14}%
\contentsline {subsubsection}{\numberline {4.5.2.15}ShowStatusIndicator()}{20}{subsubsection.4.5.2.15}%
\contentsline {subsubsection}{\numberline {4.5.2.16}Start()}{20}{subsubsection.4.5.2.16}%
\contentsline {subsubsection}{\numberline {4.5.2.17}StartPlacementPhase()}{21}{subsubsection.4.5.2.17}%
\contentsline {subsubsection}{\numberline {4.5.2.18}ThrowLogicError()}{21}{subsubsection.4.5.2.18}%
\contentsline {subsection}{\numberline {4.5.3}Member Data Documentation}{21}{subsection.4.5.3}%
\contentsline {subsubsection}{\numberline {4.5.3.1}client}{21}{subsubsection.4.5.3.1}%
\contentsline {subsubsection}{\numberline {4.5.3.2}enemyField}{21}{subsubsection.4.5.3.2}%
\contentsline {subsubsection}{\numberline {4.5.3.3}instance}{21}{subsubsection.4.5.3.3}%
\contentsline {subsubsection}{\numberline {4.5.3.4}player}{21}{subsubsection.4.5.3.4}%
\contentsline {subsubsection}{\numberline {4.5.3.5}playerField}{22}{subsubsection.4.5.3.5}%
\contentsline {subsubsection}{\numberline {4.5.3.6}statusIndicator}{22}{subsubsection.4.5.3.6}%
\contentsline {section}{\numberline {4.6}Player Class Reference}{22}{section.4.6}%
\contentsline {subsection}{\numberline {4.6.1}Detailed Description}{24}{subsection.4.6.1}%
\contentsline {subsection}{\numberline {4.6.2}Member Function Documentation}{24}{subsection.4.6.2}%
\contentsline {subsubsection}{\numberline {4.6.2.1}ConfirmPlacements()}{24}{subsubsection.4.6.2.1}%
\contentsline {subsubsection}{\numberline {4.6.2.2}DeleteLastShip()}{24}{subsubsection.4.6.2.2}%
\contentsline {subsubsection}{\numberline {4.6.2.3}Init()}{24}{subsubsection.4.6.2.3}%
\contentsline {subsubsection}{\numberline {4.6.2.4}LoadScene()}{24}{subsubsection.4.6.2.4}%
\contentsline {subsubsection}{\numberline {4.6.2.5}OnCellClicked()}{25}{subsubsection.4.6.2.5}%
\contentsline {subsubsection}{\numberline {4.6.2.6}OnCellHovered()}{25}{subsubsection.4.6.2.6}%
\contentsline {subsubsection}{\numberline {4.6.2.7}OnDestroy()}{25}{subsubsection.4.6.2.7}%
\contentsline {subsubsection}{\numberline {4.6.2.8}OnShipSelected()}{25}{subsubsection.4.6.2.8}%
\contentsline {subsubsection}{\numberline {4.6.2.9}OnShipsNotAccepted()}{25}{subsubsection.4.6.2.9}%
\contentsline {subsubsection}{\numberline {4.6.2.10}PlaceShip()}{26}{subsubsection.4.6.2.10}%
\contentsline {subsubsection}{\numberline {4.6.2.11}PlayAgain()}{26}{subsubsection.4.6.2.11}%
\contentsline {subsubsection}{\numberline {4.6.2.12}QuitGame()}{26}{subsubsection.4.6.2.12}%
\contentsline {subsubsection}{\numberline {4.6.2.13}SetPlayersTurn()}{26}{subsubsection.4.6.2.13}%
\contentsline {subsubsection}{\numberline {4.6.2.14}ShootCell()}{26}{subsubsection.4.6.2.14}%
\contentsline {subsubsection}{\numberline {4.6.2.15}ShowEndScreen()}{27}{subsubsection.4.6.2.15}%
\contentsline {subsubsection}{\numberline {4.6.2.16}ShowErrorPopUp()}{27}{subsubsection.4.6.2.16}%
\contentsline {subsubsection}{\numberline {4.6.2.17}Start()}{27}{subsubsection.4.6.2.17}%
\contentsline {subsubsection}{\numberline {4.6.2.18}StartPlayingphase()}{27}{subsubsection.4.6.2.18}%
\contentsline {subsubsection}{\numberline {4.6.2.19}ToggleOrientation()}{27}{subsubsection.4.6.2.19}%
\contentsline {subsubsection}{\numberline {4.6.2.20}UpdatePlacementShips()}{28}{subsubsection.4.6.2.20}%
\contentsline {subsection}{\numberline {4.6.3}Member Data Documentation}{28}{subsection.4.6.3}%
\contentsline {subsubsection}{\numberline {4.6.3.1}confirmButton}{28}{subsubsection.4.6.3.1}%
\contentsline {subsubsection}{\numberline {4.6.3.2}disabledColor}{28}{subsubsection.4.6.3.2}%
\contentsline {subsubsection}{\numberline {4.6.3.3}enabledColor}{28}{subsubsection.4.6.3.3}%
\contentsline {subsubsection}{\numberline {4.6.3.4}EndScreen}{28}{subsubsection.4.6.3.4}%
\contentsline {subsubsection}{\numberline {4.6.3.5}ErrorPopUp}{28}{subsubsection.4.6.3.5}%
\contentsline {subsubsection}{\numberline {4.6.3.6}field}{28}{subsubsection.4.6.3.6}%
\contentsline {subsubsection}{\numberline {4.6.3.7}horizontalPlacement}{29}{subsubsection.4.6.3.7}%
\contentsline {subsubsection}{\numberline {4.6.3.8}isPlayersTurn}{29}{subsubsection.4.6.3.8}%
\contentsline {subsubsection}{\numberline {4.6.3.9}networkManager}{29}{subsubsection.4.6.3.9}%
\contentsline {subsubsection}{\numberline {4.6.3.10}placementShipL}{29}{subsubsection.4.6.3.10}%
\contentsline {subsubsection}{\numberline {4.6.3.11}placementShipM}{29}{subsubsection.4.6.3.11}%
\contentsline {subsubsection}{\numberline {4.6.3.12}placementShipS}{29}{subsubsection.4.6.3.12}%
\contentsline {subsubsection}{\numberline {4.6.3.13}placementShipXL}{29}{subsubsection.4.6.3.13}%
\contentsline {subsubsection}{\numberline {4.6.3.14}placementWindow}{30}{subsubsection.4.6.3.14}%
\contentsline {subsubsection}{\numberline {4.6.3.15}playerIsPlacing}{30}{subsubsection.4.6.3.15}%
\contentsline {subsubsection}{\numberline {4.6.3.16}rotationState}{30}{subsubsection.4.6.3.16}%
\contentsline {subsubsection}{\numberline {4.6.3.17}selectedShip}{30}{subsubsection.4.6.3.17}%
\contentsline {subsubsection}{\numberline {4.6.3.18}shipCountL}{30}{subsubsection.4.6.3.18}%
\contentsline {subsubsection}{\numberline {4.6.3.19}shipCountM}{30}{subsubsection.4.6.3.19}%
\contentsline {subsubsection}{\numberline {4.6.3.20}shipCountS}{30}{subsubsection.4.6.3.20}%
\contentsline {subsubsection}{\numberline {4.6.3.21}shipCountXL}{31}{subsubsection.4.6.3.21}%
\contentsline {subsubsection}{\numberline {4.6.3.22}ships}{31}{subsubsection.4.6.3.22}%
\contentsline {subsubsection}{\numberline {4.6.3.23}textTurnIndicator}{31}{subsubsection.4.6.3.23}%
\contentsline {subsubsection}{\numberline {4.6.3.24}turnIndicator}{31}{subsubsection.4.6.3.24}%
\contentsline {subsubsection}{\numberline {4.6.3.25}undoButton}{31}{subsubsection.4.6.3.25}%
\contentsline {section}{\numberline {4.7}Ship Class Reference}{31}{section.4.7}%
\contentsline {subsection}{\numberline {4.7.1}Detailed Description}{32}{subsection.4.7.1}%
\contentsline {subsection}{\numberline {4.7.2}Constructor \& Destructor Documentation}{32}{subsection.4.7.2}%
\contentsline {subsubsection}{\numberline {4.7.2.1}Ship()}{32}{subsubsection.4.7.2.1}%
\contentsline {subsection}{\numberline {4.7.3}Member Function Documentation}{32}{subsection.4.7.3}%
\contentsline {subsubsection}{\numberline {4.7.3.1}AddCell()}{32}{subsubsection.4.7.3.1}%
\contentsline {subsubsection}{\numberline {4.7.3.2}Delete()}{33}{subsubsection.4.7.3.2}%
\contentsline {subsection}{\numberline {4.7.4}Member Data Documentation}{33}{subsection.4.7.4}%
\contentsline {subsubsection}{\numberline {4.7.4.1}color}{33}{subsubsection.4.7.4.1}%
\contentsline {subsubsection}{\numberline {4.7.4.2}isHorizontal}{33}{subsubsection.4.7.4.2}%
\contentsline {subsection}{\numberline {4.7.5}Property Documentation}{33}{subsection.4.7.5}%
\contentsline {subsubsection}{\numberline {4.7.5.1}cells}{33}{subsubsection.4.7.5.1}%
\contentsline {subsubsection}{\numberline {4.7.5.2}size}{33}{subsubsection.4.7.5.2}%
\contentsline {subsubsection}{\numberline {4.7.5.3}startPos}{33}{subsubsection.4.7.5.3}%
\contentsline {chapter}{\numberline {5}File Documentation}{35}{chapter.5}%
\contentsline {section}{\numberline {5.1}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Dominiks PC/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documents/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Git\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Lab/se2\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}game/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Frontend/schiffe\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}versenken/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Cell.cs File Reference}{35}{section.5.1}%
\contentsline {section}{\numberline {5.2}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Dominiks PC/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documents/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Git\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Lab/se2\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}game/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Frontend/schiffe\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}versenken/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Enemy\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Field.cs File Reference}{35}{section.5.2}%
\contentsline {section}{\numberline {5.3}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Dominiks PC/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documents/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Git\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Lab/se2\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}game/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Frontend/schiffe\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}versenken/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Field.cs File Reference}{35}{section.5.3}%
\contentsline {section}{\numberline {5.4}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Dominiks PC/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documents/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Git\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Lab/se2\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}game/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Frontend/schiffe\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}versenken/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Network\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Manager.cs File Reference}{36}{section.5.4}%
\contentsline {section}{\numberline {5.5}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Dominiks PC/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documents/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Git\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Lab/se2\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}game/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Frontend/schiffe\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}versenken/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Player.cs File Reference}{36}{section.5.5}%
\contentsline {section}{\numberline {5.6}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Dominiks PC/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documents/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Git\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Lab/se2\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}game/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Frontend/schiffe\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}versenken/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Ship.cs File Reference}{36}{section.5.6}%
\contentsline {chapter}{Index}{37}{section*.39}%
